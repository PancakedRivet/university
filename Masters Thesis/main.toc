\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {chapter}{Declaration of Authorship}{iii}{section*.1}% 
\contentsline {chapter}{Abstract}{vii}{section*.2}% 
\contentsline {chapter}{Acknowledgements}{ix}{section*.3}% 
\contentsline {chapter}{List of Figures}{xv}{chapter*.5}% 
\contentsline {chapter}{List of Abbreviations}{xvii}{chapter*.6}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.9}% 
\contentsline {section}{\numberline {1.1}Whispering Gallery History}{1}{section.10}% 
\contentsline {section}{\numberline {1.2}Whispering Gallery Phenomena}{2}{section.12}% 
\contentsline {section}{\numberline {1.3}Whispering Gallery Resonances}{2}{section.14}% 
\contentsline {section}{\numberline {1.4}Experiment Motivations}{4}{section.16}% 
\contentsline {section}{\numberline {1.5}Thesis Content}{5}{section.18}% 
\contentsline {section}{\numberline {1.6}Outcomes During This MSc}{6}{section.19}% 
\contentsline {chapter}{\numberline {2}Whispering Gallery Mode Resonator Fabrication}{7}{chapter.20}% 
\contentsline {section}{\numberline {2.1}Fabrication Methods}{7}{section.21}% 
\contentsline {section}{\numberline {2.2}Fabrication Component Discussion}{7}{section.22}% 
\contentsline {subsection}{\numberline {2.2.1}Lathe}{7}{subsection.23}% 
\contentsline {subsection}{\numberline {2.2.2}Turning Machine}{8}{subsection.25}% 
\contentsline {subsection}{\numberline {2.2.3}Paraffin Wax}{8}{subsection.27}% 
\contentsline {subsection}{\numberline {2.2.4}Brass Rod}{9}{subsection.33}% 
\contentsline {section}{\numberline {2.3}Computer Control}{10}{section.34}% 
\contentsline {section}{\numberline {2.4}Lithium Niobate Resonator Fabrication Process}{10}{section.36}% 
\contentsline {subsection}{\numberline {2.4.1}Drilling The Precursor}{10}{subsection.37}% 
\contentsline {subsection}{\numberline {2.4.2}Mounting The Precursor}{11}{subsection.38}% 
\contentsline {subsection}{\numberline {2.4.3}Cutting The Resonator}{12}{subsection.40}% 
\contentsline {section}{\numberline {2.5}Polishing of WGMRs}{12}{section.42}% 
\contentsline {section}{\numberline {2.6}Cleaning of WGMRs}{13}{section.44}% 
\contentsline {section}{\numberline {2.7}Maintenance of WGMRs}{14}{section.45}% 
\contentsline {chapter}{\numberline {3}Whispering Gallery Mode Resonators}{15}{chapter.46}% 
\contentsline {section}{\numberline {3.1}WGMR Structure}{15}{section.47}% 
\contentsline {subsection}{\numberline {3.1.1}Geometry}{15}{subsection.48}% 
\contentsline {subsection}{\numberline {3.1.2}Birefringence}{15}{subsection.51}% 
\contentsline {section}{\numberline {3.2}Total Internal Reflection}{16}{section.52}% 
\contentsline {subsection}{\numberline {3.2.1}Snell's Law}{17}{subsection.53}% 
\contentsline {subsection}{\numberline {3.2.2}Frustrated Total Internal Reflection}{17}{subsection.57}% 
\contentsline {section}{\numberline {3.3}Coupling}{18}{section.59}% 
\contentsline {subsection}{\numberline {3.3.1}Free-Space Coupling}{18}{subsection.60}% 
\contentsline {subsection}{\numberline {3.3.2}Prism Movement}{19}{subsection.61}% 
\contentsline {subsection}{\numberline {3.3.3}Coupling Angle Calculation}{19}{subsection.62}% 
\contentsline {subsection}{\numberline {3.3.4}Newton's Rings}{21}{subsection.68}% 
\contentsline {section}{\numberline {3.4}Mode Observation}{22}{section.71}% 
\contentsline {subsection}{\numberline {3.4.1}Modes Characterisation}{23}{subsection.74}% 
\contentsline {subsubsection}{Fundamental Modes}{24}{section*.79}% 
\contentsline {section}{\numberline {3.5}WGMR Characterisation}{24}{section.80}% 
\contentsline {subsection}{\numberline {3.5.1}Linewidth}{24}{subsection.81}% 
\contentsline {subsection}{\numberline {3.5.2}Coupling Contrast}{24}{subsection.83}% 
\contentsline {subsection}{\numberline {3.5.3}Quality Factor}{25}{subsection.89}% 
\contentsline {subsection}{\numberline {3.5.4}Free Spectral Range}{27}{subsection.92}% 
\contentsline {section}{\numberline {3.6}WGMR Characterisation Measurement Process}{27}{section.95}% 
\contentsline {subsection}{\numberline {3.6.1}Coupling Contrast Measurement Process}{27}{subsection.96}% 
\contentsline {subsection}{\numberline {3.6.2}Linewidth Measurement Process}{27}{subsection.97}% 
\contentsline {subsection}{\numberline {3.6.3}Q-Factor Measurement Process}{28}{subsection.98}% 
\contentsline {subsection}{\numberline {3.6.4}FSR Measurement Process}{28}{subsection.100}% 
\contentsline {chapter}{\numberline {4}Experimental Setup}{31}{chapter.102}% 
\contentsline {section}{\numberline {4.1}Experiment Objectives}{31}{section.103}% 
\contentsline {subsubsection}{\citeauthor {foreman_dielectric_2016} Theory}{31}{section*.104}% 
\contentsline {subsubsection}{Frequency Shift Contribution Discussion}{31}{section*.105}% 
\contentsline {subsubsection}{Our Experiment}{32}{section*.107}% 
\contentsline {subsubsection}{Red Shift Phenomena}{33}{section*.109}% 
\contentsline {subsubsection}{Blue Shift Phenomena}{34}{section*.110}% 
\contentsline {subsubsection}{Mode Broadening}{34}{section*.111}% 
\contentsline {section}{\numberline {4.2}Setup}{35}{section.113}% 
\contentsline {section}{\numberline {4.3}Automation}{36}{section.115}% 
\contentsline {subsection}{\numberline {4.3.1}Computer Communication Discussion}{36}{subsection.116}% 
\contentsline {subsubsection}{Breakout Boxes}{37}{section*.117}% 
\contentsline {subsubsection}{Network Access}{37}{section*.119}% 
\contentsline {subsection}{\numberline {4.3.2}Python}{38}{subsection.120}% 
\contentsline {subsubsection}{Frequency Axis Calibration}{38}{section*.122}% 
\contentsline {subsubsection}{Program Limitations}{39}{section*.131}% 
\contentsline {subsection}{\numberline {4.3.3}MATLAB}{40}{subsection.133}% 
\contentsline {section}{\numberline {4.4}Nanocube Discussion}{40}{section.134}% 
\contentsline {subsection}{\numberline {4.4.1}Nanocube Calibration}{41}{subsection.136}% 
\contentsline {section}{\numberline {4.5}Attocube Discussion}{41}{section.138}% 
\contentsline {subsubsection}{Attocube Movement Mechanisms}{42}{section*.141}% 
\contentsline {subsubsection}{Attocube Audible Feedback Mechanism}{43}{section*.145}% 
\contentsline {section}{\numberline {4.6}Attocube-Nanocube Scale Calibration}{43}{section.146}% 
\contentsline {section}{\numberline {4.7}Frequency Shift Direction}{43}{section.147}% 
\contentsline {section}{\numberline {4.8}Substrate Mounting Orientation}{44}{section.149}% 
\contentsline {section}{\numberline {4.9}Environment Control}{45}{section.155}% 
\contentsline {chapter}{\numberline {5}Experimental Method}{53}{chapter.159}% 
\contentsline {section}{\numberline {5.1}Pre-Sweep Calibrations}{53}{section.160}% 
\contentsline {subsection}{\numberline {5.1.1}Polarisation Control}{54}{subsection.161}% 
\contentsline {subsubsection}{Polarising Beam Splitter}{54}{section*.162}% 
\contentsline {subsubsection}{Paddle Fibre Polarisation Controller}{54}{section*.164}% 
\contentsline {subsubsection}{Polarisation Test}{55}{section*.166}% 
\contentsline {section}{\numberline {5.2}Data Collection Method}{56}{section.167}% 
\contentsline {chapter}{\numberline {6}Experimental Results}{59}{chapter.168}% 
\contentsline {section}{\numberline {6.1}Germanium}{59}{section.169}% 
\contentsline {subsection}{\numberline {6.1.1}TE Mode Sweep}{59}{subsection.170}% 
\contentsline {subsection}{\numberline {6.1.2}TM Mode Sweep}{59}{subsection.174}% 
\contentsline {section}{\numberline {6.2}Silicon}{65}{section.178}% 
\contentsline {subsection}{\numberline {6.2.1}TE Mode Sweep}{65}{subsection.179}% 
\contentsline {subsection}{\numberline {6.2.2}TM Mode Sweep}{65}{subsection.183}% 
\contentsline {section}{\numberline {6.3}Zinc Selenide}{71}{section.187}% 
\contentsline {subsection}{\numberline {6.3.1}TE Mode Sweep}{71}{subsection.188}% 
\contentsline {subsection}{\numberline {6.3.2}TM Mode Sweep}{71}{subsection.192}% 
\contentsline {section}{\numberline {6.4}Zinc Sulfide}{77}{section.196}% 
\contentsline {subsection}{\numberline {6.4.1}TE Mode Sweep}{77}{subsection.197}% 
\contentsline {subsection}{\numberline {6.4.2}TM Mode Sweep}{77}{subsection.201}% 
\contentsline {section}{\numberline {6.5}Sapphire}{83}{section.205}% 
\contentsline {subsection}{\numberline {6.5.1}TE Mode Sweep}{83}{subsection.206}% 
\contentsline {subsection}{\numberline {6.5.2}TM Mode Sweep}{83}{subsection.210}% 
\contentsline {section}{\numberline {6.6}Optical Glass}{88}{section.214}% 
\contentsline {subsection}{\numberline {6.6.1}TE Mode Sweep}{88}{subsection.215}% 
\contentsline {subsection}{\numberline {6.6.2}TM Mode Sweep}{88}{subsection.219}% 
\contentsline {section}{\numberline {6.7}Calcium Fluoride}{94}{section.223}% 
\contentsline {subsection}{\numberline {6.7.1}TE Mode Sweep}{94}{subsection.224}% 
\contentsline {subsection}{\numberline {6.7.2}TM Mode Sweep}{94}{subsection.228}% 
\contentsline {section}{\numberline {6.8}Substrate Refractive Index Effects on WGMR Modes}{99}{section.232}% 
\contentsline {chapter}{\numberline {7}Discussion}{101}{chapter.235}% 
\contentsline {section}{\numberline {7.1}Analysis}{101}{section.236}% 
\contentsline {section}{\numberline {7.2}Determining Criteria For Results}{102}{section.237}% 
\contentsline {subsection}{\numberline {7.2.1}Frequency Shift Measurement}{104}{subsection.238}% 
\contentsline {section}{\numberline {7.3}Temperature Control}{105}{section.239}% 
\contentsline {section}{\numberline {7.4}Conclusion}{108}{section.258}% 
\contentsline {section}{\numberline {7.5}Outlook}{108}{section.259}% 
\contentsline {chapter}{Bibliography}{109}{appendix*.260}% 
