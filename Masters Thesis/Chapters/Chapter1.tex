% Chapter 1

\chapter{Introduction} % Main chapter title

\label{Chapter1} % For referencing the chapter elsewhere, use \ref{Chapter1} 

%-----------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}

This chapter exists as several distinct sections. The first two sections are to outline the history and phenomenology of whispering galleries. It is important that this is done in absence of the experimental discussion so that a clearer picture of what a whispering gallery is and how it works is shown. This will make it easier to see how a whispering gallery is used in experiments such as the one in this thesis when later chapters are read. After the introduction to whispering galleries we will extend this discussion to the development of how a WGMR can satisfy the resonance condition. We will then move on to discuss the motivations for this thesis and look at the theoretical paper about which the experiment in this thesis is based. Finally we will outline what can be expected in the remainder of this thesis through short synopses of each chapter.


%-----------------------------------------------------------------------------
\section{Whispering Gallery History} \label{whispering_gallery_history}
%-----------------------------------------------------------------------------

A whispering gallery is usually a room with a round edge. These rooms often possess geometry that is circular in nature, although whispering galleries have been discovered with geometry that can deviate, possessing hemispherical, elliptical or ellipsoidal geometry instead. The rooms with these sorts of geometries would often be the result of being built beneath an architectural vault of some kind. Whispering galleries like the Saint Paul's Cathedral (figure \ref{fig:stpauls}) in London are the result of being built beneath a dome, a common type of architectural vault. 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter1/stpauls}
	\decoRule
	\caption[Saint Paul's Cathedral]{The whispering gallery in Saint Paul's Cathedral. Image taken from \cite{dearsley_exploring_nodate}.}
	\label{fig:stpauls}
\end{figure}

There are many examples of whispering galleries around the world. Saint Paul's Cathedral in London is a popular example as it is also the location where Lord Rayleigh began work on understanding them (detailed below in \ref{whispering_gallery_phenomena}). Other examples include Grand Central Terminal in New York, Saint Peter's Basilica in the Vatican City and The Church of the Holy Sepulcher in Jerusalem. 

The name "whispering gallery" comes from the discovery that a message whispered along a wall could be clearly heard in other parts of the gallery. Two people could have a whispered conversation in such a gallery and find that even if they stood on opposite sides of the gallery, the message would be better preserved when whispered along the wall rather than at the person directly across the room. It was noticed that the sound would seemingly "stick" to the walls as it travelled, allowing for whispered communication from one part of the wall to any other part on the circumference. Furthermore, it was noticed someone standing in the centre of the room would not be able to hear the whispers even if standing directly between those whispering.


%-----------------------------------------------------------------------------
\section{Whispering Gallery Phenomena} \label{whispering_gallery_phenomena}
%-----------------------------------------------------------------------------

In 1878, Lord Rayleigh explained what was occurring in the gallery \cite{rayleigh_theory_1877} and named the phenomenon "whispering gallery" phenomenon after the location of its discovery within Saint Paul's Cathedral. He explained that the sound was constantly reflecting off of the wall as it travelled around. To prove this Lord Rayleigh set lit candles around the gallery and used a whistle as a sound source, measuring the flickering of the candles to determine the reach of the whistle. The intensity of the sound was shown to decay slower when bouncing along the wall. It decayed as the inverse of the distance it travelled, rather than as the inverse square as is the case with an outward radiating sound from a point source in free space.

While initially discovered for acoustic waves, whispering gallery phenomena applies to electromagnetic waves also. The research into the scattering of light from spherical particles is credited to papers published in 1908 by Gustav Mie \cite{mie_beitrage_1908} and Peter Debye \cite{debye_lichtdruck_1909}. In this instance, confining light waves does not require the same scale as the whispering gallery in Saint Paul's Cathedral due to the differences in the size of their respective wavelengths. To observe the analogous acoustic reflections in the optical domain, we can build a resonator out of a dielectric material.  A large glass sphere is useful at visually demonstrating the confining of light when a laser of a visible wavelength is coupled into it. When this is set up correctly the light appears confined at the rim of the sphere. Due to the size of the sphere relative to the wavelength of light, the light appears to "bend" around the sphere as it travels (see figure \ref{fig:glass_wgmr}).

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter1/glass_ball_2}
	\decoRule
	\caption[Glass ball whispering gallery]{The light remains confined near the rim of this glass ball, analogous to how sound is confined to the rim of a whispering gallery.}
	\label{fig:glass_wgmr}
\end{figure}


%-----------------------------------------------------------------------------
\section{Whispering Gallery Resonances} \label{whispering_gallery_resonances}
%-----------------------------------------------------------------------------

Let us take a moment to discuss how the confinement of a wave at the edge of a surface contributes to the satisfaction of the resonance condition: The repeated reflections of a wave around the rim of a surface will continue infinitely if contributions from absorption and scattering from the material are neglected. To neglect these components would quickly turn this into an unphysical system, so when those components are not neglected and there is no external driving force present to compensate, we recognise that a decay in the amplitude of the wave must occur. If one takes steps to promote the lifetime of the wave as it travels, the wave would eventually circumnavigate the space and traverse its origin on the rim. The overlap between the returning wave and the point of origin is dependent on the number of times it is reflected while travelling. This is dependent on the geometry of the space and the properties of the wave. The frequency of the wave will determine the resulting phase offset the returning wave has with its point of origin. Certain frequencies will experience no phase offset, which will cause constructive self-interference. Similarly, there will be certain frequencies that return to the same starting point but exactly out of phase. This results in destructive interference and so these frequencies are suppressed in the WGMR. The constructive self-interference leads to an amplification of the wave, with the strongest amplification of the wave occurring at integer multiples of $2\pi$ after one circumnavigation by the wave. 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter1/wgmr_ray}
	\decoRule
	\caption[WGMR Resonances]{A ray optics illustration of WGMR resonance. A light ray is confined in the WGMR by continuous reflections at the rim.}
	\label{fig:resonance}
\end{figure}

This satisfies the resonance requirement as the amplitude of the frequency of the wave after travelling around the rim will exceed the amplitude of the frequency of the wave at the origin. We refer to the resonance frequencies formed in this manner as modes, hence the term whispering gallery modes. In the acoustic domain, Lord Rayleigh was able to generate the modes in the whispering gallery of Saint Paul's Cathedral by standing in the gallery and blowing on a whistle. The optical domain uses a different process to generate the modes. While there are several methods to do this, this thesis will focus on the method known as prism coupling using frustrated total internal reflection and will be discussed in detail in section \ref{frustrated_total_internal_reflection}. The discussion of modes in this section is kept brief but will be elaborated on in section \ref{mode_observation}.


%-----------------------------------------------------------------------------
\section{Experiment Motivations} \label{experiment_motivations}
%-----------------------------------------------------------------------------

The experiment in this thesis is conducted with the objective of verifying a relationship proposed in \citefull{foreman_dielectric_2016}. In this paper, it was suggested that a dielectric substrate could be brought into close proximity with a WGMR to detune a resonant mode.

This relationship was developed further, to suggest that changing the refractive index of the substrate would change the amount of frequency detuning. This relationship can be seen in figure \ref{fig:foreman3a} below. The amount of frequency detuning seen can also change depending on if the mode is transverse-electric (TE) polarised or transverse-magnetic (TM) polarised. Polarisation will be discussed in greater detail in section \ref{polarisation_control}. 

\begin{figure}[th]
	\centering
	\includegraphics{Figures/Chapter1/ForemanFig3a}
	\decoRule
	\caption[\citeauthor{foreman_dielectric_2016} (\citeyear{foreman_dielectric_2016}) Figure 3a]{We note that the relationship between the refractive index of the substrate and the resulting frequency shift of the resonant mode is non linear. Of interest is an inflection point that occurs when the refractive index exceeds that of the resonator. We can also note that an anomalous blue shift is predicted for high refractive index values when using TE polarised modes. (Figure 3a taken from \citefull{foreman_dielectric_2016})}
	\label{fig:foreman3a}
\end{figure}

Many interesting consequences occur from the relationship between refractive index and frequency detuning. We introduce the standard terms red-shift (a reduction in frequency) and blue-shift (an increase in frequency) to simplify explanations relating to these consequences. If a TE mode is observed, a red-shift is expected when the refractive index of the substrate is increased from $1$. Once the refractive indices of the substrate and resonator match, a maximal red-shift occurs. What is perhaps more interesting, is that when the refractive index of the substrate is increased further, the magnitude of the red-shift decreases, eventually reaching a point where there is no frequency shift of the resonator mode. At this point the effects of the red-shift and blue-shift compensate each other, making it as if there is no substrate nearby. When the substrate refractive index is increased beyond this point, a blue-shift of mode is expected. There are many anecdotal reports about noticing blue-shifts in experiments but there is little literature available that explores them in detail. One work that does is by \citefull{vogt_anomalous_2019} in the THz domain. It is important to note that the presence of a blue-shift is unique to TE modes. TM modes were shown to exhibit a similar pattern of behaviour but without a blue-shift.

The purpose of the experiment in this thesis is to explore the relationship proposed in \citeauthor{foreman_dielectric_2016} and to verify if the relationship holds true for both TE and TM modes. To do this we must discretize the continuous refractive index scale. This is to generate a more physical setup whereby we can use substrates with a range of different refractive indices to form a relationship between the refractive index of the substrate and resulting frequency shift of the resonator mode.

The discussion of the relationship proposed in \citeauthor{foreman_dielectric_2016} was kept brief in this introduction, and will be revisited in section \ref{experiment_objectives} with more detail.

%-----------------------------------------------------------------------------
\section{Thesis Content} \label{thesis_content}
%-----------------------------------------------------------------------------

This thesis has chapters grouped together based on different aspects of the experiment. Chapter \ref{Chapter2} and chapter \ref{Chapter3} will focus on more of the background discussion away from the direct experimental context of the experiment presented in this thesis. The remainder of this thesis will focus on aspects of the experiment more directly, from discussions about aspects of the experiment of the results of the experiment itself:

\begin{itemize}
	
	\item In chapter \ref{Chapter2} we will spend some time illuminating the complicated process of fabricating a resonator in the lab. We will then move into a description of the processes involved with both polishing and cleaning a fabricated resonator.
	
	\item In chapter \ref{Chapter3} we will discuss various aspects of WGMRs. This chapter will serve as the majority of the background required to understand the reasoning for steps taken in this experiment moving forward. Section \ref{whispering_gallery_mode_resonator_structure} will outline the geometric structure of WGMRs and how birefringence influences the refractive index of a resonator when a particular polarisation of light is coupled into it. 
	Section \ref{total_internal_reflection} will  show how Snell's law plays a vital role in confining light to a WGMR using total internal reflection. We will then extend the theory of total internal reflection to account for light entering a resonator through evanescent field coupling. Section \ref{coupling} will have a lengthy discussion about many of the aspects involved with coupling light into a WGMR. We will begin by coupling light from a fibre to free pace and continue discussion until a coupling spot is found by observing the presence of Newton's rings.	Section \ref{mode_observation} will introduce the nomenclature for characterising the spatial position of modes that are observed in WGMRs. We will take an example of this nomenclature to introduce the idea of fundamental modes. Section \ref{whispering_gallery_mode_resonator_characterisation} will take at the values that can characterise WGMRs. We will discuss what the theory of each value with reference to their importance in this experiment before detailing a process of extracting the values experimentally.
	
	\item In chapter \ref{Chapter4} we will study aspects of the experimental setup in greater detail:	Section \ref{experiment_objectives} will have us revisit the motivations behind this experiment in more detail. Here we will discuss the implications of moving from the theoretical conclusions of the background paper to the experiment devised in this thesis. Here we will analyse some of the results found in \citeauthor{foreman_dielectric_2016} and what they mean for the experiment presented in this thesis. It is in this section that we also get the first introduction to the substrates used in this experiment through table \ref{tab:substrates}. We will then move into section \ref{set_up} where we will show the physical setup with which we run the experiment. The remainder of this chapter is focused on discussions around specific elements of the setup. Section \ref{computer_communication_discussion} will examine the methods by which the computers communicate with lab apparatus. This is followed by a discussion around the two different types of positioning systems used (sections \ref{nanocube_discussion} and \ref{attocube_discussion}). 
	
	In section \ref{automation} we will elaborate on the automation processes employed in this experiment to facilitate data collection. We will conclude this chapter with an outline of some of the steps taken to mitigate potential error sources: the orientation of the substrate mounting (section \ref{substrate_mounting_orientation}) and the presence of thermal fluctuations (section \ref{environment_control}). 
	
	\item Chapter \ref{Chapter5} focuses exclusively on the methodology of this experiment. This has two important aspects, the process for readying the setup before data is taken (section \ref{pre-sweep_calibrations}) and the process of running the experiment itself (section \ref{data_collection_method}).
	
	\item  The results of the experiment are broken down by the relevant substrate in chapter \ref{Chapter6}. We will also analyse the results in this chapter.
	
	\item  This will leave a lot of the discussion to chapter \ref{Chapter7}, which will also serve as the future outlook of this experiment and the final chapter of this theses.
	
\end{itemize}

%-----------------------------------------------------------------------------
\section{Outcomes During This MSc} \label{outcomes_during_this_thesis}
%-----------------------------------------------------------------------------

While completing the research for this thesis, we had several positive outcomes. The research for this thesis was presented at the Dodd-Walls Symposium held in Auckland in July 2018. This research generated a high level of interest at this symposium with a lot of engagement during the poster presentation session in which this research was showcased. This discussion resulted in a competing work demonstrating a blue-shift measured in the THz domain \cite{vogt_anomalous_2019}, published in Optics Letters.

This research was also presented at the Frontiers in Optics Conference held in  Washington DC in September 2018. At this conference there was a high degree of interest during the poster session that this research was presented. Many interesting discussions took place between myself and various attendees, some of whom were familiar with the field and some who were not.