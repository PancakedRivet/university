%Chapter 2

\chapter{Whispering Gallery Mode Resonators}

\label{Chapter2} % For referencing the chapter elsewhere, use \ref{Chapter2} 

In this chapter we will look in more detail at various aspects of WGMRs. We will discuss the structure of the resonator itself and also the structure of the modes that the resonator produces when light is coupled into it. We will then discuss the process for coupling light into the resonator using a process called prism coupling. This chapter will end with a discussion on our process for fabricating resonators.

%-----------------------------------------------------------------------------
\section{WGMR Structure} \label{whispering_gallery_mode_resonator_structure}
%-----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
\subsection{Whispering Gallery Mode Resonator Geometry} \label{whispering_gallery_mode_resonator_geometry}

WGMRs often possess spherical or rotational symmetry. This is often achieved by fabricating a disk made from a dielectric material that contains a major radius $R$ and a minor radius $r$. 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/WGMR_structure}
	\decoRule
	\caption[WGMR Structure]{Structure of a WGMR.}
	\label{fig:wgmr_structure}
\end{figure}

The process of total internal reflection (TIR) is used to confine the light to remain within the resonator as it travels. TIR is derived from Snell's Law 

\begin{equation} \label{eq:snell1}
\mathrm{n}_1 \mathrm{sin}(\theta_{1}) = \mathrm{n}_2 \mathrm{sin}(\theta_{2}).
\end{equation}

The light will travel along the media boundary in the limit where $\theta_{2}$ approaches $\ang{90}$. We denote this as the critical angle

\begin{equation} \label{eq:snell2}
\theta_{\mathrm{critical}} =\arcsin\left(\frac{n_2}{n_1}\right).
\end{equation}

If this angle is exceeded then the light will reflect at the media boundary causing it to be totally internally reflected. This can only occur in instances where $n_\mathrm{resonator} > n_\mathrm{surrounding}$ such as at the boundary between the resonator material and air. The process for TIR is illustrated in figure

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/SnellLine}
	\decoRule
	\caption[Total Internal Reflection]{An illustration of total internal reflection.}
	\label{fig:tir}
\end{figure}

A WGMR can have different "cuts" referring to the location of the optical axis. When the optical axis is parallel to the axis of rotational symmetry (denoted as the z-axis), the cut of the resonator is called z-cut, however one can also fabricate x-cut resonators. An x-cut resonator will have its optic axis pointed in the x-axis direction, which is perpendicular to the axis of rotational symmetry. This difference is highlighted in figure \ref{fig:wgmr_z_cut_image}. 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/WGMR_pie_cut_z_cut}
	\decoRule
	\caption[WGMR z-cut diagram]{An illustration of the optical axis in a z-cut resonator. TE and TM directions are also shown }
	\label{fig:wgmr_z_cut_image}
\end{figure}

If TE polarised light (light polarised perpendicular to the WGMR place) enters a birefringent z-cut resonator, it will interact with the extraordinary refractive index of the resonator. Similarly, TM polarised light (light polarised parallel to the WGMR place) entering the resonator will interact with the ordinary refractive index of the resonator. If the resonator is x-cut instead of z-cut then the orientation of the resonator will change its effective refractive index. For an arbitrary azimuthal angle $\theta$ of light in an x-cut resonator it will be split into ordinary and extraordinary component beams. The ordinary ray will experience the ordinary refractive index $n_\mathrm{o}$ whereas the refractive index experienced by the extraordinary ray is dependant on the direction of the ray. This value will be somewhere between the extraordinary and ordinary refractive indices so we denote this value as the effective refractive index $n_\mathrm{eff}$. The differences in effective refractive index for x-cut and z-cut resonators can be seen in figure \ref{fig:wgmr_cuts_index}. There are advantages to the different cutting configurations, an advantage to a z-cut resonator is in its ability for the different polarisations of light to interact with the different refractive indices of a birefringent resonator separately. An advantage of the x-cut resonator is in having a broader phase-matching interval than a z-cut resonator. This can make it preferable in applications such as second harmonic generation.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/WGMR_z_cut}
	\decoRule
	\caption[WGMR z-cut refractive index]{The difference between the extraordinary and ordinary refractive indices of a z-cut lithium niobate resonator.}
	\label{fig:wgmr_z_cut_graph}
\end{figure}

%-----------------------------------------------------------------------------
\subsection{Whispering Gallery Resonances} \label{whispering_gallery_resonances}

Let us take a moment to discuss how the confinement of a wave at the edge of a surface contributes to the satisfaction of the resonance condition: The repeated reflections of a wave around the rim of a surface will continue infinitely if things like absorption and scattering from the material are neglected. To neglect these components would quickly turn this into an unphysical system, so when those components are not neglected and there is no external driving force present to compensate, we recognise that a decay in the amplitude of the wave must occur. If one takes steps to promote the lifetime of the wave as it travels, the wave would eventually circumnavigate the space and traverse its origin on the rim. The overlap between the returning wave and the point of origin is dependant on the number of times it is reflected while travelling. This is dependant on the geometry of the space and the properties of the wave. As acoustic and electromagnetic waves have a fixed velocity in air, the frequency of the wave will determine the wavelength and therefore the number of reflections it makes while travelling. This suggests that the frequency of the wave will determine the resulting phase offset the returning wave has with its point of origin. There will be certain frequencies that will result in no phase offset, which will cause constructive self-interference. Similarly, there will be certain frequencies that return to the same starting point but exactly out of phase. This results in destructive interference and so these frequencies are suppressed in the WGMR. The constructive self-interference leads to an amplification of the wave, with the strongest amplification of the wave occurring at integer multiples of $2\pi$ after one circumnavigation by the wave. 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/placeholder}
	\decoRule
	\caption[WGMR Resonances]{PLACEHOLDER A ray optics illustration of WGMR resonance.}
	\label{fig:resonance}
\end{figure}

This satisfies the resonance requirement as the amplitude of the frequency of the wave after travelling around the rim will exceed the amplitude of the frequency of the wave at the origin. We refer to the resonance frequencies formed in this manner as modes, hence the term whispering gallery modes. In the acoustic domain, Lord Rayleigh was able to generate the modes in the whispering gallery of Saint Paul's Cathedral by standing in the gallery and blowing on a whistle. The optical domain uses a different process to generate the modes. While there are several methods to do this, this thesis will focus on the method known as prism coupling using frustrated total internal reflection and will be discussed in detail in section \ref{frustrated_total_internal_reflection}. The discussion of modes in this section is kept brief but will be elaborated on in section \ref{whispering_gallery_mode_resonator_modes}.

%-----------------------------------------------------------------------------
\subsection{WGMR Modes} \label{whispering_gallery_mode_resonator_modes}

The spatial mode structure of WGMRs can be defined with three components:

\begin{enumerate}
	
	\item The azimuthal mode number $m$. This number denotes the number of oscillations in the plane of the resonator.
	
	\item The polar mode number $p$. This number denotes the number of minima in the polar direction
	
	\item The radial mode number $q$. This number denotes the number of maxima in the radial direction.
	
\end{enumerate}

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/placeholder}
	\decoRule
	\caption[WGMR Modes]{PLACEHOLDER An illustration of a WGM.}
	\label{fig:modes}
\end{figure}

\subsubsection{Fundamental Modes}

There are modes of particular importance in this experiment. \citeauthor{foreman_dielectric_2016} uses a "fundamental mode" to derive the relationship of interest. A fundamental mode is a mode that has $q = 1$ and $p = 0$. It is for this reason that it is important to be able to determine the position of a specific mode seen on the oscilloscope. The process for locating a fundamental mode is based on the method presented in \citefull{breunig_whispering_2013} where one can obtain the radial mode number $q$ by measuring many modes of the WGMR and stratifying the data by the frequency of the FSR. The lowest strata of data corresponds to a $q = 0$ mode.

%-----------------------------------------------------------------------------
\section{WGMR Characterisation} \label{whispering_gallery_mode_resonator_characterisation}
%-----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
\subsection{Quality Factor} \label{quality_factor}

The quality factor of a resonator (Q factor) is a useful quality for comparing resonators. It determines how much energy is lost compared to the energy stored in the resonator. Q factor is given by

\begin{equation} \label{eq:Q}
Q = \frac{f_{resonant}}{\Delta f} = \frac{\omega_{resonant}}{\Delta \omega}.
\end{equation}

The Q factor is a measure of the inverse of a resonators line width relative to its central (resonant) frequency. A high Q-factor is desirable in our experiment because we will be observing changes in the line width. Therefore if we use a resonator with a higher Q factor, we have a narrower line width, making changes more noticeable.

The resonator may have curvature that is large relative to the wavelength of the light travelling around it but the special nature of WGMRs means that the modes created by the resonant frequencies are localised near the rim. This results in a good containment of the field in WGMRs.

Losses per round trip in a WGMR occur from surface scattering, material absorption and radiative loss. The surface polishing of the resonator serves to minimise the losses by surface scattering (see section ). The light lost from radiation is usually negligible as the major radius $R$ of the disk is many times greater than the wave-length of the light moving around it.

\subsubsection{Q Factor Measurement Process}

We use a mostly automated process to determine the Q factor of a resonator. We use the MATLAB script detailed in section to automate the measurement process. We ensure light is coupled into the resonator following the process outlined in section . Once there are modes visible on the oscilloscope, we change the distance between the coupling prism and the resonator. We save the oscilloscope trace at regular intervals while the coupling prism moves. We then load these files into the MATLAB script and run it. Running the MATLAB script produces a plot that shows how the Q factor changes with the change in voltage supplied to the attocube that controls the coupling prism.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/placeholder}
	\decoRule
	\caption[Q-factor measurement screenshot]{PLACEHOLDER A screenshot of the MATLAB script that measures Q-factor.}
	\label{fig:q_factor_measurement}
\end{figure}

%-----------------------------------------------------------------------------
\subsection{Coupling Strength} \label{coupling_strength}

To best observe any changes in the line width, we want the incoming mode to match the resonator mode exactly. When this occurs, the light is coupled in at the same rate as it dissipates. This is known as \emph{critical coupling}. We can measure this point as the relative depth of the resonance, known as the coupling contrast $K$ where

\begin{equation} \label{eq:K}
K = \frac{P_\mathrm{in} - P_\mathrm{out}}{P_\mathrm{in}}.
\end{equation}

We can see from figure that we have a coupling contrast of around 5\% for this mode. The desired \emph{critical coupling} occurs when $K=1$. This is the optimum coupling regime for the experiment as the resonances have a relatively large depth but retain small line widths. Larger depths make modes easier to distinguish during data analysis and narrower line widths make it easier to find the frequency of the modes. If the system is not at critical coupling, the system is said to be in a state of either \emph{under coupling} or \emph{over coupling}. During \emph{under coupling}, the relative depth of the resonance is small but the line width is also. This would make it more difficult to observe changes in the line width than if the resonator was critically coupled. During \emph{over coupling}, the line width increases and the contrast decreases. This is a problem as there is greater uncertainty in the mode frequency.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/placeholder}
	\decoRule
	\caption[WGMR coupling strength illustration]{PLACEHOLDER An illustration of the different coupling strengths for WGMRs.}
	\label{fig:coupling_strength}
\end{figure}

%-----------------------------------------------------------------------------
\subsection{Free Spectral Range} \label{free_spectral_range}

When light is coupled into the resonator, the frequency of the light will determine how many wavelengths comprise one round trip of the resonator. The free spectral range (FSR) is a measure of the increase in resonance frequency between the coupled light and light that completes one round trip with an extra wavelength. This value is non constant when the number of wavelengths that comprise a single round trip is small. As the number of wavelengths in one round trip increases the increase in frequency becomes more constant. Due to the size of the resonator relative to the wavelength of light coupled into it we can assume that the FSR is constant. FSR is given by the equation

\begin{equation} \label{eq:FSR1}
\mathrm{FSR} =  \nu_\mathrm{m+1} - \nu_\mathrm{m}.
\end{equation}

We can directly calculate the expected FSR for a resonator using

\begin{equation} \label{eq:FSR2}
\mathrm{FSR} =  \frac{\mathrm{c}}{\pi D_\mathrm{resonator} n_\mathrm{eff}}.
\end{equation}

%\begin{equation} \label{eq:FSR3}
%\mathrm{FSR} =  \frac{1}{\tau}
%\end{equation}

\subsubsection{FSR Measurement Process}

We ensure that light is coupled into the resonator and that modes can be seen on the oscilloscope. We choose one mode to focus on and use equation \ref{eq:FSR2} to get an estimate of the FSR we would expect for the resonator. We then use an electro-optic modulator to modulate side-bands onto the light with a side-band frequency near the calculate value. If the EOM frequency is close to the FSR, then the side-bands of the modes with one wavelength higher and lower will appear equidistant from the focussed mode. Changing the EOM frequency will change the position of the other modes until they perfectly overlap the focussed mode. When this occurs the focussed mode often appears to increase in depth making it obvious visually when the FSR is found. The frequency of the EOM at this point is the FSR for that mode.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/placeholder}
	\decoRule
	\caption[FSR Measurement]{PLACEHOLDER Measuring the FSR of a WGMR mode.}
	\label{fig:fsr_measurement}
\end{figure}

%-----------------------------------------------------------------------------
\subsection{Linewidth} \label{linewidth}

The linewidth of a mode is the full width half maximum (FWHM) of a Lorentzian fit to the trace of that mode.

\subsubsection{Linewidth Measurement Process}

We use the same process as measuring the Q factor as the MATLAB script will output the change in linewidth as well as Q factor.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/placeholder}
	\decoRule
	\caption[Linewidth measurement]{PLACEHOLDER Screenshot of a Lorentzian fit applied to a WGM of interest.}
	\label{fig:modes}
\end{figure}

%-----------------------------------------------------------------------------
%\subsection{Whispering Gallery Mode Resonator Advantages} \label{whispering_gallery_mode_resonator_advantages}

%WGMRs have many advantages over other forms of resonators. WGMRs have a higher degree of tunability than other resonators, where the coupling conditions can be modified easily during an experiment if needed. WGMRs are also capable of very high Q-factors due to their high degree of field confinement and low mode volume.

%-----------------------------------------------------------------------------
\section{WGMR Coupling Theory} \label{whispering_gallery_mode_resonator_coupling_theory}
%-----------------------------------------------------------------------------

Coupling light into a WGMR often uses evanescent field coupling. This usually relies on either tapered fibre coupling or prism coupling. We will focus on prism coupling for this thesis as prism coupling was the method used in the experiment. To couple light into a WGMR the principle of frustrated total internal reflection (FTIR) is used. 

%-----------------------------------------------------------------------------
\subsection{Frustrated Total Internal Reflection} \label{frustrated_total_internal_reflection}

Frustrate total internal reflection is an extension of total internal reflection. Even if an incident wave is entirely reflected by TIR, it will still produce an evanescent wave (see figure). This wave will ordinarily transmit zero energy, however if a third medium is close enough, the evanescent wave can become a propagating wave again. If the prism has a higher refractive index than the resonator $n_\mathrm{prism} > n_\mathrm{resonator} > n_\mathrm{air}$ then the energy transmitted to the resonator can be enough to excite a wave that can continue propagating. Doing this creates evanescent wave coupling and the light is able to 'tunnel' into the resonator. Evanescent waves decay exponentially so the wave will decay to zero if there is no medium in close proximity for it to propagate in.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/FTIR}
	\decoRule
	\caption[Frustrated total internal reflection]{An illustration of frustrated total internal reflection.}
	\label{fig:ftir}
\end{figure}

%-----------------------------------------------------------------------------
\section{WGMR Coupling} \label{whispering_gallery_mode_resonator_coupling}
%-----------------------------------------------------------------------------

\subsection{Physical Coupling Process} \label{physical_coupling_process}

We have established how one would couple light into a resonator using FTIR, however the physical process of doing it becomes more complicated. When we couple light into a resonator, we often want to study only the TE or TM modes This can change the angle required to access the modes due to the birefringence of some resonator materials like lithium niobate. In addition to this, it's often the case that we want to study the fundamental modes. To ensure we have the greatest chances at seeing a fundamental mode of the correct polarisation, we use a MATLAB script developed by Luke Trainor. This MATLAB script automates a large part of the calculations that would otherwise need to be done manually.

To correctly calculate the coupling angle required, details of the coupling prism are required. This includes the refractive index of the prism, and the internal angles that it has. Although the coupling prism is often oriented parallel to the closest tangent of the resonator, this is never the face that the light enters the prism through. This makes knowing the angle the incident light face on the prism makes with the face closest to the resonator very important. Once the geometric details of the coupling prism are inputted, details of the resonator geometry are inputted. This includes the major and minor radii and the effective refractive index of the resonator. Once both sets of details are inputted, the script will output the angles that couple to TE and TM modes the best.

Once the coupling angle is known, we recreate this angle with the incident light from the laser and the coupling prism. We use a x-y-z translation stage to introduce the light to the set-up. This lets us make fine adjustments to the position of the light as it enters the coupling prism. To ensure the accuracy of the angle a protractor and marker are used to rule out a line o the optical table which follows the direction of the laser. Once light can enter the set-up at the correct angle, we need to ensure that the coupling prism is located in close proximity to the resonator. If the resonator-prism distance is too large then there is no evanescent field coupling possible as the evanescent field decays exponentially. To ensure that the prism is in contact with the resonator we mount the coupling prism on an attocube that contains a piezo actuator. The details for how the attocube operates can be found in section \ref{attocube_discussion}. We adjust the voltage and frequency of the attocube to ensure it moves the prism slowly. This reduces the chances of damaging the resonator in the set-up which avoids unwanted outcomes such as scratching or deforming the resonator, thereby making the coupling process harder. We use the audible feedback mechanism of the attocube to determine when the prism is touching the resonator. Once we hear the frequency change we stop moving the prism and know that the prism is touching the resonator. 

We use a laser with a visible wavelength of light such as a red laser fault-finder ($635$nm) that plugs into the end of the fibre running into the set-up. We then place a screen in the path of the prism's output beam to observe the reflections from the prism face closest to the resonator. We can use the GRIN lens and pigtailed ferule to adjust the aperture of the laser light and therefore change the size of the reflected image on the screen. This is effective in conjunction with the x-y-z translation stage to be able to focus on parts of the reflected image. The goal is to identify the coupling spot on the reflected image. We use the presence of Newton's rings to identify this spot. Newton's rings are an interference phenomenon that occurs at the interface of a curved surface and an adjacent flat surface. This is recreated at the interface between the curved resonator edge and the flat coupling prism face. There are many patterns that can be observed in the reflected image that will resemble Newton's rings however several of these patterns can be attributed to the presence of dust or dirt in the set-up. There will be one spot on the reflected image that will show Newton's rings that can be removed or re-introduced by increasing then decreasing the prism-resonator distance. Newton's rings can only occur when the curved and planar surfaces are in contact, so moving the coupling prism away serves to break the conditions for Newton's rings to occur, causing the interference pattern to vanish. It is reintroduced when the coupling prism is moved to touch the resonator again. This is an easy way to check that the identified coupling spot is the correct one. With the coupling spot identified, we use the translation knobs on the stage to shift the coupling spot to the centre of the reflected image. Sometimes the rim of the WGMR will glow with the laser light if enough light is coupled in. Once the coupling spot is centred on the reflected image it must be reduced as much as possible. This involves adjusting the aperture of the pigtailed ferule and GRIN lens to make the reflected image on the screen as small as possible. The coupling spot should be checked regularly while the beam spot is reduced to ensure that the spot is still in the centre of the reflected image. When the image is as small as possible the screen can be replaced with a photo-detector. A focussing lens can also be helpful if the real-estate of the set-up makes it hard for all of the light to hit the detector chip. With the physical coupling process complete the visible wavelength laser is replaced with the experiment laser and a focussing lens if needed. We used a \SI{1550}{\nano\meter} laser for the experiment. The process of obtaining modes on an oscilloscope once light is coupled in will be expanded on in section \ref{refining_wgms} below.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/Newtons_rings_diagram}
	\decoRule
	\caption[Newton's rings]{A diagram of Newton's rings and their physical appearance in the reflected image.}
	\label{fig:newtons_rings}
\end{figure}

\subsection{Refining WGMs} \label{refining_wgms}

Once light has been coupled into the resonator using the steps outlined above, there are further steps to be taken in order to see the WGMs on an oscilloscope. We sweep the laser over a frequency range. This will allow us to see modes where the frequency of the laser matches a resonance frequency in the WGMR. This will result in a dip on the oscilloscope. We use a large frequency sweep amplitude first to ensure we have the best chances at seeing modes. Once the frequency sweep is on and triggered correctly at the oscilloscope we adjust the numerical aperture once more. This often allows us to see modes before we need to change anything else. Once modes are seen, we can maximise the coupling contrast by using a combination of the prism-resonator distance and the x-y-z translation dials. Once this process is complete the oscilloscope should show a frequency trace similar to that seen in figure \ref{fig:wgms_oscilloscope}.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/placeholder}
	\decoRule
	\caption[Whispering Gallery Modes - Oscilloscope]{PLACEHOLDER An image of the oscilloscope showing WGMs.}
	\label{fig:wgms_oscilloscope}
\end{figure}