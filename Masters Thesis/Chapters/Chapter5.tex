 %Chapter 5

\chapter{Experimental Method}

\label{Chapter5}

The purpose of this chapter is to discuss the methodology of how the experiment was conducted to ensure rigorous scientific processes were followed. There will likely be references to discussions conducted in chapter \ref{Chapter2} and chapter \ref{Chapter4} where the underlying methodology of some steps taken in the method will have been explained in greater detail when they were introduced in their respective chapters.

%-----------------------------------------------------------------------------
\section{Pre-Sweep Calibrations} \label{pre-sweep_calibrations}
%-----------------------------------------------------------------------------

Before an experiment sweep can begin, there are a number of steps that must be taken to ensure that the data collection process is as accurate as possible. It is necessary to ensure that light is coupled into the resonator by following the coupling and refining processes described in section \ref{coupling}. Once light is successfully coupled to the resonator, the experiment laser must be switched on and given enough time to stabilise. With the laser operational, the desired substrate must be mounted on the attocube on top of the nanocube. This is best done by first mounting the substrate to a slide and screwing the slide into the top of the attocube. The nanocube is switched on and adjusted using the offset dial of the y-axis to move the nanocube position to \SI{99}{\micro\metre}. The substrate is then brought closer to the resonator by using the translation dial on the translation stage underneath the nanocube. After the rough translation is complete, the final movement is made by using the attocube on top of the nanocube. We set the offset of the attocube to \SI{100}{\volt} before moving it into place with the stepping functionality. The stepping movement is done with the attocube frequency set to \SI{120}{\hertz} and the attocube voltage set to \SI{12}{\volt}. At this frequency and voltage the movement is slow enough that the substrate can make contact with the resonator with minimal damage or changes to the coupling conditions. Moving the substrate faster than this would reduce the time the substrate takes to make contact with the resonator but carries a higher risk of damaging the resonator. We use the audible feedback mechanism from the attocube (section \ref{attocube_feedback}) to indicate when contact between the resonator and the substrate is made. We then reduce the nanocube offset by \SI{1}{\micro\metre} to \SI{98}{\micro\metre} and reduce the attocube offset voltage by \SI{100}{\volt} to \SI{0}{\volt}. We turn on the temperature controller and check that the connections between the temperature controller, the thermistor and the Peltier are connected. The cardboard box is then placed over the setup, this is discussed in section \ref{environment_control}. The purpose of reducing the offsets of the attocube and nanocube are to move the substrate out of the evanescent field of the resonator. When the substrate makes contact it can deform the resonator. This can change the evanescent field and lead to less conclusive data. Moving the substrate out of the evanescent field ensures it is not deforming the resonator and allows us to collect data on the interaction the substrate makes with the evanescent field when it first enters it during a sweep. 

The scan control on the laser is switched on to couple into the modes of the resonator. We ensure the detector is plugged into an oscilloscope so that we can see the modes. As the substrate should have been moved sufficiently outside of the evanescent field of the resonator, the oscilloscope should display modes from the light in the resonator unperturbed by the substrate. 

We check the polarisation of the light entering the resonator using the method described in section \ref{polarisation_control} below. One must measure the FSR of several modes to determine the location of the modes within the resonator. We change the oscilloscope settings and the laser scan control settings to observe a single mode on the oscilloscope. We launch the python script described in section \ref{python} and perform the calibration process outlined in section \ref{frequency_axis_calibration} to generate the frequency axis on the python script oscilloscope. Once the frequency calibration is complete we run a test sweep by adjusting the position of the coupling prism but without moving the substrate. This generates .dat files for the MATLAB script discussed in section \ref{MATLAB} to obtain values for the Q-factor and linewidth of the resonator. The calibration process is repeated until a mode is found with a suitable linewidth and Q-factor. The position of the coupling prism is set to match the position during the test sweep that gives the highest Q-factor, as this position corresponds with the lowest linewidth.

If we are expecting mode broadening to occur, we will modify this potion such that the resonator is slightly under-coupled. This is a precaution so that when mode broadening occurs, the increased losses in the resonator keep the coupling regime from becoming over-coupled slightly longer. This is useful for data analysis as over-coupling makes it harder to distinguish the frequency of the resonance.

\subsection{Polarisation Control} \label{polarisation_control}

We need to control the polarisation of the light entering the setup because we expect the effects of the dielectric substrates to change depending on if the light couples to TE or TM polarised modes. We utilise two methods of polarisation control simultaneously: a polarising beam splitter (PBS) and a paddle fibre polarisation controller. The functionality of each of these is described below.

\subsubsection{Polarising Beam Splitter} \label{polarising_beam_splitter}

The PBS is a cube comprised of two triangular prisms of different refractive indices that are then glued together (see figure \ref{fig:pbs_image}). The cube is mounted on a slide that can be screwed onto the front of a detector. The different refractive indices allow a PBS to separate an incident beam into a transmitted beam and a reflected beam where the transmitted beam and reflected beam have different polarisations (see figure \ref{fig:pbs_diagram}). When mounted in front of a detector, only the transmitted beam will pass through the PBS and into the detector.

%polarisations of light by only allowing light polarised in the PBS plane through. We can place this in front of the detector to only allow TE or TM polarised light through depending on the orientation of the PBS (see figure \ref{fig:pbs}). If we wish to only allow TE polarised light into the detector, and therefore only observe TE modes in the WGMR, we will orient the PBS such that it only allows TM light into the detector. We adjust the paddle fibre polarisation controller plates to extinguish as much of the light in the detector as possible. What we have done is extinguish as much of the TM light as possible, leaving only the TE polarised light behind. Removing the PBS from in front of the detector should result in only TE modes remaining at the detector.

\begin{figure*}[t!]
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[height=2in]{Figures/Chapter5/pbs_1}
		\caption{Photograph of the PBS used to ensure the correct polarisation of light is tested.}
		\label{fig:pbs_image}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[height=2in]{Figures/Chapter5/pbs}
		\caption{Diagram showing how the PBS works with an example light ray passing through it. Note the incident beam contains both TE and TM polarised light however the PBS splits this into a reflected beam of TM light and a transmitted beam of TE light. The polarisation of the transmitted and reflected beams is dependant on the orientation of the PBS relative to the incident light wave.}
		\label{fig:pbs_diagram}
	\end{subfigure}
	\decoRule
	\caption[Polarising Beam Splitter]{The PBS used in the experiment.}
	\label{fig:pbs}
\end{figure*}

\subsubsection{Paddle Fibre Polarisation Controller} \label{paddle_fibre_polarisation_controller}

The PBS is good for determining the polarisation of light entering the detector, but it cannot change the polarisation of the light entering the resonator. We use Thorlabs three-paddle polarization controllers to change the polarisation of the light. This helps ensure we are coupling into the correct mode polarisation. These controllers combine a quarter-wave plate, half-wave plate and another quarter-wave plate into a series that allows one to transform an arbitrary polarisation state into a different arbitrary polarisation state.

The mechanism for this action is in the function of each paddle. Single-mode fibre is looped several times into a spool that is centred in each paddle. The spools cause a stress induced birefringence in the fibre. This transforms each paddle into a fractional wave plate. The three-paddle design contain spools that act as quarter-wave plates at each end and a half-wave plate in the middle. The amount of birefringence induced depends on the wave-length of light used, the number of fibre loops in the spool, and the diameter of the spool. We can form an arbitrary polarisation state by rotating each of the paddles. 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter5/polarizer_plate}
	\decoRule
	\caption[Paddle Fibre Polarisation Controller]{Photograph of the paddle fibre polarisation controller used.}
	\label{fig:polarisation_control}
\end{figure}

\subsubsection{Polarisation Test}

There is a simple process that can be done to check and modify the polarisation of the light entering the setup. If we wish to study TE polarised modes, we must minimise the intensity of the TM polarised modes. To do this we observe the TM polarised modes on the oscilloscope by orienting the PBS in front of the detector such that the reflected beam is TE polarised light and the transmitted beam is TM polarised light. We then adjust each paddle of the paddle fibre polarisation controller to minimise the TM polarised light in the transmitted beam. This has the effect of changing the polarisation of the incident beam to be TE linearly polarised, maximising the intensity of the light in the TE polarised reflected beam. When the PBS is removed from in front of the detector, this results in seeing only TE polarised modes on the oscilloscope. If we wish to study TM polarised modes we can repeat this process with the orientation of the PBS changed such that the transmitted beam is TE polarised and the reflected beam is TM polarised.

%-----------------------------------------------------------------------------
\section{Data Collection Method} \label{data_collection_method}
%-----------------------------------------------------------------------------

The method for collecting data is centred mostly on the use of the python script (section \ref{python}) that receives data from the oscilloscope. After following the pre-sweep calibrations (section \ref{pre-sweep_calibrations}), the active channel within the script is set to control the position of the substrate. The substrate should not have moved so remain outside of the evanescent field of the resonator at this point. We adjust the offset voltage to the attocube controlling the substrate to be \SI{0.1}{\volt}. This avoids floating point zero-error in python that can prevent the file being analysed in MATLAB. We initiate the data collection method from within the python script by starting an offset sweep on the substrates attocube channel. This will incrementally adjust the offset of the attocube wit the substrate until we stop it or it reaches the limit of the offset voltage for the attocube. We will stop the offset sweep if the mode disappears, becomes severely deformed or moves out of the oscilloscope sample range. 

We pass the collected data to the MATLAB script to produce the raw result plots. These plots are useful in identifying any changes in linewidth which could be a sign of mode broadening if we are testing a substrate $n_\mathrm{substrate} > n_\mathrm{resonator}$. We can use the presence of linewidth changes to tailor the range of motion for the substrate on subsequent sweeps, if no linewidth changes are present it could be a sign that the substrate is not interacting with the resonator, possibly being too far away from the evanescent field of the resonator. If the output of the sweep suggests everything is working correctly, and there is a sign of interaction between the substrate and the resonator, the position of the substrate is reset by resetting the offset voltage of the attocube. Subsequent sweeps are taken with different file names to mitigate confusion when analysed by MATLAB. Often times it is useful to take a sweep with the substrate starting close to the resonator and moving away. This can be a useful sanity check as the direction of any changes should match the direction the substrate is moving in.

Once a satisfactory amount of data is collected for a single substrate, the box is removed and the attocube is used to move the substrate away from the resonator. Once there is a visible gap between the substrate and the resonator, the coarse translation dial below the nanocube is used to move the substrate away from the resonator faster. Once the substrate is safely out of the way of the resonator, the substrate is replaced with the next substrate to test and the entire process is repeated with the same mode remaining on the oscilloscope if possible. The python script allows for the resetting and recalibrating of the frequency axis without restarting the program meaning the optimal position for the coupling prism can be updated part way through the data collection process. Keeping the same mode in focus makes for a stronger comparison when generating the relationship between refractive index and frequency detuning of the resonator mode as the expected frequency shifts were all calculated from the same mode in \citeauthor{foreman_dielectric_2016}.