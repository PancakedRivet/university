%Chapter 2

\chapter{Whispering Gallery Mode Resonator Fabrication}

\label{Chapter2} % For referencing the chapter elsewhere, use \ref{Chapter2} 

This chapter will focus on the fabrication of resonators. This will include discussions around the apparatus used to fabricate the resonator. Once the fabrication process is complete the resonator is still unlikely to be useful in a setup. The resonator must be refined through bouts of polishing and cleaning before it will perform adequately in a physical setup. The process of polishing and cleaning a resonator will follow on from the conclusion of the fabrication process.

%-----------------------------------------------------------------------------
\section{Fabrication Methods} \label{fabrication_methods}
%-----------------------------------------------------------------------------

There are different fabrication methods available for WGMRs depending on their intended use and the material they are fabricated from. Common methods are melting and re-flowing and single point diamond tuning. Melting and re-flowing does not work well for single crystalline materials as the fabrication process causes the resonator to become polycrystalline. If the resonator is polycrystalline then it loses its birefringence (birefringence is explained in section \ref{birefringence}) which is an important property for our experiment in distinguishing between TE and TM modes (see section \ref{experiment_objectives}). Polycrystalline resonators can also have increased bulk and surface scattering. This will negatively affect the Q-factor (Q-factor is explained in section \ref{quality_factor}) of the resonator. This makes a single crystalline resonator more desirable than a polycrystalline resonator.

The resonator used in \citeauthor{foreman_dielectric_2016} is made from lithium niobate (LiNbO$_{3}$) so we also selected lithium niobate for our resonator material to increase compatibility. As this material possesses a single crystalline structure, we wish to preserve this for reasons explained above. This makes single point diamond tuning a more favourable fabrication method and will be the method discussed in this thesis. Single diamond tuning ensures that the fabricated resonator remains single crystalline. 

%-----------------------------------------------------------------------------
\section{Fabrication Component Discussion} \label{fabrication_component_discussion}
%-----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
\subsection{Lathe}

We use an air-bearing spindle lathe for fabrication. It is attached to an air compressor to reduce friction and achieve up to $6000$rpm when using the turning machine. The object to be spun on the lathe is first inserted into a chuck and then twisted into the front of the lathe until tight As the object is twisted it lead to instabilities while using the lathe if it rotates eccentrically. There are calibration processes available to mitigate this balancing issue that will not be detailed in this thesis so it is assumed the lathe is already balanced to a satisfactory level.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/Lathe-Microscope-cropped}
	\decoRule
	\caption[Laboratory lathe]{An image of the air-bearing spindle lathe and microscope where WGMRs are fabricated.}
	\label{fig:lathe}
\end{figure}

%-----------------------------------------------------------------------------
\subsection{Turning Machine}

The turning machine is a cutting arm mounted on the top of a platform with independently controlled x and y axis movement. This gives it a limited range of motion on one side of the lathe (see figure \ref{fig:turning_machine}).  A sharp single crystalline diamond tip is mounted at the end of the cutting arm and is used to cut the precursor into a resonator. The resonators fabricated in the lab possess rotational symmetry so the turning machine only needs access to one side of the resonator. We employ the use of a computer to make precise movements of the turning machine. We can use complex curves to shape the resonator 

\begin{figure*}[t!]
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[height=2in]{Figures/Chapter2/turning_machine}
		\caption{Two uni-axial translators are mounted perpendicular to each other to produce a stage with a large rage of movement. It can be controlled through a joystick or via a computer. The clamps allow for objects to be mounted on it such as the diamond cutter (pictured right).}
		\label{fig:stage}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[height=2in]{Figures/Chapter2/diamond_cutter}
		\caption{The diamond cutter used for the fabrication of WGMRs. The diamond tip can be seen protruding to the right. The diamond tip can be exchanged using a screw. This allows for customisable precision with smaller or larger diamond tips based on the requirements.}
		\label{fig:diamond_cutter}
	\end{subfigure}
	\decoRule
	\caption[Newton's rings illustration]{A composite figure shows a diagram of the setup conditions and a photograph of the resulting reflected image from the prism onto a screen. The left image shows A possible coupling spot due to the presence of the interference highlighted in yellow. In the right image, the coupling prism is moved away from the resonator and the interference pattern disappears (compare the yellow highlights of the left and right images). This suggests that the interference seen in the left image is caused by the contact between the spherical surface of the resonator and the flat surface of the coupling prism, generating Newton's rings. The location of this interference suggests the presence of a coupling spot.}
	\label{fig:turning_machine}
\end{figure*}

%-----------------------------------------------------------------------------
\subsection{Paraffin Wax}

We use paraffin wax as a mounting adhesive. It is used to mount a resonator material wafer onto a sheet of metal, to mount the resonator precursor onto a brass rod and to mount substrate windows onto aluminium slides.

There are many advantages to using wax over other adhesives. It retains a strong bond in the setup because the melting point is very high relative to the room temperature. The wax also doesn't have a curing time that other stronger adhesives have, meaning the object can be used once it has sufficiently cooled rather than after a certain amount of time has passed. This makes it especially useful when only minor adjustments need to be made to the position of something such as resonator precursor on the brass rod tip as often many small adjustments need to be made before the precursor is mounted satisfactorily. 

To use the wax we first need to melt it into a liquid. This is done using a hot plate (see figure \ref{fig:hot_plate}). The process for using the wax is as follows:

\begin{enumerate}
	\item We set the hot plate to a temperature around the melting point of the wax. This temperature is around \SI{110}{\degree}C. We place a small glob of wax and object we want to mount something onto. This should be a metallic item such as the brass rod or aluminium slide. As these are metallic they conduct the heat well and allow the wax to be manipulated before it cools.
	
	\item When the wax has melted into a pool a toothpick is used to collect a small amount and smear it on the item where something is to be mounted.
	
	\item The item is removed from the hot plate carefully and pressed together with the object to be mounted and the toothpick is used to finely manipulate the position of the join while the wax is still hot and liquid.
	
	\item When the position of the join is satisfactory the bound object has one of its metallic surfaces placed onto a heatsink. This speeds up the cooling process for the wax and allows the object to be used in a matter of minutes after the wax is applied. Suitable heatsinks include metallic bench vices or large blocks of metal.
\end{enumerate} 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/hot_plate_2}
	\decoRule
	\caption[Hot plate]{The hot plate used to melt the paraffin wax for use in mounting objects in the experiment.}
	\label{fig:hot_plate}
\end{figure}

%-----------------------------------------------------------------------------
\subsection{Brass Rod}

Brass rods are favoured to hold resonators for several reasons. They provide stable housing for the resonator, allowing for the changing of a resonator in a setup with relative ease. As the rod is brass, it is thermally conductive, making them useful in setups with temperature control. We employ a naming system of etching a resonator designation into the base. This makes it easy to distinguish resonators that are hard to distinguish by eye alone.

The end of the brass rod should have a tapered tip. This allows for a coupling prism to be brought into close proximity without impacting the brass and means the diamond tip of the turning machine does not need to cut the brass while it shapes the resonator. This leads to a reduction in wasted materials and prolongs the sharpness of the diamond tip.


%-----------------------------------------------------------------------------
\section{Computer Control} \label{computer_control}
%-----------------------------------------------------------------------------

We use a program written by a current PhD student to facilitate the fabrication process (see figure \ref{fig:lathe_program}). Using the program allows for precise diamond cutter movements with greater precision than can be achieved by manually using a joystick. As we have precise control over the x and y movements of the diamond, we can combine movements in both the x and y directions to simulate a curve. This allows us to shape the curved rim of the resonator and allows us to control the major and minor radius (see section \ref{whispering_gallery_mode_resonator_structure}) of the finished resonator. 

\begin{figure}[th]
	\centering
	\includegraphics[height=4in]{Figures/Chapter2/turning_machine_screenshot}
	\decoRule
	\caption[Turning machine screenshot]{A screenshot of the program used to control the turning machine when fabricating WGMRs.}
	\label{fig:lathe_program}
\end{figure}


%-----------------------------------------------------------------------------
\section{Lithium Niobate Resonator Fabrication Process} \label{lithium_niobate_resonator_fabrication_process}
%-----------------------------------------------------------------------------

Here we will detail the steps taken to fabricate a resonator using single point diamond tuning.

%-----------------------------------------------------------------------------
\subsection{Drilling The Precursor}

The resonator materials often come in the form of a wafer. We need a way to drill a small piece of the wafer out to be used in fabricating a resonator. The method we use is to mount a hollow core brass drill bit with in the lathe and push the wafer mounted to a thin metal sheet into the bit as it rotates in the lathe. We add high grain size diamond slurry to the contact point between the drill bit and the wafer to facilitate the drilling process. We can adjust the size of the resonator precursor by changing the size of the brass drill bit mounted in the lathe. The purpose of the metal sheet is two-fold. Firstly it gives stability to the drilling process by reducing the risk of fracturing given the brittle nature of some materials like lithium niobate. Secondly it serves as a novel feedback system for determining when the drilling process has completed. This process involves connecting the brass drill bit and the metal sheet underneath the wafer with a LED bulb and battery. When the drilling has completed, the brass drill bit and metal sheet will contact, completing the circuit and turning on the LED. With the feedback circuit in place and the lathe rotating at $6000$rpm we bore a small cylindrically-shaped resonator precursor out of the wafer. The precursor should have a \SI{100}{\micro\meter} radius larger than the desired resonator radius. This is a safety threshold so that cracks in the surface of the precursor can be removed without changing the desired radius of the resonator.

Once the wafer has been drilled, the metal sheet and wafer can be placed on the hot plate and the precursor can be extracted from the wafer once the wax has melted. The precursor should be cleaning by wrapping it in lens tissues and placed into a beaker with a small amount of acetone. This beaker is then placed into a hyper-sonic bath that cleans excess wax, diamond slurry and other debris from the precursor.

%-----------------------------------------------------------------------------
\subsection{Mounting The Precursor}

Once we have a suitable precursor, this is removed from the wafer and mounted on the end of a brass rod. The precursor should be mounted flat on the end of the resonator, as the turning machine operates perpendicular to the long axis of the rod. This means any deviations from flat can give rise to eccentricities in the final shape of the resonator. The 'flatness' of the precursor can be checked on a microscope eyepiece that has a gird. The resonator edge is aligned with a grid line under the microscope. If the resonator is then rotated along the rod axis, one rotation should see the edge of the resonator remain near the specified grid line. Large deviations from the grid line as it rotates suggests that the resonator is not mounted flat on the brass rod. 

We devised a way to help ensure the precursor is flat upon the brass rod tip using a large press and a height adjustable stage (see figure \ref{fig:flattener}). We place a leveler on the press so that we can tell that the press is flat, then adjust the height of the stage such that the press starts to push the precursor down on the brass rod. This is most effective when the brass rod is on the hot plate as the wax needs to be soft to change the position of the precursor.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/wgmr_flattener_4}
	\decoRule
	\caption[Flattening device]{The device we made to help ensure a resonator precursor was mounted flat on a brass rod tip. The yellow leveller is used to make sure the press is flat.}
	\label{fig:flattener}
\end{figure}

%-----------------------------------------------------------------------------
\subsection{Cutting The Resonator}

The precursor is then cut into the shape of the desired resonator. This is using the diamond cutter and turning machine. For lithium niobate resonators, they should be cut in accordance with the negative rake angle, cutting speed and cutting depth specified in section. This should be done in steps with a surface inspection by microscope to ensure there are no cracks or breaks in the material.

The rake angle and feed rate of the diamond cutter are carefully controlled to ensure that the finished resonator has a high surface quality. Rake angle and feed rate are the two parameters that have the most impact on the final surface quality of a fabricated resonator. Rake angle is the angle between the surface normal of the precursor and the diamond cutter (see figure \ref{fig:negative_rake_angle}). A negative rake angle is found to have better results for brittle substances like lithium niobate. The diamond cutter is mounted below the equatorial plane of the brass rod to ensure a negative rake angle is obtained. A positive rake angle can chip or break the material during the fabrication process. The best rake angles are found anecdotally to be between \ang{-20} and \ang{-25} for lithium niobate. 

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/Cutting}
	\decoRule
	\caption[Negative rake angle schematic]{A schematic of the negative rake angle used when fabricating WGMRs.}
	\label{fig:negative_rake_angle}
\end{figure}

The feed rate is a measure of how far the diamond cutter travels while the precursor undergoes one revolution. The diamond cutter should approach at a maximum speed of \SI{25}{\micro\meter}/s and have a maximum cutting depth of \SI{2}{\micro\meter}.


%-----------------------------------------------------------------------------
\section{Polishing of WGMRs} \label{polishing_of_whispering_gallery_mode_resonators}
%-----------------------------------------------------------------------------

When the cutting process has been completed after enough steps, polishing will be required to ensure an optically smooth surface around the resonator. Polishing can take place immediately after a resonator is cut as they are both done on a lathe. The optically smooth surface is the result of removing any imperfections in the resonator. These imperfections often take the form of scratches, chips or indents. We can often see imperfections in the reflections of the resonator when looked at under a microscope. The lathe microscope shows two light spots in the top of the resonator and rotating the resonator can highlight imperfections when they cause a change to these light spots . The polishing is done by hand with a spin speed of $60$rpm. We use diamond slurries with varying sizes of diamond grains to polish the resonator. A lens cleaning tissue is folded in half multiple times to build an edge where a slurry can be applied and held to the resonator while it spins. The grain size in the slurry will usually start at \SI{9}{\micro\meter} or \SI{3}{\micro\meter} depending on the degree to which the resonator needs polishing. At these grain sizes the slurries are often in a water based solution however for finer grain sized slurries the particles are suspended in a grease solution, requiring a drop of distilled water applied to the resonator or slurry before polishing. The grain size of the slurry can be reduced to provide a finer polish and therefore an increased optical smoothness in the resonator. The application spot on the folded tissue should be changed when a different sized slurry is used to avoid scratching the surface with residual large diamond particles. The resonator should also be cleaned with isopropanol in order to remove any excess slurry before a smaller grain is applied. When the polishing process is complete and the surface appears smooth under a microscope, the resonator can be cleaned to remove any excess slurry.

\begin{figure}[th]
	\centering
	\includegraphics[height=2in]{Figures/Chapter2/slurries_1}
	\decoRule
	\caption[Polishing slurries]{An image of the different diamond slurries used to polish WGMRs. They are labelled by the grain size of the diamonds within them and kept seperate from each other to prevent cross contamination.}
	\label{fig:slurries}
\end{figure}

%-----------------------------------------------------------------------------
\section{Cleaning of WGMRs} \label{cleaning_of_whispering_gallery_mode_resonators}
%-----------------------------------------------------------------------------

Cleaning a resonator is done by hand on the lathe with a spin speed is set to $30$rpm. Cleaning alcohols like isopropanol or ethanol can be applied to a lens tissue and wiped along the surface of the resonator. The process is best done by beginning with the top of the resonator and moving the tissue towards the base of the rod. This motion is repeated along the resonator and ensures that any detritus collected is pushed in the same direction and away from the resonator. Otherwise random cleaning motions might only serve to smear the detritus around and result in little change to the cleanliness of the resonator. A dry lens tissue is used to ensure the cleaning alcohols are wiped away from the resonator. They evaporate but can sometimes leave a residue that causes chromatic aberration when looking at the resonator surface under a microscope. These aberrations can sometimes mask particles that would reduce the optical smoothness of the resonator and so they should be removed if possible.


%-----------------------------------------------------------------------------
\section{Maintenance of WGMRs} \label{maitenance_of_whispering_gallery_mode_resonators}
%-----------------------------------------------------------------------------

If a resonator that is placed in a setup begins to behave more poorly it is useful to place it in the lathe and observe it under a microscope. Often times the image under the microscope can diagnose why a resonator might suddenly perform worse. Below is a quick description of what to look for to diagnose poor resonator performance while observing the resonator in the microscope:

\begin{itemize}
	\item Often times dark spots that appear around the resonator rim can be a sign of dust tat can be fixed by repeating the cleaning process.
	
	\item If cleaning has removed the spots but has caused chromatic aberrations of either the light points in the rim from the microscope lamp, or any spots that appear rainbow in colour, this can be a sign of residue remaining on the resonator. This can be solved by properly drying the resonator after cleaning.
	
	\item If the reflected lamp light points change shape or reflection intensity asymmetrically as the resonator is rotated, this can be a sign of an asymmetric change in shape of the resonator., in the form of a dent or mark. If cleaning does not fix the issue the resonator will likely need to be re-polished.
\end{itemize}
