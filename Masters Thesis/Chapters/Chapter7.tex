%Chapter 7

\chapter{Discussion}

\label{Chapter7}

The purpose of this chapter is to discuss in greater depth the results and analysis seen in Chapter \ref{Chapter6}. We will discuss the results in more detail before looking at the conclusions that can be drawn from this experiment. We will end this chapter with an outlook on the future of this research going forward.

%-----------------------------------------------------------------------------
\section{Analysis}
%-----------------------------------------------------------------------------

The results resemble the shapes of the figures from the theory but there appears to be disagreement between the magnitudes of the expected shifts and mode broadenings and the results collected in this thesis. One of the first possible sources is noticing that there appeared to be random fluctuations in the linewidth and the Q-factor for the substrates that had a refractive index lower than lithium niobate. Conversely, the linewidths and Q-factors for the substrates that had higher refractive indices than lithium niobate appeared to be very stable. The follow-up issue to these fluctuations is that it makes determining where the resonator and substrate are in contact much harder. It is easy to discern where the resonator and substrate are in contact when the substrate has a refractive index larger than the resonator, as the process of mode-broadening out couples light and gives an indication that the substrate is at least in close proximity to the evanescent field of the resonator, if not directly within it.

There are very few factors that could influence this, the windows are all approximately the same dimensions so it is unlikely to be caused by the relative sizes of the substrates. Furthermore it is not a consequence of the frequency scale as we can remove the mode-broadening of germanium and still see a more uniform distribution of points that the fluctuating substrates cannot replicate.

The temperature control of the setup is also unlikely to be a factor as the substrate sweeps were conducted with the thermistor reading similar temperatures each time. Temperature control will be discussed in more detail in section \ref{temperature_control} below.

It is also unlikely to be related to the experimental methodology as the same processes were followed for each substrate. The range of movement of the substrate was not kept constant however the range of movement for the optical glass substrate was similar to the range of movement of the silicon substrate so it is unlikely related to the size or distances moved by the substrates during testing.

Finally, the fluctuations are unlikely to be the result of the choice of mode we studied when collecting data. It was very difficult to stabilise the setup for long enough to observe the same mode over the course of collecting data on all seven substrates. As a result we often had to use different modes. While we understand that the theory was based on the idea of retaining the same mode, this was simply not practical in the scope of this experiment. Using different modes between the substrates with higher refractive indices appears to have had little impact on the stability of the linewidth and Q-factor of the observed mode. We do expect that changing the observed mode would change the results expected. If we observed the same mode, this mode would likely have had a similar Q-factor and linewidth for the duration of the data collection. As the mode would be constant it would also have constant mode numbers. This has implications for comparing the frequency shift between substrates as light coupling into a mode with different mode numbers will experience a slightly different index of refraction inside the resonator. This in turn can change the size of the evanescent field and therefore changing how the substrate interacts with the mode. We do not have reason to believe that this would alter our results by a great deal as we saw that there was a similar change in frequency across many modes tested with the same substrate. This observation removed the requirement to check if a mode was fundamental before the data collection was performed, significantly reducing the preliminary work that needed to be done before every data collection instance.

%-----------------------------------------------------------------------------
\section{Determining Criteria For Results}
%-----------------------------------------------------------------------------

We collected a large quantity of data during this experiment but only a small number of sweeps could be considered useful or good data. Here we will discuss why most sweeps were rejected from the results seen in chapter \ref{Chapter6}. Often it was the case that there would be too great an instability in the system. For the majority of this thesis that was likely due to limited thermal stability. We introduced the temperature stabiliser and temperature controller very late into this research and even only having a thermistor in the setup has been very useful in determining why there has been such large instabilities. 

The instabilities gave reason to discount results for numerous reasons. At times it would be difficult to obtain results for more than \SI{15}{\volt} step ranges as the mode we were observing would simply drift off of the side of the oscilloscope. This cannot be fixed mid sweep in data collection without seriously jeopardising the results. This was a problem faced a lot over this experiment. We had numerous theories about what the cause might be but the inconsistency of the drift made it hard to test it. 

Sometimes the instabilities would be of such large magnitudes that it would cover up any trace of a shift. This was a problem for the substrates that displayed little frequency detuning as the magnitude of the instability would be too great to discern the interaction on the frequency from the substrate. This was also a problem for the high refractive index substrates. There were no signs of blue-shift from germanium or silicon for the majority of this experiment. We have reason to believe that the primarily red-shift from the environment is thermal based, but more specifically from heat caused by people moving about the lab. Anecdotally it was noticed that a single person in the lab collecting data could increase the temperature of the resonator environment by as much as $\ang{4}$. This change in temperature had two effects on the resonances. The first effect we described earlier with reference to modes red-shifting out of the oscilloscope sampling range. The second effect is that it made mode retention extremely difficult between substrates. If we had a mode that showed some stability and a high Q-factor, the process of removing the box and swapping to the next substrate introduced enough heat into the system that modes would sometimes just disappear, or become so hard to discern from the background noise that new modes were sought out instead. This was confirmed when calibrating the temperature controller and purposefully introducing large amounts of heat into the resonators environment, a substantial increase in temperature could cause even a stable mode to 'disappear'. Some of these thermal instabilities manifested as fluctuations that were noticed as periodic movements in the results. These fluctuations often increased the calculated error associated with the frequency shift. Steps were taken to try and reduce the magnitude of these errors, such as refining the analysed area to be more local to the evanescent field of the WGMR, however we felt that the process we had for fitting errors was sufficiently rigorous that we were obliged to keep this method the same throughout the sweeps. This did lead to error bars with greater magnitudes than perhaps is desired, however it would also prove difficult to justify manipulating single substrate data or errors for the purpose of making the bars smaller.

Sometimes even if the mode was stable, there would be little sign of an interaction with the substrate. This results in plots that show no change beyond random noise for linewidth, Q-factor or coupling contrast. The solution to this is to adjust the distance between the resonator and substrate but there were times when adjusting the offset of the substrate position by as much as \SI{2}{\micro\metre} would not yield anything different. We tried working around this by using different techniques such as sweeping the substrate backwards immediately after hearing that it had made contact with the resonator when moved into place. This did not prove useful as some indications suggested that we had deformed the resonator by such an amount that the substrate looked to have remained in the evanescent field for the entirety of the sweep. This is not useful data as the results of constantly deforming a resonator make determining the point of contact between the substrate and resonator very difficult.

There was also an issue of mode deterioration, whereby a mode that might appear stable would sweep with a steadily worsening coupling contrast until it was indistinguishable from the baseline noise. Adjusting the setup to change the coupling into the resonator often became insufficient to recover the mode, meaning that mode deterioration often required that the resonator be removed from the setup and cleaned (using the process detailed in section \ref{cleaning_of_whispering_gallery_mode_resonators}) and re-coupled into. The cause was theorised to be a possible combination of the repeated impacts between the resonator and substrate causing an introduction of dust or other particles onto the resonator (increasing losses) and the impacts moving the resonator slightly in relation to the substrate and coupling prism causing the coupling setup to change slightly.

As can be seen throughout chapter \ref{Chapter6}, we used a  linear fit line to try and reverse some of the effects of the passive drift occurring before the substrate enters the evanescent field. There were many times when this drift was not linear and therefore would be hard to justify removing with a linear fit line. Once the temperature controller was installed, it was not able to reach thermal equilibrium due to various factors explained in \ref{temperature_control}. Because it was not able to reach thermal equilibrium it would exist in a near constant state of heating and cooling. If we collected data during an inflection point between heating and cooling, a non linear shift would be noted throughout the results of that sweep. This is because the introduction of heat would red-shift the mode, and removal of heat would blue-shift the mode. We were experiencing both directions of shift in some sweeps that could not be justified as data.

Finally there were instabilities that made it hard to separate the results into the controlled component and the interaction component. If a substrate is sufficiently far away from the evanescent field, we expect that the mode has a near constant linewidth and coupling contrast. This was not always the case as there were many sweeps conducted that showed a wildly fluctuating linewidth or coupling contrast that made us think there were grounds to discount the sweep. This was not done to cherry-pick clean data sets but used more as a first measure of data quality. Fluctuations in linewidths could still be used in data sets depending on the magnitude and frequency of the fluctuations. This was another problem of collecting the results with some substrates that did not experience a large dielectric shift because it clouds the difference between a sweep that works and shows no shift and a sweep that doesn't show a shift because it's too far away. We had to use pseudo colour plots along with the raw result outputs to make a decision regarding whether or not a substrate had interacted with the resonator.

Let us change tact for a moment and discuss how we selected the good data rather than discounted the bad. Most data sweeps were taken and analysed to produce the raw result plots seen in \ref{Chapter6}, what we looked for was mostly a difference between the start and end of a sweep. This was a good rule of thumb to suggest an interaction had taken place. The data was looked upon more favourably if there was a smooth transition between the start and end of a sweep. Discontinuities were not grounds for discounting the data set unless the discontinuities were too dispersed from a line of best fit.

Data was mostly evaluated in this way, smooth, stable data was more likely to be analysed because it would throw less exceptions when analysed in MATLAB. At no point did we choose the data sets based on the observed dielectric shift alone, there were many sweeps that demonstrated shifts that would likely fall in line with the theory, but were discounted for any of the other reasons given above.

\subsection{Frequency Shift Measurement} \label{frequency_shift_measurement}

From the discussion of the method in section \ref{data_collection_method}, one might wonder how a single mode was kept in focus despite all of the problems discussed above, for the duration of collecting data across 6 different substrates. Unfortunately there was no single mode that gave usable data across all 6 substrates as there was almost always a reason to discount data collected from one or more modes from the discussion above. 
This lead to an important question that rose from the method. When one mode is presenting data that does not need to be discounted, should repeatability or extend-ability be the priority? The argument for repeatability is that we have a mode that can be swept over multiple times for one substrate and good data for one substrate be collected, but risk the mode deteriorating before data can be collected for all 6 substrates. Conversely, if the data is "good enough" for one mode, it could be used to collect data across the remaining substrates but risk the lower sample size skewing the results for a given substrate.
Our answer to this question was to exercise judgment in each case depending on the factors at play. Often this would mean taking data from 6 sweeps (3 in each direction). Usually there would be a reason to discount at least one if not several of the sweeps for reasons discussed above. If data was collected for a sweep in each direction, and the results of these sweeps were within the bounds of error of each other, we usually tried to preserve this mode for testing on other substrates, thus striking a balance between repeatability and extend-ability.
Building a running average for each sweep across different modes would have been preferable but was not possible within the scale of this experiment. This was due to the short interval between data collection and the writing of this thesis. Instead a single sweep was selected to be representative of the sweeps collected by testing that sweep against the criteria discussed above.

%-----------------------------------------------------------------------------
\section{Temperature Control} \label{temperature_control}
%-----------------------------------------------------------------------------

We discovered that temperature played a very important role in the stability of the setup. We used the cardboard box and temperature controller to help control the temperature during the data collection. The temperature in the laboratory and the local resonator environment could have a large impact on the frequency of the mode observed if it was not controlled carefully. Often by simply existing in the lab while taking data would be enough to heat the environment around the resonator and cause a red-shift to occur in the data. This is why most of the substrate results feature a red-shift in the frequency shift that we attempted to minimise by using a linear fit. To demonstrate the effect that temperature can have on the frequency, we found some relevant literature on the thermo-optic properties of lithium niobate. Each of the two papers found are discussed in detail below.

One paper by \citefull{savchenkov_enhancement_2006} features a WGMR left for 4 hours with a sweeping laser. This shifted the frequency by approximately \SI{3}{\giga\hertz} as can be seen in figure \ref{fig:savchenkov}. As this was left over a period of \SI{4.5}{\hour}, we can determine that their resonator had a frequency drift of

\begin{equation}
f_\mathrm{drift} = \frac{\Delta\cdot f}{\mathrm{time}},
\end{equation}

\begin{equation}
f_\mathrm{drift} = 11.\overline{11} \ \mathrm{MHz}\ \mathrm{min}^{-1}.
\end{equation}

The duration of a sweep in our experiment is approximately \SI{30}{\second}. \citeauthor{savchenkov_enhancement_2006} uses figure \ref{fig:savchenkov} to show how their spectrum evolves in time. The passive frequency shift experienced by the substrates in our experiment is unlikely to be explained by this effect, due to the difference in direction of drifts experienced. The frequency drift presented in this paper would need to be negative to explain the passive red-shift experienced by the substrates in our experiment. Even if the direction was reversed, the red-shift was often at a magnitude greater than $11.\overline{11} \ \mathrm{MHz}\ \mathrm{min}^{-1}$.

\begin{figure}[th]
	\centering
	\includegraphics[height=3in]{Figures/Chapter7/savchenkov}
	\decoRule
	\caption[Savchenkov figure]{Figure taken from \citeauthor{savchenkov_enhancement_2006} showing the effect of temperature on the frequency of a WGMR..}
	\label{fig:savchenkov}
\end{figure}

The second paper we will discuss is a paper by \citefull{moretti_temperature_2005} which discusses the thermal expansion of a lithium niobate WGMR. We assumed that the cardboard box would thermalize the resonator and substrate to the same temperature within \SI{120}{\second} of placing the cardboard box over our setup. \cite{moretti_temperature_2005} derives the coefficients for thermal expansion in a z-cut lithium niobate resonator. This is identical to the one used in our setup.

To determine how temperature changes the frequency then, we have the frequency of a WGMR mode

\begin{equation}
\nu = \frac{m\cdot c}{2\pi\cdot R\cdot n_{\mathrm{eff}}}.
\end{equation}

Taking the time derivative yields

\begin{equation}
\frac{d\nu}{dT} = -\frac{\mathrm{m}\cdot \mathrm{c}}{\mathrm{R}\cdot \mathrm{n_{eff}}}\left(\frac{1}{\mathrm{R}}\frac{\partial R}{\partial T} + \frac{1}{\mathrm{n_{eff}}}\frac{\partial \mathrm{n_{eff}}}{\partial T}\right).
\end{equation}

We make the assumption that $n_{eff} \approx n_e$ because the resonator is mounted with the optical axis in the z-direction. This also assumes that only the fundamental modes are coupled into. Because of this assumption, we can therefore also assume that they will have approximately equal temperature derivatives

\begin{equation}
\frac{\partial \mathrm{n_{eff}}}{\partial T} \approx \frac{\partial \mathrm{n_e}}{\partial T}.
\end{equation}

Therefore we have

\begin{equation} \label{eq:nu}
\frac{1}{\nu}\frac{d\nu}{dT} = -\frac{1}{\mathrm{R}}\frac{\partial \mathrm{R}}{\partial \mathrm{T}} - \frac{1}{\mathrm{n_e}}\frac{\partial \mathrm{n_e}}{\partial \mathrm{T}}.
\end{equation}

We define 

\begin{equation}
\alpha_{perp} = \frac{1}{\mathrm{R}}\frac{\partial \mathrm{R}}{\partial \mathrm{T}},
\end{equation}

and

\begin{equation} \label{eq:alpha_ne}
\alpha_{ne} = \frac{1}{\mathrm{n_e}} \frac{\partial \mathrm{n_e}}{\partial \mathrm{T}}.
\end{equation}

We can now rewrite equation \ref{eq:nu} as

\begin{equation}
\frac{1}{\nu}\frac{d\nu}{dT} = - \alpha_{perp} - \alpha_{ne}.
\end{equation}

We have collected terms parallel to the optical axis $\alpha_{ne}$ and the terms perpendicular to the optical axis $\alpha_{perp}$. In \citefull{pignatiello_measurement_2007} they measure the thermal expansion coefficients of lithium niobate. We quote the result for z-cut lithium niobate

\begin{equation}
\alpha_{perp} = 13.6 \times 10^{-6} K^{-1}.
\end{equation}

\citeauthor{moretti_temperature_2005} doesn't solve for the coefficients with $1550$nm light but we assume that the results for $1523$nm are approximately the same. We also assume room temperature (\ang{22}). Starting with equation 4 of \cite{moretti_temperature_2005}

\begin{equation}
\frac{dn_e}{dT} = -2.6 + 19.8 \times 10^{-3}T(10^{-5}K^{-1}),
\end{equation}

and substitute \ang{22}C = \SI{295.15}{\kelvin} we get

\begin{equation}
\frac{dn_e}{dT} = -2.6 \cdot 10^{-5} + 19.8 \cdot 10^{-3} \cdot 295.15 \cdot 10^{-5}.
\end{equation}

\begin{equation}
\frac{dn_e}{dT} = 3.244 \times 10^{-5}
\end{equation}

We can use this result with $n_e = 2.21$ to solve equation \ref{eq:alpha_ne}

\begin{equation}
\alpha_{ne} = 1.468 \times 10^{-5}.
\end{equation}

Putting these values together to reform equation \ref{eq:nu} gives us

\begin{equation}
\frac{1}{\nu}\frac{d\nu}{dT} = -13.6 \times 10^{-6} - 1.468 \times 10^{-5} = -2.828 \times 10^{-5}.
\end{equation}

To get the derivative we multiply both sides by $\nu$ which we find by solving the wave equation \ref{eq:c} for $\lambda = 1550$

\begin{equation}
\nu_{1550} = 1.935 \times 10^{14}.
\end{equation}

Therefore

\begin{equation}
\frac{d\nu}{dT} = -5.472 \times 10^{9}.
\end{equation}

We rearrange this expression to find that a \SI{10}{\mega\hertz} change in frequency can be caused by a change in temperature of \ang{-1.83}mK.

In this experiment we used a temperature controller that could thermalize a setup with a stability of $\pm \ang{0.02}$C but there were problems with the implementation. The heat-sink we used did not seem to be effective at dispersing the heat. If the thermistor was at a higher temperature than was set on the temperature controller, the Peltier would dump heat into the heat-sink. This would cause the heat-sink to act like a proxy heater and warm up the resonator even if the brass rod was cooling down. We attempted to use a small current through the Peltier to generate a fixed red-shift that could be accounted for in the results, yet the ability to generate a small red-shift was masked by the passive heating of the resonator environment that occurred when people remained in the lab for any period of time. Further to this point, the lack of thermal insulation between the brass rod, the aluminium block it resides in and the copper temperature control block meant that the aluminium block would also function as a large heat-sink secondary to the one mounted on the Peltier. As this was connected directly to the optical table it makes it hard to gauge and calibrate the temperature controller as the system is made more complicated than passing a thermal load through a copper block. Even when the temperature would be close to stable, and oscillate over a range of $\ang{0.1}$C, the temperature fluctuations would cause the modes to oscillate back and forth on the oscilloscope. To have the modes moving forwards and backwards is less desirable than movement in a single direction. In a single direction we can be aware of the shift when we are taking data, knowing that the shift we are seeing while collecting the data. If the modes were to oscillate while data was being collected, it would be much harder to discern whether or not the shift we were seeing was resulting from the substrate of the temperature.

As we were not able to use the temperature controller to its full extent, the way we used it was more to supplement the passive temperature changes that were occurring in the local environment to the resonator. We used the temperature controller to raise the temperature of the resonator environment if the red-shift resulting from passive heating looked to increase the frequency of the observed mode past the bounds of the oscilloscope screen.

%-----------------------------------------------------------------------------
\section{Conclusion}
%-----------------------------------------------------------------------------

Our aim was to determine if the relationship between refractive index and frequency shift exists as is proposed in \citeauthor{foreman_dielectric_2016}. We have shown that the relationship is verifiable from the results produced in chapter \ref{Chapter6}, and through creating our own versions of the figures in presented in \citeauthor{foreman_dielectric_2016} that were discussed in section \ref{substrate_refractive_index_effects}. 

We were also able to show that the relationship between mode broadening and substrate refractive index can also be verified. These conclusions are strongest for the TE modes tested, however the TM modes tested appear to bear a similar shape when compared with the theory from \citeauthor{foreman_dielectric_2016}.

While these results are conclusive, the conclusions would be further strengthened if a single mode were observed for the duration of the data collection. We showed that the temperature control is crucial for mode retention as thermal fluctuations are responsible for the dismissal of a large amount of the data collected.

%-----------------------------------------------------------------------------
\section{Outlook}
%-----------------------------------------------------------------------------

There are now systems in place that would make further study of this possible, we have demonstrated the ability to collect large quantities of data using autonomous computer scripts and communication between laboratory apparatus. We were able to demonstrate that temperature fluctuations are the main source of instability in this setup. Further refining of the temperature controller design would reduce thermal fluctuations and potentially make observing one mode across all seven substrates possible. While we have shown evidence of the relationship for frequency tuned TE modes, the relationship for frequency tuning TM modes can be investigated further due to the reduced agreement between the experimental results and the theory.