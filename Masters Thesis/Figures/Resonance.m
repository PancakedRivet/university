set(0,'DefaultAxesFontSize',20,'DefaultTextFontSize',20); %Increasing the default plot size

plot_data_rel = plot_data(1,:) / max(plot_data(1,midpoint-150:midpoint+150));

plot(plot_frequency,plot_data_rel,'k')
title('WGMR Mode with Sidebands')
ylabel('Relative Voltage (V)')
xlabel('Frequency (MHz)')
xlim([-150 150])

saveas(gcf,'Resonance.png')