%% Question 2:
s = load('signal.txt'); %Loading the signal
received_s = load('shifted_signal_and_noise.txt'); %Loading the signal and noise

len_s = length(s); %Determining the length of the signal
len_rs = length(received_s); %Determining the length of the signal and noise

padded_signal = zeros(1,len_rs); %Padding the original signal to make it the same length as the signal with noise

for x = 1 : len_s
    padded_signal(x) = s(x); %Replicating the original signal within the padded one
end

flip = fliplr(padded_signal); %Flipping the signal to act as a time-reverse

RS = fft(received_s,len_rs); %Fourier Transforming the signal with noise
Flip = fft(flip,len_rs); %Fourier Transforming the padded signal

Filtered_Signal = RS .* Flip; %The product in the frequency domain is equivalent to the convolution in the time domain

filtered_signal = real(ifft(Filtered_Signal,len_rs)); %Inverse Fourier Transforming the signal back into time domain

figure(1); %Plotting for the remainder of the question
subplot(4,1,1)
plot(padded_signal)
title('Noise Free Signal')
xlabel('Time')
subplot(4,1,2)
plot(received_s)
title('Signal With Noise')
xlabel('Time')
subplot(4,1,3);
plot(filtered_signal)
title('Matched Filtered Output')
xlabel('Time')
subplot(4,1,4)
plot(filtered_signal)
title('Matched Filtered Output-Zoomed In')
xlabel('Time')
xlim([3000 3500]);

[value,index] = max(filtered_signal);

disp(index); %Printing the value of the offset

%% Question 3:
sample_length = 100; %Determining the number of bits of random data generated
sample_number_of_trials = 10000; %Determining the number of times the bits are generated for building an average

N = sample_length * 10; %The number of actual samples as each bit is sampled 10 times
T = 1 / 1e6; %Determing the period for the clock frequency
dT = T / 10; %Determinging the time per bit in each sample
freq = (1/dT) * (-N/2 : N/2-1)/N; %Generating the x-axis for plotting
num = zeros(sample_number_of_trials , sample_length); %Preallocating the vector to store the random bits
Num_Total = zeros(sample_number_of_trials , 10 * sample_length); %Preallocating the vector to store the samples
man_num = zeros(sample_number_of_trials , sample_length); %Preallocating the vector to store the random bits for Manchester Encoding
Man_Num_Total = zeros(sample_number_of_trials , 10 * sample_length); %Preallocating the vector to store the samples for Manchester Encoding

for x = 1:sample_number_of_trials
    num_pn = []; %Generating an empty vector for concatenating the extra samples of the same random bit
    man_num_pn = []; %Generating an empty vector for concatenating the extra samples of the same random bit for Manchester Encoding
    for y = 1:sample_length
        num(x,y) = -1 + 2 * randi([0 1],1,1); %Generating each random bit one at a time for values of either -1 or 1
        if num(x,y) == 1 %Testing if the random bit is positive (=1)
            pn = ones(1,10); %Used to increase the sampling frequency without changing the sample
            man_pn(1,1:5) = ones(1,5); %Used to increase the sampling frequency without changing the sample for Manchester Encoding
            man_pn(1,6:10) = -1 * ones(1,5); %In Manchester Encoding the first half of the clock signal for HIGH is = 1, the second half is = -1
        elseif num(x,y) == -1 %Testing if the random bit is negative (=-1)
            pn = -1 * ones(1,10); %Used to increase the sampling frequency without changing the sample
            man_pn(1,1:5) = -1 * ones(1,5); %Used to increase the sampling frequency without changing the sample for Manchester Encoding
            man_pn(1,6:10) = ones(1,5); %In Manchester Encoding the first half of the clock signal for LOW is = -1, the second half is = 1
        end;
        num_pn = [num_pn pn]; %Concatenate the vectors of extra samples for each random bit
        man_num_pn = [man_num_pn man_pn]; %Concatenate the vectors of extra samples for each random bit for Manchester Encoding        
    end;
    
    Num_Shift = fftshift(fft(num_pn)); %Fourier Transforms the samples
    Num_Shift_Abs = abs(Num_Shift).^2; %Getting the power values for the samples
    Num_Total(x,:) = Num_Shift_Abs; %Adding the vector of each trial's power into a matrix for every trial
    clear num_pn; %Clearing the vector in preparation for the next trial

    Man_Num_Shift = fftshift(fft(man_num_pn)); %Fourier Transforms the samples for Manchester Encoding
    Man_Num_Shift_Abs = abs(Man_Num_Shift).^2; %Getting the power values for the samples for Manchester Encoding
    Man_Num_Total(x,:) = Man_Num_Shift_Abs; %Adding the vector of each trial's power into a matrix for every trial for Manchester Encoding
    clear man_num_pn; %Clearing the vector in preparation for the next trial for Manchester Encoding
end

Num_Mean = mean(Num_Total) .* (dT^2/T); %Generating the mean for the matrix of trials and samples. Also accounting for normalisation in the units
Man_Num_Mean = mean(Man_Num_Total) .* (dT^2/T); %Generating the mean for the matrix of trials and samples for Manchester Encoding. Also accounting for normalisation in the units

Power_Spectral_Density = (1 / T) * abs((T/2) * ( (exp(j*pi*2*freq*(T/4)) .* sinc(T/2*freq)) - (exp(-j*pi*freq*(3/4)*T) .* sinc(T/2*freq)) ) ).^2; %Generating the analytical solution

figure(2) %Just plotting the data for both the simulated data creation and the Manchester Encoding of the simulated data
subplot(3,1,1)
plot(freq,Num_Mean)
title(['Averaged Simulated Bit Steam Data: ' num2str(N) ' Samples']);
xlabel('Frequency (Hz)');
ylabel('Power');
subplot(3,1,2)
plot(freq,Man_Num_Mean)
title(['Averaged Manchester Encoding of Simulated Bit Stream Data: ' num2str(N) ' Samples']);
xlabel('Frequency (Hz)');
ylabel('Power');
subplot(3,1,3)
plot(freq,Power_Spectral_Density)
title(['Analytical Solution: ' num2str(N) ' Samples']);
xlabel('Frequency (Hz)');
ylabel('Power');