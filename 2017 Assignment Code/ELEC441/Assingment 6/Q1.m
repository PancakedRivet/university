%Patrick Devane ELEC441 Assignment 6 Q1: 
%% Part a)
[f,fs] = audioread('piano_chord.wav');

piano_chord = audioplayer(f,fs);
piano_chord.play;

t = (0:(length(f)-1))/fs; % Generates a time vector
N = size(f,1); % Determine total number of samples in audio file
df = fs / N; % Determines the change in frequency for each index in the vector
Xa = (-(N/2):(N/2)-1)*df; % Generates a range of frequencies between positive and negative Nyquist frequencies
Ya = fft(f, N); % Fourier Transform of the signal
YaShift = fftshift(Ya); % Shifts the Fourier Transform to the middle of the 

bound = 8000; % Necessary to scale the x-axis between -2000Hz and 2000Hz

xaBound = Xa(N/2-bound:N/2+bound); % Scaling the x-axis to the desired range
YaBound = YaShift(N/2-bound:N/2+bound); % Scaling the y-axis to ensure matrix dimensions agree

figure(1); % Plotting for the remainder of the question
subplot(2,1,1);
plot(t,f);
title('Signal Spectrum of "piano chord"');
ylabel('Signal');
xlabel('Time (sec)');
subplot(2,1,2);
plot(xaBound,abs(YaBound).^2);
title('Spectrum of "piano chord"');
ylabel('Power Spectral Density');
xlabel('Frequency (Hz)');

%% Part b)
Xb = Xa; % Importing the variables needed from Part a)
YbShift = zeros(length(Ya),1); % Making a zero vector to save the required frequencies

for i = 1:90; % Iterating over the possible harmonics of the fundamental frequency required (523.25Hz)
    for j = 1:N; % Iterating over every index in the vector
        if abs(Xb(j)) >= 523.25*i -25 && abs(Xb(j)) <=523.25*i +25;
            YbShift(j) = YaShift(j); % Mapping the desired values into the zero vector
        elseif abs(Xb(j)) >= 523.25*(-i) -25 && abs(Xb(j)) <=523.25*(-i) +25;
            YbShift(j) = YaShift(j); % Mapping the desired values into the zero vector
        end
    end
end

Yb = ifftshift(YbShift); % Reversing the shift from Part a)
yb = real(ifft(Yb)); % Performing the inverse Fourier Transform and taking the real part to remove any residual imaginary componenets

one_note = audioplayer(yb,fs);
one_note.play;

audiowrite('one_note.wav',yb,fs); % Writes the new waveform to a file

figure(2); % Plotting for the remainder of the question
subplot(2,1,1);
plot(t,yb);
title('Signal Spectrum of "one note"');
ylabel('Signal');
xlabel('Time (sec)');
subplot(2,1,2);
plot(Xb,abs(YbShift).^2);
title('Power Spectrum of "one note"');
ylabel('Power Spectral Density');
xlabel('Frequency (Hz)');

%% Part c)
Xc = Xb;  % Importing the variables needed from Part b)
Yc = Ya;

h1 = zeros(length(t),1); % Creating the zero vectors for the Transfer Function
h2 = zeros(length(t),1);
h3 = zeros(length(t),1);

h1 = 50 * heaviside(t); % Multiplying the vectors for each part of the Transfer Function by the time vector
h2 = exp(-10 .* t);
h3 = sin(2 * pi * 440 .* t);

h = h1 .* h2 .* h3; % Assembling the entire Transfer Function from its components
H = fft(h,N); % Taking the Fourier Transform of the Transfer Function

HShift = fftshift(H); % Shifting it for plotting

Bandpass = Ya.' .* H; % Creating the bandpass filter by multiplying the frequency data by the Transfer Function
bandpass = real(ifft(Bandpass));

bandpassNorm = bandpass ./ fs; % Normallising the sound to prevent clipping

band_pass = audioplayer(bandpassNorm,fs);
band_pass.play;

audiowrite('band_pass.wav',bandpassNorm,fs); % Writes the new waveform to a file

Hangle = fftshift(angle(H));
HangleX = Xc(N/2+1892-150:N/2+1892+150);
HangleY = Hangle(N/2+1892-150:N/2+1892+150);

XcBound = Xc(N/2+1892-150:N/2+1892+150); % Refining the bounds to focus around 440Hz 
YcBound = HShift(N/2+1892-150:N/2+1892+150);

figure(3); % Plotting for the remainder of the question
subplot(3,1,1);
plot(t,bandpassNorm);
title('Signal Spectrum of "band pass"');
ylabel('Signal');
xlabel('Time (sec)');
subplot(3,1,2);
plot(XcBound,abs(YcBound));
title('Absolute Value of the Transfer Function');
ylabel('Value');
xlabel('Frequency (Hz)');
subplot(3,1,3);
plot(HangleX,HangleY);
title('Argument of the Transfer Function');
ylabel('Argument (Radians)');
xlabel('Frequency (Hz)');