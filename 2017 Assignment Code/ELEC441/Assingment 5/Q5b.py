# -*- coding: utf-8 -*-
"""
Created on Tue Apr 18 21:25:22 2017

@author: devpa499
"""

import numpy as np
import matplotlib.pyplot as plt

#using \nu_0 = 1 and T = 1

timeData = np.arange(0.0, 3, 0.01) #x axis for the sin graphs
sinCurve1Data = np.sin(2*np.pi*timeData) 
sinCurve2Data = np.sin(2*np.pi*(1+1)*timeData)

zeroDataX = np.arange(0, 3, 1)
zeroDataY = np.zeros(len(zeroDataX))

plt.figure(1)

plt.subplot(311)
plt.plot(timeData, sinCurve1Data, 'b-')

plt.subplot(312)
plt.plot(timeData, sinCurve2Data, 'r-')

plt.subplot(313)
plt.plot(timeData, sinCurve2Data, 'r-', timeData, sinCurve1Data, 'b-', zeroDataX, zeroDataY, 'ko')

plt.show()