% CIRA_86_loader.m
%
% History:
% Craig J Rodger      9 Mar 2002
% Daniel Schumayer   12 Jul 2016
%     Changed the columnwise 1d intepolation to a 2d interpolation.
%     The storage of the interpolated data is now one object instead
%     of many individual variables. The access to monthly data is by
%     the last index labelling the month, e.g. 1=January, 2=February,
%     etc.

%% Problem description
%
% The COSPAR International Reference Atmosphere (CIRA) provides
% empirical models of atmospheric parameters from 0\,km to 2000\,km as
% recommended by the Committee on Space Research (COSPAR).
%
% I provide the monthly text files with only the lower part (0km to
% 120km) of CIRA-86. The files consist of tables of the monthly mean
% values of temperature and pressure for the latitude range 80S to 80N.
% Temperatures range from 0 to 120 km, while pressures range over
% 20-120km, in height coordinates with 5km resolution. Note there are
% no temperature values for sea-level at 80S.
%
% The CIRA-86 includes latitudinal and monthly (seasonal) variation,
% but not longitudinal or diurnal. Temperature is given in Kelvin in
% the raw CIRA data files, but pressure is in millibar (mb), where you
% also need to include the exponent listed at the far right of the
% table (e.g., for January 80S at 120km altitude, the pressure is given
% as 2.714 x 10^(-5) mb. The model accurately reproduces most of the
% characteristic features of the atmosphere, such as the equatorial
% wind and the general structure of the tropopause, stratopause, and
% mesopause. 
%
% I provide you with the Matlab file CIRA_86_loader.m to load the
% various temperature and pressure files, and interpolate the data to
% 1km altitude resolution.

clf;
clear all;
close all;
if fclose('all') == -1
    warning('Some files may have been left opened.')
end;
clc;

% You have to modify these two paths so that they correctly reflect
% the paths and the filenames. If you put the *.txt files into the
% same directory as this script, you can simply remove the '../Data/'
% substring.
T_FNAME = 'Cira86_temps_month_%g.txt';
P_FNAME = 'Cira86_pressures_month_%g.txt';

raw_T_latitude = -80 : 10 : 80; % 80 S to 80 N in steps of 10 degrees
raw_T_altitude = 120 : -5 :  0; % 120 km to 0 km in steps of 5 km
new_T_altitude = 120 : -1 :  0; % 120 km to 0 km in steps of 1 km

%% Temperature data
for month = 1:12;
   % Temperature from Cira86_temps_month_XX.txt
   rawdata = load(sprintf(T_FNAME, month));

   % Two-dimensional spline interpolation
   [X,   Y] = meshgrid(raw_T_latitude, raw_T_altitude);
   [XI, YI] = meshgrid(raw_T_latitude, new_T_altitude);
   interpolated = interp2(X, Y, log(rawdata), XI, YI, 'spline');
   interpolated = exp(interpolated);

   % Store the interpolated data in CIRA86_Temperature
   CIRA86_Temperature(:, :, month) = interpolated;
end;
clear rawdata X Y XI YI interpolated month T_FNAME;

%% Questions (a) and (b)
%
% QUESTION: At what altitudes over Dunedin (45S, 170E) are the
%           locations of the tropopause, the stratopause, and that of
%           the mesopause? How do each of these heights vary with
%           season?
%
% Hint: In case you need a function which searches for peaks in a data
%       have a look at findpeaks in the Signal Processing Toolbox, or
%       you can write your own routine in a separate file.
%
% QUESTION: Looking at the global temperature values, what is the
%           latitudinal and seasonal variation in the positions of the
%           tropopause, the stratopause and the mesopause?
%
% HERE YOU CAN ANSWER THE FIRST TWO QUESTIONS.

%% Pressure data
raw_P_latitude = -80 : 10 : 80;  % 80S to 80N in steps of 10 degrees
raw_P_altitude = 120 : -5 : 20;  % Data starts at 20 km altitude!
new_P_altitude = 120 : -1 : 20;
for month =1:12;
   % Pressure from Cira86_pressures_month_XX.txt
   rawdata = load(sprintf(P_FNAME, month));

   % Capture the exponents from last column and reshape pressure data
   exponent = rawdata(:, end);
   rawdata = rawdata(:, 1:end-1);

   % Use the exponents so we have the actual pressure values, in mbar
   [~, ncol] = size(rawdata);
   exponent = repmat(exponent, 1, ncol);
   rawdata = rawdata .* (10.^exponent);

   % Two-dimensional spline interpolation
   [X,   Y] = meshgrid(raw_P_latitude, raw_P_altitude);
   [XI, YI] = meshgrid(raw_P_latitude, new_P_altitude);
   interpolated = interp2(X, Y, log(rawdata), XI, YI, 'spline');
   interpolated = exp(interpolated);
   
   % Store the interpolated data in CIRA86_Pressure
   CIRA86_Pressure(:, :, month) = interpolated;
end;
clear rawdata nrow ncol exponent X Y XI YI interpolated month;
clc;

%% Remark
% By this point you have two three-dimensional arrays in memory:
%
%              CIRA86_Temperature and CIRA86_Pressure.
%
% The third indices of these arrays label the corresponding month.
% However, note that the pressure is still in units of millibar. You
% may need to convert it to pascals for answering the questions.


%% Question (c)
%
% Question: Estimate the scale height for atmospheric pressure between
%           20 and 120km for a given latitude and month. Comment on
%           whether there is a large variability based on latitude and
%           month and what you are basing your observations on.
%
% ANSWER:

%% Questions (d), (e), and (f)
%
% Question: What is the number density altitude profile for Dunedin
%           over the 20-120km range? How significantly does the number
%           density vary over the year.
%
% ANSWER:
%
% Question: How does this contrast with an equatorial location, say
%           Quito, Ecuador (0N, 78W)? How about a high latitude
%           Antarctic location, e.g., SANAE3 base, Antarctica (70S,2W)?
%
% ANSWER:
%
% Question: What is the mean total number of oxygen molecules above
%           Dunedin over 20-120\,km altitude? How significant are the
%           seasonal variations in this number?
%
% ANSWER:


% Useful constants to answer the 4th and 5th questions
kB = 1.3807e-23;	   % Boltzmann's constant in SI units
O2_fraction = 0.2095;  % O2 fraction in atmosphere (molecular oxygen)
Dunedin_radius = 15e3; % Estimate for Dunedin's area: 15km radius



% A useful SIMPLE numerical integration command is TRAPZ. Note that
% TRAPZ assumes your data has unit spacing, which in SI units is 1m.
% You have 1km spaced numbers for pressure and temperature. Use help
% trapz to see how to get around this!