%PHSI422 Assignment #2 Question 3:

clc;
clf;
close all;

%% Part a)

tropopause_altitude = zeros(2,12);
stratopause_altitude = zeros(2,12);
mesopause_altitude = zeros(2,12);

tropopause_temperature = zeros(2,12);
stratopause_temperature = zeros(2,12);
mesopause_temperature = zeros(2,12);

for latitude = 4:5;
    for month = 1:12;

        [neg_temp, neg_locs] = findpeaks(-CIRA86_Temperature(:,latitude,month),'MinPeakProminence',4);
        [pos_temp, pos_locs] = findpeaks(CIRA86_Temperature(:,latitude,month),'MinPeakProminence',4);
        
        % Tropopause
        tropopause_altitude(latitude-3,month) = new_T_altitude(neg_locs(2));
        tropopause_temperature(latitude-3,month) = neg_temp(2);
        
        % Stratopause
        stratopause_altitude(latitude-3,month) = new_T_altitude(pos_locs(1));
        stratopause_temperature(latitude-3,month) = pos_temp(1);
        
        % Mesopause
        mesopause_altitude(latitude-3,month) = new_T_altitude(neg_locs(1));
        mesopause_temperature(latitude-3,month) = neg_temp(1);

    end;
end

dunedin_tropopause = mean(tropopause_altitude);
dunedin_stratopause = mean(stratopause_altitude);
dunedin_mesopause = mean(mesopause_altitude);

figure(1)
plot( 1:12 , dunedin_tropopause,'kx-' )
hold on
plot( 1:12 , dunedin_stratopause,'rx-' )
plot( 1:12 , dunedin_mesopause,'bx-' )
hold off
title('Dunedin Pause Altitude')
xlabel('Month')
ylabel('Altitude (km)')
legend('Troposause','Startopause','Mesopause','Location','NorthEastOutside')

%% Part b)

deg = sprintf('%c', char(176));

tropopause_temperature = zeros(17,12);
stratopause_temperature = zeros(17,12);
mesopause_temperature = zeros(17,12);

tropopause_altitude = zeros(17,12);
stratopause_altitude = zeros(17,12);
mesopause_altitude = zeros(17,12);

for month = 1:12;
    for latitude = 1:17;
        
        [~, negative_altitude] = findpeaks(-CIRA86_Temperature(:,latitude,month),'MinPeakProminence',4);
        [~, positive_altitude] = findpeaks(CIRA86_Temperature(:,latitude,month),'MinPeakProminence',4);
        
        % Tropopause
        tropopause_altitude(latitude,month) = new_T_altitude(negative_altitude(2));
        % Stratopause
        stratopause_altitude(latitude,month) = new_T_altitude(positive_altitude(1));
        % Mesopause
        mesopause_altitude(latitude,month) = new_T_altitude(negative_altitude(1));
    
    end;
end

figure(2)
subplot(3,1,1)
surf(1:12,raw_T_latitude,tropopause_altitude)
title('Tropopause')
xlabel('Month')
ylabel(['Latitude (' deg 'N)'])
zlabel('Altitude (km)')
subplot(3,1,2)
surf(1:12,raw_T_latitude,stratopause_altitude)
title('Stratopause')
xlabel('Month')
ylabel(['Latitude (' deg 'N)'])
zlabel('Altitude (km)')
subplot(3,1,3)
surf(1:12,raw_T_latitude,mesopause_altitude)
title('Mesopause')
xlabel('Month')
ylabel(['Latitude (' deg 'N)'])
zlabel('Altitude (km)')

%% Part c)

deg = sprintf('%c', char(176));

% Scale Height Constants:
g = 9.81;
M = 28.96 * 1.67 * 10^-27;

temperature_data_variation = zeros(17,12);

for month = 1:12;
    for latitude = 1:17;
        temperature_data_variation(latitude,month) = CIRA86_Temperature(51,latitude,month);
    end
end

scale_height_constant = kB / (M * g);
scale_height = CIRA86_Temperature(:,1,1) .* scale_height_constant;
scale_height_variability = temperature_data_variation .* scale_height_constant;

figure(3) % Scale Height Value
subplot(2,1,1)
plot(scale_height,new_T_altitude)
title(['Scale Height at 80' deg 'S in January (20km-120km)'])
xlabel('Scale Height (m)')
ylabel('Altitude (km)')
ylim([20,120])
subplot(2,1,2) % Seasonal variability
surf(1:12,raw_T_latitude,scale_height_variability)
title('Scale Height Variability')
xlabel('Month')
ylabel(['Latitude (' deg 'N)'])
zlabel('Scale Height at 70km Altitude (m)')

%% Part d)

% Scale Height Constants:
M = 28.96 * 1.67 * 10^-27;
g = 9.81;
R = 8.314;
N_A = 6.022 * 10^23;

dunedin_temperature = zeros(101,12);
dunedin_pressure = zeros(101,12);

for month = 1:12;
    dunedin_temperature(:,month) = (CIRA86_Temperature(1:101,4,month) + CIRA86_Temperature(1:101,5,month))./2;
    dunedin_pressure(:,month) = 100 * (CIRA86_Pressure(:,4,month) + CIRA86_Pressure(:,5,month))./2;
end

dunedin_mole_density = dunedin_pressure ./ (dunedin_temperature .* R);
dunedin_number_density = N_A * dunedin_mole_density;

colour_gradient = linspace(0,1,12);

figure(4)
subplot(2,1,1)
plot(dunedin_number_density(:,1),new_P_altitude)
title('January Number Density in Dunedin')
xlabel('Number Density (molecules/m^3)')
ylabel('Altitude (km)')
subplot(2,1,2)
hold on
for month = 1:12;
    plot(dunedin_number_density(:,month),new_P_altitude,'Color',[colour_gradient(month) 0 1-colour_gradient(month)]);
end
hold off
title('Monthly Number Density in Dunedin')
xlabel('Number Density (molecules/m^3)')
ylabel('Altitude (km)')
legend('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Location','NorthEastOutside')
% xlim([0 1 * 10^20])
% ylim([60 80])

%% Part e)

% Scale Height Constants:
M = 28.96 * 1.67 * 10^-27;
g = 9.81;
R = 8.314;
N_A = 6.022 * 10^23;

ecuador_temperature = zeros(101,12);
ecuador_pressure = zeros(101,12);

antarctica_temperature = zeros(101,12);
antarctica_pressure = zeros(101,12);

for month = 1:12;
    ecuador_temperature(:,month) = CIRA86_Temperature(1:101,9,month);
    ecuador_pressure(:,month) = CIRA86_Pressure(:,9,month) * 100;
    
    antarctica_temperature(:,month) = CIRA86_Temperature(1:101,2,month);
    antarctica_pressure(:,month) = CIRA86_Pressure(:,2,month) * 100;
end

ecuador_mole_density = ecuador_pressure ./ (ecuador_temperature .* R);
ecuador_number_density = N_A * dunedin_mole_density;

antarctica_mole_density = antarctica_pressure ./ (antarctica_temperature .* R);
antarctica_number_density = N_A * antarctica_mole_density;

colour_gradient = linspace(0,1,12);

figure(5)
subplot(2,2,1)
plot(ecuador_number_density(:,1),new_P_altitude)
title('January Number Density in Quito, Ecuador')
xlabel('Number Density (molecules/m^3)')
ylabel('Altitude (km)')
subplot(2,2,3)
hold on
for month = 1:12;
    plot(ecuador_number_density(:,month),new_P_altitude,'Color',[colour_gradient(month) 0 1-colour_gradient(month)]);
end
hold off
title('Monthly Number Density in Quito, Ecuador')
xlabel('Number Density (molecules/m^3)')
ylabel('Altitude (km)')
legend('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Location','NorthEastOutside')
% xlim([0 1 * 10^20])
% ylim([60 80])
subplot(2,2,2)
plot(antarctica_number_density(:,1),new_P_altitude)
title('January Number Density in SANAE 3 base, Antarctica')
xlabel('Number Density (molecules/m^3)')
ylabel('Altitude (km)')
subplot(2,2,4)
hold on
for month = 1:12;
    plot(antarctica_number_density(:,month),new_P_altitude,'Color',[colour_gradient(month) 0 1-colour_gradient(month)]);
end
hold off
title('Monthly Number Density in SANAE 3 base,Antarctica')
xlabel('Number Density (molecules/m^3)')
ylabel('Altitude (km)')
legend('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Location','NorthEastOutside')
% xlim([0 1 * 10^20])
% ylim([60 80])

%% Part f)

% Scale Height Constants:
M = 28.96 * 1.67 * 10^-27;
g = 9.81;
R = 8.314;
N_A = 6.022 * 10^23;

dunedin_temperature = zeros(101,12);
dunedin_pressure = zeros(101,12);
oxygen_molecules = zeros(12,1);

dunedin_area = pi * (Dunedin_radius)^2;

for month = 1:12;
    dunedin_temperature = (CIRA86_Temperature(1:101,4,month) + CIRA86_Temperature(1:101,5,month))./2;
    dunedin_pressure = (CIRA86_Pressure(:,4,month) * 100 + CIRA86_Pressure(:,5,month) * 100)./2;
    dunedin_mole_density = dunedin_pressure ./ (dunedin_temperature .* R);
    dunedin_number_density = N_A .* dunedin_mole_density;
    O2_dunedin = dunedin_number_density .* O2_fraction .* dunedin_area;
    oxygen_molecules(month) = trapz(O2_dunedin) * 1000;
end

figure(6)
plot(oxygen_molecules,'kx-')
title('Number of Oxygen Molecules Above Dunedin')
xlabel('Month')
ylabel('Number of molecules')
