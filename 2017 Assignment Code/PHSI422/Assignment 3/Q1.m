clc;
close all;

%% a)

z = linspace(1,35786000,10000);

n_o = 10^9;
H_o = 60000;
H_ion = 190000;

n_e = n_o .* 10.^( 9.*((z - H_o) ./ H_ion) .* exp(-(z - H_o) ./ H_ion) );

% Plasma Frequency

q = 1.6*10^-19;
e_o = 8.85*10^-12;
m_e = 9.109*10^-31;

omega_p = sqrt( (n_e * q^2) / (e_o * m_e) );
plasma_frequency = omega_p / (2 * pi);

max_freq_MHz = max(plasma_frequency) * 10^-6

figure(1)
plot(n_e,z/1000)
ylim([1 1000])
title('Electron Density')
xlabel('Electron Density (m^-^3)')
ylabel('Altitude (km)')

figure(2)
plot(plasma_frequency * 10^-6,z/1000)
ylim([1 1000])
title('Plasma Frequency')
xlabel('Plasma Frequency (MHz)')
ylabel('Altitude (km)')
%% b)

global n_e;
global n_o;
global H_o;
global H_ion;

n_o = 10^9;
H_o = 60000;
H_ion = 190000;

q = 1.6*10^-19;
e_o = 8.85*10^-12;
m_e = 9.109*10^-31;

freq = 5*10^6;
omega_p = freq * 2 * pi;

n_e = ( omega_p^2 * e_o * m_e ) / q^2;

% Solving for z:

fun = @chapman;
x0 = [139000 141000];

reflection_height_km = fzero( fun,x0 ) / 1000

%% c)

global n_e;
global n_o;
global H_o;
global H_ion;

n_o = 10^9;
H_o = 60000;
H_ion = 190000;

q = 1.6*10^-19;
e_o = 8.85*10^-12;
m_e = 9.109*10^-31;

freq = 5*10^6;
omega_p = freq * 2 * pi;

n_e = ( omega_p^2 * e_o * m_e ) / q^2;

% Solving for z:

fun = @chapman;
x0 = [141000 6000000];

reflection_height_km = fzero( fun,x0 ) / 1000