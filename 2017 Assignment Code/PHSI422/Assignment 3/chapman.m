function F = chapman(x)

global n_e;
global n_o;
global H_o;
global H_ion;

% F = 9 * ((x - H_o) ./ H_ion) .* exp(-(x - H_o) ./ H_ion) - log10(n_e ./ n_o);
F = n_o * 10^(9 * ((x - H_o) ./ H_ion) .* exp(-(x - H_o) ./ H_ion)) - n_e;