%% Deriving 8.53:
sig_pos = [0 1 ; 0 0]; 
sig_neg = [0 0 ; 1 0];
sig_z = [1 0 ; 0 -1];

first_bracket = (sig_z * sig_pos) - (sig_z * sig_neg) - (sig_pos * sig_z) + (sig_neg * sig_z)
second_bracket = (2 * sig_pos * sig_z * sig_neg) - (sig_z * sig_pos * sig_neg) - (sig_pos * sig_neg * sig_z)

%% Deriving 8.54:
sig_pos = [0 1 ; 0 0]; 
sig_neg = [0 0 ; 1 0];
sig_z = [1 0 ; 0 -1];

first_bracket_pos = (sig_pos * sig_pos) - (sig_pos * sig_neg) - (sig_pos * sig_pos) + (sig_neg * sig_pos)
second_bracket_pos = (2 * sig_pos * sig_pos * sig_neg) - (sig_pos * sig_pos * sig_neg) - (sig_pos * sig_neg * sig_pos)

first_bracket_neg = (sig_neg * sig_pos) - (sig_neg * sig_neg) - (sig_pos * sig_neg) + (sig_neg * sig_neg)
second_bracket_neg = (2 * sig_pos * sig_neg * sig_neg) - (sig_neg * sig_pos * sig_neg) - (sig_pos * sig_neg * sig_neg)