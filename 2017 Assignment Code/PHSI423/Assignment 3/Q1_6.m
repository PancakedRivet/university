j = 10;

prob = zeros(100, 100, 6);
probPlotU = zeros(6,100);
probPlot = zeros(6,100);

for e_val = 1:100
    for u = 1:100
        
        e1 = e_val;
        e2 = 0;
        e3 = e_val;
        
        A = 1/j * [2*(e1+u), j*sqrt(2), 0, 0, 0, 0 ; j*sqrt(2), e1 + e2, j, j*sqrt(2), 0, 0 ; 0, j, e1 + e3, 0, j, 0 ; 0, j*sqrt(2), 0 2*(e2+u), j*sqrt(2), 0 ; 0, 0, j, j*sqrt(2), e2 + e3, j*sqrt(2) ; 0, 0, 0, 0, j*sqrt(2), 2*(e3 + u)];
        
        [V, lamda] = eig(A);
        prob(e_val,u,:) = V(:,1).^2;
        probPlotU(:,u) = V(:,1).^2;
        probPlot(:,e_val) = V(:,1).^2;
    end
end

% x = -50:50;
% y = -50:50;
% [X,Y] = meshgrid(x,y);

for x = 1:6
   figure(1)
   subplot(3,2,x)
   mesh(prob(:,:,x))
   title(['State ' num2str(x)])
   xlabel('\epsilon/J')
   ylabel('U/J')
   zlabel('Probability')
   
   figure(2)
   subplot(3,2,x)
   plot(probPlotU(x,:))
   title(['State ' num2str(x)])
   xlabel('U/J')
   ylabel('Probability')
   
   figure(3)
   subplot(3,2,x)
   plot(probPlot(x,:))
   title(['State ' num2str(x)])
   xlabel('\epsilon/J')
   ylabel('Probability')
end

figure(4)
subplot(1,2,1)
plot(probPlot(:,1))
title('Each state probability at \epsilon/J = 1')
subplot(1,2,2)
plot(probPlotU(:,1))
title('Each state probability at U/J = 1')