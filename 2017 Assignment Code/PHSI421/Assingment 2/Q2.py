# -*- coding: utf-8 -*-
"""
Created on Wed May 03 23:48:52 2017

@author: devpa499
"""
import numpy as np
import matplotlib.pyplot as plt

maxL = 500
temperature = np.arange(500)
parahydrogenValuesOfL = np.arange(0,maxL,2)
orthohydrogenValuesOfL = np.arange(1,maxL,2)
k_B = 1.38064852 * (10**(-23))
epsilon = 85.4 * k_B

def heatCapacity(statesOfL):
    C_V = np.zeros(len(temperature))
    for i in temperature[1:500]:
        C_V[i] = (k_B * (85.4)**2/temperature[i]**2) * ( (1/sumOfValues(1,statesOfL,temperature[i])) * (sumOfValues(2,statesOfL,temperature[i])) - ( (1/sumOfValues(1,statesOfL,temperature[i])) * sumOfValues(3,statesOfL,temperature[i]))**2)
    return C_V
    
def heatCapacityQ5(statesOfLOrtho, statesOfLPara):
    C_V = np.zeros(len(temperature))
    for i in temperature[1:500]:
        C_V[i] = (1/k_B * 1/(temperature[i])**2 * ( epsilon**2/( sumOfValues(1,statesOfLPara,temperature[i]) + 3 * sumOfValues(1,statesOfLOrtho,temperature[i]) ) * ( sumOfValues(2,statesOfLPara,temperature[i]) + 3 * sumOfValues(2,statesOfLOrtho,temperature[i]) ) - 1/( sumOfValues(1,statesOfLPara,temperature[i]) + 3 * sumOfValues(1,statesOfLOrtho,temperature[i]) )**2 * ( epsilon * sumOfValues(3,statesOfLPara,temperature[i]) + 3 * epsilon * sumOfValues(3,statesOfLOrtho,temperature[i]) )**2 ))
    return C_V

def sumOfValues(sumNum , statesOfL , temperature):
    totalResult = 0
    if sumNum == 1:
       for i in statesOfL:
           result = (2*i+1) * np.exp(-85.4 * i * (i + 1) / temperature)
           totalResult += result
    elif sumNum == 2:
        for i in statesOfL:
           result = (2*i+1) * (i*(i + 1))**2 * np.exp(-85.4 * i * (i + 1) / temperature)
           totalResult += result
    elif sumNum == 3:
        for i in statesOfL:
           result = (2*i+1) * i * (i + 1) * np.exp(-85.4 * i * (i + 1) / temperature)
           totalResult += result
    return totalResult

parahydrogen = heatCapacity(parahydrogenValuesOfL)
orthohydrogen = heatCapacity(orthohydrogenValuesOfL)

#Part b:
combined = 0.75 * heatCapacity(orthohydrogenValuesOfL) + 0.25 * heatCapacity(parahydrogenValuesOfL)

#Part d:
scale = 4.184 / 8.314
tempBrinkworth = [290,273, 252, 195, 155, 90]
C_V_Brinkworth = [4.879* scale, 4.844* scale, 4.729* scale, 4.488* scale, 4.142* scale, 3.301* scale]

tempGiacomini = [291, 273, 258, 234, 227, 224, 211, 208, 83]
C_V_Giacomini = [4.847* scale, 4.80* scale, 4.76* scale, 4.68* scale, 4.65* scale, 4.57* scale, 4.52* scale, 4.48* scale, 3.196* scale]

tempCornish = [372.52, 369.4, 333.31, 308.96, 294.27, 269.02, 238.23, 203.63, 182.41, 165.58, 145.64, 135.71, 81.12]
C_V_Cornish = [2.516, 2.504, 2.505, 2.486, 2.466, 2.444, 2.390, 2.298, 2.213, 2.147, 2.041, 1.967, 1.6015]

#Part e:
h2HeatCapacity = heatCapacityQ5(orthohydrogenValuesOfL, parahydrogenValuesOfL)

plt.figure(1)#Part b and c
paraLine, = plt.plot(temperature,parahydrogen,'b-',label = 'Parahydrogen')
orthoLine, = plt.plot(temperature,orthohydrogen,'r-',label = 'Orthohydrogen')
comboLine, = plt.plot(temperature,combined,'k-',label = '1:3 Ratio')
plt.legend(handles=[paraLine,orthoLine,comboLine])
plt.title('Part b: Heat Capacity for a single molecule')
plt.xlabel('Temperature (Kelvin)')
plt.ylabel('C_V / molecule')

plt.figure(2)#Part d
paraLine, = plt.plot(temperature,parahydrogen * 1/k_B,'b-',label = 'Parahydrogen')
orthoLine, = plt.plot(temperature,orthohydrogen*1/k_B,'r-',label = 'Orthohydrogen')
comboLine, = plt.plot(temperature,combined* 1/k_B,'k-',label = '1:3 Ratio')
brinkworthLine, = plt.plot(tempBrinkworth,C_V_Brinkworth,'yo-',label = 'Brinkworth')
giacominiLine, = plt.plot(tempGiacomini,C_V_Giacomini,'co-',label = 'Giacomini')
cornishLine, = plt.plot(tempCornish,C_V_Cornish,'go-',label = 'Cornish')
plt.legend(handles=[paraLine,orthoLine,comboLine,brinkworthLine,giacominiLine,cornishLine],bbox_to_anchor = (1.3,1.05))
plt.title('Part d: Heat Capacity')
plt.xlabel('Temperature (Kelvin)')
plt.ylabel('C_V / R')

tempBrinkworthAdjusted = [290,273, 252, 195, 155, 90]
C_V_BrinkworthAdjusted = [4.879* scale-1.5, 4.844* scale-1.5, 4.729* scale-1.5, 4.488* scale-1.5, 4.142* scale-1.5, 3.301* scale-1.5]

tempGiacominiAdjusted = [291, 273, 258, 234, 227, 224, 211, 208, 83]
C_V_GiacominiAdjusted = [4.847*scale-1.5, 4.80* scale-1.5, 4.76* scale-1.5, 4.68* scale-1.5, 4.65* scale-1.5, 4.57* scale-1.5, 4.52* scale-1.5, 4.48* scale-1.5, 3.196* scale-1.5]

tempCornishAdjusted = [372.52, 369.4, 333.31, 308.96, 294.27, 269.02, 238.23, 203.63, 182.41, 165.58, 145.64, 135.71, 81.12]
C_V_CornishAdjusted = [2.516-1.5, 2.504-1.5, 2.505-1.5, 2.486-1.5, 2.466-1.5, 2.444-1.5, 2.390-1.5, 2.298-1.5, 2.213-1.5, 2.147-1.5, 2.041-1.5, 1.967-1.5, 1.6015-1.5]

plt.figure(3)#Part d with adjustement
paraLineAdjusted, = plt.plot(temperature,parahydrogen * 1/k_B,'b-',label = 'Parahydrogen')
orthoLineAdjusted, = plt.plot(temperature,orthohydrogen * 1/k_B,'r-',label = 'Orthohydrogen')
comboLineAdjusted, = plt.plot(temperature,combined * 1/k_B,'k-',label = '1:3 Ratio')
brinkworthLineAdjusted, = plt.plot(tempBrinkworth,C_V_BrinkworthAdjusted,'yo-',label = 'Brinkworth')
giacominiLineAdjusted, = plt.plot(tempGiacomini,C_V_GiacominiAdjusted,'co-',label = 'Giacomini')
cornishLineAdjusted, = plt.plot(tempCornish,C_V_CornishAdjusted,'go-',label = 'Cornish')
plt.legend(handles=[paraLineAdjusted,orthoLineAdjusted,comboLineAdjusted,brinkworthLineAdjusted,giacominiLineAdjusted,cornishLineAdjusted],bbox_to_anchor = (1.3,1.05))
plt.title('Part d: Heat Capacity with adjustment')
plt.xlabel('Temperature (Kelvin)')
plt.ylabel('C_V / R')

plt.figure(4)#Part e
h2Line, = plt.plot(temperature,h2HeatCapacity * 1/k_B,'b-', label = 'H2 Heat Capacity')
comboLineQ5, = plt.plot(temperature,combined * 1/k_B,'r-',label = '1:3 Ratio')
plt.legend(handles=[h2Line,comboLineQ5])
plt.title('Part e: Heat Capacity of H2')
plt.xlabel('Temperature (Kelvin)')
plt.ylabel('C_V / R')

plt.show()