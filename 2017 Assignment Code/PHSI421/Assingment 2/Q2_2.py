# -*- coding: utf-8 -*-
"""
Created on Thu May 04 05:04:49 2017

@author: devpa499
"""
import numpy as np
import matplotlib.pyplot as plt
#Rewriting the C_V script:

#variables:
maxL = 200
k_B = 1.38064852 * 10**(-23)
temperature = np.arange(1,501)
epsilon = 85.4 * k_B
beta = 1/(k_B * temperature)

orthoSpin = np.arange(1,maxL,2)
paraSpin = np.arange(0,maxL,2)

def partitionFunctionGenerator(spin):
#    z_dash_dash = np.zeros(maxL/2)
#    z_dash = np.zeros(maxL/2)
#    z = np.zeros(maxL/2)
    z_dash_dash_total = 0
    z_dash_total = 0
    z_total = 0
    
    total = np.zeros(len(temperature))
    
    for l in spin:
        for k in temperature[1:499]:
            z_dash_dash = (2l+1)*(l*(l+1))**2 * np.exp(- beta[k]*l*(l+1)*epsilon)
            z_dash_dash_total += z_dash_dash
            
            z_dash = (2l+1)*(l*(l+1)) * np.exp(- beta[k]*l*(l+1)*epsilon)
            z_dash_total += z_dash
            
            z = (2l+1) * np.exp(- beta[k]*l*(l+1)*epsilon)
            z_total += z
            
            total[k] = k_B * beta[k]**2 * ((z_dash_dash_total * z_total - z_dash_total * z_dash_total) / z_total**2)
    return total

ortho = partitionFunctionGenerator(orthoSpin)
para = partitionFunctionGenerator(paraSpin)

plt.figure(1)
plt.plot(temperature,ortho,'b-')
plt.plot(temperature,para,'r-')
#plt.plot(temperature,combined,'k-')
plt.show()

#def z_dash_dash(temp):
#    z_dash_dash = np.zeros(maxL/2)
#    for 
#        z_dash_dash[l] = (2l+1)*(l(l+1))**2 * np.exp(- beta*l(l+1)*epsilon)
#    return z_dash_dash