function z = BEC(x)
%Rearrange to get zero on one side, use at least 500 values, get it to plot
%for one value of kT_hbarOmega

global kT_hbarOmega;

n = 0:500;

N = 2000;

z(1) = N - sum( (n+1) ./ (exp( (1/kT_hbarOmega).*(n - x) ) - 1) );
end