clear all %Clearing the Workspace variables
close all %Closing any still open figures
clc %Clearing the Command Window

set(0,'DefaultAxesFontSize',32,'DefaultTextFontSize',24);

%Global Variables
global kT_hbarOmega; %Declaring the value of kT_hbarOmega to be used in the function BEC

%% 1c)

kT_hbarOmega_List = [40, 36, 35, 34, 33, 32]; %The list of values for kT_hbarOmega

solutions = zeros(length(kT_hbarOmega_List),1); %Preallocating the solutions vector

energy_level = 0:500; %The number of energy levels to plot over

particles = zeros(length(solutions),length(energy_level)); %Preallocating the particles array

colours = linspace(1,0,length(kT_hbarOmega_List)); %Establishing a colour gradient for the lines

hold on
figure(1)
for i = 1:length(kT_hbarOmega_List)
    kT_hbarOmega = kT_hbarOmega_List(i); %Assigning the value of kT_hbarOmega
    fctn = @(x) BEC(x); %Calling the function
     solutions(i,1) = fzero(fctn,[-150,-0.0000001]); %Finding where the function is zero
    
    particles(i,:) = (energy_level + 1) ./ (exp((1/kT_hbarOmega) .* (energy_level - solutions(i,1) ) ) - 1 ); %Finding the number of particles in each energy level
    plot(particles(i,:),'Color',[colours(i) 0 0]); %Plotting the number of particles as a function of the energy level
end

%Plotting for 1c)
legend('\mu = 40','\mu = 36','\mu = 35','\mu = 34','\mu = 33','\mu = 32')
ylabel('Number of Particles')
xlabel('Energy Level')
title('1c) Particles in each energy level for a given \mu')
xlim([0,200])
hold off

%% 1d)

kT_hbarOmega_List = 1:100; %Declaring a range of temperatures to iterate over

critical_temp = int64(sqrt(6*2000)/pi); %Determining the value of the critical temperature from 1a)
comp_temps = 1:critical_temp-1; %Creating a vector of temperatures from 
comparrison = 2000 - (1/6) * (comp_temps .* pi).^2'; %Calculating the number of particles analytically
comparrison_pad = padarray(comparrison,66,'post')'; %PAdding the tmperatures above T_c with zeros

energy_level = 0; %Setting the energy level to the ground state

solutions = zeros(length(kT_hbarOmega_List),1); %Preallocating the particles array
% comparrison = zeros(length(comp_temps),1); %Preallocating the comparrison array

for i = 1:length(kT_hbarOmega_List)
    kT_hbarOmega = kT_hbarOmega_List(i); %Assigning the value of kT_hbarOmega
    fctn = @(x) BEC(x); %Calling the function
     solutions(i,1) = fzero(fctn,[-200,-0.0000001]); %Finding where the function is zero
    particles(i,:) = (energy_level + 1) ./ (exp((1/kT_hbarOmega) .* (energy_level - solutions(i,1) ) ) - 1 ); %Finding the number of particles in each energy level
end

%Plotting for 1d)
figure(2)
plot(particles,'k');
hold on
plot(comparrison_pad,'r--')
xlabel('Temperature ($$ k / $$ $$\hbar$$ $$\omega$$)','interpreter','latex')
ylabel('Number of Particles')
title('1d) Ground State Particle Number as a Function of Temperature')
legend('Numerical Solution','Analytical Solution')
xlim([0,70]);