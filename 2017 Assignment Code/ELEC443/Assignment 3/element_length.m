function element_lengths = element_length(data)
%Generating the Euclidian Norm (Length) of each vector in the boundary_values matrix:

[~, cols] = size(data); %Finding the length of the vector needed

element_lengths = zeros(1,cols); %Preallocating the vector

for x = 1:cols
    element_lengths(1,x) = sqrt( (data(3,x)-data(1,x))^2 + (data(4,x) - data(2,x))^2 ); %Populating the vector
end

end