function plot_normals(nvect, midpt)
%Plots normal vectors
[m, n] = size(nvect);
for k = 1:n;
    line([midpt(1, k), midpt(1, k) + nvect(1, k)], ...
        [midpt(2, k), midpt(2, k) + nvect(2, k)], 'Color', 'green');
end

end