function [start_points, end_points] = circle_points(segments)
%Generating the points for the semi-cirlce in region 2:

x = linspace(0.5,-0.5,segments); %Generating a range of x points 

y = -sqrt(0.5^2-(x).^2); %Generating the corresponding range of y points

start_points = zeros(length(x)-1,1); %Preallocating the array for start points 
end_points = zeros(length(x)-1,1); %Preallocating the array for end points

for k = 1:length(x)-1;
    
    start_points(k,1) = x(k); %Assigning the start x points
    start_points(k,2) = y(k); %Assigning the start y points

    end_points(k,1) = x(k+1); %Assigning the end x points
    end_points(k,2) = y(k+1); %Assigning the end y points
end

end