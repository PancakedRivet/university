function elements = elements(start_xy,end_xy,segments)
%Discretizing a line into a number of segments

m = linspace(0,1,segments+1); %Generating the number of segments for the line to be broken into
[rows, ~] = size(start_xy); %Finding the length of the array needed
vector = end_xy - start_xy; %Finding the length of the vector
c = 0; %Allocating a counter to monitor populating the segments correctly
elements = zeros(4,rows * segments); %Preallocating the array for the segments

for x = 1:rows
    
    x_vect = start_xy(x,1) + m * vector(x,1); %Generating the x componenets of the segment
    y_vect = start_xy(x,2) + m * vector(x,2); %Generating the y components of the segment
    
    for y = 1:length(m)-1
        
        elements(1,y+c) = x_vect(1,y); %Populating the array with the start x value
        elements(2,y+c) = y_vect(1,y); %Populating the array with the start y value
        elements(3,y+c) = x_vect(1,y+1); %Populating the array with the end x value
        elements(4,y+c) = y_vect(1,y+1); %Populating the array with the end y value
        
    end
    
    c = segments * x; %Increasing the counter such that allocation moves to the next line segment
    
end

end