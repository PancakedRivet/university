function [XI,YI,ZI] = plot_solutions(solutions,mesh_xy,points)
%Plots the solutions
x = mesh_xy(1,:); %Assigning the x vector of coordinates from the grid generated
y = mesh_xy(2,:); %Assigning the y vector of coordiantes from the grid generated
xi=linspace(min(x),max(x),points); %Regenerating the x components of the grid without the repetitions
yi=linspace(min(y),max(y),points); %Regenerating the y components of the grid without the repititions
[XI, YI]=meshgrid(xi,yi); %Making a meshgrid of the new coordinate vectors
ZI = griddata(x,y,solutions,XI,YI); %Alligning the new grid with the solutions
mesh(XI,YI,ZI); %mesh plotting the solutions over the new grid
xlim([-1.1,1.1]); %Limiting what's plotted to the region asked for in the assignment
ylim([-1.1,1.1]);

end