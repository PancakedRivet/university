function H = H_matrix_make(data,xi,lengths)
%Generating the H matrix
weights = [0.3478548451 0.6521451549 0.6521451549 0.3478548451]; %Vector of the weights for the Gaussian Quadrature

[~, cols] = size(data); %Determining the number of columns the H matrix should have
[~, rows] = size(xi); %Determing the number of rows the H matrix should have

H = zeros(rows,cols); %Preallocating the H matrix

for x = 1:cols
    for y = 1:rows
        
        gauss_points = [data(1,x) data(2,x) ; data(3,x) data(4,x) ; data(5,x) data(6,x) ; data(7,x) data(8,x)]; %Loading in the Gaussian Quadrature data for a segment
        
        H(y,x) = (lengths(x)/2) * ( ( weights(1) * h_x_xi(gauss_points(1,:),xi(:,y)) ) + ... %Calculating the first term in the Gaussian Quadrature sum
                                    ( weights(2) * h_x_xi(gauss_points(2,:),xi(:,y)) ) + ... %Calculating the second term in the Gaussian Quadrature sum
                                    ( weights(3) * h_x_xi(gauss_points(3,:),xi(:,y)) ) + ... %Calculating the third term in the Gaussian Quadrature sum
                                    ( weights(4) * h_x_xi(gauss_points(4,:),xi(:,y)) ) );    %Calculating the fourth term in the Gaussian Quadrature sum
        
        H(x,x) = ( lengths(1,x) / (2*pi) ) * (1 - log(0.5*lengths(1,x)) ); %Analytic solution for the diagonal elements
        
    end
end

end