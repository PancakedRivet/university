function K = K_matrix_make(data,normal,xi,lengths)
%Generating the K matrix

weights = [0.3478548451 0.6521451549 0.6521451549 0.3478548451]; %Vector of the weights for the Gaussian Quadrature

[~, cols] = size(data); %Derterming the number of columns in the K matrix
[~, rows] = size(xi); %Determing the number of rows in the K matrix

K = zeros(rows,cols); %Preallocating the K matrix

for x = 1:cols
    for y = 1:rows
        
        gauss_points = [data(1,x) data(2,x) ; data(3,x) data(4,x) ; data(5,x) data(6,x) ; data(7,x) data(8,x)]; %Loading the Gaussian Quadrature data for a segment
        
        K(y,x) = (lengths(x)/2) * ( ( weights(1) * dot(grad_h(gauss_points(1,:),xi(:,y)) , normal(:,x)') ) + ... %Calculating the first term in the Gaussian Quadrature sum
                                    ( weights(2) * dot(grad_h(gauss_points(2,:),xi(:,y)) , normal(:,x)') ) + ... %Calculating the second term in the Gaussian Quadrature sum
                                    ( weights(3) * dot(grad_h(gauss_points(3,:),xi(:,y)) , normal(:,x)') ) + ... %Calculating the third term in the Gaussian Quadrature sum
                                    ( weights(4) * dot(grad_h(gauss_points(4,:),xi(:,y)) , normal(:,x)') ) );    %Calculating the fourth term in the Gaussian Quadrature sum

        K(x,x) = 0; %Analytical solution for the diagonal terms
        
    end
end

end