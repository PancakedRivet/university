function plot_gauss(gpnts)
%Plots Gaussian Integration Points
[m, n] = size(gpnts);
for k = 1:n;
    line(gpnts([1, 3, 5, 7], k), gpnts([2, 4, 6, 8], k), 'Marker', 'x');
end
end