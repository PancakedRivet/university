function plot_bnd(elements)
%Plots elements
[m, n] = size(elements);
for k = 1:n;
    line(elements([1, 3], k), elements([2, 4], k));
    line(elements([1, 3], k), elements([2, 4], k), 'Marker', 'o');
end

end