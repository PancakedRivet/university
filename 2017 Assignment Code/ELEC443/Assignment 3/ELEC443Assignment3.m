%% Region 1, taking bottom left corner to be (0,0):

segments = 20; %The number of segments per boundary (recommended = 20)
internal_points = 50; %The number of points on each axis for the interior mesh (recommended = 50)

%Segment Generation: The 4 Outside Boundaries are specified first:
start_xy = [1 1 ; -1 1 ; -1 -1 ; 1 -1 ; 0 0.5 ; 0.5 0 ; 0 -0.5 ; -0.5 0]; %Beginning coordinates of the boundaries
end_xy =   [-1 1 ; -1 -1 ; 1 -1 ; 1 1 ; 0.5 0 ; 0 -0.5 ; -0.5 0 ; 0 0.5]; %Ending coordinates of the boundaries

boundary_values = elements(start_xy,end_xy,segments); %Generating the segments for each boundary

%Boundary Solutions: Outside = 0, Inside = 1:
u_solutions = boundary_solutions(boundary_values,segments); %Assigning the value of u on the boundary for each segment

%Element Lengths:
lengths = element_length(boundary_values); %Generating the length of each segment

%Element Normals:
element_normals = normals(boundary_values,lengths); %Generating the unit normal vector for each segment

%Element Midpoints:
element_midpoints = midpoints(boundary_values); %Generating the midpoint for each segment

%Gaussian Quadrature Creation:
gauss_quad_matrix = gauss_quad_create(boundary_values,element_midpoints); %Generating the Gaussian Quadrature points for each segment

%H Matrix Creation:
H_matrix = H_matrix_make(gauss_quad_matrix,element_midpoints,lengths); %Generating the H matrix for the boundary segments

%K Matrix Creation:
K_matrix = K_matrix_make(gauss_quad_matrix,element_normals,element_midpoints,lengths); %Generating the K matrix for the boundary segments

%Normal Derivatives:
normal_derivatives = (H_matrix \ ((K_matrix + 0.5 * eye(size(K_matrix))) * u_solutions'))'; %Solving for the normal derivatives

%Generating the mesh for solutions:
mesh_xy = generate_grid(internal_points); %Generating a grid of internal points to find a solution for

%Interior H Matrix Creation:
H_matrix_inside = H_matrix_make(gauss_quad_matrix,mesh_xy,lengths); %Generating the H matrix for the interior region points

%Interior K Matrix Creation:
K_matrix_inside = K_matrix_make(gauss_quad_matrix,element_normals,mesh_xy,lengths); %Generating the K matrix for the interior region points

%Solutions:
solutions = H_matrix_inside * normal_derivatives' - K_matrix_inside * u_solutions'; %Solving for the solutions within the region

%Plotting the solutions:
figure(1)
[XI, YI, ZI] = plot_solutions(solutions,mesh_xy,internal_points); %Mesh plotting the region with solutions
title('Region 1')
figure(4)
subplot(3,2,1)
plot_bnd(boundary_values); %Plotting the boundary value points
plot_normals(element_normals,element_midpoints); %Plotting the element normals at their respective midpoints
plot_gauss(gauss_quad_matrix); %Plotting the Gaussian Quadrature points
title('Region 1-Boundaries');
subplot(3,2,2);
pcolor(XI,YI,ZI); %pcolor plot of the region
xlim([-1,1]); %Limits to show the correct area of each region
ylim([-1,1]);
title('Region 1-pcolor');
%% Region 2:

%Creating the points for the circular part of boundary 2:
[start_circle, end_circle] = circle_points(segments);

%Segment Generation: The 4 Outside Boundaries are specified first:
start_xy = [1 1 ; -1 1 ; -1 -1 ; 1 -1 ; -0.5 0 ; 0 0.5 ; start_circle]; %Beginning coordinates of the boundaries
end_xy =   [-1 1 ; -1 -1 ; 1 -1 ; 1 1 ; 0 0.5 ; 0.5 0 ; end_circle]; %Ending coordinates of the boundaries

boundary_values = elements(start_xy,end_xy,segments); %Generating the segments for each boundary

%Boundary Solutions: Outside = 0, Inside = 1:
u_solutions = boundary_solutions(boundary_values,segments); %Assigning the value of u on the boundary for each segment

%Element Lengths:
lengths = element_length(boundary_values); %Generating the length of each segment

%Element Normals:
element_normals = normals(boundary_values,lengths); %Generating the unit normal vector for each segment

%Element Midpoints:
element_midpoints = midpoints(boundary_values); %Generating the midpoint for each segment

%Gaussian Quadrature Creation:
gauss_quad_matrix = gauss_quad_create(boundary_values,element_midpoints); %Generating the Gaussian Quadrature points for each segment

%H Matrix Creation:
H_matrix = H_matrix_make(gauss_quad_matrix,element_midpoints,lengths); %Generating the H matrix for the boundary segments

%K Matrix Creation:
K_matrix = K_matrix_make(gauss_quad_matrix,element_normals,element_midpoints,lengths); %Generating the K matrix for the boundary segments

%Normal Derivatives:
normal_derivatives = (H_matrix \ ((K_matrix + 0.5 * eye(size(K_matrix))) * u_solutions'))'; %Solving for the normal derivatives

%Generating the mesh for solutions:
mesh_xy = generate_grid(internal_points); %Generating a grid of internal points to find a solution for

%Interior H Matrix Creation:
H_matrix_inside = H_matrix_make(gauss_quad_matrix,mesh_xy,lengths); %Generating the H matrix for the interior region points

%Interior K Matrix Creation:
K_matrix_inside = K_matrix_make(gauss_quad_matrix,element_normals,mesh_xy,lengths); %Generating the K matrix for the interior region points

%Solutions:
solutions = H_matrix_inside * normal_derivatives' - K_matrix_inside * u_solutions'; %Solving for the solutions within the region

%Plotting the solutions:
figure(2) %Region 2 solutions
[XI, YI, ZI] = plot_solutions(solutions,mesh_xy,internal_points); %Mesh plotting the region with solutions
title('Region 2')
figure(4) %Combined figure of boundary points and pcolor
subplot(3,2,3)
plot_bnd(boundary_values); %Plotting the boundary value points
plot_normals(element_normals,element_midpoints); %Plotting the element normals at their respective midpoints
plot_gauss(gauss_quad_matrix); %Plotting the Gaussian Quadrature points
title('Region 2-Boundaries');
subplot(3,2,4);
pcolor(XI,YI,ZI); %pcolor plot of the region
xlim([-1,1]); %Limits to show the correct area of each region
ylim([-1,1]);
title('Region 2-pcolor');

%% Region 3:

%Segment Generation: The 4 Outside Boundaries are specified first:
start_xy = [1 1 ; -1 1 ; -1 -1 ; 1 -1 ; 0 0.5 ; sqrt(3)/4 -0.25 ; -sqrt(3)/4 -0.25]; %Beginning coordinates of the boundaries
end_xy =   [-1 1 ; -1 -1 ; 1 -1 ; 1 1 ; sqrt(3)/4 -0.25 ; -sqrt(3)/4 -0.25 ; 0 0.5]; %Ending coordinates of the boundaries

boundary_values = elements(start_xy,end_xy,segments); %Generating the segments for each boundary

%Boundary Solutions: Outside = 0, Inside = 1:
u_solutions = boundary_solutions(boundary_values,segments); %Assigning the value of u on the boundary for each segment

%Element Lengths:
lengths = element_length(boundary_values); %Generating the length of each segment

%Element Normals:
element_normals = normals(boundary_values,lengths); %Generating the unit normal vector for each segment

%Element Midpoints:
element_midpoints = midpoints(boundary_values); %Generating the midpoint for each segment

%Gaussian Quadrature Creation:
gauss_quad_matrix = gauss_quad_create(boundary_values,element_midpoints); %Generating the Gaussian Quadrature points for each segment

%H Matrix Creation:
H_matrix = H_matrix_make(gauss_quad_matrix,element_midpoints,lengths); %Generating the H matrix for the boundary segments

%K Matrix Creation:
K_matrix = K_matrix_make(gauss_quad_matrix,element_normals,element_midpoints,lengths); %Generating the K matrix for the boundary segments

%Normal Derivatives:
normal_derivatives = (H_matrix \ ((K_matrix + 0.5 * eye(size(K_matrix))) * u_solutions'))'; %Solving for the normal derivatives

%Generating the mesh for solutions:
mesh_xy = generate_grid(internal_points); %Generating a grid of internal points to find a solution for

%Interior H Matrix Creation:
H_matrix_inside = H_matrix_make(gauss_quad_matrix,mesh_xy,lengths); %Generating the H matrix for the interior region points

%Interior K Matrix Creation:
K_matrix_inside = K_matrix_make(gauss_quad_matrix,element_normals,mesh_xy,lengths); %Generating the K matrix for the interior region points

%Solutions:
solutions = H_matrix_inside * normal_derivatives' - K_matrix_inside * u_solutions'; %Solving for the solutions within the region

%Plotting the solutions:
figure(3) %Region 3 solutions
[XI, YI, ZI] = plot_solutions(solutions,mesh_xy,internal_points); %Mesh plotting the region with solutions
title('Region 3')
figure(4) %Combined figure of boundary points and pcolor
subplot(3,2,5)
plot_bnd(boundary_values); %Plotting the boundary value points
plot_normals(element_normals,element_midpoints); %Plotting the element normals at their respective midpoints
plot_gauss(gauss_quad_matrix); %Plotting the Gaussian Quadrature points
title('Region 1-Boundaries');
subplot(3,2,6);
pcolor(XI,YI,ZI); %pcolor plot of the region
xlim([-1,1]); %Limits to show the correct area of each region
ylim([-1,1]);
title('Region 1-pcolor');