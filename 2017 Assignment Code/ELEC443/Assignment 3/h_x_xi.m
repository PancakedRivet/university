function fundamental_solution = h_x_xi(x,xi)
%Calculating the value of the fundamental solution: (x data is a row vector, xi is a column vector)

fundamental_solution = (-1/(2*pi))*log( sqrt((x(1,1)-xi(1,1))^2 + (x(1,2)-xi(2,1))^2)); %Calculating the fundamental solution based on the inputs

end