function boundary_solutions = boundary_solutions(data,segments)
%Generating the solution for the points generated from elements.m,

[~, cols] = size(data); %Finding the length of the vector needed

boundary_solutions = ones(1,cols); %Assigning everything in the vector =1

boundary_solutions(1,1:4 * segments) = 0; %Assigning exterior boundaries =0

end