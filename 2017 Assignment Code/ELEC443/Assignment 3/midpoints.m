function midpoints = midpoints(data)
%Calculates the midpoints from a array of coordinates

[~, cols] = size(data); %Determining the size of the midpoints array

midpoints = zeros(2,cols); %Preallocating the midpoints array

for x = 1:cols
    
    midpoints(1,x) = (data(3,x) + data(1,x) ) /2; %Populates the array with the x component of the midpoint
    midpoints(2,x) = (data(4,x) + data(2,x) ) /2; %Populates the array with the y component of the midpoint
    
end

end