function gauss_quad = gauss_quad_create(data,midpoint_list)
%Generating the 4 Gaussian Quadrature points for each vector, making an 8xn matrix:

[~, cols] = size(data); %Finding the length of the array required

gauss_quad = zeros(8,cols); %Preallocating the array

m = [-0.8611363116 -0.3399810436 0.3399810436 0.8611363116]; %Vector of the points (zi)

for x = 1:cols

    x_vect = ((data(3,x) - midpoint_list(1,x)) * m) + midpoint_list(1,x); %Generating the x points for the vector
    y_vect = ((data(4,x) - midpoint_list(2,x)) * m) + midpoint_list(2,x); %Generating the y points for the vector
    
    gauss_quad(1,x) = x_vect(1,1); %Populating the x coordinate of the first Gaussian Quadrature point
    gauss_quad(2,x) = y_vect(1,1); %Populating the y coordinate of the first Gaussian Quadrature point
    gauss_quad(3,x) = x_vect(1,2); %Populating the x coordinate of the second Gaussian Quadrature point
    gauss_quad(4,x) = y_vect(1,2); %Populating the y coordinate of the second Gaussian Quadrature point
    gauss_quad(5,x) = x_vect(1,3); %Populating the x coordinate of the third Gaussian Quadrature point
    gauss_quad(6,x) = y_vect(1,3); %Populating the y coordinate of the third Gaussian Quadrature point
    gauss_quad(7,x) = x_vect(1,4); %Populating the x coordinate of the fourth Gaussian Quadrature point
    gauss_quad(8,x) = y_vect(1,4); %Populating the y coordinate of the fourth Gaussian Quadrature point
    
end

end