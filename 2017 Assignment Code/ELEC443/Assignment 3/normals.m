function normals = normals(data,lengths)
%Calculates the unit normal vectors to the boundary elements: (Points to the right of the vector)

[~, cols] = size(data); %Determining the size of the normals array

temp_data = zeros(2,cols); %Preallocating the termporary data array
normals = zeros(2,cols); %Prealocating the normals array

for x = 1:cols
    temp_data(1,x) = data(3,x) - data(1,x); %Temporarily getting the x length of a segment
    temp_data(2,x) = data(4,x) - data(2,x); %Temporarily getting the y length of a segment
    
    normals(1,x) = temp_data(2,x) / lengths(1,x); %Generating the x component of the unit normals array
    normals(2,x) = -temp_data(1,x) / lengths(1,x); %Genearting the y component of the unit normals array
end

end