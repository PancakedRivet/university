function mesh_xy = generate_grid(data)
%Generating the grid to plot the solution on

list_x = linspace(-1.3,1.3,data); %Genearting the range of x values to plot the solution on
list_y = linspace(-1.1,1.1,data); %Generating the range of y values to plot the solution on

amount = length(list_x); %Determining the number of repitions required to encompass every point on the grid

list_x_total = repelem(list_x,amount); %Replicating each element to cover the entire grid
list_y_total = repmat(list_y,1,amount); %Replicating the vector to cover the entire grid

mesh_xy = [list_x_total ; list_y_total]; %Concatenating the x and y grid elements

end