function grad_vector = grad_h(x,xi)
%The value of grad(x) = h( x|xi ): (x data is a row vector, xi data is a column vector)

grad_vector = zeros(1,2); %Preallocating the vector

grad_vector(1,1) = (-1/(2*pi)) * ((x(1,1)-xi(1,1)) / ((x(1,1)-xi(1,1))^2 + (x(1,2)-xi(2,1))^2) ) ; %Populating the x value of the gradient
grad_vector(1,2) = (-1/(2*pi)) * ((x(1,2)-xi(2,1)) / ((x(1,1)-xi(1,1))^2 + (x(1,2)-xi(2,1))^2) ) ; %Populating the y value of the gradient

end