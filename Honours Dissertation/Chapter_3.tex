\chapter{Experimental Discussion}

\section{Set Up}
The resonator (with extraordinary and ordinary refractive indexes $n_e = 2.21, n_o = 2.30$) is mounted to the top of a brass rod (see figure \ref{fig:wgmr}). This is done to ensure that the resonator is in a stable, accessible location that is stabilised with a screw into its base. We couple the \SI{1550}{\nano\meter} laser into the WGMR using frustrated TIR \cite{strekalov_nonlinear_2016} (as discussed in section \ref{WGMRCoupling}) using the coupling prism ($n = 2.3878$) for coupling in with. Two sidebands of $\SI{100}{\mega\Hz}$ are modulated onto the laser light using the signal generator. The internal piezo of the \SI{1550}{\nano\meter} is switched on to begin sweeping the frequency of the light the controller is outputting. We then measure if the lithium niobate substrate ($n_e = 2.21, n_o = 2.30$) can cause a change in the central frequency of the resonance. Moving it away from the WGMR is expected to change the frequency of the modes which we would see from the detector data. The measurement is done using data collected from the detector set up with the focusing lens. Together they are used to measure the intensity of the light that is coupled out of the resonator. The breakout box is used to connect the computer with the detector and the nanocube controller via BNC cables. As we know the frequency of the sidebands modulated onto the signal, we will be able to calculate the change in the line width using equation \ref{eq:Q} and the change in frequency of the resonance using equation \ref{eq:ScaleFactor}. The direction of this change will define if it is a red or blue shift. After the calculations are made the experiment is repeated with a different substrate. A plot of the results comparable to figure \ref{fig:foreman} will allow for an easy verification of the expected shift.

\begin{figure}[h!]
	\centering
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/setupschematic1.png}
		\caption{The light coupled into the resonator through the prism undergoes total internal reflection as it moves around the resonator.\newline}
		\label{fig:setupschem1}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[height=2.2in,width=\textwidth,keepaspectratio]{./Figures/setupschematic2.png}
		\caption{The substrate is now moved into proximity of the WGMR. This has caused a frequency shift.\newline}
		\label{fig:setupschem2}
	\end{subfigure}
	\caption{A schematic showing the experiment method. Notice that the presence of the substrate in (b) has caused a change in the points where the light wave undergoes total internal reflection. This will change what frequencies in the resonator constructively interfere, meaning the resonant frequencies have now changed. This is indicated by the green line (diagram not to scale).}
	\label{fig:setupschem}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[height=4in]{./figures/pigtail.png}
	\caption{Image of the components that control the laser coupling to free air interaction.\newline}
	\label{fig:pigtail}
\end{figure}

\section{Nanocube Discussion}
The accuracy in measuring the substrate position is crucial to determining how much the substrate perturbs the evanescent field. To maximise the accuracy of these measurements we need a very precise system to control the position of the substrate. Piezos are popular for taking positional measurements at the micron level. We are using a Physics Instruments 3 Dimensional Nanocube Controller (see figure \ref{fig:nanocubecontroller}) and nanocube for this task. This apparatus is capable of translating by up to \SI{100}{\micro\meter} in each dimension. There is an internal piezo crystal in each dimension of the nanocube. The piezos are designed to expand and contract in response to a voltage being passed through them, with the magnitude of the voltage controlling the amount of the length change. As this is a physical change in the crystal to change the position, there can be a hysteresis problem, where the amounts of expansion and contraction are not always equal. There are servos present in the nanocube to reduce the effects of hysteresis as well as dampen fluctuations in the voltage. This keeps the position stable to a tenth of a micron. Initial testing of the nanocube controller deemed the y-axis the only appropriate axis for the direction of the substrate movement. The z-axis was unusable as it moves the substrate perpendicular to the table instead of towards or away from the WGMR. The x-axis was not able to remain in a stable position regardless of the servos being active, with the output displaying wildly fluctuating voltages and positions.

Each axis of the nanocube controller has two BNC ports associated with it. One is an input to modify the voltage of the axis within the nanocube and the other is an output of the voltage for connecting to an external measurement device. We utilise both ports when we automate the data collection process (discussed below in section \ref{Automation}). We also tested the calibration of the nanocube to ensure that the values it displayed were accurate. This process is described in section \ref{nanocalibration}.

The entire nanocube is mounted on top of a one dimensional translator slide. This is done such that the substrate can be screwed in to the top of the attocube without danger of coming into premature contact with the resonator. Everything is attached and lined up before the entire slide is moved into an initial proximity to the resonator. The remaining distance is covered by the attocube (discussed below in section \ref{atto}).

\begin{figure}[h!]
	\includegraphics[width=\linewidth]{./figures/nanocube.png}
	\caption{Front panel of the nanocube controller. The position of the y-axis is currently at \SI{0}{\micro\meter} and the servo controlling the y-axis is currently switched on.\newline}
	\label{fig:nanocubecontroller}
\end{figure}

\section{Nanocube Calibration} \label{nanocalibration}
Accuracy was ensured between the position of the nanocube and the digital readout of the nanocube controller by building a Michelson Interferometer. The interferometer was built during a summer studentship. The movement of the nanocube was matched with the change in voltage read by the detector (see figure \ref{fig:michel}). We used a sine fit to determine the relationship between the position and voltage. The results of this relationship were used to check if the readout of the position from the nanocube controller were consistent with the voltage we expected from calculations. We found that the relationship is approximately \SI{1}{\volt} to \SI{10}{\micro\meter}. This is consistent with the display of the nanocube controller. While the input of the nanocube controller accepts volts, it automatically converts voltage to position by the internal components of the nanocube controller, resulting in any input being converted into an output of position. This simplifies the calculations needed to make for how the nanocube behaves. This also means that we have an effective range of \SI{0}{\volt}-\SI{10}{\volt} which corresponds to a positional change of \SI{0}{\micro\meter}-\SI{100}{\micro\meter}. This is the maximum range of the breakout box however the offset knob on the front panel of the nanocube controller could be used to increase this range. We decided not to utiise this for two reasons. The first reason is that we already have an automation process in place that manual offset tuning would detract from the efficiency of. The second reason is that we don't believe the extra range will give any extra insight into the frequency shift. The evanescent field decays exponentially so the extra range is deemed unnecessary.

\begin{figure}[h!]
	\includegraphics[height=3in]{./figures/calibration.png}
	\caption{Calibration results with a sine curve fit.}
	\label{fig:michel}
\end{figure}

\section{Attocube Discussion} \label{atto}

While the nanocube has proven to be excellent at determining where it is in each direction, there is no feedback system to determine its position relative to the WGMR. This is a problem because we want the substrates to be close enough to the resonator to perturb the field. We also need a reference point in space so that we can accurately compare the frequency shifts of each substrate. This is to ensure that the differences in the frequency shifts are due to the refractive indexes of the substrates and not the distances from the resonator of each one.

In order to facilitate finding a distance between the resonator and substrate, we employ the second attocube. It is identical to the first one which is used in coupling the light into the resonator. Because both attocubes are identical, we can use the same frequency feedback system that is used when moving the coupling prism into contact with the resonator. The second attocube is mounted on top of the nanocube (see figure \ref{fig:nanoatto}). We then move the substrate into contact with the resonator and listen for the change in frequency which indicates that it cannot move any further in that direction.

Once the substrate is in position touching the resonator we can use the nanocube display to determine how far away from this point we move over the experiment. This allows us to build plots of the frequency shift that have the same distance axis and therefore make them more comparable to one another.

\begin{figure}[h!]
	\centering
	\includegraphics[height=3in]{./figures/nanocubeattocube.png}
	\caption{The nanocube with the second attocube mounted on top of it. The rutile substrate is currently mounted on the attocube and is in close proximity to the resonator.}
	\label{fig:nanoatto}
\end{figure}

\section{WGMR Coupling Process} \label{ExpCouple}
We want the light entering the coupling prism to totally internally reflect {figure \ref{fig:tir}(c)) so we solve equation \ref{eq:critical} for diamond ($n = 2.3878$)\cite{phillip_kramers-kronig_1964} and find that the angles necessary to couple into the modes are \ang{21.3} for TE and \ang{10.6} for TM. The difference in these angles comes from how we treat the refractive index of the lithium niobate resonator for reasons discussed in section \ref{Polarisation}. In order to determine the angle of incidence of the laser we use a protractor and marker to sketch a crude angle guide onto the optical table (see figure \ref{fig:angles}). We use this to line up the translator platform such that we couple light into the resonator at the correct angle, thereby using the correct polarisation of light.

\begin{figure}[h!]
	\centering
	\includegraphics[height=3in]{./figures/angles.png}
	\caption{The angle lines marked onto the table. The translator platform is lined up with the angle required for TE mode coupling.}
	\label{fig:angles}
\end{figure}

Once the angle of incidence of the laser is correct we use the piezo controller and one of the attocubes. The attocube contains a piezo which translates using a slip stick system, meaning that it does a step at a several kHz. This frequency of the step can be modified from the piezo controller and so provides an aural feedback mechanism while it moves. We use this to move the prism as close as possible to the WGMR. When the prism is unable to move further the amount the piezo steps is different so the frequency changes. With the prism as close as possible to the WGMR, we must locate the very small coupling spot. In order to find this, we use a laser that has a wavelength in the visible spectrum such as \SI{632}{\nano\meter}. We then adjust the aperture of the output light from the laser to expand the size of the beam coupling into the WGMR. With the larger beam we can look for Newton's rings on a screen. Newton's rings are circular interference patterns caused by the reflection of light between a curved surface and a straight surface. When the prism is moved away from the WGMR the Newton's rings will disappear, as the straight surface will move away and no longer cause interference. This signifies where the WGMR coupling spot is located. Circular interference patterns can also be caused by dirt, dust or manufacturing imperfections in the resonator but they will not disappear when the prism is moved. Trial and error is the best method for finding the coupling point. Once the correct spot is located, the beam is continuously re-centered and focused onto a point. The photo detector is placed such that the beam is focused on the photosensitive part of it to record as much information from the incoming light as possible. As the beam was still larger than this photosensitive spot in the detector we employed a focusing lens to ensure we capture as much information as possible. 

We revert to the \SI{1550}{\nano\meter} laser and slowly adjust the focus of the beam until clear resonances are seen in the output of the detector. The output is refined using fine adjustment knobs on the large translator platform to maximise the coupling of the resonator. Often the coupling spot over couples light into the resonator so the prism distance is also adjusted to find the critical coupling point instead.

\section{Frequency Shift Direction}

While we can use the data to see \emph{if} there is a shift in the frequency of a resonant mode, there is no way to tell from the data alone if the shift is in the correct direction or not. To determine the direction of any frequency shift we find, we use the wavelength meter. First we follow the same steps as above in section \ref{ExpCouple} to couple light into the resonator and adjust it such that the coupling contrast is maximised, the mode line width is minimised and the coupling prism is in a critical coupling spot. Next we identify a suitable mode (high contrast and narrow line width) to track. We then use the DC offset knob on the laser controller to manually change the position of this mode on the oscilloscope screen. We determined that turning the knob clockwise resulted in a shift of the mode to the right. We then attenuated the signal to a very low amplitude as it is easy to overload the wavelength meter. We then plug the laser fiber into the wavelength meter instead of the experiment and measure the wavelength. We note this value and then turn the DC offset knob clockwise again. The result is that a clockwise turn of the DC offset knob reduces the wavelength. From the basic wave equation

\begin{equation} \label{eq:c}
\mathrm{c} = f \cdot \lambda,
\end{equation}

we know that a reduction in wavelength must be compensated by an increase in frequency. Because the DC offset is applied equally, there is the same frequency distribution on the oscilloscope screen. The frequency of the mode has not changed because we only adjusted the offset and not the physical position of anything that would change it. This means the area to the left of the mode must have higher frequency values to ensure equation \ref{eq:c} remains satisfied. Therefore when the DC offset knob is turned clockwise, we are seeing a \emph{decrease} in frequency. This is vital information because without it we would not be able to conclude which direction the frequency shift is in.

\section{Automation} \label{Automation}
The experiment is testing for a shift in the frequency of a mode when a substrate is in close proximity of the WGMR. We use the detector to observe these frequency shifts because the movement of the substrate will change what frequencies remain in the resonator. If we collected this data manually we would need to change the position of the substrate and save the frequency trace measured by the detector. This process would need to be repeated enough times to build up a range of substrate positions and analysis of the data would be needed to highlight any observable shift in the frequency. This would be very slow and likely very inaccurate. Ideally this process would be done quickly to avoid environmental changes in pressure or temperature that might affect the results. To most efficiently collect data we automated the procedure. There are two parts to the automation of this experiment. Part 1 is the automation of data collection, this is performed through a python script. Part 2 is the manipulation and analysis of the data, this is performed through a MATLAB script. Both parts are discussed in detail below.

\begin{figure}[h!]
	\centering
	\includegraphics[height=3in]{./figures/breakout.png}
	\caption{The breakout box with BNC cables attaching the nanocube controller and detector with the computer.}
	\label{fig:breakout}
\end{figure}

\subsection{Python}

We automated the data collection process by utilising the BNC ports on the nanocube controller and the breakout box (figure \ref{fig:breakout}) to work with a modified python script originally written by Alfredo Rueda. The program creates a GUI that shows the signals from the trigger and the detector while also allowing the user to input the position that the nanocube controller will change to. The program will decrement the voltage it writes to the nanocube until it reaches a user-specified level, saving at a user-specified number of volts. This allows the user to get both low resolution sweeps to see if a trend exists and high resolution sweeps if a trend is identified and needs to be investigated at a higher resolution. An advantage of using the Physics Instruments Nanocube Controller is that while it is the voltage that is changed within the python script, the internal systems of the nanocube controller handle the conversion from voltage to position. This simplifies the maths required to convert the voltages used as input to the positions used as outputs as this is not a step we control in the experiment.

There are ways that would allow us to observe the voltage being written to the nanocube on a separate oscilloscope but the dangers of cross talk between ports and discontinuous voltage writing are risks that outweigh the potential benefits. We utilised T-bars on BNC ports so that we could check the GUI and the oscilloscope showed matching voltage values. We did this to ensure that the program was still functioning as required when changes were made to it. We also noticed that there would be spikes and dips in an otherwise stable signal, which did not occur when the T-bars were removed. This was attributed to the cross talk and the decision made to remove these when the experiment would be properly run with the substrates. We found a compromise in writing a process that that averages the voltage being written to the nanocube using a different port than that leading to the nanocube controller. This was an acceptable compromise between observing the writing process and not putting the signal at risk because we could confirm the voltage matched what we expected as the experiment was being conducted, without the associated risks of cross talk.

The saving process written into the python script enables us to determine at how many steps the program would save. This would call a process at each of the correct steps that would save the voltage values received at a given point in time at the detector. This information was encoded into .dat file and placed into the directory of the python script along with the voltage that it was saved at and the date and time of the computer when it was saved.

\subsection{MATLAB}

When running the experiments, we found that low resolution sweeps produced 10 data files, and higher resolution sweeps could produce from 100 to 500 data files. We then built a MATLAB script that would allow a user to read the .dat files in the directory of the script and analyse them. We used two different computers, one for the python script and one for the MATLAB script so the data had to be transferred between them however this could easily be done by a single computer and a single directory.

When the data is loaded into the program, it manipulates the data. The user is required to identify the start and end bound on the mode and associated sidebands so that the script knows what to process. This step was not automated as it was deemed easier to leave it to the user to enter two values than to attempt to automate this section. When a range of values for the mode to be studied are correctly selected the program will use the min() function to identify the minimum value within this range. This corresponds to the minimum of the central resonance. This value is then saved in a vector. While this step could be done more accurately using a Lorentz function fit, using the min() function is much easier to automate the process with and the accuracy disparity is very small because of the line width of modes we are using. The program will then identify the minimum to the left and right of this central minimum with a grace period included so that the program doesn't immediately. These values correspond to the modulated sidebands which we know are a certain frequency from the central minimum. The script will then calculate a colour gradient based on the number of data files it loads. The colour gradient allows one to see the general trend of the shift if one is present. We then manipulate the data to generate a frequency axis instead of a sample number axis. We can calculate the frequency

\begin{equation} \label{eq:ScaleFactor}
\mathrm{Hz} = \frac{2 \cdot f_\mathrm{{sideband}}}{d_\mathrm{{sideband}}}.
\end{equation}

Where $f_{sideband} = \SI{100}{\mega\hertz}$ (the sideband modulation frequency) and

\begin{equation} \label{eq:ScaleFactord}
d_\mathrm{sideband} = n_\mathrm{Right} - n_\mathrm{Left}.
\end{equation}

We use this relationship to make an axis for plotting. The zero point of this axis only corresponds to the data file that was taken at the furthest distance from the resonator (\SI{100}{\micro\meter}). The advantage to using this as the zero point is that all of the sweeps will contain data for the frequency at the furthest distance from the resonator. This also saves having to manually account for the collision of the resonator with the substrate which if taken as the zero point can lead to inaccurate frequency axes. An inaccurate frequency axis makes it extremely difficult to conclude how much of a shift has taken place especially if there is no discernible mode or associated sidebands seen in the data files taken close to the resonator. From experience we found that typically the modes and associated sidebands that were being tracked by the MATLAB script would disappear from data files saved at \SI{0}{\micro\meter} up to \SI{5}{\micro\meter} which is likely due to the collision of the resonator with the substrate, causing a change in the resonator structure and changing the coupling conditions, removing the mode in the process.

There is a difficulty in reading the shift from the colour gradient alone. This difficulty is compounded when the sample size is increased to the aforementioned 100 or 500 samples because the colour gradient increment is not enough to distinguish between successive samples. The results of the script are plotted in 3 figures:

\begin{itemize}
	\item A plot of the raw data.
	\item A plot of the user identified mode and associated sidebands.
	\item A plot of the frequency shift itself.
\end{itemize}

The third plot is a pcolour plot that is useful in visualizing the frequency shift underneath the colour gradient. This plot plots each data file at a location on the y-axis corresponding to the position it was saved at. This means the shift in frequency of the resonance over the \SI{100}{\micro\meter} distance can be seen at every point. The darker the colour on the plot the less light was received at the detector. This corresponds to the frequencies that are trapped in the resonator. The x-axis for this plot is the same frequency axis as was generated earlier, making it easy to see how the central frequency moves relative to the zero-point (\SI{100}{\micro\meter}). The script then saves these figures with various extensions in the same directory as the data and script. To avoid having to copy large amounts of data each time the script was run, we found it easier to paste a copy of the script in each data folder. This meant we could tailor the graphs to artificially increase the resolution of the pcolour plot. This stems from knowing the sideband frequency, meaning regardless of how many points on the plot the sidebands have between them they are always \SI{100}{\mega\hertz} to either side of the mode. Thus if we set bounds on the plot to move these sidebands to the left and right edges of the plot, we maximise the number of points between them. This artificially increases the resolution of the plot with no extra data and minimal extra effort on the part of the user. There is a limit to how much the resolution can increase, but \SI{100}{\mega\hertz} sidebands were found to work well and not exceed this limit. An exceeded limit happens when the number of data points between the sidebands is less than the number of points that get plotted between the sidebands. This causes the computer to repeat values and makes the plot look pixelated instead of smooth.

\section{Substrate Mounting Orientation} \label{substratemount}

Due to the spherical symmetry of the resonator, it doesn't matter where the substrates make contact with the resonator. This means it does not matter at what angle the substrates are mounted in the x-y plane. It does matter at what angle the substrates are mounted in the y-z plane however. Because we are using wax to mount the substrates, it can be difficult to gage the angle in the y-z plane. When the substrate is brought into proximity with the resonator, we can use the attocube frequency feedback to ensure that the substrate is touching the resonator, but we cannot know if the interaction surface between the resonator and the substrate is perpendicular. Ideally the substrates and the resonator would be mounted in the y-z plane at \ang{0}. As these angles are only controlled by eye during the substrate mounting process, there can be a few degrees of rotation present. We look at the substrates under the microscope after they are mounted, and if there is anything more than a few degrees of elevation the substrate is removed and the mounting process restarted. Despite there only being a few degrees of elevation, it is useful to know what effect this can have on our results. Using trigonometry, we can derive a relationship between the angle and the distance for the resonator-substrate interaction. We start with the right angled triangle with sides $r$, $r + \Delta y$ and the substrate edge (see figure \ref{fig:delx2}). 

\begin{equation}
\cos \alpha = \frac{\mathrm{Adjacent \ Side}}{\mathrm{Hypotenuse}},
\end{equation}

\begin{equation}
\cos \alpha = \frac{r}{\Delta y + r},
\end{equation}

\begin{equation} \label{eq:delx}
\Delta y = \frac{r}{\cos \alpha} - r.
\end{equation}

Where $\Delta y$ is the distance between the perpendicular surface of the resonator and the substrate, r is the minor radius of the resonator and $\alpha$ is the angle of elevation to the touching edge of the substrate. $\Delta y$ is chosen as the displacement amount to keep consistent with the direction of translation for the nanocube axis. The optimum and non-optimum cases are demonstrated in figure \ref{fig:delx}.

\begin{figure}[h!]
	\centering
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/delx2.png}
		\caption{The optimum mounting case.\\ Here $\alpha$ = \ang{0} and $\Delta y$ = $0$.\newline}
		\label{fig:delx1}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/delx1.png}
		\caption{The non-optimum case. Here there is some angle $\alpha$ which results in a distance between the substrate and the resonator $\Delta y$.\newline}
		\label{fig:delx2}
	\end{subfigure}
	\caption{2 cases that demonstrate how some angle $\alpha$ can cause a gap between the resonator and the substrate.}
	\label{fig:delx}
\end{figure}

To investigate what this means for the distance that we measure, we approximated that the minor radius of the resonator is \SI{1}{\milli\meter} and generated a range of angles between \ang{-90} and \ang{90} to determine how large the resulting displacement would be. The results can be seen in figure \ref{fig:dispangle}. What can be seen is that even for small angles there can be a significant displacement relative to the distance the nanocube moves. From figure \ref{fig:dispangle} we see that an angle of elevation of \ang{10} results in a displacement of \SI{15}{\micro\meter} and an angle of elevation of \ang{5} results in a displacement of \SI{4}{\micro\meter}. As the total range of motion for the nanocube is up to \SI{100}{\micro\meter} we need to keep this angle to a minimum or it can significantly affect results.

\begin{figure}[h!]
	\centering
	\includegraphics[height=2.5in]{./figures/displacementangle2.png}
	\caption{This shows how the distance increases between the substrate and the resonator when not mounted at \ang{0}.}
	\label{fig:dispangle}
\end{figure}

\section{Environmental Control} \label{Tempcontrol}

We automise the process to speed up data collection in the interest of minimising environmental variation. The maximum speed of data collection over \SI{100}{\micro\meter} is approximately 60 seconds. This is long enough that despite automation environmental variation may impact the results. We conducted a series of control tests to determine how the environmental factors can change over a single trial. The results of one of these control tests is presented below in figure \ref{fig:environcontrol}.

\begin{figure}[h!]
	\centering
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[height=2.5in]{./figures/nobox.png}
		\caption{Plot of the fluctuations of the mode without a moving substrate or cardboard box.}
		\label{fig:environcontrol1}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\textwidth}
		\includegraphics[height=2.5in]{./figures/LiNbO3/box1.png}
		\caption{The same mode with a cardboard box without a moving substrate.}
		\label{fig:environcontrol2}
	\end{subfigure}
	\caption{This shows how the inclusion of a cardboard box can dampen the fluctuations in the environment. The remaining shift present in (b) is discussed through section \ref{Results}.}
	\label{fig:environcontrol}
\end{figure}

This involved setting up the experiment without a substrate. Because there is no substrate moving the expectation is that there will be no change in the perturbation of the field and therefore there should be no shift in the frequency of the resonance. We run this experiment twice to see how the frequency changes. Despite no substrate moving the length of the sweep is identical to a sweep measured when a substrate is moving. Once the results of the controls are measured, we place a cardboard box around the experiment (see figure \ref{fig:box}). The cardboard box isolates the resonator and substrate from air currents and heat radiated from equipment or people nearby. We wait \SI{120}{sec} for the environmental factors like pressure and temperature to reach an equilibrium and then repeat the process to gather controls with the box on (see figure \ref{fig:environcontrol2}). Establishing an environmental equilibrium results in a more constant frequency position. Due to the more stable environment of the experiment, the results of the tested substrates will be more attributable to the perturbation of the evanescent field. This is valuable because without a stable background the frequency shift could be caused by background fluctuations instead of the substrates.

\begin{figure}[h!]
	\centering
	\includegraphics[height=2.5in]{./figures/box.png}
	\caption{The cardboard box around the experiment. There is a hole cut into the back to let the light through for the detector to receive.}
	\label{fig:box}
\end{figure}

\section{Polarisation Discussion} \label{Polarisation}

The difference in the refractive index of lithium niobate between $n_e = 2.21, n_o = 2.30$ shows the birefringence of the resonator. The optical axis is in the z-direction (with the resonator mounted in the x-y-plane). This is important to know as the polarisation of the light entering the resonator will determine what modes are seen on the detector. We can set the polarisation of the light entering the resonator through the fiber with the 3 polarising plates. We place the beam splitter in the path of the light before it enters the detector and this filters out light that is polarised transverse to the beam splitter. (see figure \ref{fig:detectorb}) The beam splitter being made of two different refractive index materials stuck together acts as a filter between the TM and TE modes. If the beam splitter is mounted such that it filters out the TE modes, the polarising plates can be adjusted to nullify the signal at the detector as much as possible. This minimum signal is the point where as much of the TM modes have been filtered out as possible. We mount the beam splitter on the detector as shown in figure \ref{fig:detector}. This is critical for the experiment as the figure \ref{fig:foreman} shows that TE modes and TM modes affect the frequency by different amounts so we need certainty in which polarisation we are testing.

\begin{figure}[h!]
	\centering
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/detectora.png}
		\caption{The detector with all apparatus attached.\newline}
		\label{fig:detectora}
	\end{subfigure}
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/detectorb.png}
		\caption{The detector with the beam splitter.\newline}
		\label{fig:detectorb}
	\end{subfigure}
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/detectorc.png}
		\caption{The detector with all apparatus removed.\newline}
		\label{fig:detectorc}
	\end{subfigure}
	\caption{The detector shown in stages of attachment. Note the orientation of the beam splitter on the front of the detector. This shows that the detector is currently only detecting TE polarised light.}
	\label{fig:detector}
\end{figure}

One of the tested substrates is lithium niobate. The same birefringent properties are present in the substrate also. Depending on how the optical axis is mounted on the testing slide will determine which refractive index it has for the purposes of testing. The lithium niobate substrate is mounted with the optical axis along the x-axis so the refractive index is $n_o$ for the purposes of this experiment (see figure \ref{fig:lithnioboptical}).

\begin{figure}[h!]
	\centering
	\includegraphics[height=2in]{./figures/lithiumniobateoptical.png}
	\caption{Lithium niobate substrate with the optical axis highlighted. It is mounted upside down on the attocube so the z axis is flipped here.}
	\label{fig:lithnioboptical}
\end{figure}

The lithium niobate substrate with a transverse oriented optical axis can exhibit out-coupled light. This can occur because the light will encounter the refractive index $n_e$ in the resonator and the refractive index $n_o$ in the substrate. This criteria fulfills the requirement for frustrated total internal reflection to occur as was discussed in section \ref{WGMRCoupling}. This results in light that couples out of the resonator and can result in a broadening of the modes seen at the detector.

\section{Data Collection Method} \label{datacollection}

Once the substrates are mounted correctly on the attocube, we move the nanocube into position and check by eye that the height of the substrate will not impact the brass rod first. This is one of the advantages to having the resonator mounted on a tapered rod (as was discussed in section \ref{WGMRfabrication}). The nanocube is then brought into close proximity with the resonator using the $1$D translator mounted beneath the nanocube. The piezo controller is then used with a step of \SI{11.0}{\volt} and a frequency of \SI{136}{\hertz} to move the substrate the remaining distance into contact with the resonator. We know that the resonator has reached this position when the frequency generated while moving changes from \SI{136}{\hertz} to a higher frequency, indicating that the piezo can no longer continue translating in that direction.

Once we are certain that the substrate is touching the resonator we open the python script and move the nanocube to the maximum possible displacement from the resonator. This moves the nanocube a distance of \SI{100}{\micro\meter}. The advantage of using the maximum displacement from the resonator as the starting point is that it allows us to find a mode that we can track for the duration of the sweep. When the resonator is in contact with the substrate it can be hard to notice a mode. By finding a mode beforehand it allows us to track the frequency shift and also means we can keep the same mode for comparison between sweeps. We then tell the program to translate the nanocube \SI{100}{\micro\meter} in steps of \SI{0.05}{\micro\meter} and saving at every \SI{0.1}{\micro\meter} but we disable the signal output to the nanocube. This allows us to generate the control sweeps (as discussed in \ref{Tempcontrol}). We gather the two controls without the cardboard box and the two controls with the cardboard box.

After these control sweeps are completed and the data saved, we enable the nanocube such that the program will now change the nanocube position. We run the program a total of six times for each sample. This encompasses three sweeps moving from \SI{100}{\micro\meter} to \SI{0}{\micro\meter} and three sweeps moving from \SI{0}{\micro\meter} to \SI{100}{\micro\meter}. Each of these six sweeps are completed with the same parameters as the controls (steps of \SI{0.05}{\micro\meter} and saving at every \SI{0.1}{\micro\meter}). This gives us enough data to determine a trend for if a shift occurs and also lets us see if the trend changes depending on the direction of movement. Finally we run two more control sweeps with the box still on. This is so that we can compare the consistency of the control sweep before and after the data is taken. This leads to a total of twelve sweeps per substrate, with six of them being data to determine the frequency shift and the other six being consistency checks and verification data so that we can draw stronger conclusions.