\chapter{WGMR Fabrication}

\section{Fabrication of WGMRs} \label{WGMRfabrication}
For this experiment we have used a resonator made from Lithium Niobate (LiNbO$_{3}$) A process of single point diamond tuning was used to fabricate this resonator. This process is necessary to ensure that the final product remains single crystalline. There are other fabrication methods available such as melting and re-flowing but this causes the resonator to become polycrystalline instead of single crystalline. If the resonator is polycrystalline then it loses its birefringence, an important property for our experiment (see section \ref{Polarisation}). Polycrystalline resonators can also have increased bulk and surface scattering. This will negatively affect the Q-factor of the resonator. We need a high Q-factor resonator for this experiment for reasons discussed in section \ref{Qfactor}. This makes a single crystalline resonator more desirable than a polycrystalline resonator.

Single point diamond tuning is carried out on a lathe (see figure \ref{fig:lathe}) and turning machine. The lathe is attached to an air compressor to reduce friction and achieve up to $6000$rpm when using the turning machine. A sharp single crystalline diamond is used to cut the precursor into a resonator. The diamond cutter is mounted on a stage where a computer controls the movement in the x and y directions. This allows us to cut the resonator to shape with good accuracy. The rake angle and feed rate of the diamond cutter are carefully controlled to ensure that the finished resonator has a high surface quality as these are the two parameters that have the most impact. Rake angle is the angle between the surface normal of the precursor and the diamond cutter. A negative rake angle is found to have better results for brittle substances like lithium niobate. The diamond cutter is mounted below the equatorial plane of the brass rod to ensure a negative rake angle (see figure \ref{fig:cutting}). A positive rake angle can chip or break the material during the fabrication process. The best rake angles are \ang{-20} and \ang{-25} for lithium niobate. The feed rate is a measure of how far the diamond cutter travels while the precursor undergoes one revolution. The diamond cutter should approach at a maximum speed of \SI{25}{\micro\meter}/s and have a maximum cutting depth of \SI{2}{\micro\meter}.

\begin{figure}[h!]
	\centering
	\includegraphics[height=1.5in]{./Figures/Cutting.png}
	\caption{Diagram of the cutting process taking place: A WGMR precursor is rotated by the turning machine while the cutting diamond shapes it into a resonator. The negative rake angle is illustrated.\newline}
	\label{fig:cutting}
\end{figure}

The fabrication process is performed over $4$ steps:

\begin{enumerate}
	\item We bore a small cylindrically-shaped precursor to the resonator out of a lithium niobate wafer. This can be done in multiple ways, such as using a drilling machine and a hollow core brass drill or a turning machine and a high grain size diamond slurry. The method we use is to mount the hollow core brass drill in the lathe and pushing the  wafer into it as it rotates. The precursor should have a \SI{100}{\micro\meter} radius larger than the desired resonator radius. This is done in the event of cracks in the surface of the precursor needing to be removed without changing the final radius of the resonator.
	\item  Once we have a suitable precursor, this is fixed to the end of a brass rod and mounted in the lathe. Fixing the resonator to a brass rod on a tapered tip is desirable. If the rod has a greater radius than the resonator this makes it very difficult to use the diamond cutter.
	\item The precursor is then cut into the shape of the desired resonator. This is using the diamond cutter and turning machine. For lithium niobate resonators, they should be cut in accordance with the negative rake angle, cutting speed and cutting depth specified in the previous paragraph. This should be done in steps with a surface inspection by microscope to ensure there are no cracks or breaks in the material.
	\item The resonator is then polished using diamond slurries as discussed in section \ref{WGMRpolishing} below.
	\item Once polishing is complete and satisfactory the resonator can be cleaned with isopropanol or ethanol as discussed in section \ref{WGMRcleaning} below
\end{enumerate}

\begin{figure}[h!]
	\centering
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/LatheMicroscope2.png}
		\caption{The lathe and microscope.\newline}
		\label{fig:lathea}
	\end{subfigure}
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[height=2.5in,width=\textwidth,keepaspectratio]{./Figures/latheClose.png}
		\caption{Close up of the lathe showing resonator position.\newline}
		\label{fig:latheb}
	\end{subfigure}
	\caption{The Lathe}
	\label{fig:lathe}
\end{figure}

\section{Polishing of WGMRs} \label{WGMRpolishing}

When the cutting process has been completed after enough steps, polishing will be required to ensure an optically smooth surface around the resonator. Polishing can take place immediately after a resonator is cut as they are both done on a lathe. The optically smooth surface is the result of removing any indents in the resonator. These indents are noted by imperfections in the reflections of the resonator when looked at under a microscope. There will be two reflection spots and an indent in the resonator will cause a dark spot to appear within one of these reflections. The polishing is done by hand with a spin speed of $60$rpm. We use diamond slurries with varying sizes of diamond grains to polish the resonator. A lens cleaning tissue is folded in half multiple times to build an edge where a slurry can be applied and held to the resonator while it spins. The grain size in the slurry will usually start at \SI{9}{\micro\meter} or \SI{3}{\micro\meter} depending on the degree to which the resonator needs polishing. At these grain sizes the slurries are often in a water based solution however for finer grain sized slurries the particles are suspended in a grease solution, requiring a drop of distilled water applied to the resonator or slurry before polishing. The grain size of the slurry can be reduced to provide a finer polish and therefore an increased optical smoothness in the resonator. The application spot on the folded tissue should be changed when a different sized slurry is used to avoid scratching the surface with residual large diamond particles. When the polishing process is complete and the surface appears smooth under a microscope, the resonator can be cleaned to remove any excess slurry.

\begin{figure}[h!]
	\centering
	\includegraphics[height=2in]{./Figures/Slurry.png}
	\caption{A range of slurries arranged by the size of the diamond particles in them. The sizes are used decreasingly from a coarse to a fine polish. The grey syringes indicate grease base instead of water base.\newline}
	\label{fig:slurry}
\end{figure}

\section{Cleaning of WGMRs} \label{WGMRcleaning}

Cleaning a resonator is done by hand on the lathe with a spin speed is set to $30$rpm. Isopropanol and ethanol can be applied to a lens tissue and wiped along the surface of the resonator. This process should remove any outstanding particles from the surface of the resonator and ensure a smooth surface when inspected under a microscope.