\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Whispering Gallery Phenomena}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}WGMR Structure}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}WGMR Coupling Theory}{3}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Frustrated Total Internal Reflection}{3}{subsection.1.3.1}
\contentsline {section}{\numberline {1.4}Frequency Shift}{4}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Q-Factor}{5}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Coupling Strength}{6}{subsection.1.4.2}
\contentsline {chapter}{\numberline {2}WGMR Fabrication}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Fabrication of WGMRs}{8}{section.2.1}
\contentsline {section}{\numberline {2.2}Polishing of WGMRs}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Cleaning of WGMRs}{11}{section.2.3}
\contentsline {chapter}{\numberline {3}Experimental Discussion}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Set Up}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Nanocube Discussion}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Nanocube Calibration}{15}{section.3.3}
\contentsline {section}{\numberline {3.4}Attocube Discussion}{16}{section.3.4}
\contentsline {section}{\numberline {3.5}WGMR Coupling Process}{17}{section.3.5}
\contentsline {section}{\numberline {3.6}Frequency Shift Direction}{19}{section.3.6}
\contentsline {section}{\numberline {3.7}Automation}{19}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Python}{20}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}MATLAB}{21}{subsection.3.7.2}
\contentsline {section}{\numberline {3.8}Substrate Mounting Orientation}{23}{section.3.8}
\contentsline {section}{\numberline {3.9}Environmental Control}{24}{section.3.9}
\contentsline {section}{\numberline {3.10}Polarisation Discussion}{26}{section.3.10}
\contentsline {section}{\numberline {3.11}Data Collection Method}{28}{section.3.11}
\contentsline {chapter}{\numberline {4}Results}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Results}{29}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}SF11}{30}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Lithium Niobate}{31}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Diamond}{33}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Rutile}{34}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Silicon}{35}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Analysis}{37}{section.4.2}
\contentsline {section}{\numberline {4.3}Conclusion}{42}{section.4.3}
\contentsline {chapter}{\numberline {5}Appendix}{44}{chapter.5}
