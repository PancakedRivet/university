\chapter{Results}

\section{Results} \label{Results}

We collected the data in batches so that we could find and track a suitable mode for the experiment. We were able to keep a single mode for testing in each batch of data that was collected. The batch of data taken with the mode that showed the best coupling contrast is analysed in this section. The results can be seen below with a sub section given to each substrate.

The way in which the data is collected between the directions is subtly represented in the figures. When the substrate is moving towards the resonator, the data is taken starting at \SI{100}{\micro\meter} and continues until the sweep finishes at \SI{0}{\micro\meter}. When the sweep away from the resonator is taken, this ordering is reversed. The data is taken starting at \SI{0}{\micro\meter} and moves away until it finishes at \SI{100}{\micro\meter}. We measure the frequency shift sweep moving towards the resonator first, followed by the sweep away from the resonator. 

The results for the two control sweeps are shown in the top two figures of each sub section. This drift is constant across the control sweeps. This means we can calculate what the drift is for each substrate and subtract it from the data we generate during the experiment. The bottom two figures of each sub section are generated with this calculation already done. This is done to maximise the effects of the substrate on the frequency shift of the resonator. The drift calculations have a greater impact for the sweeps taken of the substrate moving \emph{towards} the resonator. After accounting for the drift in the substrate sweeps there are some sweeps where the frequency doesn't change at large distances from the resonator. This makes it clear where the substrate begins perturbing the evanescent field and thereby shifting the frequency of the mode we are studying. We know this because at large distances from the resonator the substrate is unable to shift the frequency of the mode. Only when it approaches the resonator will it start to have an effect on the frequency and if we can account for the drift and the environment as much as possible, the only remaining factor that can change is the substrate position. The magnitude of the frequency shift increases when close to the resonator. This is expected because the evanescent field decays away exponentially radially from the edge of the resonator. This means the substrate should have an exponentially increasing effect on the frequency of the mode as it approaches. 

For the purposes of discussion for this section, we refer to a "forwards" sweep  to mean a sweep taken \emph{in} the direction of the substrate. The opposite definition is then used for "backwards" where a "backwards" sweep is  to mean a sweep taken in the direction \emph{away} from the resonator. This definition follows from the distance axes of the plots below and how the movement of the substrate has been defined in relation to the resonator hitherto.

\subsection{SF11} \label{Results:SF11}

\begin{figure}[h!]
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{./figures/SF11/box1.png}
		\caption{Control sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/SF11/box2.png}
		\caption{Control sweep backwards}
	\end{subfigure}
	\medskip
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/SF11/1.png}
		\caption{Substrate sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/SF11/2.png}
		\caption{Substrate sweep backwards}
	\end{subfigure}
	\caption{The figures showing a SF11 substrate causing a frequency shift in the WGMR.\\$n = 1.7434$}
\end{figure}

Based on the control sweeps, the passive drift over the duration of the forwards control sweep is \SI{0.26}{\mega\hertz}. The passive drift over the duration of the backwards control sweep is \SI{-2.11}{\mega\hertz}. The passive frequency drift appears to have become much weaker when the control sweeps for SF11 were taken, with almost no drift noticed for the forwards control sweep. There is comparatively little drift in the backwards control sweep. The result of having to account for such a small drift is that the frequency does not change significantly at large distances from  the resonator. The asymptote of the mode is useful in determining the magnitude of the frequency change caused by the substrate. This is because we expect the frequency shift to be constant while the substrate is outside of the range required to perturb the evanescent field. This way when a frequency shift occurs and the background variations are accounted for the frequency shift is more likely to have been a result of the substrate.

\begin{figure}[h!]
	\centering
	\includegraphics[height=1.5in]{./figures/SF11/SF11.png}
	\caption{An image of the mounted SF11 substrate.\newline}
	\label{fig:SF11substrate}
\end{figure}


\subsection{Lithium Niobate} \label{Results:LiNbO3}

\begin{figure}[h!]
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{./figures/LiNbO3/box1.png}
		\caption{Control sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/LiNbO3/box2.png}
		\caption{Control sweep backwards}
	\end{subfigure}
	\medskip
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/LiNbO3/1.png}
		\caption{Substrate sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/LiNbO3/2.png}
		\caption{Substrate sweep backwards}
	\end{subfigure}
	\caption{The figures showing a LiNbO$_{3}$ substrate causing a frequency shift in the WGMR.\\$n_e = 2.21$, $n_o = 2.30$ \cite{zelmon_infrared_1997}}
\end{figure}

Based on the control sweeps, the passive drift over the duration of the forwards control sweep is \SI{-21.51}{\mega\hertz}. The passive drift over the duration of the backwards control sweep is \SI{-17.58}{\mega\hertz}. There is a \SI{5}{\mega\hertz} disparity between the passive frequency drift of the control sweep simulating movement towards the resonator and the control sweep simulating movement away from the resonator. This is unexpected because the drift is relatively constant. This means that the substrate sweeps must be scaled by a different amount depending on which direction the substrate moves. We scale the backwards substrate sweep by the amount calculated from the backwards control sweep. The resulting plot shows many interesting characteristics. Assuming that the passive frequency drift is removed to a reasonable degree, there remains a kink in which the frequency shift is initially negative, but at \SI{35}{\micro\meter} reverses direction and begins to positively shift the frequency.This also makes it difficult to quantify by how much the substrate is shifting the frequency, because there is no asymptote to use as a reference point of no substrate perturbation. The directions of the frequency shifts appear to be opposite the directions of the frequency shifts in the forwards substrate sweep.

\begin{figure}[h!]
	\centering
	\includegraphics[height=1.5in]{./figures/LiNbO3/lithiumniobate2.png}
	\caption{An image of the mounted LiNbO3 substrate.\newline}
	\label{fig:LiNbO3substrate}
\end{figure}

\subsection{Diamond} \label{Results:Diamond}

\begin{figure}[h!]
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{./figures/Trapezoid/box1.png}
		\caption{Control sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Trapezoid/box2.png}
		\caption{Control sweep backwards}
	\end{subfigure}
	\medskip
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Trapezoid/1.png}
		\caption{Substrate sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Trapezoid/2.png}
		\caption{Substrate sweep backwards}
	\end{subfigure}
	\caption{The figures showing a diamond substrate causing a frequency shift in the WGMR.\\$n = 2.3878$ \cite{phillip_kramers-kronig_1964}}
\end{figure}

Based on the control sweeps, the passive drift over the duration of the forwards control sweep is \SI{-12.37}{\mega\hertz}. The passive drift over the duration of the backwards control sweep is \SI{-13.72}{\mega\hertz}. Despite accounting for the constant shift from the forwards control sweep, the forwards substrate sweep does not display an asymptote within the \SI{100}{\micro\meter} range in which it was studied. This makes it harder to estimate the full frequency shift caused by the substrate. This is ahrder because ideally we want to observe no frequency shift at large distances from the resonator. This allows us to see where the effects of the substrate start. While we can approximate the frequency shift it would be more accurate to model the mode frequency and determine its asymptote.

\begin{figure}[h!]
	\centering
	\includegraphics[height=1.5in]{./figures/Trapezoid/diamond.png}
	\caption{An image of the mounted diamond substrate.\newline}
	\label{fig:Diamondsubstrate}
\end{figure}

\subsection{Rutile} \label{Results:Rutile}

\begin{figure}[h!]
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{./figures/Rutile/box1.png}
		\caption{Control sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Rutile/box2.png}
		\caption{Control sweep backwards}
	\end{subfigure}
	\medskip
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Rutile/1.png}
		\caption{Substrate sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Rutile/2.png}
		\caption{Substrate sweep backwards}
	\end{subfigure}
	\caption{The figures showing a rutile substrate causing a frequency shift in the WGMR.\\$n = 2.4538$ \cite{devore_refractive_1951}}
\end{figure}

Based on the control sweeps, the passive drift over the duration of te forwards control sweep is \SI{-15.46}{\mega\hertz}. The passive drift over the duration of the backwards sweep is \SI{-15.49}{\mega\hertz}. The amount of passive frequency drift is approximately identical in both directions. This is what is expected as the drift appears constant and should have the same affect on the frequency regardless of direction. The backwards substrate sweep displays similar effects to lithium niobate. There is a clear kink in the sweep that indicates that the frequency shift has reversed direction. There is also no asymptote in the backwards substrate sweep which makes it difficult to quantify the frequency shift from the substrate.

\begin{figure}[h!]
	\centering
	\includegraphics[height=1.5in]{./figures/Rutile/rutile.png}
	\caption{An image of the mounted rutile substrate.\newline}
	\label{fig:Rutilesubstrate}
\end{figure}

\subsection{Silicon} \label{Results:Silicon}

\begin{figure}[h!]
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{./figures/Silicon/box1.png}
		\caption{Control sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Silicon/box2.png}
		\caption{Control sweep backwards}
	\end{subfigure}
	\medskip
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Silicon/1.png}
		\caption{Substrate sweep forwards}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figures/Silicon/2.png}
		\caption{Substrate sweep backwards}
	\end{subfigure}
	\caption{The figures showing a silicon substrate causing a frequency shift in the WGMR.\\$n = 3.4850$ \cite{green_self-consistent_2008}}
\end{figure}

Based on the control sweeps, the passive drift over the duration of a sweep moving towards the resonator is \SI{-10.50}{\mega\hertz}. The passive drift over the duration of a sweep moving away from the resonator is \SI{-11.60}{\mega\hertz}. The pcolour plot for silicon is very different to the rest of the pcolour plots in that it has a gradient of intensity matching the resonator-substrate distance. This is due to light coupling out of the resonator and into the silicon substrate using the frustrated total internal reflection process discussed in section \ref{FTIR}. Because the light has coupled out of the resonator it reduces the intensity of the light arriving at the detector. This explains the colour gradient showing a decrease in intensity with distance. There is also a striking difference between this and the theorised shift from \cite{foreman_dielectric_2016}. In \cite{foreman_dielectric_2016} it is theorised that a substrate with a high refractive index like silicon would cause a blue-shift or a positive frequency shift in the resonator. According to the forwards substrate sweep the frequency shift from silicon is a red shift demonstrated by the negative frequency shift. 

\begin{figure}[h!]
	\centering
	\includegraphics[height=1.5in]{./figures/Silicon/silicon.png}
	\caption{An image of the mounted silicon substrate.\newline}
	\label{fig:Siliconsubstrate}
\end{figure}

\pagebreak

\section{Analysis}

The discrepancies between the forwards and backwards sweeps lead us to believe that an interaction has taken place that has changed the coupling conditions of the resonator. If the resonator has been impacted and the coupling conditions have changed, this lends itself to explaining why the backward sweeps look so different to the forward sweeps. This may be a kind of rebounding effect where the resonator is recoiling back into position while the substrate moves away. The boundary line appears thicker on the backwards sweep figures. This lends itself to the rebound theory, as the mode vanishes when contact is made. If contact is continued for an extra micron on the backwards sweep this might be the rebound as the resonator restores to the position it was in before the impact. Furthermore we find that it is the substrates that have a larger physical size that display odd behaviour on the backwards sweep. The larger size and weight of LiNbO$_3$ and rutile would be the substrates more likely to move the resonator as they would have greater mass than the smaller substrates too. 

We can also see that a broadening of the mode takes place in each of the substrates backwards sweeps. This broadening of the mode is possible to attribute to light coupling out of the resonator and this is the case for the Silicon sweeps (see section \ref{Results:Silicon}) but mode broadening cannot happen for substrates that have a refractive index lower than that of the lithium niobate resonator. Because mode broadening still occurs this is likely due to the coupling conditions having changed after the substrate impacts the resonator. The mode broadening tells us that light is now over-coupling into the resonator.

Another theory is that the frequency axis on half of the figures is backwards. While this may seem true by inspection, there is no evidence that this is happening from within the MATLAB script. Furthermore, there are two substrates that have frequency shift shapes that look as though an axis flip would fix them, however the shape of the remaining substrates frequency shifts would then also be flipped, and the problem remains.

Within \SI{5}{\micro\meter} of the resonator a distinctive boundary can be seen. This is likely where the substrate has impacted the resonator and as a result, it changes the coupling conditions in the resonator. This would explain why the mode vanishes in this range. 

We are trying to ascertain whether or not figure \ref{fig:foreman} correctly describes the relationship between refractive index and frequency shift. By saving the vectors containing the information for the frequency shift of the mode, we can determine the frequency shift before the substrate impacts the resonator. We record this frequency shift and the substrates refractive index then plot them in the same way as figure \ref{fig:foreman} is presented. This reconstructed plot is figure \ref{fig:foremanexperiment}. We can see that there is a semblance of the same shape as figure \ref{fig:foreman}. A curve can be formed if the diamond substrate is removed as an out-lier. Alternatively the best matchup with the theory presented in \cite{foreman_dielectric_2016} occurs when Rutile is removed as an out-lier. Regardless, it can be seen that refractive index has an influence on the frequency shift. Initially the frequency shift is negative when refractive index increases, but once it passes a threshold it reverses direction and becomes a positive frequency shift. Due to the mode-broadening that occurs after the first forwards substrate sweep, it is difficult to use multiple sweeps to produce this figure, as the mode-broadening increases the line width, making it harder to determine the frequency shift of each substrate. Using the first forward sweep of each substrate builds the most accurate picture between refractive index and frequency shift.

\begin{figure}[h!]
	\centering
	\includegraphics[height=3in]{./figures/foreman.png}
	\caption{Reconstructing figure \ref{fig:foreman} with the experiment results.\newline}
	\label{fig:foremanexperiment}
\end{figure}

It is very disappointing that the mode quality deteriorates so rapidly once contact with the resonator is made. The method of data collection discussed in section \ref{datacollection} was designed for consistency in making several measurements without influencing the system to build up a strong picture of what the interactions taking place were. The issue stems from the way the automation process moves the substrate into and out of contact with the resonator, causing the data to because distorted at distances close to the resonator (\SI{0}{\micro\meter} - \SI{5}{\micro\meter}). One of the assumptions made in \cite{foreman_dielectric_2016} is that the substrates were touching the resonator. We can replicate this in the experiment however the control of the substrate needs to improve such that we can touch the resonator with the substrate but without the force or momentum required to change the coupling conditions. While the data deteriorated over successive sweeps from multiple resonator-substrate impacts, the consistency in keeping the same mode for testing between substrates is useful as the consistency makes the frequency shift comparisons between substrates easier. There are many variables that are kept the same between substrates by using the same mode, such as the coupling efficiency, where data taken between a high coupling efficiency and low coupling efficiency can make it harder to determine the frequency shift if the resolution is not the same across all substrates tested.
	
We found relevant literature discussing possible causes of the observed passive frequency drift in the data we gathered. One paper by Savchenkov \cite{savchenkov_enhancement_2006} features a WGMR left for 4 hours with a sweeping laser. This shifted the frequency by approximately \SI{3}{\giga\hertz} as can be seen in figure \ref{fig:savchenkov}. As this was left over a period of \SI{4.5}{\hour}, we can determine that their resonator had a frequency drift of

\begin{equation}
	f_\mathrm{drift} = \frac{\Delta\cdot f}{\mathrm{time}},
\end{equation}

\begin{equation}
f_\mathrm{drift} = 11.\overline{11} \ \mathrm{MHz}\ \mathrm{min}^{-1}.
\end{equation}

The duration of a sweep is approximately \SI{60}{\second}. The shifts stated for each substrate would therefore be the frequency drifts per minute. Of the substrates tested, the calculated drift of Savchenkov et al. would most closely match the drift experienced by the silicon substrate. Silicon experienced a frequency drift of \SI{-10.50}{\mega\hertz} on its forward control sweep and \SI{-11.60}{\mega\hertz} on its backward control sweep. While the magnitude is similar, the direction is reversed. Savchenkov et al. use figure \ref{fig:savchenkov} to show how their spectrum evolves in time, this was in the same direction as the drift we experienced, but only after using the wavelength metre did we determine that it was a reduction in frrequency and not an increase. The difference in magnitude between our drift and Savchenkov et al. may be explained by this difference in analysis.

\begin{figure}[h!]
	\centering
	\includegraphics[height=3in]{./figures/savchenkov.png}
	\caption{Image taken from \cite{savchenkov_enhancement_2006} to demonstrate that on large time scales we expect ambient fluctuations..\newline}
	\label{fig:savchenkov}
\end{figure}

There is a paper by Moretti et al. \cite{moretti_temperature_2005} which discusses the thermal expansion of a lithium niobate WGMR. We assumed that the cardboard box would thermalise the resonator and substrate to the same temperature within \SI{120}{\second} of placing the cardboard box over our set up. \cite{moretti_temperature_2005} derives the coefficients for thermal expansion in a z-cut lithium niobate resonator. This is identical to the one used in our set up.

To determine how temperature changes the frequency then, we have the frequency of a WGMR mode

\begin{equation}
	\nu = \frac{m\cdot c}{2\pi\cdot R\cdot n_{\mathrm{eff}}}.
\end{equation}

Taking the time derivative yields

\begin{equation}
	\frac{d\nu}{dT} = -\frac{\mathrm{m}\cdot \mathrm{c}}{\mathrm{R}\cdot \mathrm{n_{eff}}}\left(\frac{1}{\mathrm{R}}\frac{\partial R}{\partial T} + \frac{1}{\mathrm{n_{eff}}}\frac{\partial \mathrm{n_{eff}}}{\partial T}\right).
\end{equation}

We make the assumption that $n_{eff} \approx n_e$ because the resonator is mounted with the optical axis in the z-direction. This also assumes that only the fundamental modes are coupled into. Because of this assumption, we can therefore also assume that they will have approximately equal temperature derivatives

\begin{equation}
	\frac{\partial \mathrm{n_{eff}}}{\partial T} \approx \frac{\partial \mathrm{n_e}}{\partial T}.
\end{equation}

Therefore we have

\begin{equation} \label{nu}
	\frac{1}{\nu}\frac{d\nu}{dT} = -\frac{1}{\mathrm{R}}\frac{\partial \mathrm{R}}{\partial \mathrm{T}} - \frac{1}{\mathrm{n_e}}\frac{\partial \mathrm{n_e}}{\partial \mathrm{T}}.
\end{equation}

We define 

\begin{equation}
	\alpha_{perp} = \frac{1}{\mathrm{R}}\frac{\partial \mathrm{R}}{\partial \mathrm{T}},
\end{equation}

and

\begin{equation}
	\alpha_{ne} = \frac{1}{\mathrm{n_e}} \frac{\partial \mathrm{n_e}}{\partial \mathrm{T}}.
\end{equation}

This is a standard way of collecting terms into the the optical axis term $\alpha_{ne}$ and the terms perpendicular to the optical axis $\alpha_{perp}$ From \cite{moretti_temperature_2005} the paper doesn't solve for the coefficients with $1550$nm light but we assume that the results for $1523$nm are approximately the same as the results for what $1550$nm would be. We also take the temperature to be \ang{22} (room temperature). Equation 4 of \cite{moretti_temperature_2005}

\begin{equation}
	\frac{dn_e}{dT} = -2.6 + 19.8 \times 10^{-3}T(10^{-5}K^{-1}).
\end{equation}

We substitute \ang{22}C = \SI{295.15}{\kelvin}

\begin{equation}
	\frac{dn_e}{dT} = -2.6 + 19.8 \times 10^{-3}\cdot295.15\cdot(10^{-5}K^{-1}).
\end{equation}

\begin{equation}
	\frac{dn_e}{dT} = -2.6 + 5.84^{-5}
\end{equation}

\begin{equation}
	\alpha_{ne} = -1.176
\end{equation}

From \citealp{pignatiello_measurement_2007} we quote the value

\begin{equation}
	\alpha_{perp} = 13.6 \times 10^{-6} K^{-1}.
\end{equation}

Putting these values together to reform equation \ref{nu}

\begin{equation}
	\frac{1}{\nu}\frac{d\nu}{dT} = -1.176.
\end{equation}

To get the derivative we multiply both sides by $\nu$ which we find by solving the wave equation \ref{eq:c} for $\lambda = 1550$

\begin{equation}
	f_{1550} = 1.935 \times 10^{14}.
\end{equation}

Therefore

\begin{equation}
	\frac{d\nu}{dT} = \SI{193000000}{\mega\hertz\kelvin^{-1}}
\end{equation}

Thus a shift of \SI{15}{\mega\hertz} could be caused by a \SI{77}{\nano\kelvin}. If these assumptions hold for our system then we will need gain much more stringent control over the temperature around the experiment.

\section{Conclusion}

Our aim was to determine if the relationship between refractive index and frequency shift exists as is proposed in \cite{foreman_dielectric_2016}. We were not able to prove this relationship, due to the difference in shape between figure \ref{fig:foreman} and figure \ref{fig:foremanexperiment}. While we were unable to prove that the relationship exists as proposed, we did unearth a phenomenon that occurs at the interface between substrates made of lithium niobate and rutile. There is a confounding problem that causes the frequency shifts of these two substrates to be dependent on the direction of the sweep. This is not what is predicted in the theory or what was observed with other substrates made of SF11, silicon or diamond. To repeat this experiment in future one would need a better way of retaining the line width over multiple sweeps. In this experiment we believe impacts occurred as part of the forwards sweep that caused changes in the coupling conditions for the resonator. As a result we saw mode broadening on every substrate on the backwards sweep when ideally only the substrates with refractive indexes higher than LiNbO$_3$ should experience mode broadening (referring to silicon, rutile and diamond of the substrates used in this experiment). The coupling contrast was very low for this resonator. This was probably caused by the multiple impacts suffered during testing. The solution would be to clean the resonator between substrate tests however care must be taken to ensure that it is the same mode being tested.

While we were unable to prove that the relationship is as was proposed in \cite{foreman_dielectric_2016}, we were able to show that there is a complex relationship between the refractive index and the frequency shift. If we remove the outlier of rutile from figure \ref{fig:foremanexperiment} then we can see that a similar shape may exist. It would appear as though the units are off by a large factor as well. If we could solve the complexities around the frequency drift that this experiment was subject to then there is a higher chance of verifying the relationship.

We used the attocube piezo translation system to ensure that the substrate made contact with the resonator. We do not know to what extent this initial collision changes the coupling conditions. If it's found that the conditions are changed significantly then attempting to use a less invasive system to determine an absolute position is advised. The servos in the nanocube are excellent at ensuring the nanocube position is stable, may have contributed to a possible rebounding effect that the backwards sweeps of the physically larger substrates indicated. We believe that the servos rigidity is both an advantage and disadvantage in this sense, because a translation system less rigid would apply less pressure on the resonator, causing less deformation of the substrate and resonator.

The source of the frequency drift experienced during the control sweeps should also be investigated further. There were control sweeps we conducted that match the drift presented in \cite{savchenkov_enhancement_2006}, however the transient nature of this drift and the magnitude of its effect are still not well understood. We saw a large drift between two subsequent samples in the LiNbO$_3$ control sweeps but saw nearly identical drift in the rutile control sweeps. We experienced the mode broadening expected of silicon as can be seen from section \ref{fig:Siliconsubstrate}. We also should have experienced mode broadening of rutile and diamond on the forwards substrate sweeps because they also have refractive indexes higher than that of LiNbO$_3$. There was no such mode broadening on the forwards sweep of either substrate. 

There is also the question of what the cause of the odd striations in the pcolour plots may be. We are only concerned with the mode and the sidebands and it seems as though they have no bearing on these values however the way some substrates have them while other substrates do not is interesting. Even more interesting is the direction of these striations. The can losely follow the direction of the mode and sidebands or they can be nearly tangential to it.

We were not able to collect data on any blue-shifts. This was part of figure \ref{fig:foreman} and according to the theory, silicon has a refractive index that would have blue shifted the frequency of the mode we were studying. This did not happen although the frequency shift by silicon was the weakest shift of the substrates.

The consistency of this experiment has been useful. A rigorous data collection method and use of control sweeps to offset background fluctuations has shown it improves the quality of the data. The issues that have been identified with the experiment are unlikely to be random occurrences due to the systematic sampling method and the use of multiple sweeps in both directions. The drawback is the persistent issue with the deterioration of the mode line-width.

The results of this experiment has laid ground work for further study. There are crude systems in place that with refinements can produce results that can validate or deny the proposed relationship between refractive index and frequency shift.