\chapter{Introduction}
\pagenumbering{arabic}
\section{Whispering Gallery Phenomena} \label{WGMRphenom}

Whispering Gallery is a term initially used to describe the phenomenon of three people standing in an elliptical room. Two can stand on opposite sides of the room and whisper messages to each other along the wall in either direction without a third person being able to hear despite standing directly between them. It was Lord Rayleigh who first scientifically described this phenomenon \cite{rayleigh_theory_1877} for Saint Paul's Cathedral (see figure \ref{fig:stpauls}). The sound waves are being refocused by continuously reflecting along the walls. This allows them to remain audible for longer than if they were traveling freely through air. This is an observable analogue to the process by which a WGMR works.

\begin{figure}[h!]
	\centering
	\includegraphics[height=2in]{./figures/stpauls}
	\caption{The Saint Paul's Cathedral whispering gallery that inspired Lord Rayleigh's work on whispering gallery waves. Image taken from http://www.planetware.com/london/st-pauls-cathedral-eng-l-spal.htm.}
	\label{fig:stpauls}
\end{figure}

\section{WGMR Structure} \label{WGMRstructure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth]{./figures/WGMR.png}
	\caption{Diagram of a WGMR.\newline}
	\label{fig:wgmrschem}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[height=2in]{./figures/resonatorcropped.png}
	\caption{Close up on the WGMR used in the experiment. The rutile substrate and diamond coupling prism are seen to the left and right of the WGMR respectively.}
	\label{fig:wgmr}
\end{figure}

While this will work for any wave moving along a concave surface with the right conditions, we restrict our use of WGMR to refer to a highly polished disk made from a dielectric material (see figure \ref{fig:wgmr}). These disks have near spherical geometry with a major radius $R$ and a smaller internal radius $r$ (see figure \ref{fig:wgmrschem}). To keep the light waves traveling along the curvature of the outer surface, the light must be kept inside the resonator. This is reliant on total internal reflection (TIR), derived from Snell's Law (see figure \ref{fig:tir}a): 

\begin{equation}
	n_1 \sin(\theta_1)=n_2 \sin(\theta_2).
\end{equation}

Two conditions are required for TIR to occur: the index of refraction must decrease across the boundary $n_1 > n_2$ and the incident beam angle must exceed the critical angle

\begin{equation} \label{eq:critical}
\theta_{\mathrm{c}} =\arcsin\left(\frac{n_2}{n_1}\right),
\end{equation}

(see figure \ref{fig:tir}b). This results in light that will not refract through the boundary between mediums but will reflect off the boundary instead. The beam is reflected at the same angle as the incident beam $\theta_{1} = \theta_{R}$ (see figure \ref{fig:tir}c).

Considering the wave like nature of light, there will be certain frequencies of light that are sustained in the resonator. This is the result of the waves returning to the same starting point with the same phase after one round trip in the resonator. These frequencies cause constructive interference within the WGMR and resonances occur. Similarly, there will be certain frequencies that return to the same starting point but exactly out of phase. This results in destructive interference and so these frequencies are suppressed in the WGMR.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth]{./figures/SnellLine.png}
	\caption{Interaction of light at a boundary between two media.}
	\label{fig:tir}
\end{figure}

\section{WGMR Coupling Theory} \label{WGMRCoupling}

\subsection{Frustrated Total Internal Reflection} \label{FTIR}

An incident ray of light travels through a prism located near the resonator and undergoes TIR. Even if the incident wave is entirely reflected by TIR, it will still produce an evanescent wave (see figure \ref{fig:ftir}). This wave will ordinarily transmit zero energy, however if a third medium is close enough, the evanescent wave can become a propagating wave again. Evanescent waves decay exponentially so the wave will decay to zero if there is no proximal medium for it to propagate in. The third medium must have a refractive index higher than the first and second media $n_3 > n_1 > n_2$ for this to occur. The energy transmitted to the third medium can be enough to excite a wave in the third medium that can continue propagating. Doing this creates evanescent wave coupling and the light is able to 'tunnel' into the resonator. This is known as frustrated total internal reflection \cite{zhu_frustrated_1986}. It is also frustrated TIR that couples light back out of the resonator by this process working in reverse. The light undergoing TIR in the resonator generates an evanescent field that transmits energy back into the coupling prism. This creates a second evanescent wave coupling that causes light to travel from back out of the resonator to the prism. The light that travels back out of the prism then travels to a detector. When we plug the detector into an oscilloscope or a computer, we see an image like figure \ref{fig:resonance}. 

\begin{figure}[h!]
	\centering
	\includegraphics[height=2in]{./figures/FTIR.png}
	\caption{Frustrated total internal reflection.\newline}
	\label{fig:ftir}
\end{figure}

\section{Frequency Shift}

As can be seen in figure \ref{fig:resonance}, there are 3 noticeable dips in the signal. The central and largest dip is the resonant frequency of the mode that the light is coupled into. The smaller dips to the right and left are the sidebands. These sidebands are modulated onto the original signal by the wave modulator. As we know what frequency  the sidebands are relative to the central resonance, we can make a frequency axis as seen along the x-axis of the figure. Because the sidebands are modulated onto the original signal, it is important to improve the original signal as much as possible. If the original signal has a large line width and low contrast, the sidebands will also. This can make it hard to calculate a frequency axis due to the uncertainty in the mode frequency and sidebands.

\begin{figure}[h!]
\centering
\includegraphics[height=3in]{./Figures/Resonance.png}
\caption{A sweep from the LiNbO$_3$ resonator showing the mode of interest as the central dip with \SI{100}{\mega\Hz} sidebands on either side. The position of these sidebands relative to the mode generates the frequency axis.\newline}
\label{fig:resonance}
\end{figure}

Figure \ref{fig:foreman} indicates that the frequency of a mode will change with the refractive index of the substrate brought into its proximity. This is demonstrated by the presence of a red shift and a blue shift in the frequency of the mode. The location of this change is interesting, as the relationship is not linear. The maximum redshift occurs when the refractive index of the substrate matches that of the extraordinary refractive index of lithium niobate ($n_e = 2.21$). Further increasing the refractive index causes a reduction in the amount of redshift, eventually returning to zero. Increasing the refractive index past this point causes a blue shift instead, where the mode shift is now positive instead of negative.

\begin{figure}[h!]
	\centering
	\includegraphics[height=3.5in]{./Figures/ForemanFigureCropped.png}
	\caption{Figure taken from \cite{foreman_dielectric_2016} showing how mode frequency changes with refractive index.\newline}
	\label{fig:foreman}
\end{figure}

\subsection{Q-Factor} \label{Qfactor}

An important way to compare the performance of resonators is in the comparison of quality factors or Q-factors where the Q-factor is given by

\begin{equation} \label{eq:Q}
	Q = \frac{f_{resonant}}{\Delta f} = \frac{\omega_{resonant}}{\Delta \omega}.
\end{equation}

The Q factor is a measure of the inverse of a resonators line width relative to its central (resonant) frequency. A high Q-factor is desirable in our experiment because we will be observing changes in the line width \cite{thomas_effect_2006}\cite{gorodetsky_optical_1999}. Therefore if we use a resonator with a higher Q factor, we have a narrower line width, making changes more noticeable.

The resonator may have curvature that is large relative to the wavelength of the light traveling around it but the special nature of WGMRs means that the modes created by the resonant frequencies are localised near the rim \cite{breunig_whispering_2013}. This results in a good containment of the field in WGMRs.

Losses per round trip in a WGMR occur from surface scattering, material absorption and radiative loss. The surface polishing of the resonator serves to minimise the losses by surface scattering (see section \ref{WGMRpolishing}. The light lost from radiation is usually negligible as the major radius $R$ of the disk is many times greater than the wave-length of the light moving around it.

\subsection{Coupling Strength}

To best observe any changes in the line width, we want the incoming mode to match the resonator mode exactly. When this occurs, the light is coupled in at the same rate as it dissipates. This is known as \emph{critical coupling}. We can measure this point as the relative depth of the resonance, known as the coupling contrast $K$ where

\begin{equation} \label{eq:K}
K = \frac{P_\mathrm{in} - P_\mathrm{out}}{P_\mathrm{in}}.
\end{equation}

We can see from figure \ref{fig:resonance} that we have a coupling contrast of around 5\% for this mode. The desired \emph{critical coupling} occurs when $K=1$. This is the optimum coupling regime for the experiment as the resonances have a relatively large depth but retain small line widths. Larger depths make modes easier to distinguish during data analysis and narrower line widths make it easier to find the frequency of the modes. If the system is not at critical coupling, the system is said to be in a state of either \emph{under coupling} or \emph{over coupling}. During \emph{under coupling}, the relative depth of the resonance is small but the line width is also. This would make it more difficult to observe changes in the line width than if the resonator was critically coupled. During \emph{over coupling}, the line width increases and the contrast decreases. This is a problem as there is greater uncertainty in the mode frequency.