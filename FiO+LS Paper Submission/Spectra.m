close all
clc

set(0,'DefaultAxesFontSize',20,'DefaultTextFontSize',24);

start_set = 1;
end_set = 90;
XLIM = [-20 10];
figureTitle = 'Frequency of a Resonant Mode';
figureLabelX = 'WGM Frequency Shift';
figureLabelY = 'Voltage';
[~, index_point] = min(plot_data(90,:));
shift_line = plot_frequency(index_point);

figure(1)
title(figureTitle);
xlabel(figureLabelX);
ylabel(figureLabelY);
xlim(XLIM)
hold on
plot(plot_frequency,plot_data(1,:),'Color',[0 0 1],'LineWidth',2);
plot([0 0], ylim,'k--');
hold off

figure(2)
title(figureTitle);
xlabel(figureLabelX);
ylabel(figureLabelY);
xlim(XLIM);
hold on
plot(plot_frequency,plot_data(end_set,:),'Color',[1 0 0],'LineWidth',2);
plot([shift_line, shift_line], ylim,'k--');
plot([0 0], ylim,'k--');
hold off