# -*- coding: utf-8 -*-
"""
Created on Wed July 17 13:32:54 2018

@author: Patrick Devane
"""
import numpy
import math
from matplotlib import pyplot as plt
from datetime import datetime
from tkinter import *
import threading
import os
import sys

import nidaqmx  # Manages voltage to breakout box
import telnetlib # Manages Telnet connection
from nidaqmx.constants import AcquisitionType, Edge
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from instrumental import instrument, list_instruments  # For daq 

# Initial values
clear = lambda: os.system('cls')
clear()
OSC_DATA_LENGTH = 30000 # the amount of data plotted on screen
NANO_STARTING_DISTANCE = 0 *10  # default is 0 micron, *10 is for unit equality
ANC_HOST ="172.22.2.89"
ANC_PORT = '7230'

class Setup(object): # initalises the devices

    def __init__(self):
        tn = telnetlib.Telnet(ANC_HOST, ANC_PORT)
        tn.read_until("Authorization code: ")
        tn.write("123456\n")
        tn.read_until("Authorization success")
        print("Connection successfull \n")

        daq = instrument('NIDAQ')

        signal = numpy.zeros((OSC_DATA_LENGTH*3,), dtype=numpy.float64)
        trigger = numpy.zeros((OSC_DATA_LENGTH*3,), dtype=numpy.float64)

        self.Nanocube = Nanocube_Device(daq.ao0, NANO_STARTING_DISTANCE)
        # self.Nanocube = Nanocube_Device(daq.ao1, NANO_STARTING_DISTANCE)
        self.Detector = Detector_Device(signal, trigger)

        anc_4 = ANC_Device(tn, " 4 ", "0", "0", "0")
        anc_5 = ANC_Device(tn, " 5 ", "0", "0", "0")
        anc_6 = ANC_Device(tn, " 6 ", "0", "0", "0")

        # self.ANC_Channel_List = anc_4
        self.ANC_Channel_List = [anc_4, anc_5, anc_6]


class Nanocube_Device(object): # reads and writes to nanocibe only

    def __init__(self, port, distance):
        self.port = port
        self.distance = distance
        self.milivoltage = self.distance * 10
        self.port.write(str(self.milivoltage) + 'mV')  # Writing initial voltage

    def write_to_channel(self, difference, step_size):
        self.distance += math.copysign(step_size, difference)
        self.milivoltage = self.distance * 10
        self.port.write(str(self.milivoltage) + 'mV')


class Detector_Device(object): # reads and writes to device channels only

    def __init__(self, signal, trigger):
        self.signal = signal
        self.trigger = trigger

    def read_from_channel(self):
        with nidaqmx.Task() as task:
            task.ai_channels.add_ai_voltage_chan("Dev2/ai3:4") # signal:trigger
            task.timing.cfg_samp_clk_timing(300000, active_edge=Edge.RISING, sample_mode=AcquisitionType.FINITE, samps_per_chan=len(self.signal))
            (self.signal, self.trigger) = task.read(number_of_samples_per_channel=len(self.signal))

class ANC_Device(object): # reads and writes to device channels only

    def __init__(self, tn, port, offset_voltage, step_voltage, step_frequency):
        self.tn = tn
        self.port = port
        self.offset_voltage = [offset_voltage, "a"]
        self.step_voltage = [step_voltage, "v"]
        self.step_frequency = [step_frequency, "f"]

        self.tn.write("setm"+self.port+"stp+ \n") # channel ready for external inputs
        self.offset_voltage[0] = self.read(self.offset_voltage) # initial value read
        self.step_voltage[0] = self.read(self.step_voltage)
        self.step_frequency[0] = self.read(self.step_frequency)

        self.tn.write("set"+self.offset_voltage[1]+self.port+" "+self.offset_voltage[0]+"\n") # initial value write
        self.tn.write("set"+self.step_voltage[1]+self.port+" "+self.step_voltage[0]+"\n")
        self.tn.write("set"+self.step_frequency[1]+self.port+" "+self.step_frequency[0]+"\n")

    def read(self, variable):
        self.tn.write("get"+variable[1]+self.port+"\r\n")
        if variable[1] in ["a", "v"]:
            value_read = self.tn.read_until("V")
            value_index = value_read.find("=")
        elif variable[1] in "f":
            value_read = self.tn.read_until("H")
            value_index = value_read.find("=")
        read_value = value_read[value_index+2:-2]
        return read_value

    def write(self, variable, amount):
        write_command = str("set"+variable[1]+self.port+amount+"\n")
        self.tn.write(write_command)
        if variable[1] == "ä":
            self.offset_voltage[0] = amount
        elif variable[1] == "f":
            self.step_frequency[0] = amount
        elif variable[1] == "v":
            self.step_voltage[0] = amount


class ProgramGUI(object): # displays the data only
    def __init__(self, master):
        self.master = master
        master.title("Automation Program")
        vcmd = master.register(self.validate) # we have to wrap the command to check only a number is entered
        self.current_operation = None

        # OSC
        self.osc_should_save = IntVar()
        self.osc_callibration_frequency = StringVar()
        self.osc_lower_bound = StringVar()
        self.osc_upper_bound = StringVar()

        self.osc_callibration_button = Button(master, text="Callibrate", width='20')
        self.osc_callibration_entry_label = Label(master, text="Frequency (MHz)")
        self.osc_callibration_entry = Entry(master, textvariable=self.osc_callibration_frequency, width=5)
        self.osc_lower_bound = Entry(master, validate="key", validatecommand=(vcmd, '%P'), width=5)
        self.osc_upper_bound = Entry(master, validate="key", validatecommand=(vcmd, '%P'), width=5)
        self.osc_bounds_label = Label(master, text="Select Lower and Upper Bounds")
        self.osc_should_save_checkbutton = Checkbutton(master, text="Save?", variable=self.osc_should_save)

        self.osc_callibration_button.grid(row=6, column=3)
        self.osc_callibration_entry.grid(row=6, column=0)
        self.osc_callibration_entry_label.grid(row=7, column=0)
        self.osc_lower_bound.grid(row=6, column=1)
        self.osc_upper_bound.grid(row=6, column=2)
        self.osc_bounds_label.grid(row=7, column=1, columnspan=2)
        self.osc_should_save_checkbutton.grid(row=6, column=4)

        self.osc_should_save_checkbutton.select()

        # DAQ
        self.daq_step_size = IntVar()
        self.daq_save_interval = IntVar()
        self.daq_distance_label_field = StringVar()
        self.daq_step_size.set(10)
        self.daq_save_interval.set(10)
        self.daq_distance_label_field.set("Distance (microns): " + str(NANO_STARTING_DISTANCE))

        self.daq_step_size_select_radioButton1 = Radiobutton(root, text="0.1", variable=self.daq_step_size, value=1)
        self.daq_step_size_select_radioButton2 = Radiobutton(root, text="1", variable=self.daq_step_size, value=10)
        self.daq_step_size_select_radioButton3 = Radiobutton(root, text="10", variable=self.daq_step_size, value=100)
        self.daq_save_interval_select_radioButton1 = Radiobutton(root, text="0.1", variable=self.daq_save_interval, value=1)
        self.daq_save_interval_select_radioButton2 = Radiobutton(root, text="1", variable=self.daq_save_interval, value=10)
        self.daq_save_interval_select_radioButton3 = Radiobutton(root, text="10", variable=self.daq_save_interval, value=100)
        self.daq_execute_and_callibrate_button = Button(master, text="Execute+Callibrate")
        self.daq_execute_button = Button(master, text="Execute")
        self.daq_reset_button = Button(master, text="Reset Location")
        self.daq_execute_entry = Entry(master, validate="key", validatecommand=(vcmd, '%P'), width=5)
        self.daq_distance_label = Label(master, textvariable=self.daq_distance_label_field)
        self.daq_execute_label = Label(master, text="Move To (microns)")
        self.daq_step_size_label = Label(master, text="Nanocube Step Size (Microns)")
        self.daq_save_interval_label = Label(master, text="Save Interval Threshold (Microns)")
        
        self.daq_step_size_select_radioButton1.grid(row=1, column=7)
        self.daq_step_size_select_radioButton2.grid(row=1, column=8)
        self.daq_step_size_select_radioButton3.grid(row=1, column=9)
        self.daq_save_interval_select_radioButton1.grid(row=2, column=7)
        self.daq_save_interval_select_radioButton2.grid(row=2, column=8)
        self.daq_save_interval_select_radioButton3.grid(row=2, column=9)
        self.daq_execute_and_callibrate_button.grid(row=3, column=9)
        self.daq_execute_button.grid(row=3, column=8)
        self.daq_reset_button.grid(row=0, column=6)
        self.daq_execute_entry.grid(row=3, column=7)
        self.daq_distance_label.grid(row=0, column=7)
        self.daq_execute_label.grid(row=3, column=6)
        self.daq_step_size_label.grid(row=1, column=6)
        self.daq_save_interval_label.grid(row=2, column=6)

        # ANC
        self.anc_update_amount = StringVar()
        self.anc_variable = StringVar()
        self.anc_channel_offsetVoltage_field = StringVar()
        self.anc_channel_stepVoltage_field = StringVar()
        self.anc_channel_stepFrequency_field = StringVar()
        self.anc_active_index = IntVar()
        self.anc_variable.set("a")
        self.anc_active_index.set(0)
        self.anc_channel_offsetVoltage_field.set("Channel "+str(self.anc_active_index.get()+4)+" Offset Voltage: ")
        self.anc_channel_stepVoltage_field.set("Channel "+str(self.anc_active_index.get()+4)+" Step Voltage: ")
        self.anc_channel_stepFrequency_field.set("Channel "+str(self.anc_active_index.get()+4)+" Step Frequency: ")
        
        self.anc_variable_update_button = Button(master,text="Update")
        self.anc_variable_update_entry = Entry(master,textvariable=self.anc_update_amount)
        self.anc_channel_offsetVoltage = Label(master, textvariable=self.anc_channel_offsetVoltage_field)
        self.anc_channel_stepVoltage = Label(master, textvariable=self.anc_channel_stepVoltage_field)
        self.anc_channel_stepFrequency = Label(master, textvariable=self.anc_channel_stepFrequency_field)
        self.anc_variable_select_radioButton1 = Radiobutton(root, text="Offset Voltage", variable=self.anc_variable, value="a")
        self.anc_variable_select_radioButton2 = Radiobutton(root, text="Step Voltage", variable=self.anc_variable, value="v")
        self.anc_variable_select_radioButton3 = Radiobutton(root, text="Step Frequency", variable=self.anc_variable, value="f")
        self.anc_channel_select_radioButton4 = Radiobutton(root, text="Channel 4", variable=self.anc_active_index, value=0)
        self.anc_channel_select_radioButton5 = Radiobutton(root, text="Channel 5", variable=self.anc_active_index, value=1)
        self.anc_channel_select_radioButton6 = Radiobutton(root, text="Channel 6", variable=self.anc_active_index, value=2)

        self.anc_variable_update_button.grid(row=4, column=10)
        self.anc_variable_update_entry.grid(row=4, column=9)
        self.anc_channel_offsetVoltage.grid(row=6, column=6)
        self.anc_channel_stepVoltage.grid(row=7, column=6)
        self.anc_channel_stepFrequency.grid(row=8, column=6)
        self.anc_variable_select_radioButton1.grid(row=5, column=6)
        self.anc_variable_select_radioButton2.grid(row=5, column=7)
        self.anc_variable_select_radioButton3.grid(row=5, column=8)
        self.anc_channel_select_radioButton4.grid(row=4, column=6)
        self.anc_channel_select_radioButton5.grid(row=4, column=7)
        self.anc_channel_select_radioButton6.grid(row=4, column=8)

        # # Other 
        self.f = Figure(figsize=(10,5), dpi=100)
        self.a = self.f.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.f, master)
        self.canvas.get_tk_widget().grid(row=0, column=0,columnspan=5, rowspan=5)

    def validate(self, new_text):
        if not new_text: # the field is being cleared
            self.entered_number = 0
            return True
        try:
            self.entered_number = int(new_text)
            return True
        except ValueError:
            return False


class Controller(object): # processes data only

    def __init__(self, Nanocube_Device, ANC_Device, Detector_Device, GUI):
        self.Nanocube_Device = Nanocube_Device
        self.ANC_Device = ANC_Device
        self.Detector_Device = Detector_Device
        self.GUI = GUI

        self.processed_detector_signal = numpy.zeros(OSC_DATA_LENGTH,)
        self.processed_detector_trigger = numpy.zeros(OSC_DATA_LENGTH,)

        self.save_file_number = 0
        self.last_saved_distance = self.Nanocube_Device.distance

        # reassigning GUI buttons to Controller Methods:
        self.GUI.osc_callibration_button.configure(command=lambda: self.gui_callibration_button_pressed())
        self.GUI.daq_execute_and_callibrate_button.configure(command=lambda: self.gui_callibrate_execute_button_pressed())
        self.GUI.daq_execute_button.configure(command=lambda: self.gui_execute_button_pressed())
        self.GUI.daq_reset_button.configure(command=lambda: self.gui_reset_button_pressed())
        self.GUI.anc_variable_update_button.configure(command=lambda: self.gui_anc_update_button_pressed())
        self.GUI.anc_channel_select_radioButton4.configure(command=lambda: self.gui_anc_channel_radioButton_pressed())
        self.GUI.anc_channel_select_radioButton5.configure(command=lambda: self.gui_anc_channel_radioButton_pressed())
        self.GUI.anc_channel_select_radioButton6.configure(command=lambda: self.gui_anc_channel_radioButton_pressed())

        self.current_operation = None
        self.callibration_frequency_old = None
        self.osc_callibration_factor = 1
        self.osc_zero_ref = 0
        self.osc_x_axis = self.osc_callibration_factor * (numpy.array(range(len(self.processed_detector_signal)))-self.osc_zero_ref)

        self.run()
        
    def process_data(self):
        trigger_edge = self.trigger_edge_finder()

        self.processed_detector_signal = self.Detector_Device.signal[trigger_edge:trigger_edge+(OSC_DATA_LENGTH/6)]
        self.processed_detector_trigger = self.Detector_Device.trigger[trigger_edge:trigger_edge+(OSC_DATA_LENGTH/6)]

    def trigger_edge_finder(self):
        index = 0
        while ((self.Detector_Device.trigger[index + 1] - self.Detector_Device.trigger[index]) < 1):
            index += 1
            if index == len(self.Detector_Device.trigger) - 1:
                print("Trigger Edge Not Found! Check Trigger \n")
                index = 0
                break
        return index

    def save_data(self): # update to accept the saving header from Nanocube or ANC
        header = '%s' %(float(self.Nanocube_Device.distance)/10) + " microns " + str(datetime.now())
        numpy.savetxt('data_%s.dat' % self.save_file_number,
                      (self.processed_detector_signal,self.processed_detector_trigger),
                      header=header)

        print("Saved data_%s.dat" % self.save_file_number)
        self.last_saved_distance = self.Nanocube_Device.distance
        self.save_file_number += 1

    def save_number_reset(self):
        self.save_file_number = 0

    def begin_nanocube_movement(self, distance, should_save):
        thread = threading.Thread(target=self.nanocube_movement,  args=(distance, should_save))
        thread.start()
        return thread

    def nanocube_movement(self, distance, should_save):
        difference = distance - self.Nanocube_Device.distance
        step_size = self.GUI.daq_step_size.get()
        save_interval = self.GUI.daq_save_interval.get()

        if should_save:
            self.save_data()  # saving pre-sweep trace

        while self.Nanocube_Device.distance != distance:
            self.Nanocube_Device.write_to_channel(difference, step_size)
            if should_save and abs(self.Nanocube_Device.distance - self.last_saved_distance) >= save_interval:
                self.save_data()
                # break
        
        self.save_number_reset()
        print("Distace changed to %s microns \n" % str(float(self.Nanocube_Device.distance)/10))
    
    def gui_execute_button_pressed(self):
        distance = int(self.GUI.daq_execute_entry.get()) * 10
        should_save = self.GUI.osc_should_save.get()

        if distance >= 0 and distance <= 1000:
            if self.current_operation:
                self.current_operation.join()
            self.current_operation = self.begin_nanocube_movement(distance, should_save)
            self.GUI.daq_distance_label_field.set("Distance (microns): " + str(float(distance)/10))
            self.GUI.daq_execute_entry.delete(0, END)
        else:
            print("%s microns is outside the safe range for the Nanocube." % (float(distance)/10))
        
    def gui_reset_button_pressed(self):
        if self.current_operation:
            self.current_operation.join()
        self.current_operation = self.begin_nanocube_movement(NANO_STARTING_DISTANCE, should_save=False)
        self.GUI.daq_distance_label_field.set("Distance (microns): " + str(NANO_STARTING_DISTANCE))
        print("Distance Reset! \n")

    # def peaksfinder(self,x,y):
    #     totalpoints=len(y) # 8000
    #     radi=int(round(totalpoints/20)-1) # 
    #     flag=1
    #     z=len(y)
    #     n=0
    #     peaks=numpy.zeros(int(round(totalpoints/4)))
    #     for i in range(radi,z-radi):
    #         if y[i]<y[i+1]:
    #             j=1
    #             ru=0
    #             while (j<=radi):
    #                 if ((y[i+j]<y[i]) or (y[i-j]<=y[i])):
    #                     j=radi+10
    #                     ru=1
    #                 j=j+1
    #             if (ru==0):
    #                 peaks[n]=i
    #                 n=n+1
    #     while (flag>0):
    #         flag=-1
    #         for t in range(0,2*n):
    #             if (y[int(peaks[t])]>y[int(peaks[t+1])]):
    #                 R=peaks[t]
    #                 peaks[t]=peaks[t+1]
    #                 peaks[t+1]=R
    #                 flag=1
    #     return peaks[0:3]

    def gui_callibration_button_pressed(self):
        """
        function [frequency_axis, bounded_data_range, central_minimums] = frequency_scaling(data_array,bounds,side_band_frequency,zero_reference_trace,search_length)
        minimum_index_storage = zeros(min(size(data_array)),3); #Initializing the matrix to store the 3 minimum points per data file
        bounded_data_range = data_array(:,bounds(1):bounds(2)); #Focussing on the data located between the specified bounds
        [~,minimum_index_storage(:,2)] = min(bounded_data_range,[],2); #Finding the central minimas. They are the largest in magnitude of the 3
        zero_frequency_index = minimum_index_storage(zero_reference_trace,2); #Finding the index of the central minima of the zero-reference-trace

        for k = 1:min(size(data_array))
            left_minimum_range = data_array(k, bounds(1): bounds(1) + minimum_index_storage(k,2) - search_length); #Focussing on the data between the lower bound and the zero-point
            [~,minimum_index_storage(k,1)] = min(left_minimum_range,[],2); #Finding the left side-band between the lower bound and the zero-point

            right_minimum_range = data_array(k, (bounds(1) + minimum_index_storage(k,2)) + search_length : bounds(2)); #Focussing on the data between the zero-point bound and the upper bound
            [~,right_minimum_index_rough] = min(right_minimum_range,[],2); #Finding the left side-band between the zero-point and the upper bound
            minimum_index_storage(k,3) = right_minimum_index_rough + minimum_index_storage(k,2) + search_length; #Rescaling the right sideband to be relative to the lower bound
        end

        side_band_distances_3 = [minimum_index_storage(:,2) - minimum_index_storage(:,1) , minimum_index_storage(:,3) - minimum_index_storage(:,2) , round((minimum_index_storage(:,3) - minimum_index_storage(:,1))/2)]; #Combining the distances between sidebands
        side_band_distances_1 = round(mean(side_band_distances_3,2)); #Averaging the 3 distances to find the average length used for each data file
        average_side_band_distance = round(mean(side_band_distances_1)); #Averaging the lengths for each data file to find the best length to set the frequency axis
        scale_factor = side_band_frequency / average_side_band_distance; #Determing the scale factor based on the number of points and the side band frequency
        new_x_axis = linspace(0,bounds(2)-bounds(1),bounds(2)-bounds(1)+1); #Generating a new x-axis for the new frequency scale
        frequency_axis = -(new_x_axis - zero_frequency_index) * scale_factor; #Setting the zero-reference minimum as zero and rescaling the rest of the axi accordingly
        central_minimums = minimum_index_storage(:,2);
        end"""
        
        minimum_storage = numpy.zeros(3,)
        lower_bound = int(self.GUI.osc_lower_bound.get())
        upper_bound = int(self.GUI.osc_upper_bound.get())
        self.callibration_frequency = float(self.GUI.osc_callibration_entry.get())
        self.callibration_frequency_old = self.callibration_frequency
        bound_range = upper_bound - lower_bound
        search_regiion = int(bound_range / 3)

        bounded_data_range = self.processed_detector_signal[lower_bound:upper_bound]
        bounded_data_range_left = self.processed_detector_signal[lower_bound:lower_bound+search_regiion]
        bounded_data_range_right = self.processed_detector_signal[lower_bound+(2*search_regiion):upper_bound]

        minimum_storage[0] = bounded_data_range_left.argmin
        minimum_storage[1] = bounded_data_range.argmin
        minimum_storage[2] = bounded_data_range_right.argmin + (2*search_regiion)

        self.osc_callibration_factor = - (2*self.callibration_frequency) / (minimum_storage[2]-minimum_storage[0])
        self.osc_zero_ref = minimum_storage[1]+lower_bound

        self.osc_x_axis = self.osc_callibration_factor * (numpy.array(range(len(self.processed_detector_signal)))-self.osc_zero_ref)

        self.GUI.osc_callibration_button.configure(text="Revert (#)", command=lambda: self.gui_callibration_button_pressed_revert())

    def gui_callibration_button_pressed_revert(self):
        self.osc_callibration_factor = 1
        self.GUI.osc_callibration_button.configure(text="Callibrate (MHz)", command=lambda: self.gui_callibration_button_pressed())

    def gui_radio_button_pressed(self):
        self.GUI.anc_channel_offsetVoltage_field.set("Channel"+str(self.ANC_Device[self.GUI.anc_active_index.get()].port)+"Offset Voltage: "+str(self.ANC_Device[self.GUI.anc_active_index.get()].offset_voltage))
        self.GUI.anc_channel_stepVoltage_field.set("Channel"+str(self.ANC_Device[self.GUI.anc_active_index.get()].port)+"Step Voltage: "+str(self.ANC_Device[self.GUI.anc_active_index.get()].step_voltage))
        self.GUI.anc_channel_stepFrequency_field.set("Channel"+str(self.ANC_Device[self.GUI.anc_active_index.get()].port)+"Step Frequency: "+str(self.ANC_Device[self.GUI.anc_active_index.get()].step_frequency))

    def gui_anc_update_button_pressed(self):
        self.ANC_Device[self.GUI.anc_active_index.get()].write(self.GUI.anc_update_amount.get(), self.GUI.anc_variable.get())
        self.GUI.anc_variable_update_entry.delete(0, END)
        self.gui_anc_channel_radioButton_pressed()

    def gui_callibrate_execute_button_pressed(self):
        self.gui_callibration_button_pressed
        self.gui_execute_button_pressed

    def gui_anc_channel_radioButton_pressed(self):
        anc_active_index = int(self.GUI.anc_active_index.get())
        anc_active_offsetVoltage = self.ANC_Device[anc_active_index].offset_voltage[0]
        anc_active_stepVoltage = self.ANC_Device[anc_active_index].step_voltage[0]
        anc_active_stepFrequency = self.ANC_Device[anc_active_index].step_frequency[0]

        anc_active_index_label = str(anc_active_index + 4)
        self.GUI.anc_channel_offsetVoltage_field.set("Channel "+anc_active_index_label+" Offset Voltage: "+str(anc_active_offsetVoltage))
        self.GUI.anc_channel_stepVoltage_field.set("Channel "+anc_active_index_label+" Step Voltage: "+str(anc_active_stepVoltage))
        self.GUI.anc_channel_stepFrequency_field.set("Channel "+anc_active_index_label+" Step Frequency: "+str(anc_active_stepFrequency))

    # def gui_callibration_button_pressed(self):
    #     lower_bound = int(self.GUI.osc_lower_bound.get())
    #     upper_bound = int(self.GUI.osc_upper_bound.get())
    #     frequency = float(self.GUI.osc_callibration_entry.get())
    #     y = self.processed_detector_signal[lower_bound:upper_bound]
    #     x= range(len(y))
    #     peaks_position = self.peaksfinder(x,y)
    #     ordenador_peaks = sorted(peaks_position)
    #     self.osc_callibration_factor = - (2*frequency) / (ordenador_peaks[2]-ordenador_peaks[0])
    #     self.osc_zero_ref = ordenador_peaks[1]+lower_bound
    # 
    def collect_signal(self):
        self.Detector_Device.read_from_channel()
        self.process_data()

    def run(self):
        self.Detector_Device.read_from_channel()
        self.process_data()

        self.GUI.a.clear()
        self.GUI.a.plot(self.processed_detector_signal)
        self.GUI.a.plot(self.processed_detector_trigger)
        self.GUI.canvas.show()
        self.GUI.master.after(0, self.run)


if __name__ == '__main__':
    context = Setup()
    root = Tk()
    view = ProgramGUI(root)
    controller = Controller(context.Nanocube, context.ANC_Channel_List, context.Detector, view)
    root.mainloop()