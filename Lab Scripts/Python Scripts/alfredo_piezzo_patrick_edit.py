# Piezo Sweep Program, initial author Alfredo Rueda, re-written by Patrick Devane. Last modified 02/11/18

import os
import sys
import math
import telnetlib
import time
from datetime import datetime
import PIL.Image
import PIL.ImageTk
import Tkinter as Tk
import numpy as np

import matplotlib.pyplot as plt
import visa
import win32com.client
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg,
                                               NavigationToolbar2TkAgg)
from matplotlib.figure import Figure

ANC_LIST = { # Add to this dict the name and IP of the ANC
        "Patrick ANC":"172.22.2.153",
        "GHz Comb ANC":"172.22.2.155",
        "Plastic Curtain ANC":"172.22.2.154"}
        
SAVE_PATH = "C:/Users/ROLab/Desktop/Alfredo Program Lab Data/Patrick/23-02-19/"
# SAVE_PATH = "C:/Users/ROLab/Desktop/Alfredo Program Lab Data/" # Make sure this is uncommented when in use

class Model(object):

        def __init__(self, HOST):

                # Oscilloscope Connection
                self.scope=win32com.client.Dispatch("LeCroy.ActiveDSOCtrl.1")  #creates instance of the ActiveDSO control
                self.scope.MakeConnection("IP:172.22.2.152")

                # Signal Modulator Connection
                self.rm = visa.ResourceManager()
                self.generator = self.rm.open_resource('GPIB0::28::INSTR')

                # ANC300 Connection
                PORT = '7230'
                self.tn = telnetlib.Telnet(HOST,PORT)
                self.tn.read_until("Authorization code: ")
                self.tn.write("123456\n")
                self.tn.read_until("Authorization success")
                print ("ANC Connection successfull")
                
        def anc_step(self,step_direction,anc_port):
                self.tn.write("step"+str(step_direction)+" "+str(anc_port)+" 1\n")
            
        def anc_set(self,anc_field,anc_port,anc_value):
                self.tn.write("set"+str(anc_field)+" "+str(anc_port)+" "+str(anc_value)+"\n")

        def program_close(self):
                self.scope.Disconnect()
                self.generator.close()
                self.rm.close()
                print("Disconnected")
                

class GUI(Tk.Frame):

        def __init__(self, window):

                self.window = window
                self.window.title("Attocube Program")
                
                self.anc_select()

                # Variables
                self.volal01 = Tk.StringVar()
                self.active_port = Tk.IntVar()
                self.step_direction = Tk.IntVar()
                self.nametosave = Tk.StringVar()
                self.piezostep = Tk.StringVar()
                self.freqsource = Tk.StringVar()
                self.mincal = Tk.StringVar()
                self.maxcal = Tk.StringVar()
                self.powermin = Tk.StringVar()
                self.powermax = Tk.StringVar()
                self.powerstep = Tk.StringVar()
                self.freqal01 = Tk.StringVar()
                self.volt = Tk.StringVar()
                self.sweeplimitcheck = Tk.IntVar()
                self.sweeplimit = Tk.DoubleVar()

                self.volal01.set(str(0.1))
                self.step_direction.set(1)
                self.nametosave.set("file_name")
                self.piezostep.set(str(0.1))
                self.freqal01.set(str(120))
                self.volt.set(str(12))
                self.sweeplimitcheck.set(0)
                
                # Elements
                self.button1 = Tk.Button(window,text="Set Offset Voltage (V)", width='20')
                self.button2 = Tk.Button(window,text="Save Spectrum (.dat)", width='20')
                self.button3 = Tk.Button(window,text="Save Spectrum+Step", width='20')
                self.button4 = Tk.Button(window,text="Set EOM Frequency (MHz)", width='20')
                self.button5 = Tk.Button(window,text="EOM On", width='20')
                self.button6 = Tk.Button(window,text="EOM Off", width='20')
                self.button8 = Tk.Button(window,text="Refresh Oscilloscope", width='20')
                self.button9 = Tk.Button(window,text="Calibrate X-Axis", width='20')
                self.button10 = Tk.Button(window,text="Setpower (dBm)", width='20')
                self.button11 = Tk.Button(window,text="Power Sweep", width='20')
                self.button12 = Tk.Button(window,text="Refresh ANC Ports", width='20')
                self.button13 = Tk.Button(window,text="Reset Callibration", width='20')
                self.button14 = Tk.Button(window,text="Help", width='20')
                self.button15 = Tk.Button(window,text="Set Frequency (Hz)", width='20')
                self.button16 = Tk.Button(window,text="Set Voltage (V)", width='20')
                self.button17 = Tk.Button(window,text="Step +", width='20')
                self.button18 = Tk.Button(window,text="Step -", width='20')
                self.button19 = Tk.Button(window,text="Save Spectrum (.jpeg)", width='20')
                self.button20 = Tk.Button(window,text="Start offset sweep", width='20')
                self.button21 = Tk.Button(window,text="Stop offset sweep", width='20')

                self.tempcurrentDisplay1 = Tk.Entry(window,textvariable = self.volal01, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay2 = Tk.Entry(window,textvariable = self.nametosave, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay3 = Tk.Entry(window,textvariable = self.piezostep, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay4 = Tk.Entry(window,textvariable = self.freqsource, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay6 = Tk.Entry(window,textvariable = self.mincal, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay7 = Tk.Entry(window,textvariable = self.maxcal, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay8 = Tk.Entry(window,textvariable = self.powermin, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay9 = Tk.Entry(window,textvariable = self.powermax, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay10 = Tk.Entry(window,textvariable = self.powerstep, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay11 = Tk.Entry(window,textvariable = self.freqal01, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay12 = Tk.Entry(window,textvariable = self.volt, bd= 10, insertwidth= 1 , font= 30)
                self.tempcurrentDisplay13 = Tk.Entry(window,textvariable = self.sweeplimit, bd= 10, insertwidth= 1 , font= 30)
                                
                self.templabel2 = Tk.Label(window, text = "Lower Callibration Bound",font= 30)
                self.templabel3 = Tk.Label(window, text = "Upper Callibration Bound",font= 30)
                self.templabel4 = Tk.Label(window, text = "Final Power (dBm)",font= 30)
                self.templabel5 = Tk.Label(window, text = "Step Power (dBm)",font= 30)
                self.templabel6 = Tk.Label(window, text = "Connected to: ",font= 30)
                self.templabel7 = Tk.Label(window, text = "ANC Step Direction",font= 30)
                self.templabel8 = Tk.Label(window, text = "Saved File Name",font= 30)
                
                self.checkbutton1 = Tk.Checkbutton(window, text="Sweep Limit", variable=self.sweeplimitcheck)
                
                self.radiobutton_step_direction1 = Tk.Radiobutton(window, text="Up", variable=self.step_direction, value=1)
                self.radiobutton_step_direction2 = Tk.Radiobutton(window, text="Down", variable=self.step_direction, value=-1)
                
                self.port_radiobuttons = []
                anc_channel_number = 7 # number of channels on ANC
                for i in range(anc_channel_number): 
                        self.port_radiobuttons.append(Tk.Radiobutton(window, text="Port "+str(i+1), variable=self.active_port, value=i+1))
                        self.port_radiobuttons[i].grid(row=i+1,column=3)
                
                # Grid
                self.button1.grid(row=13, column=3)
                self.button2.grid(row=2, column=8)
                self.button3.grid(row=3, column=8)
                self.button4.grid(row=0, column=4)
                self.button5.grid(row=2, column=4)
                self.button6.grid(row=3, column=4)
                self.button8.grid(row=0, column=8)
                self.button9.grid(row=8, column=0)
                self.button10.grid(row=5, column=4)
                self.button11.grid(row=4, column=4)
                self.button12.grid(row=8, column=3)
                self.button13.grid(row=9, column=0)
                self.button14.grid(row=15, column=8)
                self.button15.grid(row=9, column=3)
                self.button16.grid(row=11, column=3)
                self.button17.grid(row=15, column=3)
                self.button18.grid(row=16, column=3)
                self.button19.grid(row=1, column=8)
                self.button20.grid(row=10, column=8)
                self.button21.grid(row=11, column=8)

                self.tempcurrentDisplay1.grid(row=14, column=3, columnspan=1, rowspan=1)
                self.tempcurrentDisplay2.grid(row=9, column=8, columnspan=1, rowspan=1)
                self.tempcurrentDisplay3.grid(row=4, column=8, columnspan=1, rowspan=1)
                self.tempcurrentDisplay4.grid(row=1, column=4, columnspan=1, rowspan=1)
                self.tempcurrentDisplay6.grid(row=8, column=1, columnspan=1, rowspan=1)
                self.tempcurrentDisplay7.grid(row=8, column=2, columnspan=1, rowspan=1)
                self.tempcurrentDisplay8.grid(row=6, column=4, columnspan=1, rowspan=1)
                self.tempcurrentDisplay9.grid(row=8, column=4, columnspan=1, rowspan=1)
                self.tempcurrentDisplay10.grid(row=10, column=4, columnspan=1, rowspan=1)
                self.tempcurrentDisplay11.grid(row=10, column=3, columnspan=1, rowspan=1)
                self.tempcurrentDisplay12.grid(row=12, column=3, columnspan=1, rowspan=1)
                self.tempcurrentDisplay13.grid(row=13, column=8, columnspan=1, rowspan=1)

                self.templabel2.grid(row=9, column=1, padx=(10,0), pady=(10,10))
                self.templabel3.grid(row=9, column=2, padx=(10,0), pady=(10,10))
                self.templabel4.grid(row=7, column=4, columnspan=1, rowspan=1)
                self.templabel5.grid(row=9, column=4, columnspan=1, rowspan=1)
                self.templabel6.grid(row=0, column=3, columnspan=1, rowspan=1)
                self.templabel7.grid(row=5, column=8, columnspan=1, rowspan=1)
                self.templabel8.grid(row=8, column=8, columnspan=1, rowspan=1)
                
                self.checkbutton1.grid(row=12, column=8)
                
                self.radiobutton_step_direction1.grid(row=6, column=8)
                self.radiobutton_step_direction2.grid(row=7, column=8)

                #GUI
                self.f = Figure(figsize=(9,5), dpi=100)
                self.a = self.f.add_subplot(111)
                self.canvas = FigureCanvasTkAgg(self.f, window)
                self.canvas.get_tk_widget().grid(row=0, column=0,columnspan=3, rowspan=8)

                self.window.withdraw()

        def anc_select(self):
                self.ANCHOST = Tk.StringVar()
                self.ANCHOST.set("172.22.2.153")

                self.anc_select_window = Tk.Toplevel(self.window)
                self.anc_select_window.title('ANC Selection')

                anc_select_label = Tk.Label(self.anc_select_window, text='Select the ANC to use:')
                self.anc_select_button = Tk.Button(self.anc_select_window, text='Select')

                anc_radiobuttons = []

                j=0
                for i in ANC_LIST:
                        anc_radiobuttons.append(Tk.Radiobutton(self.anc_select_window, text=str(i), variable=self.ANCHOST, value=ANC_LIST.get(str(i))))
                        anc_radiobuttons[j].grid(row=1,column=j)
                        j+=1

                anc_select_label.grid(row=0,column=0, columnspan=len(anc_radiobuttons))
                self.anc_select_button.grid(row=2,column=0,columnspan=len(anc_radiobuttons))
                
        def help(self):
                self.help_window = Tk.Toplevel(self.window)
                self.help_window.title('Help')
                self.im = PIL.Image.open("C:/Users/ROLab/Documents/Bitbucket/python-piezzocontrol/Python Scripts/program_screenshot_annotated.png")
                self.image = PIL.ImageTk.PhotoImage(self.im)
                self.help_image = Tk.Label(self.help_window, image=self.image)
                self.help_image.pack()

        def on_closing(self):
                self.anc_select_window.destroy()
                self.window.deiconify()


class Controller(object): # Processes

        def __init__(self, GUI):
                
                self.GUI = GUI

                self.cont= np.zeros((10,), dtype=np.float64)
                self.cont[0]=0.1 # here is the starting voltage #
                self.cont[2]=5
                
                self.offset_sweep_is_active = False
                
                self.numberofpoints = 10000

                # Reassigning GUI
                self.GUI.button1.configure(command=lambda:self.al01vol())
                self.GUI.button2.configure(command=lambda:self.save_spectrum(isStepped=False))
                self.GUI.button3.configure(command=lambda:self.save_spectrum_and_step(clicked=True))
                self.GUI.button4.configure(command=lambda:self.Model.generator.write(":SOUR:FREQ:CW "+self.GUI.freqsource.get()+"MHz"))
                self.GUI.button5.configure(command=lambda:self.Model.generator.write(":OUTP:STAT ON"))
                self.GUI.button6.configure(command=lambda:self.Model.generator.write(":OUTP:STAT OFF"))
                self.GUI.button8.configure(command=lambda:self.refresh(clicked=True))
                self.GUI.button9.configure(command=lambda:self.calibration())
                self.GUI.button10.configure(command=lambda:self.Model.generator.write(":SOUR:POW:LEV:IMM:AMPL "+self.GUI.powermin.get()))
                self.GUI.button11.configure(command=lambda:self.sourcepowsweep())
                self.GUI.button12.configure(command=lambda:self.anc_check_channel_modes())
                self.GUI.button13.configure(command=lambda:self.revert_callibration_button_pressed())
                self.GUI.button14.configure(command=lambda:self.GUI.help())
                self.GUI.button15.configure(command=lambda:self.Model.anc_set("f",self.GUI.active_port.get(),self.GUI.volal01.get()))
                self.GUI.button16.configure(command=lambda:self.Model.anc_set("v",self.GUI.active_port.get(),self.GUI.volt.get()))
                self.GUI.button17.configure(command=lambda:self.anc_step_button_pressed(step_direction="u"))
                self.GUI.button18.configure(command=lambda:self.anc_step_button_pressed(step_direction="d"))
                self.GUI.button19.configure(command=lambda:self.save_spectrum_image())
                self.GUI.button20.configure(command=lambda:self.start_offset_sweep())
                self.GUI.button21.configure(command=lambda:self.stop_offset_sweep(), state=Tk.DISABLED)
                
                self.GUI.anc_select_button.configure(command=lambda:self.anc_select_button_pressed())
                
                self.GUI.window.bind('<Escape>', self.stop_offset_sweep)
                self.GUI.anc_select_window.protocol("WM_DELETE_WINDOW", lambda:self.anc_select_button_pressed())
                self.GUI.window.protocol("WM_DELETE_WINDOW", lambda:self.on_closing())
                
        def anc_select_button_pressed(self):
                self.Model = Model(self.GUI.ANCHOST.get())

                self.waveform = self.Model.scope.GetScaledWaveform("C1",self.numberofpoints,0)
                self.xaxis = range(len(self.waveform))
                self.osc_plot(self.xaxis,self.waveform)
                
                self.GUI.templabel6.configure(text="Connected to: %s on %s" %(ANC_LIST.keys()[ANC_LIST.values().index(self.GUI.ANCHOST.get())],self.GUI.ANCHOST.get()),fg='dark green' )
                self.GUI.anc_select_window.destroy()
                self.GUI.window.deiconify()
                
                self.anc_check_channel_modes()
                
        def revert_callibration_button_pressed(self):
                self.xaxis = range(len(self.waveform))
                self.osc_plot(self.xaxis,self.waveform)
                
        def anc_check_channel_modes(self):
                for i in xrange(len(self.GUI.port_radiobuttons)):
                        self.Model.tn.write("getm %s \n" %str(i+1))
                        mode_check = self.Model.tn.read_until("OK", 0.1 )
                        output = mode_check.splitlines()[-2]
                        anc_mode = output[-3:]
                        
                        if anc_mode == 'ROR':
                            self.GUI.port_radiobuttons[i].configure(state=Tk.DISABLED)
                        elif anc_mode == 'gnd':
                            self.GUI.port_radiobuttons[i].configure(fg="red")
                        elif anc_mode == 'off':
                            self.GUI.port_radiobuttons[i].configure(fg="orange")
                        else:
                            self.GUI.port_radiobuttons[i].configure(fg="black")

        def al01vol(self):
                self.Model.anc_set("a",self.GUI.active_port.get(),self.GUI.volal01.get())
                self.cont[0]=float(self.GUI.volal01.get())
                
        def anc_step_button_pressed(self, step_direction):
                self.Model.anc_step(step_direction, self.GUI.active_port.get())
                self.refresh()
            
        def save_spectrum_and_step(self, clicked=False):
                if clicked:
                        self.GUI.button3.focus_set()
                
                self.refresh()
                self.save_spectrum(isStepped=True)
                
                self.cont[0]=self.cont[0] + math.copysign(float(self.GUI.piezostep.get()), int(self.GUI.step_direction.get()))  # here is the voltage step #     
                self.GUI.volal01.set(str(self.cont[0]))
                
                self.Model.anc_set("a",self.GUI.active_port.get(),self.GUI.volal01.get())
                
                if self.offset_sweep_is_active:
                                        
                        if self.cont[0] <= self.low_offset_sweep_limit or self.cont[0] >= self.high_offset_sweep_limit:
                                self.offset_sweep_is_active = False
                                print('Attocube offset value is beyond limit, stopping sweep...')
                                self.GUI.button20.configure(state=Tk.NORMAL)
                                self.GUI.button21.configure(state=Tk.DISABLED)
                        else:
                                self.GUI.window.after(100, self.save_spectrum_and_step)
                
        def save_spectrum(self, isStepped):
                nb_str = ""
                if isStepped:
                        nb_str = "_" + str(int(round(10*(self.cont[0])))) + "V"        
                save_suffix = str(nb_str) +'.dat'
                
                save_file = self.file_name_check(self.GUI.nametosave.get(), save_suffix)
                
                np.savetxt(save_file,zip(self.xaxis,self.waveform), fmt="%f %f")
                print("saved: %s" %save_file)
                
        def save_spectrum_image(self):
                save_suffix = '_spectrum.jpeg'
                save_file = self.file_name_check(self.GUI.nametosave.get(), save_suffix)
                self.GUI.f.savefig(save_file, bbox_inches='tight')
                print("saved: %s" %save_file)
                    
        def file_name_check(self, save_name, save_suffix):            
                name_to_check = SAVE_PATH + save_name + save_suffix
                if not os.path.isfile(name_to_check):
                    return name_to_check
                else:
                    save_name_new = save_name + "_new"
                    print("%s already exists! Changing name to %s" %(save_name,save_name_new))
                    self.GUI.nametosave.set(save_name_new)
                    return self.file_name_check(save_name_new, save_suffix)
                    
        def start_offset_sweep(self):
                self.offset_sweep_is_active = True
                self.GUI.button20.configure(state=Tk.DISABLED)
                self.GUI.button21.configure(state=Tk.NORMAL)
                
                self.low_offset_sweep_limit = 0.1
                self.high_offset_sweep_limit = 100
                
                if self.GUI.sweeplimitcheck.get():
                        offsetlimit = self.GUI.sweeplimit.get()
                        direction = self.GUI.step_direction.get()

                        if direction == 1 and offsetlimit < self.high_offset_sweep_limit:
                                self.high_offset_sweep_limit = offsetlimit
                                
                        if direction == -1 and offsetlimit > self.low_offset_sweep_limit:
                                self.low_offset_sweep_limit = offsetlimit
                
                self.refresh()
                self.save_spectrum_and_step()
                
        def stop_offset_sweep(self, event=None):
                self.offset_sweep_is_active = False
                self.GUI.button20.configure(state=Tk.NORMAL)
                self.GUI.button21.configure(state=Tk.DISABLED)

        def sourcepowsweep(self):
                pownow=float(self.GUI.powermin.get())
                limit=float(self.GUI.powermax.get())
                step=float(self.GUI.powerstep.get())
                stepnumber=int(1)
                while (limit>=pownow):
                        self.Model.generator.write(":SOUR:POW:LEV:IMM:AMPL "+str(pownow))
                        time.sleep(1)
                        self.refresh()
                        np.savetxt(self.GUI.nametosave.get()+'_'+str(stepnumber) +'.dat',zip(self.xaxis,self.waveform), fmt="%f %f") #write here the number that you need
                        stepnumber+=1
                        pownow=pownow+step

        def calibration(self):
                position=self.peaksfinder(self.xaxis[int(float(self.GUI.mincal.get())):int(float(self.GUI.maxcal.get()))],self.waveform[int(float(self.GUI.mincal.get())):int(float(self.GUI.maxcal.get()))])
                ordenador=sorted(position)
                
                self.xaxis=(2*float(self.GUI.freqsource.get())/(ordenador[2]-ordenador[0]) )*(np.array(self.xaxis)-ordenador[1]-float(self.GUI.mincal.get()))
                self.osc_plot(self.xaxis,self.waveform)

        def refresh(self, clicked=False):
                if clicked:
                        self.GUI.button8.focus_set()
                self.waveform=self.Model.scope.GetScaledWaveform("C1",self.numberofpoints,0)
                self.osc_plot(self.xaxis,self.waveform)

        def peaksfinder(self,x,y):
                totalpoints=len(y)
                radi=int(round(totalpoints/20)-1)
                flag=1
                z=len(y)
                n=0
                peaks=np.zeros(int(round(totalpoints/4)))
                for i in range(radi,z-radi):
                        if y[i]<y[i+1]:
                                j=1
                                ru=0
                                while (j<=radi):
                                        if ((y[i+j]<y[i]) or (y[i-j]<=y[i])):
                                                j=radi+10
                                                ru=1
                                        j=j+1
                                if (ru==0):
                                        peaks[n]=i
                                        n=n+1
                while (flag>0):
                        flag=-1
                        for t in range(0,2*n):
                                if (y[int(peaks[t])]>y[int(peaks[t+1])]):
                                        R=peaks[t]
                                        peaks[t]=peaks[t+1]
                                        peaks[t+1]=R
                                        flag=1
                return peaks[0:3]

        def lorentz(self,p,x):
                return p[3]- p[2]*0.5*p[1] / ((p[0]-x )**2 + 0.25*p[1]**2)

        def errorfunc(self,p,x,y):
                return self.lorentz(p,x)-y

        def osc_plot(self,xaxis,waveform):
                self.GUI.a.clear()
                self.GUI.a.plot(xaxis,waveform)
                self.GUI.canvas.show()

        def on_closing(self):
                self.Model.program_close()
                self.GUI.window.destroy()


if __name__ == '__main__':
        root = Tk.Tk()
        view = GUI(root)
        controller = Controller(view)
        root.mainloop()