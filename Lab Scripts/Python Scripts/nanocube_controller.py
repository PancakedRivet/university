# -*- coding: utf-8 -*-
"""
Created on Mon November 11 14:10:09 2018

@author: Patrick Devane
"""
import numpy
import math
# from Tkinter import *
import Tkinter as Tk
import time
import os
import sys

import nidaqmx  # Manages voltage to breakout box
from nidaqmx.constants import AcquisitionType, Edge
from instrumental import instrument, list_instruments  # For daq 

clear = lambda: os.system('cls')
clear()

# Initial values
NANO_STARTING_DISTANCE = 0  # nanometer units 

class Nanocube_Device(object): # reads and writes to nanocibe only

    def __init__(self, port, distance):
        self.port = port
        self.distance = distance
        self.port.write(str(self.distance / 10) + 'mV')  # Writing initial voltage

    def write_to_channel(self, difference, step_size):
        self.distance += math.copysign(step_size, difference)
        milivoltage = self.distance / 10
        self.port.write(str(milivoltage) + 'mV')


class ProgramGUI(object): # displays the data only
    def __init__(self, master):
        self.master = master
        self.master.title("Nanocube Controller")

        self.daq_step_size = Tk.DoubleVar()
        self.daq_execute_entry_value = Tk.DoubleVar()
        self.daq_distance_label_field = Tk.StringVar()

        self.daq_step_size.set(0.1)
        self.daq_distance_label_field.set("Distance (microns): " + str(NANO_STARTING_DISTANCE))

        self.daq_execute_button = Tk.Button(master, text="Execute")
        self.daq_execute_entry = Tk.Entry(master, textvariable=self.daq_execute_entry_value, width=5)
        self.daq_step_size_entry = Tk.Entry(master, textvariable=self.daq_step_size, width=5)
        self.daq_distance_label = Tk.Label(master, textvariable=self.daq_distance_label_field)
        self.daq_execute_label = Tk.Label(master, text="Move To (microns)")
        self.daq_step_size_label = Tk.Label(master, text="Nanocube Step Size (microns)")
        
        self.daq_execute_button.grid(row=0, column=1)
        self.daq_execute_entry.grid(row=2, column=1)
        self.daq_step_size_entry.grid(row=1, column=1)
        self.daq_distance_label.grid(row=0, column=0)
        self.daq_execute_label.grid(row=2, column=0)
        self.daq_step_size_label.grid(row=1, column=0)


class Controller(object): # processes data only

    def __init__(self, Nanocube_Device, GUI):
        self.Nanocube_Device = Nanocube_Device
        self.GUI = GUI
        self.GUI.daq_execute_button.configure(command=lambda: self.gui_execute_button_pressed())
        self.GUI.master.protocol("WM_DELETE_WINDOW", lambda:self.on_closing())

    def nanocube_movement(self, distance):
        self.GUI.daq_execute_button.configure(state=Tk.DISABLED)

        final_distance = distance
        initial_distance = self.Nanocube_Device.distance
        step_size = float(self.GUI.daq_step_size.get()) * 1000
        difference = final_distance - initial_distance
        number_of_steps = difference / step_size

        for dummy in xrange(abs(int(number_of_steps))):
            self.Nanocube_Device.write_to_channel(difference, step_size)
            time.sleep(0.1) # buffering sample
        
        self.GUI.daq_execute_button.configure(state=Tk.NORMAL)
        print("Distace changed to %s microns \n") %(float(self.Nanocube_Device.distance)/1000)
    
    def gui_execute_button_pressed(self):
        distance = float(self.GUI.daq_execute_entry_value.get()) * 1000

        if distance >= 0 and distance <= 100000:
            self.nanocube_movement(distance)
            self.GUI.daq_distance_label_field.set("Distance (microns): %s" % (float(self.Nanocube_Device.distance)/1000))
            self.GUI.daq_execute_entry.delete(0, Tk.END)
        else:
            print("%s microns is outside the safe range for the Nanocube." % (float(self.Nanocube_Device.distance)/1000))

    def on_closing(self):
        self.nanocube_movement(NANO_STARTING_DISTANCE)
        if self.Nanocube_Device.distance != NANO_STARTING_DISTANCE:
            self.GUI.daq_step_size.set(0.1)
            self.nanocube_movement(NANO_STARTING_DISTANCE)
        self.GUI.master.destroy()
        print("Program Closed")


if __name__ == '__main__':
    Nanocube = Nanocube_Device(instrument('NIDAQ').ao0, NANO_STARTING_DISTANCE)
    root = Tk.Tk()
    view = ProgramGUI(root)
    controller = Controller(Nanocube, view) # comment when GUI testing
    root.mainloop()