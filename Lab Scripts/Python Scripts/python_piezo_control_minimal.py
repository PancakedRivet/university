# -*- coding: utf-8 -*-
"""
Created on Wed May 30 17:32:54 2018

@author: devpa499
"""
import numpy
import math
from matplotlib import pyplot as plt
from datetime import datetime

from instrumental import instrument, list_instruments  # For daq 
import nidaqmx  # Manages voltage

# Initial values (mV)
STARTING_VOLTAGE = 0  # default is 0
STEP_SIZE = 100  # default is 100
SAVE_INTERVAL = 1000  # default is 1000


class Context(object): # Initialises

    def __init__(self):
        print 'Starting Voltage: %sV' % (float(STARTING_VOLTAGE) / 1000)
        print 'Starting Step Size: %sV' % (float(STEP_SIZE) / 1000)
        print 'Starting Save Interval: %sV' % (float(SAVE_INTERVAL) / 1000)

        daq = instrument('NIDAQ')

        signal = numpy.zeros((30000,), dtype=numpy.float64)
        trigger = numpy.zeros((30000,), dtype=numpy.float64)

        self.nanocube = Device(daq.ao0, STARTING_VOLTAGE, STEP_SIZE, signal, trigger)
        self.nanocube_data = DataManager(self.nanocube, signal, SAVE_INTERVAL, save_file_number=0, last_saved_voltage=STARTING_VOLTAGE)


class Device(object): # ONLY READS AND WRITES TO CHANNELS

    def __init__(self, port, voltage, step_size, signal, trigger):
        self.port = port
        self.voltage = voltage
        self.step_size = step_size
        self.signal = signal
        self.trigger = trigger

        self.port.write(str(self.voltage) + 'mV')  # Writing initial voltage


    def write_to_channel(self, difference):
        self.voltage += math.copysign(self.step_size, difference)
        self.port.write(str(self.voltage) + 'mV')

    def read_from_channel(self):
        with nidaqmx.Task() as task:
            task.ai_channels.add_ai_voltage_chan("Dev1/ai3:4")
            task.timing.cfg_samp_clk_timing(300000, active_edge=Edge.RISING, sample_mode=AcquisitionType.FINITE, samps_per_chan=len(self.signal))
            (self.signal, self.trigger) = task.read(number_of_samples_per_channel=len(self.signal))


class DataManager(object): # ONLY PROCESSES THE DATA - ATTACHED TO A SINGLE DEVICE PER INSTANCE

    def __init__(self, device, processed_data, save_interval, save_file_number, last_saved_voltage):
        self.device = device
        self.processed_data = processed_data
        self.save_interval = save_interval
        self.save_file_number = save_file_number
        self.last_saved_voltage = last_saved_voltage
    

    def process_data(self):
        trigger_edge = self.trigger_edge_finder()
        self.processed_data = numpy.transpose(
            self.device.signal[trigger_edge:trigger_edge + len(self.device.signal)/2])  # Re-casting signal to column

        self.device.trigger = self.device.trigger[trigger_edge:trigger_edge + len(self.device.signal)/2]

    def save_data(self, data):
        header = '%s' %(float(self.device.voltage) / 100) + " microns " + str(datetime.now())
        numpy.savetxt('data_%s.dat' % self.save_file_number,
                      data,
                      header=header)

        print "Saved data_%s.dat" % self.save_file_number
        self.last_saved_voltage = self.device.voltage
        self.save_file_number += 1

    def save_number_reset(self):
        self.save_file_number = 0

    def trigger_edge_finder(self):
        index = 0
        while ((self.device.trigger[index + 1] - self.device.trigger[index]) < 1):
            index += 1
        return index

    def executing_voltage_change(self, final, should_save):
        difference = final - self.device.voltage

        if should_save:
            self.save_data(self.processed_data)  # Saving pre-sweep traced

        while self.device.voltage != final:
            self.device.write_to_channel(difference)
            if should_save and abs(self.device.voltage - self.last_saved_voltage) >= self.save_interval:
                self.save_data(self.processed_data)
        
        self.save_number_reset()
        print "Distace changed to %s microns" % (float(self.device.voltage) / 100)


def get_input():
    while True:
        try:
            val = int(raw_input("Enter the distance (microns) to move to: "))
        except ValueError:
            print('That is not a valid number.')
            continue

        if val < 0 or val > 100:
            if val == 123:
                print "Program Shutting Down"
                break
            print "%s microns is outside the safe range for the Nanocube." %val
        else:
            print "Moving to: %s microns" %val
            break

    voltage = val*100
    return voltage

## Running the program
if __name__ == '__main__':
    context = Context()

    while True:
        user_input = get_input()
        if user_input != 12300:  # The exit command
            context.nanocube_data.executing_voltage_change(user_input, should_save=True)
        else:
            # Resetting the voltage of the device
            context.nanocube_data.executing_voltage_change(STARTING_VOLTAGE, should_save=False)
            break
