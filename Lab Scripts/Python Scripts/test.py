class Material(object):
    def __init__(self, wavelength, name, website, equation, \
        a1=None, a2=None, a3=None, a4=None, \
        b1=None, b2=None, b3=None, b4=None, \
        c1=None, c2=None, c3=None, c4=None, \
        d1=None, d2=None, d3=None, d4=None):

        self.wavelength = wavelength
        self.name = name
        self.website = website
        self.equation = equation

        if a1 is not None:
            self.a1 = a1
        if a2 is not None:
            self.a2 = a2
        if a3 is not None:
            self.a3 = a3
        if a4 is not None:
            self.a4 = a4
        if b1 is not None:
            self.b1 = b1
        if b2 is not None:
            self.b2 = b2
        if b3 is not None:
            self.b3 = b3
        if b4 is not None:
            self.b4 = b4
        if c1 is not None:
            self.c1 = c1
        if c2 is not None:
            self.c2 = c2
        if c3 is not None:
            self.c3 = c3
        if c4 is not None:
            self.c4 = c4
        if d1 is not None:
            self.d1 = d1
        if a2 is not None:
            self.d2 = d2
        if d3 is not None:
            self.d3 = d3
        if d4 is not None:
            self.d4 = d4

prism_material = Material(1.550, "LN", "Check","a1[0]+a1[1]")
print(prism_material.name)