# Main Script for Resonant Optics Lab
# Written for Python 2.7 by Patrick Devane

import lab_equipment.attocube as atto
import lab_equipment.eom as eom
import lab_equipment.oscilloscope as osc

attocube1 = atto.Attocube(anc_ip="172.22.2.153", anc_channel=5)

attocube1_mode = attocube1.anc_get("f")
print(attocube1_mode)