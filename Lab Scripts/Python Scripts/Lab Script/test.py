class Test(object):
    def __init__(self, value):
        value_list = []
        value_list.append(value)
        print(value_list)

        self.value = value

    def print_value(self):
        print(self.value)

test1 = Test(1)
test2 = Test(2)

test1.print_value()
test2.print_value()