# The Oscilloscope file. Add/edit anything related to the oscilloscope in this file.
# Written for Python 2.7 by Patrick Devane

import win32com.client

OSC_IP_LIST = []

def osc_setup(osc_ip):
        scope = win32com.client.Dispatch("LeCroy.ActiveDSOCtrl.1")  #creates instance of the ActiveDSO control
        scope.MakeConnection(osc_ip)
        print("Oscilloscope Connected")
        return scope

class Oscilloscope(object):

    def __init__(self, osc_ip, number_of_points, osc_channel):
        
        self.osc_ip = "IP:" + str(osc_ip)

        if self.osc_ip not in OSC_IP_LIST:
            self.scope = osc_setup(self.osc_ip)
        
        self.number_of_points = number_of_points
        self.oscilloscope_channel = "C" + str(osc_channel)

    def read_data(self):
        self.waveform = self.scope.GetScaledWaveform(self.oscilloscope_channel, self.number_of_points, 0)
        return self.waveform

    def close(self):
        self.scope.Disconnect()
        print("Oscilloscope Disconnected")