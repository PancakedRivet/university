# The Signal Modulator file. Add/edit anything related to the EOM in this file.
# Written for Python 2.7 by Patrick Devane

import visa

class EOM(object):

    def __init__(self):

        # Signal Modulator Connection
        self.rm = visa.ResourceManager()
        self.generator = self.rm.open_resource('GPIB0::28::INSTR')
        print("EOM Connected")

    def sourcepowsweep(self):
        pownow=float(self.GUI.powermin.get())
        limit=float(self.GUI.powermax.get())
        step=float(self.GUI.powerstep.get())
        stepnumber=int(1)
        while (limit>=pownow):
            self.Model.generator.write(":SOUR:POW:LEV:IMM:AMPL "+str(pownow))
            time.sleep(1)
            self.refresh()
            np.savetxt(self.GUI.nametosave.get()+'_'+str(stepnumber) +'.dat',zip(self.xaxis,self.waveform), fmt="%f %f") #write here the number that you need
            stepnumber+=1
            pownow=pownow+step

    def close(self):
        self.generator.close()
        self.rm.close()
        print("EOM Disconnected")