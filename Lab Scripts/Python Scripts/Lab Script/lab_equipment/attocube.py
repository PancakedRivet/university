# The Attocube file. Add/edit anything related to the attocube in this file.
# Written for Python 2.7 by Patrick Devane

import telnetlib

ANC_IP_LIST = []

def anc_setup(anc_ip):

        # ANC300 Connection
        connection_port = '7230'
        tn = telnetlib.Telnet(anc_ip, connection_port)
        tn.read_until("Authorization code: ")
        tn.write("123456\n")
        tn.read_until("Authorization success")
        print ("ANC Connected")
        ANC_IP_LIST.append(anc_ip)
        return tn

class Attocube(object):

    def __init__(self, anc_ip, anc_channel):

        if anc_ip not in ANC_IP_LIST:
            self.tn = anc_setup(anc_ip)
            
        self.anc_channel = anc_channel

    def anc_step(self,step_direction):
        command = "step"+str(step_direction)+" "+str(self.anc_channel)+" 1\n"
        self.tn.write(command)
            
    def anc_set(self,anc_field,anc_value):
        command = "set"+str(anc_field)+" "+str(self.anc_channel)+" "+str(anc_value)+"\n"
        self.tn.write(command)

    def anc_get(self, anc_field):
        self.tn.write("get%s "%anc_field + str(self.anc_channel) + " \n")
        raw_output = self.tn.read_until("OK", 0.1 )
        output_split = raw_output.split(' ')

        output = output_split[5]

        if anc_field == "m":
            output = output_split[5].split("\r")
            output = output[0]
            
        return output

    def close(self):
        print("ANC Disconnected")