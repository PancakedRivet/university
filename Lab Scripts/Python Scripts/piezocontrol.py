# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 00:32:53 2016

@author: labadmin
"""
from datetime import datetime
import numpy
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.figure import Figure
from instrumental import u

from PyDAQmx import *
from Tkinter import * 
from scipy.signal import argrelextrema
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from instrumental.drivers.daq.ni import NIDAQ, Task
from instrumental import instrument, list_instruments

daq = instrument('NIDAQ')
window = Tk()
window.title("Master")
window.configure(background='grey')

#Global Variables 
startOsciVoltage = 0.0
startNanoVoltage = 0.0
currentOscilloscopeVoltage = startOsciVoltage
currentNanocubeVoltage = startNanoVoltage
lastSavedOscilloscopeVoltage = currentOscilloscopeVoltage
lastSavedNanocubeVoltage = currentNanocubeVoltage
readyToCollectData = False
sweepStarted = False
saveFileNumber = 0 #Starting SaveFileNumber
stepSize = 0.1 #The increment of voltage during the sweep
saveInterval = 1 #Number of V between each save file

#Initialising the zero arrays for the sweeps of each channel
osciSweep = numpy.zeros((2,), dtype=numpy.float64)
nanoSweep = numpy.zeros((3,), dtype=numpy.float64)

#Writing the starting voltage through each channel before the program properly starts
daq.ao0.write(str(currentOscilloscopeVoltage) + 'V')
daq.ao1.write(str(currentNanocubeVoltage) + 'V')

#List of user inputs from GUI
num1GUIReading = StringVar()
num2DesiredOscilloscopeVoltage = StringVar()
num3DesiredNanocubeVoltage = StringVar()
num4SaveInterval = StringVar()
num5StepSize = StringVar()

#The initialisation of each element
labelGUIInputAI0 = Label(window, text = "Channel AI0 Reading:",fg='black',background='grey',font= 30) #The labels for the GUI displays
labelGUISaveInterval = Label(window, text = "Minimum Save Interval (V)",fg='black',background='grey',font= 30)
labelGUIStepSize = Label(window, text = "Step Size (V)",fg='black',background='grey',font= 30)
labelDesiredOscilloscopeVoltage = Label(window, text = "Channel AO0 (Oscilloscope) Voltage:",fg='black',background='grey',font= 30)
labelDesiredNanocubeControllerVoltage = Label(window, text = "Channel AO1 (Piezzo) Voltage:",fg='black',background='grey',font= 30)
displayGUIInputAI0 = Entry(window,textvariable = num1GUIReading, bd= 10, insertwidth= 1, font= 30)
displayGUISaveInterval = Entry(window,textvariable = num4SaveInterval, bd= 10, insertwidth= 1, font= 30) #The displays for the GUI values
displayGUIStepSize = Entry(window,textvariable = num5StepSize, bd= 10, insertwidth= 1, font= 30) #The displays for the GUI values
displayDesiredOscilloscopeVoltage = Entry(window,textvariable = num2DesiredOscilloscopeVoltage, bd= 10, insertwidth= 1, font= 30)
displayDesiredNanocubeControllerVoltage = Entry(window,textvariable = num3DesiredNanocubeVoltage, bd= 10, insertwidth= 1, font= 30)
buttonOscilloscopeVoltageOutput = Button(window, height = 2, width = 5, text = "Direct", #The clickable buttons on the GUI
           command = lambda:oscilloscopeVoltageDirect(), font= 30, background='black',fg='white')
buttonOscilloscopeVoltageSweep = Button(window, height = 2, width = 5, text = "Sweep",
           command = lambda:oscilloscopeVoltageSweep(), font= 30, background='black',fg='white')
buttonNanocubeControllerVoltageOutput = Button(window, height = 2, width = 5, text = "Direct",
           command = lambda:nanocubeControllerVoltageDirect(), font= 30,background='black',fg='white')
buttonNanocubeControllerVoltageSweep = Button(window, height = 2, width = 5, text = "Sweep",
           command = lambda:nanocubeControllerVoltageSweep(), font= 30,background='black',fg='white')
buttonSaveData = Button(window, height = 2, width = 10, text = "Save Trace",
           command = lambda:saveData(2), font= 30,background='white',fg='black')
buttonCloseProgram = Button(window, height = 2, width = 5, text = "Reset",
           command = lambda:closeProgram(), font= 30,background='red',fg='white')

#Locations of each element within a grid
labelGUIInputAI0.grid(row=0, column=0)
labelGUISaveInterval.grid(row=1, column=0)
labelGUIStepSize.grid(row=2, column=0)
labelDesiredOscilloscopeVoltage.grid(row=3, column=0)
labelDesiredNanocubeControllerVoltage.grid(row=4, column=0)
displayGUIInputAI0.grid(row=0, column=1)
displayGUISaveInterval.grid(row=1, column=1)
displayGUIStepSize.grid(row=2, column=1)
displayDesiredOscilloscopeVoltage.grid(row=3, column=1)
displayDesiredNanocubeControllerVoltage.grid(row=4, column=1)
buttonOscilloscopeVoltageOutput.grid(row=3, column=3)
buttonOscilloscopeVoltageSweep.grid(row=3, column=4)
buttonNanocubeControllerVoltageOutput.grid(row=4, column=3)
buttonNanocubeControllerVoltageSweep.grid(row=4, column=4)
buttonSaveData.grid(row=0, column=5)
buttonCloseProgram.grid(row=0, column=7)

num4SaveInterval.set(str(saveInterval)) #Setting the inital save interval to the default value
num5StepSize.set(str(stepSize))

f = Figure(figsize=(10,5), dpi=100)
a = f.add_subplot(111)
canvas = FigureCanvasTkAgg(f, window)
canvas.get_tk_widget().grid(row=5, column=0,columnspan=5, rowspan=5)

signal=numpy.zeros((10000,), dtype=numpy.float64)
trigger=numpy.zeros((10000,), dtype=numpy.float64)

physicalChannel = ["Dev1/ai0","Dev1/ai3:4"]  #Trigger AIx Signal AIy
limit = None
reset = False
if type(physicalChannel) == type(""):
    physicalChannel = [physicalChannel]
else:
    physicalChannel = physicalChannel
    numberOfChannel = physicalChannel.__len__()
if limit is None:
    limit = dict([(name, (-10.0,10.0)) for name in physicalChannel])
elif type(limit) == tuple:
    limit = dict([(name, limit) for name in physicalChannel])
else:
    limit = dict([(name, limit[i]) for  i,name in enumerate(physicalChannel)])           
taskHandles = dict([(name,TaskHandle(0)) for name in physicalChannel])

channel = [physicalChannel[0],physicalChannel[1]] 

DAQmxCreateTask("",byref(taskHandles[channel[0]]))
DAQmxCreateAIVoltageChan(taskHandles[channel[0]],channel[0],"",DAQmx_Val_RSE,limit[name][0],limit[channel[0]][1],DAQmx_Val_Volts,None)
DAQmxCfgSampClkTiming(taskHandles[channel[0]],"",100000.0,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,200) 
#here we open the channel with a sampling rate of 100 KHz and after taking 200 points the port closes
# we open simustaniously the trigger and detector port with a 100 k sample rate 
# we take 20 k points for both and then close the ports  +++++
DAQmxCreateTask("",byref(taskHandles[channel[1]]))
DAQmxCreateAIVoltageChan(taskHandles[channel[1]],channel[1],"",DAQmx_Val_RSE,limit[channel[1]][0],limit[channel[1]][1],DAQmx_Val_Volts,None)
DAQmxCfgSampClkTiming(taskHandles[channel[1]],"",100000.0,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,20000) 

data = numpy.zeros((40000,), dtype=numpy.float64)
data_trigger = numpy.zeros((10000,), dtype=numpy.float64)
dataForGUIDisplay = numpy.zeros((200,), dtype=numpy.float64)

#Processes
def oscilloscopeVoltageDirect():
    global currentOscilloscopeVoltage
    desiredOscilloscopeVoltage = int(num2DesiredOscilloscopeVoltage.get())
    steps = int(abs(desiredOscilloscopeVoltage - currentOscilloscopeVoltage)/stepSize)
    voltageRampData = numpy.linspace(currentOscilloscopeVoltage, desiredOscilloscopeVoltage, steps)*u.V
    daq.ao0.write(voltageRampData, duration='1s', freq ='1Hz')
    print 'Osciloscope Voltage Changed from %sV to %sV' %(currentOscilloscopeVoltage, desiredOscilloscopeVoltage)
    currentOscilloscopeVoltage = desiredOscilloscopeVoltage
    return
    
def nanocubeControllerVoltageDirect():
    global currentNanocubeVoltage
    desiredNanocubeVoltage = int(num3DesiredNanocubeVoltage.get())
    if desiredNanocubeVoltage <= 12 and desiredNanocubeVoltage >= 0:
        steps = int(abs(desiredNanocubeVoltage - currentNanocubeVoltage)/stepSize)
        voltageRampData = numpy.linspace(currentNanocubeVoltage, desiredNanocubeVoltage, steps)*u.V
        daq.ao1.write(voltageRampData, duration='1s', freq ='1Hz')
        print 'Piezo Voltage Changed from %sV to %sV' %(currentNanocubeVoltage, desiredNanocubeVoltage)
        currentNanocubeVoltage = desiredNanocubeVoltage
    else:
        print 'Voltage Out Of Bounds, No Voltage Change'
    return
    
def oscilloscopeVoltageSweep():
    global sweepStarted
    global osciSweep
    osciSweep[0] = 1 #Flag
    osciSweep[1] = float(num2DesiredOscilloscopeVoltage.get()) #Desried Output
    print 'Sweeping Oscillscope Voltage to ' + str(osciSweep[1]) + 'V'
    sweepStarted = True
    return
    
def nanocubeControllerVoltageSweep():
    global sweepStarted
    global nanoSweep
    nanoSweep[0] = 1 #Flag
    nanoSweep[2] = float(num3DesiredNanocubeVoltage.get()) #Desried Output
    if nanoSweep[2] <= 12 and nanoSweep[2] >= 0:
        nanoSweep[1] = nanoSweep[2]
        print 'Sweeping Nanocube Controller Voltage to ' + str(nanoSweep[1]) + 'V'
        sweepStarted = True
    else:
        nanoSweep[0] = 0
        print 'Voltage Out Of Bounds, No Voltage Change'
    return

def signalSorter(data): #Putting the signal in the right order
    i=0
    while ((data[i+1]-data[i])<1):#and (i<19990): #The trigger is maxing out its search, implying there's no point where the trigger has a positive slope.
        i=i+1
    return i

def saveData(saveType):
    global saveFileNumber
    global currentNanocubeVoltage
    global currentOscilloscopeVoltage
    if saveType == 1:
        numpy.savetxt('data_%s.dat' %saveFileNumber, signal, header='%sV ' %currentOscilloscopeVoltage + 'Osci ' + str(datetime.now()))
        print 'Saved: data_%s.dat (%sV)' %(saveFileNumber, currentOscilloscopeVoltage)            
    if saveType == 2:
        numpy.savetxt('data_%s.dat' %saveFileNumber, signal, header='%sV ' %currentNanocubeVoltage + 'Nano ' + str(datetime.now()))
        print 'Saved: data_%s.dat (%sV)' %(saveFileNumber, currentNanocubeVoltage)
    saveFileNumber += 1
    return

def closeProgram():
    global saveFileNumber
    global currentOscilloscopeVoltage
    global currentNanocubeVoltage
    global lastSavedOscilloscopeVoltage
    global lastSavedNanocubeVoltage
    steps = 100
    if currentOscilloscopeVoltage > startOsciVoltage:
        steps = int(abs(startOsciVoltage - currentOscilloscopeVoltage)/stepSize)
    oscilloscopeVoltageRampData = numpy.linspace(currentOscilloscopeVoltage, startOsciVoltage, steps)*u.V #linspace(intial,final,steps)
    if currentNanocubeVoltage > startNanoVoltage:
        steps = int(abs(startNanoVoltage - currentNanocubeVoltage)/stepSize)
    nanocubeVoltageRampData = numpy.linspace(currentNanocubeVoltage, startNanoVoltage, steps)*u.V
    daq.ao0.write(oscilloscopeVoltageRampData, duration='1s', freq ='1Hz')
    currentOscilloscopeVoltage = startOsciVoltage
    lastSavedOscilloscopeVoltage = startOsciVoltage
    print 'Osciloscope Voltage Reset to %sV' %startOsciVoltage
    daq.ao1.write(nanocubeVoltageRampData, duration='1s', freq ='1Hz')
    currentNanocubeVoltage = startNanoVoltage
    lastSavedNanocubeVoltage = startNanoVoltage
    print 'Nanocube Controller Voltage Reset to %sV' %startNanoVoltage    
    saveFileNumber = 0
    print 'Program Reset'
    print ' '
    return
    
#Main Loop for the program
def gui_window_main_program_run(window): 
    global currentOscilloscopeVoltage
    global currentNanocubeVoltage
    global sweepStarted
    global lastSavedOscilloscopeVoltage
    global lastSavedNanocubeVoltage
    global readyToCollectData
    global sweepUp
    global saveFileNumber
    
    taskHandle = [taskHandles[physicalChannel[0]],taskHandles[physicalChannel[1]]]
    read = int32()
    
    DAQmxStartTask(taskHandle[1])
    DAQmxReadAnalogF64(taskHandle[1],20000,10.0,DAQmx_Val_GroupByChannel,data,40000,byref(read),None)
    DAQmxStopTask(taskHandle[1]) #command to read out the the trigger and detector...it take 20 k points for the trigger and 20 k for the detector.
    
    DAQmxStartTask(taskHandle[0])
    DAQmxReadAnalogF64(taskHandle[0],200,10.0,DAQmx_Val_GroupByChannel,dataForGUIDisplay,200,byref(read),None)
    DAQmxStopTask(taskHandle[0])   
    
    p=signalSorter(data[20000:39999]) #looks for the uprising in the trigger signal
    signal[0:9999]=data[p:p+9999]
    trigger[0:9999]=data[20000+p:29999+p] #Shape issue with casting the input array
    
    num1GUIReading.set( str( numpy.mean(dataForGUIDisplay) ) ) #Could display the position of the piezo
    
    if (osciSweep[0] > 0): #Note that this sweep is up from 0V
        saveInterval = float(num4SaveInterval.get())
        stepSize = float(num5StepSize.get())
        if sweepStarted is True:
            saveData(1)
            lastSavedOscilloscopeVoltage = currentOscilloscopeVoltage
            sweepStarted = False
            direction  = currentOscilloscopeVoltage - osciSweep[1]
            if direction < 0:
                sweepUp = True
            else:
                sweepUp = False
        if sweepUp is True:
            currentOscilloscopeVoltage += stepSize
        if sweepUp is False:
            currentOscilloscopeVoltage -= stepSize
        daq.ao0.write(str(currentOscilloscopeVoltage) + 'V')
        difference = abs(currentOscilloscopeVoltage - lastSavedOscilloscopeVoltage)
        if difference >= (saveInterval-(stepSize/2)):
            saveData(1)
            lastSavedOscilloscopeVoltage = currentOscilloscopeVoltage
        if (currentOscilloscopeVoltage >= osciSweep[1]-(stepSize/2)) and (sweepUp is True): 
            osciSweep[0] = 0
            lastSavedOscilloscopeVoltage = currentOscilloscopeVoltage
            saveFileNumber = 0
            print 'Oscilloscope Data Saving Complete'
            print ' '
        if (currentOscilloscopeVoltage <= osciSweep[1]+(stepSize/2)) and (sweepUp is False): 
            osciSweep[0] = 0
            lastSavedOscilloscopeVoltage = currentOscilloscopeVoltage
            saveFileNumber = 0
            print 'Oscilloscope Data Saving Complete'
            print ' '

    if (nanoSweep[0] > 0): #Note that this sweep is up from 0V
        saveInterval = float(num4SaveInterval.get())
        stepSize = float(num5StepSize.get())
        if sweepStarted is True:
            saveData(2)
            lastSavedNanocubeVoltage = currentNanocubeVoltage
            sweepStarted = False
            direction  = currentNanocubeVoltage - nanoSweep[1]
            if direction < 0:
                sweepUp = True
            else:
                sweepUp = False
        if (currentNanocubeVoltage <= nanoSweep[1]) and (sweepUp is True):
            currentNanocubeVoltage += stepSize
        if (currentNanocubeVoltage >= nanoSweep[1]) and (sweepUp is False):
            currentNanocubeVoltage -= stepSize
        daq.ao1.write(str(currentNanocubeVoltage) + 'V')
        difference = abs(currentNanocubeVoltage - lastSavedNanocubeVoltage)
        if difference >= (saveInterval-(stepSize/2)):
            saveData(2)
            lastSavedNanocubeVoltage = currentNanocubeVoltage
        if (currentNanocubeVoltage >= nanoSweep[1]-(stepSize/2)) and (sweepUp is True): 
            nanoSweep[0] = 0
            lastSavedNanocubeVoltage = currentNanocubeVoltage
            saveFileNumber = 0
            print 'Nanocube Controller Data Saving Complete'
            print ' '
        if (currentNanocubeVoltage <= nanoSweep[1]+(stepSize/2)) and (sweepUp is False): 
            nanoSweep[0] = 0
            lastSavedNanocubeVoltage = currentNanocubeVoltage
            saveFileNumber = 0
            print 'Nanocube Controller Data Saving Complete'
            print ' '
              
    #Plotting     
    a.clear()    
    a.plot(signal[0:9999]) #actual detector output
    a.plot(trigger[0:9999]) #actual detector output
    canvas.show()
    window.after(0, gui_window_main_program_run, window)
    return

gui_window_main_program_run(window)
window.mainloop()