# -*- coding: utf-8 -*-
"""
Created on Wed May 30 17:32:54 2018

@author: devpa499
"""
import numpy
import math
from matplotlib import pyplot as plt
from datetime import datetime
from tkinter import *
import threading

from instrumental import instrument, list_instruments  # For daq 
import nidaqmx  # Manages voltage
from nidaqmx.constants import AcquisitionType, Edge
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg

# Initial values (mV)
STARTING_VOLTAGE = 0  # default is 0
STEP_SIZE = 1  # default is 100
SAVE_INTERVAL = 10  # default is 1000

class Context(object): # Initialises

    def __init__(self):
        print 'Starting Voltage: %sV' % (float(STARTING_VOLTAGE) / 1000)
        print 'Starting Step Size: %sV' % (float(STEP_SIZE) / 1000)
        print 'Starting Save Interval: %sV' % (float(SAVE_INTERVAL) / 1000)

        daq = instrument('NIDAQ')

        signal = numpy.zeros((30000,), dtype=numpy.float64)
        trigger = numpy.zeros((30000,), dtype=numpy.float64)

        self.nanocube = Device(daq.ao0, STARTING_VOLTAGE, STEP_SIZE, signal, trigger)
        self.nanocube_data = DataManager(self.nanocube, signal, SAVE_INTERVAL, save_file_number=0, last_saved_voltage=STARTING_VOLTAGE)


class Device(object): # ONLY READS AND WRITES TO CHANNELS

    def __init__(self, port, voltage, step_size, signal, trigger):
        self.port = port
        self.voltage = voltage
        self.step_size = step_size
        self.signal = signal
        self.trigger = trigger

        self.port.write(str(self.voltage) + 'mV')  # Writing initial voltage


    def write_to_channel(self, difference):
        self.voltage += math.copysign(self.step_size, difference)
        self.port.write(str(self.voltage) + 'mV')

    def read_from_channel(self):
        with nidaqmx.Task() as task:
            task.ai_channels.add_ai_voltage_chan("Dev2/ai3:4")
            task.timing.cfg_samp_clk_timing(300000, active_edge=Edge.RISING, sample_mode=AcquisitionType.FINITE, samps_per_chan=len(self.signal))
            (self.signal, self.trigger) = task.read(number_of_samples_per_channel=len(self.signal))


class DataManager(object): # ONLY PROCESSES THE DATA - ATTACHED TO A SINGLE DEVICE PER INSTANCE

    def __init__(self, device, processed_data, save_interval, save_file_number, last_saved_voltage):
        self.device = device
        self.processed_data = processed_data
        self.save_interval = save_interval
        self.save_file_number = save_file_number
        self.last_saved_voltage = last_saved_voltage
        
    def process_data(self):
        trigger_edge = self.trigger_edge_finder()
        self.processed_data = numpy.transpose(
            self.device.signal[trigger_edge:trigger_edge + len(self.device.signal)/2])  # Re-casting signal to column

        self.device.trigger = self.device.trigger[trigger_edge:trigger_edge + len(self.device.signal)/2]

    def save_data(self):
        header = '%s' %(float(self.device.voltage) / 100) + " microns " + str(datetime.now())
        numpy.savetxt('data_%s.dat' % self.save_file_number,
                      self.processed_data,
                      header=header)

        print "Saved data_%s.dat" % self.save_file_number
        self.last_saved_voltage = self.device.voltage
        self.save_file_number += 1

    def save_number_reset(self):
        self.save_file_number = 0

    def trigger_edge_finder(self):
        index = 0
        while ((self.device.trigger[index + 1] - self.device.trigger[index]) < 1):
            index += 1
        return index

    def begin_execute_voltage_change(self, final, should_save):
        thread = threading.Thread(target=self.execute_voltage_change,  args=(final, should_save))
        thread.start()
        return thread

    def execute_voltage_change(self, final, should_save):
        difference = final - self.device.voltage

        if should_save:
            self.save_data()  # Saving pre-sweep traced

        while self.device.voltage != final:
            self.device.write_to_channel(difference)
            if should_save and abs(self.device.voltage - self.last_saved_voltage) >= self.save_interval:
                self.save_data()
        
        self.save_number_reset()
        print "Distace changed to %s microns" % (float(self.device.voltage) / 100)
        print ""


class PiezoGUI(object): # ONLY DISPLAYS THE DATA
    def __init__(self, master):
        self.master = master

        master.title("Oscilloscope")

        vcmd = master.register(self.validate) # we have to wrap the command

        self.execute_label = Label(master, text="Move To (microns)")
        self.execute_label.grid(row=0, column=1)
        self.execute_entry = Entry(master, validate="key", validatecommand=(vcmd, '%P'), width=5)
        self.execute_entry.grid(row=1, column=1)
        self.execute_entry.focus()

        self.execute_button = Button(master, text="Execute", command=lambda: self.execute_button_pressed())
        self.execute_button.grid(row=0,column=0)

        self.should_save = IntVar()
        self.should_save_checkbutton = Checkbutton(master, text="Save?", variable=self.should_save)
        self.should_save_checkbutton.grid(row=1, column=2)
        self.should_save_checkbutton.select()

        self.reset_button = Button(master, text="Reset Location", command=lambda: self.reset_button_pressed())
        self.reset_button.grid(row=1, column=0)

        self.distance_label_field = StringVar()
        self.distance_label = Label(master, textvariable=self.distance_label_field)
        self.distance_label.grid(row=0, column=2)
        self.distance_label_field.set("Distance (microns): " + str((int(STARTING_VOLTAGE)/100))) 

        self.device = context.nanocube
        self.data_manager = context.nanocube_data

        self.current_operation = None

        self.f = Figure(figsize=(10,5), dpi=100)
        self.a = self.f.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.f, master)
        self.canvas.get_tk_widget().grid(row=2, column=0,columnspan=5, rowspan=1)

        self.run()


    def reset_button_pressed(self):
        if self.current_operation:
            self.current_operation.join()
        self.current_operation = self.data_manager.begin_execute_voltage_change(STARTING_VOLTAGE, should_save=False)
        self.distance_label_field.set("Distance (microns): " + str((int(STARTING_VOLTAGE)/100))) 
        print "Distance Reset!"
        print ""

    def execute_button_pressed(self):
        distance = int(self.execute_entry.get())
        should_save = self.should_save.get()

        if distance >= 0 and distance <= 100:
            volts = distance*100
            if self.current_operation:
                self.current_operation.join()
            self.current_operation = self.data_manager.begin_execute_voltage_change(volts, should_save)
            self.distance_label_field.set("Distance (microns): " + str(distance))
            self.execute_entry.delete(0, END)
        else:
            print "%s microns is outside the safe range for the Nanocube." % (float(distance))

    def validate(self, new_text):
        if not new_text: # the field is being cleared
            self.entered_number = 0
            return True

        try:
            self.entered_number = int(new_text)
            return True
        except ValueError:
            return False
        
    def collect_signal(self):
        self.device.read_from_channel()
        self.data_manager.process_data()

    def plot_signal(self):
        self.a.clear()
        self.a.plot(self.data_manager.processed_data)
        self.a.plot(self.device.trigger)
        self.canvas.show()

    def run(self):
        self.collect_signal()
        self.plot_signal()
        self.master.after(0, self.run)

if __name__ == '__main__':
    context = Context()
    root = Tk()
    main_window = PiezoGUI(root)
    root.mainloop()