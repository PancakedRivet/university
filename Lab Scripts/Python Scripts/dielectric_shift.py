# -*- coding: utf-8 -*-
"""
Created on Wed August 8 18:48:09 2018

@author: Patrick Devane
"""
import numpy
import math
from matplotlib import pyplot as plt
from datetime import datetime
import Tkinter as Tk
import threading
import os
import sys
import time

import nidaqmx  # Manages voltage to breakout box
from nidaqmx.constants import AcquisitionType, Edge
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from instrumental import instrument, list_instruments  # For daq 

clear = lambda: os.system('cls')
clear()

# Initial values
SAMPLE_DATA_LENGTH = 30000 # sample size
PROCESS_DATA_LENGTH = 10000 # save size
NANO_STARTING_DISTANCE = 0  # nanometer units

class Setup(object): # initalises the devices

    def __init__(self):
        daq = instrument('NIDAQ')

        signal = numpy.zeros((SAMPLE_DATA_LENGTH,), dtype=numpy.float64)
        trigger = numpy.zeros((SAMPLE_DATA_LENGTH,), dtype=numpy.float64)

        self.Nanocube = Nanocube_Device(daq.ao0, NANO_STARTING_DISTANCE)
        self.Detector = Detector_Device(signal, trigger, "Dev2/ai3:4")


class Nanocube_Device(object): # reads and writes to nanocibe only

    def __init__(self, port, distance):
        self.port = port
        self.distance = distance
        self.milivoltage = self.distance / 10
        self.port.write(str(self.milivoltage) + 'mV')  # Writing initial voltage

    def write_to_channel(self, difference, step_size):
        self.distance += math.copysign(step_size, difference)
        self.milivoltage = self.distance / 10
        self.port.write(str(self.milivoltage) + 'mV')


class Detector_Device(object): # reads and writes to device channels only

    def __init__(self, signal, trigger, port):
        self.signal = signal
        self.trigger = trigger
        self.port = port

    def read_from_channel(self):
        with nidaqmx.Task() as task:
            task.ai_channels.add_ai_voltage_chan(self.port) # signal:trigger
            task.timing.cfg_samp_clk_timing(330000, active_edge=Edge.RISING, sample_mode=AcquisitionType.FINITE, samps_per_chan=SAMPLE_DATA_LENGTH)
            (self.signal, self.trigger) = task.read(number_of_samples_per_channel=SAMPLE_DATA_LENGTH)


class ProgramGUI(object): # displays the data only
    def __init__(self, master):
        self.master = master
        master.title("Automation Program")
        vcmd = master.register(self.validate) # we have to wrap the command to check only a number is entered
        self.current_operation = None

        # OSC
        self.osc_should_save = Tk.IntVar()
        self.osc_callibration_frequency = Tk.StringVar()
        self.osc_lower_bound = Tk.StringVar()
        self.osc_upper_bound = Tk.StringVar()

        self.osc_callibration_button = Tk.Button(master, text="Callibrate", width='20')
        self.osc_callibration_entry_label = Tk.Label(master, text="Frequency (MHz)")
        self.osc_callibration_entry = Tk.Entry(master, textvariable=self.osc_callibration_frequency, width=5)
        self.osc_lower_bound = Tk.Entry(master, validate="key", validatecommand=(vcmd, '%P'), width=5)
        self.osc_upper_bound = Tk.Entry(master, validate="key", validatecommand=(vcmd, '%P'), width=5)
        self.osc_bounds_label = Tk.Label(master, text="Select Lower and Upper Bounds")
        self.osc_should_save_checkbutton = Tk.Checkbutton(master, text="Save?", variable=self.osc_should_save)

        self.osc_callibration_button.grid(row=6, column=3)
        self.osc_callibration_entry.grid(row=6, column=0)
        self.osc_callibration_entry_label.grid(row=7, column=0)
        self.osc_lower_bound.grid(row=6, column=1)
        self.osc_upper_bound.grid(row=6, column=2)
        self.osc_bounds_label.grid(row=7, column=1, columnspan=2)
        self.osc_should_save_checkbutton.grid(row=6, column=4)

        self.osc_should_save_checkbutton.select()

        # DAQ
        self.daq_step_size = Tk.IntVar()
        self.daq_save_interval = Tk.IntVar()
        self.daq_distance_label_field = Tk.StringVar()
        self.daq_step_size.set(1)
        self.daq_save_interval.set(1)
        self.daq_distance_label_field.set("Distance (microns): " + str(NANO_STARTING_DISTANCE))

        self.daq_step_size_select_radioButton1 = Tk.Radiobutton(root, text="0.001", variable=self.daq_step_size, value=1)
        self.daq_step_size_select_radioButton2 = Tk.Radiobutton(root, text="0.01", variable=self.daq_step_size, value=10)
        self.daq_step_size_select_radioButton3 = Tk.Radiobutton(root, text="0.1", variable=self.daq_step_size, value=100)
        self.daq_step_size_select_radioButton4 = Tk.Radiobutton(root, text="1", variable=self.daq_step_size, value=1000)
        self.daq_save_interval_select_radioButton1 = Tk.Radiobutton(root, text="0.001", variable=self.daq_save_interval, value=1)
        self.daq_save_interval_select_radioButton2 = Tk.Radiobutton(root, text="0.1", variable=self.daq_save_interval, value=10)
        self.daq_save_interval_select_radioButton3 = Tk.Radiobutton(root, text="1", variable=self.daq_save_interval, value=100)
        self.daq_save_interval_select_radioButton4 = Tk.Radiobutton(root, text="1", variable=self.daq_save_interval, value=1000)
        self.daq_execute_button = Tk.Button(master, text="Execute")
        self.daq_reset_button = Tk.Button(master, text="Reset Program")
        self.daq_execute_entry = Tk.Entry(master, validate="key", validatecommand=(vcmd, '%P'), width=5)
        self.daq_distance_label = Tk.Label(master, textvariable=self.daq_distance_label_field)
        self.daq_execute_label = Tk.Label(master, text="Move To (microns)")
        self.daq_step_size_label = Tk.Label(master, text="Nanocube Step Size (Microns)")
        self.daq_save_interval_label = Tk.Label(master, text="Save Interval Threshold (Microns)")
        
        self.daq_step_size_select_radioButton1.grid(row=1, column=7)
        self.daq_step_size_select_radioButton2.grid(row=1, column=8)
        self.daq_step_size_select_radioButton3.grid(row=1, column=9)
        self.daq_step_size_select_radioButton4.grid(row=1, column=10)
        self.daq_save_interval_select_radioButton1.grid(row=2, column=7)
        self.daq_save_interval_select_radioButton2.grid(row=2, column=8)
        self.daq_save_interval_select_radioButton3.grid(row=2, column=9)
        self.daq_save_interval_select_radioButton4.grid(row=2, column=10)
        self.daq_execute_button.grid(row=3, column=9)
        self.daq_reset_button.grid(row=0, column=6)
        self.daq_execute_entry.grid(row=3, column=7)
        self.daq_distance_label.grid(row=0, column=7)
        self.daq_execute_label.grid(row=3, column=6)
        self.daq_step_size_label.grid(row=1, column=6)
        self.daq_save_interval_label.grid(row=2, column=6)

        # # Other 
        self.f = Figure(figsize=(10,5), dpi=100)
        self.a = self.f.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.f, master)
        self.canvas.get_tk_widget().grid(row=0, column=0,columnspan=5, rowspan=5)

    def validate(self, new_text):
        if not new_text: # the field is being cleared
            self.entered_number = 0
            return True
        try:
            self.entered_number = int(new_text)
            return True
        except ValueError:
            return False


class Controller(object): # processes data only

    def __init__(self, Nanocube_Device, Detector_Device, GUI):
        self.Nanocube_Device = Nanocube_Device
        self.Detector_Device = Detector_Device
        self.GUI = GUI

        self.processed_detector_signal = numpy.zeros(SAMPLE_DATA_LENGTH,)
        self.processed_detector_trigger = numpy.zeros(SAMPLE_DATA_LENGTH,)

        self.save_file_number = 0
        self.last_saved_distance = self.Nanocube_Device.distance

        # reassigning GUI buttons to Controller Methods:
        self.GUI.osc_callibration_button.configure(command=lambda: self.gui_callibration_button_pressed())
        self.GUI.daq_execute_button.configure(command=lambda: self.gui_execute_button_pressed())
        self.GUI.daq_reset_button.configure(command=lambda: self.gui_reset_button_pressed())

        self.current_operation = None
        self.osc_frequency = 0
        self.osc_callibration_factor = 1
        self.osc_zero_ref = 0
        self.osc_x_axis = self.osc_callibration_factor * (numpy.array(range(len(self.processed_detector_signal)))-self.osc_zero_ref)

        self.run()
        
    def process_data(self):
        trigger_edge = self.trigger_edge_finder()

        self.processed_detector_signal = self.Detector_Device.signal[trigger_edge:trigger_edge+PROCESS_DATA_LENGTH]
        self.processed_detector_trigger = self.Detector_Device.trigger[trigger_edge:trigger_edge+PROCESS_DATA_LENGTH]

    def trigger_edge_finder(self):
        index = 0
        while ((self.Detector_Device.trigger[index + 1] - self.Detector_Device.trigger[index]) < 1):
            index += 1
            if index == len(self.Detector_Device.trigger) - 1:
                print("Trigger Edge Not Found! Check Trigger \n")
                index = 0
                break
        return index

    def begin_nanocube_movement(self, distance, should_save):
        thread = threading.Thread(target=self.nanocube_movement,  args=(distance, should_save))
        thread.start()
        return thread

    def nanocube_movement(self, distance, should_save):
        difference = distance - self.Nanocube_Device.distance
        step_size = self.GUI.daq_step_size.get()
        save_interval = self.GUI.daq_save_interval.get()

        self.save_file_create(abs(difference), save_interval)
        sweep_start_time = time.time()

        if should_save:
            self.save_file_add() # saving pre-sweep trace

        while self.Nanocube_Device.distance != distance:
            self.Nanocube_Device.write_to_channel(difference, step_size)
            
            if should_save and abs(self.Nanocube_Device.distance - self.last_saved_distance) >= save_interval:
                time.sleep(0.25) # buffering sample
                self.save_file_add()

        sweep_end_time = time.time()
        sweep_time_elapsed = sweep_end_time-sweep_start_time

        if should_save:
            self.save_file_number += 1
            self.save_file_save(step_size, save_interval, sweep_time_elapsed)
        
        self.GUI.daq_execute_button.configure(state=Tk.NORMAL)
        print("Distace changed to %s microns \n") %(float(self.Nanocube_Device.distance)/1000)

    def save_file_create(self, distance, save_interval):
        number_of_saves = int((distance/save_interval) + 1)
        self.single_save_file = numpy.zeros((PROCESS_DATA_LENGTH+1, number_of_saves), dtype=numpy.float64)
        self.single_save_file_index = 0

    def save_file_add(self):
        self.single_save_file[0, self.single_save_file_index] = float(self.Nanocube_Device.distance / 1000)
        self.single_save_file[1:, self.single_save_file_index] = numpy.transpose(self.processed_detector_signal)
        self.last_saved_distance = self.Nanocube_Device.distance
        self.single_save_file_index += 1

    def save_file_save(self, step_size, save_interval, time_elapsed):
        save_file_name = "sweep%d.dat" %(self.save_file_number)
        header = "start= %d end= %d frequecy= %d step_size= %d save_interval= %d time_taken= %s date= %s" \
            %(self.single_save_file[0,0],
            self.single_save_file[0,-1],
            self.osc_frequency,
            step_size, save_interval,
            time_elapsed,
            datetime.now())

        numpy.savetxt(save_file_name,
                      self.single_save_file,
                      header=header)

        print("Saved: %s") %(save_file_name)

        if self.osc_frequency != 0:
            numpy.savetxt("sweep%d_callibration.dat" %(self.save_file_number),
                        self.osc_x_axis, 
                        header=header)
            print('%dMHz callibration saved') %(self.osc_frequency)
        
        self.single_save_file_index = 0

    def peaksfinder(self,x,y): # Alfredo's callibration code
        totalpoints=len(y)
        radi=int(round(totalpoints/20)-1)
        flag=1
        z=len(y)
        n=0
    #    flag2=1
        peaks=numpy.zeros(int(round(totalpoints/4)))
        for i in range(radi,z-radi):
            if y[i]<y[i+1]:
                j=1
                ru=0
                while (j<=radi):
                    if ((y[i+j]<y[i]) or (y[i-j]<=y[i])):
                        j=radi+10
                        ru=1
                    j=j+1
                if (ru==0):
                    peaks[n]=i
                    n=n+1
        while (flag>0):
            flag=-1
            for t in range(0,2*n):
                if (y[int(peaks[t])]>y[int(peaks[t+1])]):
                    R=peaks[t]
                    peaks[t]=peaks[t+1]
                    peaks[t+1]=R
                    flag=1
 #      print peaks[0:3]
        return peaks[0:3]
    
    def gui_execute_button_pressed(self):
        distance = int(self.GUI.daq_execute_entry.get()) * 1000
        should_save = self.GUI.osc_should_save.get()

        if distance >= 0 and distance <= 100000:
            if self.current_operation:
                self.current_operation.join()
            self.current_operation = self.begin_nanocube_movement(distance, should_save)
            self.GUI.daq_distance_label_field.set("Distance (microns): " + str(float(distance)/1000))
            self.GUI.daq_execute_entry.delete(0, Tk.END)
            self.GUI.daq_execute_button.configure(state=Tk.DISABLED)
        else:
            print("%s microns is outside the safe range for the Nanocube." % (float(distance)/1000))
        
    def gui_reset_button_pressed(self):
        if self.current_operation:
            self.current_operation.join()
        self.current_operation = self.begin_nanocube_movement(NANO_STARTING_DISTANCE, should_save=False)
        self.save_file_number = 0
        self.osc_callibration_factor = 1
        self.osc_zero_ref = 0
        self.GUI.daq_distance_label_field.set("Distance (microns): " + str(float(NANO_STARTING_DISTANCE)/1000))
        print('Callibration reset')
        print("Distance reset \n")

    def gui_callibration_button_pressed(self):
        lower_bound = int(self.GUI.osc_lower_bound.get())
        upper_bound = int(self.GUI.osc_upper_bound.get())
        self.osc_frequency = float(self.GUI.osc_callibration_entry.get())
        y = self.processed_detector_signal[lower_bound:upper_bound]
        x= range(len(y))
        peaks_position = self.peaksfinder(x,y)
        ordenador_peaks = sorted(peaks_position)
        self.osc_callibration_factor = - (2*self.osc_frequency) / (ordenador_peaks[2]-ordenador_peaks[0])
        self.osc_zero_ref = ordenador_peaks[1]+lower_bound

    def plot_signal(self):
        self.osc_x_axis= self.osc_callibration_factor * (numpy.array(range(len(self.processed_detector_signal)))-self.osc_zero_ref)
        self.GUI.a.clear()
        self.GUI.a.plot(self.osc_x_axis,self.processed_detector_signal)
        self.GUI.a.plot(self.osc_x_axis,self.processed_detector_trigger)
        self.GUI.canvas.show()

    def run(self):
        self.Detector_Device.read_from_channel()
        self.process_data()
        self.plot_signal()
        self.GUI.master.after(0, self.run)


if __name__ == '__main__':
    context = Setup() # comment when GUI testing
    root = Tk.Tk()
    view = ProgramGUI(root)
    controller = Controller(context.Nanocube, context.Detector, view) # comment when GUI testing
    root.mainloop()