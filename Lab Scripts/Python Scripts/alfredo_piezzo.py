from Tkinter import *
import sys
import telnetlib
from datetime import datetime
import visa
import numpy as np
import matplotlib.pyplot as plt
import os
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import win32com.client
import time

scope=win32com.client.Dispatch("LeCroy.ActiveDSOCtrl.1")  #creates instance of the ActiveDSO control
scope.MakeConnection("IP:172.22.2.152")

rm = visa.ResourceManager()
print(rm.list_resources())
generator = rm.open_resource('GPIB0::28::INSTR')
#generator.write(":SOUR:FREQ:CW 10GHz")
#generator.write(":OUTP:STAT OFF")
  
HOST = "172.22.2.154" #172.22.2.154 for test setup piezo
#172.22.2.153 for patrick setup piezo
PORT = '7230'
tn = telnetlib.Telnet(HOST,PORT)
tn.read_until("Authorization code: ")
tn.write("123456\n")
tn.read_until("Authorization success")
print ("Connection successfull")
tn.write("setf 1 200\n")

window = Tk()
window.title("powered by Hassani")
window.configure(background='black')

#####################################################################################################
#####################################################################################################
##################### AL01

button2 = Button(window,text="set offset Voltage[V]", fg="red", width='20',command=lambda:al01vol())
button2.grid(row=5, column=0)
volal01=StringVar()
tempcurrentDisplay=Entry(window,textvariable = volal01, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=6, column=0, columnspan=1, rowspan=1)
#volal01.set(str(0.1))  

#button32 = Button(window,text="connect your piezo", fg="red", width='20',command=lambda:connectpiezo())
#button32.grid(row=12, column=0);
var1 = IntVar()
Checkbutton(window, text="port 1", variable=var1).grid(row=7, column=0, columnspan=1, rowspan=1)
var2 = IntVar()
Checkbutton(window, text="port 2", variable=var2).grid(row=8, column=0, columnspan=1, rowspan=1)
var3 = IntVar()
Checkbutton(window, text="port 3", variable=var3).grid(row=9, column=0, columnspan=1, rowspan=1)
var4 = IntVar()
Checkbutton(window, text="port 4", variable=var4).grid(row=10, column=0, columnspan=1, rowspan=1)
var5 = IntVar()
Checkbutton(window, text="port 5", variable=var5).grid(row=11, column=0, columnspan=1, rowspan=1)
var6 = IntVar()
Checkbutton(window, text="port 6", variable=var6).grid(row=12, column=0, columnspan=1, rowspan=1)
var7 = IntVar()
Checkbutton(window, text="port 7", variable=var7).grid(row=13, column=0, columnspan=1, rowspan=1)

button29 = Button(window,text="Save Spectrum", fg="red", width='20',command=lambda:solosavingspec())
button29.grid(row=5, column=1)
nametosave=StringVar()
tempcurrentDisplay=Entry(window,textvariable = nametosave, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=6, column=1, columnspan=1, rowspan=1)
     #showing the actual value of the temp
nametosave.set("putsomething")

button30 = Button(window,text="Save Spectrum and step", fg="red", width='20',command=lambda:savingspec())
button30.grid(row=5, column=2)

piezostep=StringVar()
tempcurrentDisplay=Entry(window,textvariable = piezostep, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=6, column=2, columnspan=1, rowspan=1)
piezostep.set(str(0.1))

button31 = Button(window,text="set Freq [MHz]", fg="red", width='20',command=lambda:setfreqsource())
button31.grid(row=5, column=3)
freqsource=StringVar()
tempcurrentDisplay=Entry(window,textvariable = freqsource, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=6, column=3, columnspan=1, rowspan=1)
button68 = Button(window,text="source on", fg="red", width='20',command=lambda:sourceon())
button68.grid(row=5, column=4)
button69 = Button(window,text="source off", fg="red", width='20',command=lambda:sourceoff())
button69.grid(row=6, column=4)

button70 = Button(window,text="disconnect", fg="red", width='20',command=lambda:discon())
button70.grid(row=5, column=5)
button71 = Button(window,text="refresh", fg="red", width='20',command=lambda:refresh())
button71.grid(row=6, column=5)

button28 = Button(window,text="calibration", fg="red", width='20',command=lambda:calibration())
button28.grid(row=0, column=5)
numtocal=StringVar()
tempcurrentDisplay=Entry(window,textvariable = numtocal, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=1, column=5, columnspan=1, rowspan=1)

mincal=StringVar()
tempcurrentDisplay=Entry(window,textvariable = mincal, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=2, column=5, columnspan=1, rowspan=1)
maxcal=StringVar()
tempcurrentDisplay=Entry(window,textvariable = maxcal, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=3, column=5, columnspan=1, rowspan=1)

templabel8 = Label(window, text = "Mod freq",fg='red',background='black',font= 30)
templabel8.grid(row=1, column=4, padx=(10,0), pady=(10,10))
templabel8 = Label(window, text = "lower limit",fg='red',background='black',font= 30)
templabel8.grid(row=2, column=4, padx=(10,0), pady=(10,10))
templabel8 = Label(window, text = "upper limit",fg='red',background='black',font= 30)
templabel8.grid(row=3, column=4, padx=(10,0), pady=(10,10))

powermin=StringVar()
tempcurrentDisplay=Entry(window,textvariable = powermin, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=8, column=1, columnspan=1, rowspan=1)
button71 = Button(window,text="setpower [dBm]", fg="red", width='20',command=lambda:setpowsource())
button71.grid(row=7, column=1)

powermax=StringVar()
tempcurrentDisplay=Entry(window,textvariable = powermax, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=8, column=2, columnspan=1, rowspan=1)
templabel10 = Label(window, text = "finalpow [dBm]",fg='red',background='black',font= 30)
templabel10.grid(row=7, column=2,columnspan=1, rowspan=1)

powerstep=StringVar()
tempcurrentDisplay=Entry(window,textvariable = powerstep, bd= 10, insertwidth= 1 , font= 30)
tempcurrentDisplay.grid(row=8, column=3, columnspan=1, rowspan=1)

templabel11 = Label(window, text = "steppow [dBm]",fg='red',background='black',font= 30)
templabel11.grid(row=7, column=3, columnspan=1, rowspan=1)

button72 = Button(window,text="powsweep", fg="red", width='20',command=lambda:sourcepowsweep())
button72.grid(row=8, column=4)

#button28 = Button(window,text="Fit mode", fg="red", width='20',command=lambda:calibration())
#button28.grid(row=0, column=12);

#####################################################################################################
f = Figure(figsize=(9,5), dpi=100)
a = f.add_subplot(111)
canvas = FigureCanvasTkAgg(f, window)
canvas.get_tk_widget().grid(row=0, column=0,columnspan=4, rowspan=4)
numberofpoints=10000
waveform=scope.GetScaledWaveform("C1",numberofpoints,0)
xaxis=range(len(waveform))
#xaxis=2*np.array(xaxis)
a.clear()
a.plot(xaxis,waveform)

canvas.show()

selected=np.zeros((7,), dtype=int)
cont= np.zeros((10,), dtype=np.float64)
cont[0]=3 # here is the starting voltage #
#volal01.set(str(cont[0]))
#tn.write("seta 2 "+volal01.get()+"\n")  #channel 2. 
cont[2]=5

#####################################################################################################
####################################################################################################

def al01vol():
        selected[0]=var1.get()
        selected[1]=var2.get()
        selected[2]=var3.get()
        selected[3]=var4.get()
        selected[4]=var5.get()
        selected[5]=var6.get()
        selected[6]=var7.get()
        tn.write("seta "+str(selected.argmax()+1)+" "+volal01.get()+"\n")        
#        tn.write("seta 2 "+volal01.get()+"\n")  #channel 2.
        cont[0]=float(volal01.get())
#        print "seta "+str(selected.argmax()+1)+" "+volal01.get()+"\n"
#        print "seta 2 "+volal01.get()+"\n"

def savingspec():
        waveform=scope.GetScaledWaveform("C1",numberofpoints,0)
        a.clear()
        a.plot(xaxis,waveform)
        canvas.show()
        #eucl_div = divmod(10*cont[0], 10)
        #file_nb_str = str(eucl_div[0]) + 'v' + str(eucl_div[1])
        nb_str = int(round(10*(cont[0])))
        np.savetxt(nametosave.get()+ str(nb_str) +'V.dat',zip(xaxis,waveform), fmt="%f %f") #write here the number that you need
#        np.savetxt(nametosave.get()+ str(nb_str) +'V.dat',(xaxis,waveform), delimiter='    ') #write here the number that you need        
        cont[0]=cont[0]+float(piezostep.get())  # here is the voltage step #
        volal01.set(str(cont[0]))
        print cont[0]
#        tn.write("seta 2 "+volal01.get()+"\n")  #channel 2.   
        tn.write("seta "+str(selected.argmax()+1)+" "+volal01.get()+"\n")

def solosavingspec():
#        np.savetxt(nametosave.get()+'.dat',(xaxis,waveform), delimiter='    ') #write here the number that you need
        np.savetxt(nametosave.get()+'.dat',zip(xaxis,waveform), fmt="%f %f") #write here the number that you need

def discon():
        scope.Disconnect()
        generator.close()
        rm.close()
        print ("Disconected")

def sourceon():
        generator.write(":OUTP:STAT ON")

def sourceoff():
        generator.write(":OUTP:STAT OFF")

def setpowsource():
        generator.write(":SOUR:POW:LEV:IMM:AMPL "+powermin.get())

def setfreqsource():
        generator.write(":SOUR:FREQ:CW "+freqsource.get()+"MHz")

def sourcepowsweep():
       pownow=float(powermin.get())
       limit=float(powermax.get())
       step=float(powerstep.get())
       stepnumber=int(1)
       while (limit>=pownow):
            generator.write(":SOUR:POW:LEV:IMM:AMPL "+str(pownow))
            time.sleep(1)
            waveform=scope.GetScaledWaveform("C1",numberofpoints,0)
            a.clear()
            a.plot(xaxis,waveform)
            canvas.show()            
            np.savetxt(nametosave.get()+ str(stepnumber) +'.dat',zip(xaxis,waveform), fmt="%f %f") #write here the number that you need
            stepnumber=stepnumber+1
            pownow=pownow+step

def calibration():
        global xaxis
        global waveform
        position=peaksfinder(xaxis[int(float(mincal.get())):int(float(maxcal.get()))],waveform[int(float(mincal.get())):int(float(maxcal.get()))])
        ordenador=sorted(position)

        print np.array(ordenador)+float(mincal.get())
        xaxis=(2*float(numtocal.get())/(ordenador[2]-ordenador[0]) )*(np.array(xaxis)-ordenador[1]-float(mincal.get()))
        a.clear()
        a.plot(xaxis,waveform)
        canvas.show()

def refresh():
        global xaxis
        global waveform
        waveform=scope.GetScaledWaveform("C1",numberofpoints,0)
        a.clear()
        a.plot(xaxis,waveform)
        canvas.show()

def peaksfinder(x,y):
    totalpoints=len(y)
    radi=int(round(totalpoints/20)-1)
    flag=1
    z=len(y)
    n=0
#    flag2=1
    peaks=np.zeros(int(round(totalpoints/4)))
    for i in range(radi,z-radi):
        if y[i]<y[i+1]:
           j=1
           ru=0
           while (j<=radi):
               if ((y[i+j]<y[i]) or (y[i-j]<=y[i])):
                   j=radi+10
                   ru=1
               j=j+1
           if (ru==0):
               peaks[n]=i
               n=n+1
    while (flag>0):
        flag=-1
        for t in range(0,2*n):
            if (y[int(peaks[t])]>y[int(peaks[t+1])]):
                R=peaks[t]
                peaks[t]=peaks[t+1]
                peaks[t+1]=R
                flag=1
 #  print peaks[0:3]
    return peaks[0:3]

def lorentz(p,x):
    return p[3]- p[2]*0.5*p[1] / ((p[0]-x )**2 + 0.25*p[1]**2)

#def lorentzzz(p,x):
#    return p[3]- p[2]*0.5*p[1] / ((p[0]-x )**2 + 0.25*p[1]**2)- p[6]*0.5*p[5] / ((p[4]-x )**2 + 0.25*p[5]**2)- p[9]*0.5*p[8] / ((p[7]-x )**2 + 0.25*p[8]**2)

def errorfunc(p,x,y):
        return lorentz(p,x)-y

window.mainloop()