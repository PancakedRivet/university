# coding=utf-8
# ###########################################################################
# ########## Copyright Luke Trainor 2017-2018. All rights reserved ##########
# ###########################################################################
# # Translated into a Python 2.7 script  by Patrick Devane
# ################################# Inputs ##################################
# # Assumes the surrounding is air or vacuum, and that the prism is uncoated.

# w = 1.55;
# # Wavelength in microns

# prismAngle = 60;
# # Prism angle in degrees
# angleType = 'b';
# # 'a' or 'b'. See diagram below. If 'b', it assumes that the triangle is
# # isoceles. If 'a' and the angle opposite 'a' are different, you should run
# # this twice (with both angles) to ensure both in- and out-coupling.
# #            |\
# #          \ |a\
# #      WGM  \|  \
# # Resonator ||   \
# #     here  ||  b/
# #           /|  /
# #          / | /
# #            |/

# resonatorMaterial = 'LN';
# prismMaterial = 'diamond';
# # For possible materials, see sellmeier.m
# resonatorCut = 'z';
# # 'iso' for isotropic
# # 'x','y', 'z' or 'xy' for uniaxial
# # 'x','y' or 'z' for biaxial nz>=ny>=nx

# prismCut = 'iso';
# # 'iso' for ISOTROPIC
# #
# # 'z' ,'xz', or 'xy' (or 'yz', 'yx') for UNIAXIAL. When not z-cut, first
# # index is for TE modes, second for TM if emmitted exactly orthogonal to
# # resonator normal (along coupling side of prism).
# # e.g. 'xz' means nTE = no, nTM(90Â° incidence) = ne, nTM(0Â° incidence) = no
# #
# # for BIAXIAL, first index is for TE modes, second for TM if emmitted
# # exactly orthogonal to resonator normal (along coupling side of prism)
# # e.g. 'zy' means nTE = nz, nTM(90Â°) = ny, nTM(0Â°) = nx, nz>=ny>=nx
# #
# # Note: Some strange uniaxial or any biaxial prisms will throw a warning
# # that the TM angles might not have been solved properly, as these have not
# # been thoroughly tested.

# # If these are set to a value other than false, they will override the
# # script from calculating refractive indices for that part. Useful for
# # high-order modes.
# nResonatorOverride = false;
# nPrismOverride = false;

# ###########################################################################
# # Written by Luke Trainor on 15/03/2017.                                  #
# # Updated 13/12/17 to print settings                                      #
# # Updated 20/03/18 to correct fresnel transmittance                       #
# # Updated 04/07/18 to correct fresnel transmittance (for real this time)  #
# #                  also a sin was missing that didn't affect it much.     #
# #                  general readablity also improved.                      #
# # Updated 26/07/18 Added refractive index overrides, added errors.        #
# ###########################################################################

# ## Set refractive indices

# # set materials
# if nPrismOverride
#     np = nPrismOverride;
#     warning('Warning: Prism refractive index override is on')
# else
#     np = sellmeier(w, prismMaterial);# Refractive indices in prism [ord, ext] or [nx, ny, nz], where nx<ny<nz
# end
# if nResonatorOverride
#     nr = nResonatorOverride;
#     warning('Warning: Resonator refractive index override is on')
# else
#     nr = sellmeier(w, resonatorMaterial);# Refractive indices in resonator [ord, ext] or [nx, ny, nz], where nx<ny<nz
# end
    
# # check correct format of refractive indices returned.
# if size(np,1)~=1 || size(np,2)<1 || size(np,2)>3
#     error('Prism refractive indices not returned with size 1x1, 1x2 or 1x3. Aborting.')
# elseif size(nr,1)~=1 || size(nr,2)<1 || size(nr,2)>3
#     error('Resonator refractive indices not returned with size 1x1, 1x2 or 1x3. Aborting.')
# end
    
# if length(np) == 1 && ~strcmp(prismCut,'iso') # Sanity check. If there is only one refractive index, the material must be isotropic!
#     prismCut = 'iso';
#     warning('Only one prism refractive index received. Defaulting to isotropic.')
# elseif length(np) == 2 || length(np) == 3 # Add number of refractive indices to end of string (to deal with biaxial)
#     prismCut = [prismCut,num2str(length(np))];
# end

# if length(nr) == 1 && ~strcmp(resonatorCut,'iso') # Sanity check. If there is only one refractive index, the material must be isotropic!
#     resonatorCut = 'iso';
#     warning('Only one resonator refractive index received. Defaulting to isotropic.')
# elseif length(nr) == 2 || length(nr) == 3 # Add number of refractive indices to end of string (to deal with biaxial)
#     resonatorCut = [resonatorCut,num2str(length(nr))];
# end

# # refractive indices in prism
# # the boolean weirdPrism is true if the TM refractive index is a function
# # of angle.
# switch prismCut
#     case 'iso'
#         weirdPrism=false;
#         npTE = np;
#         npTM = np;
#     case 'z2' # zcut uniaxial
#         weirdPrism=false;
#         npTE = np(2);#e
#         npTM = np(1);#o
#     case {'xz2','yz2'} # strange uniaxial
#         weirdPrism=true;
#         npTE = np(1);#o
#         npTM1 = np(2);#e
#         npTM2 = np(1);#o
#     case {'xy2','yx2'} # strange uniaxial
#         weirdPrism=true;
#         npTE = np(1);#o
#         npTM1 = np(1);#o
#         npTM2 = np(2);#e
#     case 'zx3' # biaxial
#         weirdPrism=true;
#         npTE = np(3);
#         npTM1 = np(1);
#         npTM2 = np(2);
#     case 'xz3' # biaxial
#         npTE = np(1);
#         npTM1 = np(3);
#         npTM2 = np(2);
#     case 'zy3' # biaxial
#         weirdPrism=true;
#         npTE = np(3);
#         npTM1 = np(2);
#         npTM2 = np(1);
#     case 'yz3' # biaxial
#         weirdPrism=true;
#         npTE = np(2);
#         npTM1 = np(3);
#         npTM2 = np(1);
#     case 'xy3' # biaxial
#         weirdPrism=true;
#         npTE = np(1);
#         npTM1 = np(2);
#         npTM2 = np(3);
#     case 'yx3' # biaxial
#         weirdPrism=true;
#         npTE = np(2);
#         npTM1 = np(1);
#         npTM2 = np(3);
#     otherwise
#         error('Prism cut not set correctly. Aborting.')
# end

# if weirdPrism
#     warning('#s\n#s',...
#     'TM solution(s) may be off, as TM refractive index in prism will be combination of different refractive indices.',...
#     'These solutions have not been tested thoroughly. Are you really using such a strange prism?')
# end

# # So that the next switch won't fail for weird prisms.
# if ~exist('npTM','var')
#     npTM=NaN;
# end

# # Set refractive indices for use-cases
# switch resonatorCut
#     case 'iso'
#         nPrism = [npTE, npTM];
#         nResonator = [nr, nr];
#         displayText = 'TE        TM';
#     case 'z2'
#         nPrism = [npTE, npTM];
#         nResonator = [nr(2), nr(1)];
#         displayText = 'TE        TM';
#     case {'xy2','x2','y2'}
#         nPrism = [npTE, npTM, npTM];
#         nResonator = [nr(1), nr(2), nr(1)];
#         displayText = 'TE        TM (OA)   TM (perp OA)';
#     case 'z3'
#         nPrism = [npTE, npTM, npTM];
#         nResonator = [nr(3), nr(1), nr(2)];
#         displayText = 'TE        TM (x)    TM (y)';
#     case 'y3'
#         nPrism = [npTE, npTM, npTM];
#         nResonator = [nr(2), nr(3), nr(1)];
#         displayText = 'TE        TM (z)    TM (x)';
#     case 'x3'
#         nPrism = [npTE, npTM, npTM];
#         nResonator = [nr(1), nr(2), nr(3)];
#         displayText = 'TE        TM (y)    TM (z)';
#     otherwise
#         error('Resonator cut not set correctly. Aborting.')
# end
# nSols=length(nPrism);#Number of solutions to solve for

# ## Calculate angles
# degree = pi/180; # Conversion factor for degrees

# #Find angle of light inside prism away from normal of resonator (critical coupling)
# if ~weirdPrism #Nice and easy if TM refractive index not dependent on outcoupled angle
#     beta = asin(nResonator./nPrism);
# else
#     beta=zeros(1,nSols);#Right number of empty slots.
#     #TE is easy
#     beta(1)=asin(nResonator(1)/nPrism(1));
    
#     #Snell's law must be solved numerically if TM refractive index is a
#     #function of angle.
    
#     #Refractive index in prism as function of angle for TM
#     nPrismAngleFun=@(x) 1./sqrt(sin(x).^2/npTM1^2+cos(x).^2/npTM2^2);
#     for ii = 2:nSols
               
#         #Function to find root of (Snell's law)
#         SnellFun= @ (x) nResonator(ii)-nPrismAngleFun(x).*sin(x);
#         #If end-points have same sign, there is no solution
#         if sign(SnellFun(0))==sign(SnellFun(90*degree))
#             beta(ii)=NaN;
#         else
#             beta(ii)=fzero(SnellFun,[0,90*degree]);
#         end
#         #Calculate here the refractive index in prism for this angle.
#         nPrism(ii)=nPrismAngleFun(beta(ii));
#     end
# end

# # Convert prism angle to what the end angle would be if the prism were
# # isoceles
# if strcmp(angleType,'a')
#     endAngle = 180-2*prismAngle;
#     prismText='close to resonator';
# elseif strcmp(angleType,'b')
#     endAngle = prismAngle;
#     prismText='away from resonator';
# else
#     warning('Angle type not set properly. Aborting')
#     return
# end

# nair=1;
# # Can use 1.000277 (at STP) if you really care, but it maybe changes the
# # 4th decimal place of the angle (as at most change is linear).

# endAngle = endAngle*degree; # Convert to radians
# alpha = (pi-endAngle)/2; # Angle of prism next to resonator (doubly degenerate)
# gamma = asin((nPrism/nair).*sin(beta-alpha));# Angle between out-coupled light and normal of outer prism edge, as angle of incidence inside prism is (beta-alpha).
# delta = pi/2 - alpha - gamma;# Angle between coupled light outside of the resonator and the plane of the prism
# delta(imag(delta)~=0) = NaN;# If delta has an imaginary part, it doesn't work

# ## Fresnel transmittance

# # Fresnel power transmittance for angle of incidence theta1 in refractive
# # index n1, going into refractive index n2. s- and p-polarised.
# fresnelPowerTs = @(n1,n2,theta1) 1 - ( (n1*cos(theta1)-n2*sqrt(1-(n1/n2*sin(theta1))^2))/(n1*cos(theta1)+n2*sqrt(1-(n1/n2*sin(theta1))^2)) )^2;
# fresnelPowerTp = @(n1,n2,theta1) 1 - ( (-n2*cos(theta1)+n1*sqrt(1-(n1/n2*sin(theta1))^2))/(n2*cos(theta1)+n1*sqrt(1-(n1/n2*sin(theta1))^2)) )^2;

# theta = beta-alpha;# Angle of incidence inside prism for outgoing beam
# fresnel = zeros(1,nSols);
# # TE (s-polarised)
# fresnel(1) = fresnelPowerTs(nPrism(1),nair,theta(1));
# # TM (p-polarised)
# for ii = 2:nSols
#     fresnel(ii) = fresnelPowerTp(nPrism(ii),nair,theta(ii));
# end

# # Note that power transmittance is reciprocal, so this is not needed!
# # gamma is the angle of incidence outside prism for incoming beam
# # fresnel2 = zeros(1,nSols);
# # fresnel2(1) = fresnelPowerTs(nair,nPrism(1),gamma(1));
# # for ii = 2:nSols
# #     fresnel2(ii) = fresnelPowerTp(nair,nPrism(ii),gamma(ii));
# # end

# ## Display results

# # display settings
# fprintf('\n#s-cut #s resonator,\n',resonatorCut,resonatorMaterial)
# fprintf('#s-cut #s prism with #.2fÂ° angle #s (type "#s"),\n',...
#     prismCut,prismMaterial,prismAngle,prismText,angleType)
# fprintf('#.3f micrometer wavelength.\n\n',w)

# # angles
# fprintf('#s\n#s\n#s\n',...
#     'Angles between coupled light outside of the resonator and the',...
#     'coupling plane of the prism (in degrees) (NaN if it will not work):',...
#     'Note that a negative angle is valid (but rare)!')
# fprintf('   #s\n',displayText)
# disp(delta/degree)

# # fresnel transmittance
# fprintf('#s\n#s\n',...
#     'Fresnel power transmittance from (assumed uncoated) prism into air/vacuum:',...
#     'Or from air/vacuum into prism!')
# fprintf('   #s\n',displayText)
# disp(fresnel)

# # # angle of incidence on prism face
# # fprintf('Angle of incidence of light on prism (in degrees) (0 means normal incidence):\n')
# # fprintf('   #s\n',displayText)
# # disp(gamma/degree)

# % A list of materials (which name to use comes first):
# % calcite
# % diamond
# % LN (Lithium Niobate)
# % MgF (Magnesium Fluoride)
# % CaF (calcium fluoride)
# % rutile
# % SF11 (N-SF11 (SCHOTT))
# % YVO (Yttrium Orthovanadate)
# % YSO
# % aBBO (alpha - BBO)
# % silica
# % hKTP (hydrothermally grown KTP)
# % fKTP (flux-groxn KTP)
# % sapphire
# % YSZ yttria-stabilized cubic zirconia
# % YAG
# % Silicon
# % GaAs
# % BK7

import Tkinter as Tk

# Variables needed:
# Wavelength
# Prism: angle1, angle2, andgle3, material
# Resonator: material, cut

material_names = ["Calcite", "Diamond", "Lithium Niobate", "Magesium Fluoride", "Calcium Fluoride", \
    "Rutile", "SF11", "Yttrium Orthovanadate", "YSO", "Alpha BBO", "Silica", "Hydrothermally-grown KTP", "Flux-grown KTP", \
    "Sapphire", "Yttria-stabilized Cubic Zirconia", "YAG", "Silicon", "GaAs", "BK7"]
material_names.sort(key=str.lower)

material_name_index = {}
for i in range(len(material_names)):
    material_name_index[i] = material_names[i]

# print(range(len(material_names)))
# print(material_name_index)

class MaterialList(object):
    def __init__(self):
        
        self.information = []
        self.information.append(Material(name="Calcite",\
                        website="Ghosh 1999 http://refractiveindex.info/?shelf=main&book=CaCO3&page=Ghosh-o", \
                        equation_values=[ ["a1", [0.73358749, 0.35859695]], \
                                        ["b1", [0.96464345, 0.82427830]], \
                                        ["c1", [0.0194325203, 0.0106689543]], \
                                        ["b2", [1.82831454, 0.14429128]], \
                                        ["c2", [120, 120]] ], \
                        sellmeier_equation="sqrt(1+a1+(b1*w^2)./(w^2-c1)+(b2*w^2)./(w^2-c2))" ) )

        self.information.append(Material(name="Alpha BBO", \
                        website="http://www.mt-optics.net/a-BBO.html", \
                        equation_values=[ ["a", [2.7471, 2.3174]], \
                                        ["b", [0.01878, 0.01224]], \
                                        ["c", [0.01822, 0.01667]], \
                                        ["d", [0.01354, 0.01516]] ], \
                        sellmeier_equation=sqrt(a+b./(w^2-c)-d*w^2)) )

        self.information.append(Material(name="BK7", \
                        website="https://refractiveindex.info/?shelf=glass&book=BK7&page=SCHOTT"
                        equation_values=[ ["a1", 1.0391212], \
                                        ["b1", 0.00600069867], \
                                        ["a2", 0.231792344], \
                                        ["b2", 0.0200179144], \
                                        ["a3", 1.01046945], \
                                        ["b3", 103.560653] ], \
                        sellmeier_equation=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3))) )
        
        self.information.append(Material(name="Diamond", \
                        website="http://refractiveindex.info/?shelf=main&book=C&page=Peter", \
                        equation_values=[ ["a1", 0.3306], \
                                        ["b1", 0.1750^2], \
                                        ["a2", 4.3356], \
                                        ["b2", 0.1060^2] ], \
                        sellmeier_equation=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2))) )
        
        self.information.append(Material(name="GaAs", \
                        website="https://refractiveindex.info/?shelf=main&book=GaAs&page=Skauli"
                        equation_values=[ ["a0", 4.3725], \
                                        ["a1", 5.4667], \
                                        ["b1", 0.4431^2], \
                                        ["a2", 0.0243], \
                                        ["b2", 0.8746^2], \
                                        ["a3", 1.9575], \
                                        ["b3", 36.9166] ], \
                        sellmeier_equation=sqrt(1+a0+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3))) )

        self.information.append(Material(name="Silicon", \
                        website="https://refractiveindex.info/?shelf=main&book=Si&page=Salzberg"
                        equation_values=[ ["a1", 10.6684], \
                                        ["b1", 0.3015^2], \
                                        ["a2", 0.0030], \
                                        ["b2", 1.1348^2], \
                                        ["a3", 1.5413], \
                                        ["b3", 1104] ], \
                        sellemeier_equation=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3))) )

        self.information.append(Material(name="Lithium Niobate", \
                        website="Zelmon et al. 1997 http://refractiveindex.info/?shelf=main&book=LiNbO3&page=Zelmon-o"
                        equation_values=[ ["a1", [2.6734, 2.9804]], \
                                        ["b1", [0.01764, 0.02047]], \
                                        ["a2", [1.2290, 0.5981]], \
                                        ["b2", [0.05914, 0.0666]], \
                                        ["a3", [12.614, 8.9543]], \
                                        ["b3", [474.60, 416.08]] ], \
                        sellmeier_equation=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3))) )

        self.information.append(Material(name="Magnesium Fluoride", \
                        website="Dodge 1984 http://refractiveindex.info/?shelf=main&book=MgF2&page=Dodge-o", \
                        equation_values=[ ["a1", [0.48755108, 0.41344023]], \
                                        ["b1", [0.04338408^2, 0.03684262^2]], \
                                        ["a2", [0.39875031, 0.50497499]], \
                                        ["b2", [0.09461422^2, 0.09076162^2]], \
                                        ["a3", [2.3120353, 2.4904962]], \
                                        ["b3", [23.793604^2, 23.771995^2]] ], \
                        sellmeier_equation=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3))) )

class Material(object):
    def __init__(self, name, website, equation_values, sellmeier_equation):

        self.name = name
        self.website = website
        self.sellmeier_equation = sellmeier_equation
        self.equation_values = equation_values

class GUI(Tk.Frame):

    def __init__(self, window):

        self.window = window
        self.window.title("Coupling Angle Calculator")

        resonator_column = 0
        prism_column = resonator_column + 2

        # Variables
        self.wavelength = Tk.StringVar()
        self.prism_angle = [Tk.StringVar(), Tk.StringVar(), Tk.StringVar()]
        self.prism_material_index = Tk.IntVar()
        self.resonator_material_index = Tk.IntVar()
        self.resonator_cut = Tk.StringVar()

        self.wavelength.set(str(1.550))
        self.resonator_material_index.set(0)
        self.prism_material_index.set(0)

        # Elements
        self.wavelength_entry = Tk.Entry(window,textvariable=self.wavelength, bd=10, insertwidth=1 , font=30)

        self.update_button = Tk.Button(window,text="Update", width="20")

        self.prism_angle1_entry = Tk.Entry(window,textvariable=self.prism_angle[0], bd=10, insertwidth=1 , font=30)
        self.prism_angle2_entry = Tk.Entry(window,textvariable=self.prism_angle[1], bd=10, insertwidth=1 , font=30)
        self.prism_angle3_entry = Tk.Entry(window,textvariable=self.prism_angle[2], bd=10, insertwidth=1 , font=30)

        self.resonator_material_label = Tk.Label(window, text="Resonator Material", font= 30)
        self.prism_material_label = Tk.Label(window, text="Prism Material", font= 30)

        self.material_information_fields = ["Material Information:", "Name: ", "Website:", "Equation Values", "Equation"]

        self.resonator_material_information_labels = []
        self.prism_material_information_labels = []
        self.resonator_radiobuttons = []
        self.prism_radiobuttons = []

        for i in range(len(self.material_information_fields)):
            self.resonator_material_information_labels.append(Tk.Label(window, text=self.material_information_fields[i], font= 30))
            self.resonator_material_information_labels[i].grid(row=len(material_names)+3+i, column=resonator_column)

            self.prism_material_information_labels.append(Tk.Label(window, text=self.material_information_fields[i], font= 30))
            self.prism_material_information_labels[i].grid(row=len(material_names)+3+i, column=prism_column)

        for i in range(len(material_names)): 
                self.resonator_radiobuttons.append(Tk.Radiobutton(window, text=material_names[i], variable=self.resonator_material_index, value=i))
                self.resonator_radiobuttons[i].grid(row=i+3,column=resonator_column)

                self.prism_radiobuttons.append(Tk.Radiobutton(window, text=material_names[i], variable=self.prism_material_index, value=i))
                self.prism_radiobuttons[i].grid(row=i+3,column=prism_column)

        # Grid
        self.wavelength_entry.grid(row=0, column=0)
        self.update_button.grid(row=0, column=1)
        self.prism_angle1_entry.grid(row=1, column=0)
        self.prism_angle2_entry.grid(row=1, column=1)
        self.prism_angle3_entry.grid(row=1, column=2)
        self.resonator_material_label.grid(row=2, column=resonator_column)
        self.prism_material_label.grid(row=2, column=prism_column)

class Controller(object):
    def __init__(self, GUI):

        self.GUI = GUI

        self.material_list = MaterialList()

        self.GUI.update_button.configure(command=lambda:self.update_pressed())
        

    def update_pressed(self):

        # Collecting variables:
        self.wavelength = float(self.GUI.wavelength.get())
        resonator_material_index = self.GUI.resonator_material_index.get()
        prism_material_index = self.GUI.prism_material_index.get()

        # self.resonator_material = material_names[resonator_material_index]
        # self.prism_material = material_names[prism_material_index]
        # print([self.resonator_material,self.prism_material])

        # self.resonator_material = self.material_list[resonator_material_index]
        # self.prism_material = self.material_list[]

        # print([material_name_index[resonator_material_index], material_name_index[prism_material_index]]) # Works

        print(self.material_list.information[resonator_material_index].name) # As this loads the name, this can also load the rest of the information related to it.

        # test_string = material_name_index[resonator_material_index]
        # test = getattr(self.material_list.calcite, "name")
        # print(test)

        # print(self.material_list.calcite.name)


    def make_material_list(self):
        index = 0

        # material_calculation = "calcite"
        # term = str("self.material_list." + material_calculation + ".equation_values[0][1][index]")
        # term_value = eval(term)
        # print(term_value+1)
        # output = eval("self.material_list.calcite.equation_values[0][1][index] + 1") # WORKS
        # output = eval("self.material_list.calcite.equation_values[0][1][index] + 1") 
        # print(output)

if __name__ == '__main__':
    root = Tk.Tk()
    view = GUI(root)
    controller = Controller(view)
    root.mainloop()