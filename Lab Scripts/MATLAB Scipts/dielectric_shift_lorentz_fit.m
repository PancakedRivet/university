% LorentzFit
clear all
clc

script_folder = 'C:\Users\devpa499\Documents\BitBucket\python-piezocontrol\MATLAB Scipts';
substrate_folder = 'C:\Users\devpa499\Documents\MATLAB\Project\Data 04-09-18\4.00';

cd(substrate_folder);

data_array = dlmread('sweep1.dat',' ',2,0); % loading data from file
bounds = [4881 5259];
cd(script_folder)

[frequency_x_axis, frequency_traces, central_minimums] = frequency_scaling(data_array,[4644, 5511],50,1,150); %Calling the frequency_scaling function

[Ext,Linewidth,Amplitude,Shift,pal,po]=LocalSmoothLorentzFit(data_array(:,1),bounds(1),bounds(2),0);

% data = linspace(0,10,11)';
% 
% [idx1]=find(data(:,1)<3);
% % idx1=find(abs(data(:,1)-4) < 0.001);
% lx=idx1(end);
% % lx=1;
% [idx2]=find(data(:,1)>7);
% rx=idx2(1);
% % rx=500;
% Irange=lx:rx;
% % Irange=Left:Right;
% 
% plot(data(Irange,1),'g-')