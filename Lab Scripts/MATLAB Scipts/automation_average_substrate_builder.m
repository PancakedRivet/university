%Automating building an average plot for each substrate.
%Requires automation.m to run to generate the complete.mat file first.
%Sweeps for comparrison require the same length dimensions per variable
%Written by Patrick Devane
%Last Modified: 14/07/18

% Loading:
clc %Clearing the command window
close all %Closing all open figures
clear all %Clearing all data from the workspace
set(0,'DefaultAxesFontSize',18); %Increasing the default plot text size

tested_window_input = input('Tested Window Type? ','s');

script_folder = 'C:\Users\devpa499\Documents\BitBucket\python-piezocontrol\MATLAB Scipts';
substrate_folder = fullfile('C:\Users\devpa499\Documents\MATLAB\Project', tested_window_input);

cd(substrate_folder);
folder_contents = dir;
sweeps = folder_contents(3:end-1);
number_of_sweeps = length(sweeps);

sweep_direction_array = {};

for k = 1:number_of_sweeps;
    sweep_name = sweeps(k).name;
    sweep_folder_name = fullfile('C:\Users\devpa499\Documents\MATLAB\Project', tested_window_input, sweep_name);
    cd(sweep_folder_name)
    workspace = ['auto_complete_' sweep_name '_data.mat'];
    load(workspace,'exponential_fit_line_noshift','traces_noshift','distances','linear_fit_line','exponential_fit_line','central_minimum_frequency','refractive_index','sweep_direction','cutoff_distance');
    
    exponential_fit_line_noshift_array(k,:) = exponential_fit_line_noshift(1:end);
    traces_noshift_array(k,:) = traces_noshift(1:end);
    linear_fit_line_array(k,:) = linear_fit_line(1:end);
    exponential_fit_line_array(k,:) = exponential_fit_line(1:end);
    central_minimum_frequency_array(k,:) = central_minimum_frequency(1:end);
    sweep_direction_array(k) = cellstr(sweep_direction(1:end));
    cutoff_distance_array(k) = cutoff_distance;

    if strcmp(char(sweep_direction_array(k)),'Backwards') == 1
        exponential_fit_line_noshift_array(k,:) = fliplr(exponential_fit_line_noshift_array(k,:));
        traces_noshift_array(k,:) = fliplr(traces_noshift_array(k,:));
        linear_fit_line_array(k,:) = fliplr(linear_fit_line_array(k,:));
        exponential_fit_line_array(k,:) = fliplr(exponential_fit_line_array(k,:));
        central_minimum_frequency_array(k,:) = fliplr(central_minimum_frequency_array(k,:));
    end
    
    disp(['Loaded: ' sweep_name])
end

if sign(distances(1)-distances(end)) < 0;
    distances = fliplr(distances);
end

clear exponential_fit_line_noshift traces_noshift linear_fit_line exponential_fit_line central_minimum_frequency sweep_direction

%% Analysing
average_all_traces = mean(central_minimum_frequency_array);
average_cutoff_distance = mean(cutoff_distance_array);
[~, cutoff_index] = min( abs(distances-average_cutoff_distance));

average_linear_fit_data = [average_all_traces(1:cutoff_index)', distances(1:cutoff_index)];
average_exponential_fit_data = [average_all_traces(cutoff_index:end)', distances(cutoff_index:end)];
        
average_linear_fit = polyfit(average_linear_fit_data(:,2),average_linear_fit_data(:,1),1);
average_linear_fit_line = average_linear_fit(1)*distances + average_linear_fit(2);
average_exponential_fit = coeffvalues(fit(average_exponential_fit_data(:,2),average_exponential_fit_data(:,1),'exp1'));
average_exponential_fit_line = average_exponential_fit(1)*exp(average_exponential_fit(2)*distances);

average_trace_noshift = average_all_traces' - average_linear_fit_line;
average_exponential_fit_noshift = coeffvalues(fit(distances,average_trace_noshift,'exp1','StartPoint',[0,0]));
average_exponential_fit_line_noshift = average_exponential_fit_noshift(1)*exp(average_exponential_fit_noshift(2)*distances);

%% Plotting
figure(1)
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0.64, 0.3, 0.3]); %Setting the position of the figure
hold on
for k = 1:number_of_sweeps;
    if strcmp(char(sweep_direction_array(k)),'Forwards') == 1
        p_for = plot(central_minimum_frequency_array(k,:),distances,'y');
    else
        p_back = plot(central_minimum_frequency_array(k,:),distances,'c');
    end
end
p_lin_av = plot(average_all_traces(1:cutoff_index),distances(1:cutoff_index),'r');
p_linfit_av = plot(average_linear_fit_line,distances,'m');
p_exp_av = plot(average_all_traces(cutoff_index:end),distances(cutoff_index:end),'b');
p_expfit_av = plot(average_exponential_fit_line,distances,'g');
legend([p_for p_back p_exp_av p_lin_av p_expfit_av p_linfit_av],'Towards WGMR','Away from WGMR','Mean Exp','Mean Lin','Mean Fitted Exp','Mean Fitted Lin','Location','northwest')
hold off
title(['Average ' tested_window_input ' Trace (' num2str(number_of_sweeps) ' Traces)'])
xlabel('Frequency Detuning of Mode (MHz)')
ylabel('Substrate-WGMR Distance (\mum)')
disp(' ')
disp('Figure 1 Sucessful')

figure(2)
hold on
for k = 1:number_of_sweeps;
    p_noshift = plot(traces_noshift_array(k,:),distances,'y');
    p_fit_noshift = plot(exponential_fit_line_noshift_array(k,:),distances,'c');
    p_av_noshift = plot(average_trace_noshift,distances,'k');
    p_fit_av_noshift = plot(average_exponential_fit_line_noshift,distances,'g');
end
hold off
legend([p_noshift p_fit_noshift p_av_noshift p_fit_av_noshift],'Traces','Traces-Fitted Exp','Mean Trace','Mean Trace-Fitted Exp','Location','northwest')
title(['Average ' tested_window_input ' Trace without Linear Drift'])
xlabel('Frequency Detuning of Mode (MHz)')
ylabel('Substrate-WGMR Distance (\mum)')
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0.3, 0.64, 0.3, 0.3]); %Setting the position of the figure
disp('Figure 2 Sucessful')

%% Saving
cd(substrate_folder);
variables_to_save = {'average_*','distances','number_of_sweeps','tested_window_input','refractive_index'};
workspace = ['average_' tested_window_input '_data.mat'];
save(workspace, variables_to_save{:})
disp(' ')
disp(['Saved: ' workspace])