%Automating MATLAB data import, data import starts at data_0.dat
%Written by Patrick Devane
%Last Modified: 14/07/18

%Loading the files into MATLAB
clc %Clearing the command window
close all %Closing all open figures
clear all %Clearing all data from the workspace
data_length = 5000; %Number of data points per file
set(0,'DefaultAxesFontSize',18); %Increasing the default plot text size

tested_window_input = input('Tested Window Type? ','s');
tested_sweep_input = input('Sweep Number? ','s');

script_folder = 'C:\Users\devpa499\Documents\BitBucket\python-piezocontrol\MATLAB Scipts';
data_folder = fullfile('C:\Users\devpa499\Documents\MATLAB\Project\Window Data', tested_window_input, tested_sweep_input);

cd(data_folder);

first_file = 0; %Setting the first data file to be loaded. Default is data_0.dat
last_file = length(dir('*.dat')); %Setting the last data file to be loaded. Default is the last data file in the folder

filename = ['auto_' tested_sweep_input '_data.mat'];
workspace_data_exists = exist(filename,'file');
if first_file == last_file
    disp('No files found. Stopping program...')
    return
end

if workspace_data_exists == 0
    cd(script_folder);
    [data_array, file_info, distances, files_loaded, zero_reference_trace, sweep_direction] = load_data(data_folder ,data_length, first_file, last_file, tested_sweep_input); %Loading the data into MATLAB from the .dat files
else
    load(filename);
    disp(['Loaded: ' filename])
end

refractive_index_exists = exist('refractive_index','var');
if refractive_index_exists == 0
    refractive_index_input = input(['Refractive index of ' tested_window_input '? '],'s');
    refractive_index = str2double(refractive_index_input);
    save(filename,'refractive_index','-append')
end

colour_gradient = linspace(0,1,files_loaded); %Setting a colour gradient for the number of files found

%% Plot 1: The raw data outputted to a plot with the trace to be used as the zero reference in green
figure(1)
hold on
for k = 1:files_loaded; %Plotting data
    if k == zero_reference_trace
        plot(data_array(k,:),'g') %Plotting the trace for the zero reference in green for visibility
    else
        plot(data_array(k,:),'Color',[colour_gradient(k) 0 1-colour_gradient(k)]); %Plotting the data of each file from blue to red in colour
    end
end
title('Raw Data')
xlabel('Sample Number (#)')
ylabel('Voltage (V)')
% legend(file_info(:,4),'Location','northeastoutside'); %Setting the legend to be in microns
hold off
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0.64, 0.3, 0.3]); %Setting the position of the figure
disp(' ')
disp('Figure 1: Raw Plot Sucessful')
saveas(gcf,'auto_fig1_raw.fig')
saveas(gcf,'auto_fig1_raw.png')
saveas(gcf,'auto_fig1_raw.jpeg')
disp('Figure 1: Raw Plot Saved')

bounds_exist = exist('bounds','var');
if bounds_exist == 0
    disp(' ')
    disp('Select the lower and upper bounds of the trace for analysis: ')
    [sidebands,~] = ginput(2);
    bounds = [fix(sidebands(1)), fix(sidebands(2))]; %Bounds on the file to eliminate the noise between spectrums
    save(filename,'bounds','-append')
    disp('Bounds Saved')
end

%% Plot 2: The limited spectra around the 2 limits specified
side_band_frequency_exists = exist('side_band_frequency','var');
if side_band_frequency_exists == 0
    side_band_frequency_input = input('Sideband Frequency (MHz)? ','s');
    side_band_frequency = str2double(side_band_frequency_input); %Setting the sideband frequency for the scale factor (MHz)
    save(filename,'side_band_frequency','-append')
end
search_length = 150; %Setting the minimum number of samples to wait before finding the next minimum
central_minimums_exists = exist('central_minimums','var');
if central_minimums_exists == 0
    cd(script_folder)
    [frequency_x_axis, frequency_traces, central_minimums] = frequency_scaling(data_array,bounds,side_band_frequency,zero_reference_trace,search_length); %Calling the frequency_scaling function
    cd(data_folder)
end

figure(2)
hold on
for k = 1:files_loaded; %Plotting data
    if k == zero_reference_trace
        plot(frequency_x_axis,frequency_traces(k,:),'g') %Plotting the trace for the zero reference in green for visibility
    else
        plot(frequency_x_axis,frequency_traces(k,:),'Color',[colour_gradient(k) 0 1-colour_gradient(k)]); %Plotting the data of each file from blue to red in colour
    end
end
title('Frequency Shift of Mode')
xlabel('Frequency Shift (MHz)')
ylabel('Voltage (V)')
% legend(file_info(:,4),'Location','northeastoutside'); %Setting the legend to be in microns
xlim([frequency_x_axis(end),frequency_x_axis(1)]) %Removing the extra whitespace on either side of the trace in the plot
hold off
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0.3, 0.64, 0.3, 0.3]); %Setting the position of the figure
disp(' ')
disp('Figure 2: Frequency Plot Sucessful')
saveas(gcf,'auto_fig2_freq.fig')
saveas(gcf,'auto_fig2_freq.png')
saveas(gcf,'auto_fig2_freq.jpeg')
disp('Figure 2: Frequency Plot Saved')

%% Plot 3: The PColour plot for the frequency shift
figure(3)
pcolor(frequency_x_axis,distances,frequency_traces); %Generating a pcolor plot of the data
title('Frequency Detuning of Mode')
xlabel('Frequency Detuning from Maximum Displacement (MHz)')
ylabel('Substrate-Resonator Distance (\mum)')
shading flat
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0.6, 0.64, 0.3, 0.3]); %Setting the position of the figure
disp(' ')
disp('Figure 3: PColor Sucessful')
saveas(gcf,'auto_fig3_pcolor.fig')
saveas(gcf,'auto_fig3_pcolor.png')
saveas(gcf,'auto_fig3_pcolor.jpeg')
disp('Figure 3: PColor Plot Saved')

%% Figure 4: Breaking the mode trace into linear and exponential pieces
central_minimum_frequency = frequency_x_axis(central_minimums(:));

figure(4)
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0.28, 0.3, 0.3]); %Setting the position of the figure
hold on
plot(central_minimum_frequency,distances,'k')

cutoff_distance_exists = exist('cutoff_distance','var');
if cutoff_distance_exists == 0
    disp(' ')
    disp('Select the substrate-resonator distance to decompose the result into linear and exponential pieces: ')
    [~, cutoff_distance_input] = ginput(1);
    cutoff_distance = fix(cutoff_distance_input);
    if cutoff_distance > max(distances)
        cutoff_distance = max(distances);
    end
    if cutoff_distance < min(distances)
        cutoff_distance = min(distances);
    end
    save(filename,'cutoff_distance','-append')
    disp(['Cutoff distance saved: ' num2str(cutoff_distance) 'microns'])
    disp(' ')
end

cutoff_index = find(distances == cutoff_distance);

switch sweep_direction
    case 'Forwards'
        linear_fit_data = [central_minimum_frequency(1:cutoff_index)', distances(1:cutoff_index)];
        exponential_fit_data = [central_minimum_frequency(cutoff_index:end)', distances(cutoff_index:end)];
        
        switch cutoff_distance
            case 0
                linear_fit = polyfit(linear_fit_data(:,2),linear_fit_data(:,1),1);
                linear_fit_line = linear_fit(1)*distances + linear_fit(2);
                p_lin = plot(central_minimum_frequency(1:cutoff_index),distances(1:cutoff_index),'r');
                p_linfit = plot(linear_fit_line,distances,'m');
                legend([p_lin p_linfit],'Lin','Fitted Lin','Location','southeast')
        
            case max(distances)
                exponential_fit = coeffvalues(fit(exponential_fit_data(:,2),exponential_fit_data(:,1),'exp1'));
                exponential_fit_line = exponential_fit(1)*exp(exponential_fit(2)*distances);
                p_exp = plot(central_minimum_frequency(cutoff_index:end),distances(cutoff_index:end),'b');
                p_expfit = plot(exponential_fit_line,distances,'g');
                legend([p_exp p_expfit],'Exp','Fitted Exp','Location','southeast')
        
            otherwise
                linear_fit = polyfit(linear_fit_data(:,2),linear_fit_data(:,1),1);
                linear_fit_line = linear_fit(1)*distances + linear_fit(2);
                exponential_fit = coeffvalues(fit(exponential_fit_data(:,2),exponential_fit_data(:,1),'exp1'));
                exponential_fit_line = exponential_fit(1)*exp(exponential_fit(2)*distances);
                p_lin = plot(central_minimum_frequency(1:cutoff_index),distances(1:cutoff_index),'r');
                p_linfit = plot(linear_fit_line,distances,'m');
                p_exp = plot(central_minimum_frequency(cutoff_index:end),distances(cutoff_index:end),'b');
                p_expfit = plot(exponential_fit_line,distances,'g');
                legend([p_exp p_lin p_expfit p_linfit],'Exp','Lin','Fitted Exp','Fitted Lin','Location','southeast')
        end
        
    case 'Backwards'
        linear_fit_data = [central_minimum_frequency(cutoff_index:end)', distances(cutoff_index:end)];
        exponential_fit_data = [central_minimum_frequency(1:cutoff_index)', distances(1:cutoff_index)];
        
        switch cutoff_distance
            case 0
                linear_fit = polyfit(linear_fit_data(:,2),linear_fit_data(:,1),1);
                linear_fit_line = linear_fit(1)*distances + linear_fit(2);
                p_lin = plot(central_minimum_frequency(cutoff_index:end),distances(cutoff_index:end),'r');
                p_linfit = plot(linear_fit_line,distances,'m');
                legend([p_lin p_linfit],'Lin','Fitted Lin','Location','southeast')
        
            case max(distances)
                exponential_fit = coeffvalues(fit(exponential_fit_data(:,2),exponential_fit_data(:,1),'exp1'));
                exponential_fit_line = exponential_fit(1)*exp(exponential_fit(2)*distances);
                p_exp = plot(central_minimum_frequency(1:cutoff_index),distances(1:cutoff_index),'b');
                p_expfit = plot(exponential_fit_line,distances,'g');
                legend([p_exp p_expfit],'Exp','Fitted Exp','Location','southeast')
        
            otherwise
                linear_fit = polyfit(linear_fit_data(:,2),linear_fit_data(:,1),1);
                linear_fit_line = linear_fit(1)*distances + linear_fit(2);
                exponential_fit = coeffvalues(fit(exponential_fit_data(:,2),exponential_fit_data(:,1),'exp1'));
                exponential_fit_line = exponential_fit(1)*exp(exponential_fit(2)*distances);
                p_lin = plot(central_minimum_frequency(cutoff_index:end),distances(cutoff_index:end),'r');
                p_linfit = plot(linear_fit_line,distances,'m');
                p_exp = plot(central_minimum_frequency(1:cutoff_index),distances(1:cutoff_index),'b');
                p_expfit = plot(exponential_fit_line,distances,'g');
                legend([p_exp p_lin p_expfit p_linfit],'Exp','Lin','Fitted Exp','Fitted Lin','Location','southeast')
        end
end

hold off
disp(' ')
disp('Fits Sucessful')
title('Frequency Detuning of Mode')
xlabel('Frequency Detuning from Maximum Displacement (MHz)')
ylabel('Substrate-Resonator Distance (\mum)')
disp('Figure 4: Fitted Plot Sucessful')
saveas(gcf,'auto_fig4_fits.fig')
saveas(gcf,'auto_fig4_fits.png')
saveas(gcf,'auto_fig4_fits.jpeg')
disp('Figure 4: Fitted Plot Saved')

%% Figure 5: Mode detuning with the linear component removed
traces_noshift = central_minimum_frequency' - linear_fit_line;
exponential_fit_noshift = coeffvalues(fit(distances,traces_noshift,'exp1','StartPoint',[0,0]));
exponential_fit_line_noshift = exponential_fit_noshift(1)*exp(exponential_fit_noshift(2)*distances);

figure(5)
hold on
plot(traces_noshift,distances,'k')
plot(exponential_fit_line_noshift,distances,'g')
hold off
title('Frequency Detuning of Mode (Drift Removed)')
xlabel('Frequency Detuning from Maximum Displacement (MHz)')
ylabel('Substrate-Resonator Distance (\mum)')
legend('data','fitted exp','Location','southeast')
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0.3, 0.28, 0.3, 0.3]); %Setting the position of the figure
disp(' ')
disp('Figure 5: Cleaned Trace Sucessful')
saveas(gcf,'auto_fig5_clean.fig')
saveas(gcf,'auto_fig5_clean.png')
saveas(gcf,'auto_fig5_clean.jpeg')
disp('Figure 5: Cleaned Trace Saved')

%% Figure 6: Error bars:

traces_noshift = central_minimum_frequency' - linear_fit_line;
exponential_fit_noshift = coeffvalues(fit(distances,traces_noshift,'exp1','StartPoint',[0,0]));
exponential_fit_line_noshift = exponential_fit_noshift(1)*exp(exponential_fit_noshift(2)*distances);
%
index_0 = find(distances == 0);
index_1 = find(distances == 1);
micron_index = abs(index_0 - index_1);

errorbar_x = traces_noshift(1:micron_index:end,:);
errorbar_y = distances(1:micron_index:end,:);
errorbar_error = 0.05*ones(size(errorbar_y));
%
figure(5)
hold on
plot(traces_noshift,distances,'k')
plot(exponential_fit_line_noshift,distances,'g')

errorbar(errorbar_x, errorbar_y, errorbar_error, 'r')

hold off
title('Frequency Detuning of Mode (Drift Removed)')
xlabel('Frequency Detuning from Maximum Displacement (MHz)')
ylabel('Substrate-Resonator Distance (\mum)')
legend('data','fitted exp','Location','southeast')
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0.6, 0.28, 0.3, 0.3]); %Setting the position of the figure
disp(' ')
disp('Figure 5: Cleaned Trace Sucessful')
saveas(gcf,'auto_fig5_clean.fig')
saveas(gcf,'auto_fig5_clean.png')
saveas(gcf,'auto_fig5_clean.jpeg')
disp('Figure 5: Cleaned Trace Saved')

%% END

complete_workspace = ['auto_complete_' tested_sweep_input '_data.mat'];
save(complete_workspace)
disp(' ')
disp(['Saved: ' complete_workspace])

% FUTURE:
% Figure : Mode broadening/Linewidth measurement
% Figure : Comparing two sucessive sweeps (forwards and backwards)