% LocalSmoothLorentzFit.m
%
% function [Extremum,Linewidth,Amplitude,Shift]=LocalSmoothLorentzFit(Data,Left,Right,debug)
%
% finds the fitted maxima with a Lorentz fit, needs the Data and a Left and a Right value
% between where the extremum is to be found.
%
%   debug=1 allows for plotting of the initial estimate and the actual fit
%           have the hold off before
%
%   Harald Schwefel (Feb. 2014)

function [Extremum,Linewidth,Amplitude,Shift,losx,losy]=LocalSmoothLorentzFit(Data,Left,Right,debug)

% range around the approximate peak
[idx]=find(Data(:,1)<Left);
lx=idx(end);
% lx=1;
[idx]=find(Data(:,1)>Right);
rx=idx(1);
% rx=500;
Irange=lx:rx;
% Irange=Left:Right;

% if debug % shows the selected range and the central point
%     plot(Data(Irange,1),Data(Irange,2),'g-')
% end

%% this model is a Lorentzian fit

model=@(a,x)(a(4)-a(3)*(a(1)./((x-a(2)).^2+(0.5*a(1)).^2)));

% Find minima and maxima
[yMin,iMin]=min(Data(Irange,2));
[yMax,iMax]=max(Data(Irange,2));
idxL=find(Data(Irange(1:iMin),2)<(yMin+(yMax-yMin)/2));
idxR=find(Data(Irange(iMin:end),2)>(yMin+(yMax-yMin)/2));

fwhm=Data(Irange(idxR(end)),1)-Data(Irange(idxL(1)),1);
Linewidth_guess=fwhm;
Extremum_guess=Data(Irange(iMin),1);
Amplitude_guess=(yMax-yMin)*fwhm/2;
shift_guess=Data(Irange(iMax),2);

a0=[Linewidth_guess,Extremum_guess,Amplitude_guess,shift_guess];

%if debug % tests the approximated parameters and plot the curve
    x=linspace(Data(1,1),Data(end,1),10000);
    y=model(a0,x);
   % plot(x,y,'r')
%end
%% does the nonlinear fit, following Levenberg-Marquardt
xx=Data(Irange,1);
yy=Data(Irange,2);
[ahat,r,J,cov,mse] = nlinfit(xx,yy,model,a0);

if debug % tests the approximated parameters and plot the curve
    figure; plot(x,model(ahat,x),Data(:,1),Data(:,2))
end
losx(:,1)=x;
losy(:,1)=model(ahat,x);
q=max(model(ahat,x));
qq=min(model(ahat,x));
Linewidth=ahat(1);
Extremum=ahat(2);
Amplitude=q-qq;
Shift=ahat(4);
end % function
