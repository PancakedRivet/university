% Reading data into MATLAB to generate WGMR plots
% Program written by Alfredo Rueda and edited by Patrick Devane
% Last Updated: 23/02/19
%%%%%%%%%%%%%%%%%%%%%%%%%%%% User Preferences %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sources of sweep data: Uncomment the used one
% file_location = 'C:\Users\ROLab\Desktop\Alfredo Program Lab Data\Patrick\24-02-19\'; %Patrick Program Save location
file_location = 'C:\Users\ROLab\Desktop\Alfredo Program Lab Data\'; %Patrick Program Save location
% file_location = 'C:\Users\ROLab'; %Alfredo Program Save location
% file_location = 'C:\Users\ROLab\Desktop\datatryinh';
% file_location = 'C:\Users\devpa499\Documents\MATLAB\Project\23-02-19\';
% file_location = 'C:\Users\devpa499\Documents\MATLAB\Project\Best data sets\CaF2';

% lorentz_fit_location = 'C:\Users\devpa499\Documents\BitBucket\python-piezocontrol\MATLAB Scipts';
lorentz_fit_location = 'C:\Users\ROLab\Desktop\datatryinh';

file_name_prefix = 'CaF2_mode2_test3_f'; % Name of the file prefix

start_file_number = 1; % First file number to be loaded
end_file_number = 100; % Last file number to be loaded

see_the_fits = false; % Change to true to see the Lorentzian fits of each sweep
see_exponential_fit = false; % Change to true to see the exponential fit to the linewidth
see_dielectric_shift = false; % Change to true to see the dielectric shift and linewidth in detail
see_pcolor = false; % Change to see the pcolor plot of the raw data
see_custom_title = false; % Change to true to overwrite default figure titles

save_file_information = true; % Saves the file information to a file for later loading speed
load_file_information = false; % Loads the file information from a file for speed

save_file_types = {'-djpeg'}; % Figure file type extensions. Add '-d' and the file extension (jpeg, png etc)

laser_frequency = 195.3*10^6; % f(1550nm) = 195.3*10^6 Hz
sweepstep = 0.1; % Voltage change of each step

search_range = 1500; % Specify how much extra data LorentzSmoothFit.m looks through to either side of the given bounds
max_fun_eval_value = 1000; % Increase this number if the output complains about incomplete iterations

custom_title = 'CaF_2 Substrate';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
cd(file_location)

number_of_files = length(dir([file_name_prefix '*.dat']))  % Uncomment this to automatically determine end_file_number
% end_file_number = number_of_files + start_file_number - 1;

% set(0,'DefaultAxesFontSize',18); % Change the default plot text size

disp(['end_file_number = ' num2str(end_file_number)])

clear existing_entry_index file_information
file_information_is_exist = exist('file_information.mat','file');
file_information_is_load = 0;

if load_file_information == true % Loading information from file
    
    disp('Loading data from file')
    
    if  file_information_is_exist == 0 % Creating new .mat file if none exists
        
        file_information = {file_name_prefix, start_file_number, end_file_number};
        disp('No file information exists! Loading stopped, using local variables')
        
    else
        load('file_information.mat')
        
        file_information_string = string(file_information);
        file_name_prefix_list = file_information_string(:,1);
        
        if max(strcmp(file_name_prefix, file_name_prefix_list)) == 1
            
            existing_entry_index = find(strcmp(file_name_prefix, file_name_prefix_list));
            
            start_file_number = file_information{existing_entry_index, 2};
            end_file_number = file_information{existing_entry_index, 3};
            
            disp(['Loaded ' file_name_prefix ' data:'])
            disp('start_file_number =')
            disp(start_file_number)
            disp('end_file_number =')
            disp(end_file_number)
            
            file_information_is_load = 1;
           
        else
            disp([file_name_prefix ' not found in file_information.mat! Using local variables'])
            disp(' ')
        end
    end
end

sweepstep = sweepstep*10;
results = zeros(end_file_number-start_file_number+1, 7);
next_results_entry = 1;

if see_pcolor
    data_array = zeros(end_file_number-start_file_number+1, 10000);
end

for i=start_file_number:sweepstep:end_file_number % Building a step_x variable for each step loaded
nombre = strcat(file_name_prefix,strrep('_#V.dat','#',num2str( i )));

if see_pcolor && i == start_file_number
    axis_read = dlmread(nombre,' ');
    frequency_axis = axis_read(:,1);
end

try
    A = dlmread(nombre,' ');
catch ME
    warning(['Problem with reading from file! Aborting! Last file loaded: ' num2str(i - 1)]);
    warning([num2str(end_file_number - i) ' files not loaded! Setting end_file_number = ' num2str(i - 1)]);
    end_file_number = i - 1;
    msgText = getReport(ME)
    break
end
eval(['step_' num2str(i) ' = A;']);

if see_pcolor
    data_array(next_results_entry,:) = A(:,2); % Loading the data array
    next_results_entry = next_results_entry + 1;
end

clear A;
end

cd(lorentz_fit_location)

h = waitbar(0,'warte mf warte...');

next_results_entry = 1;

for file_number = start_file_number:sweepstep:end_file_number
 
eval(['data = step_' num2str(file_number) ' ;']);

if (file_number==start_file_number)
    previous_extremum_minimum = find(data(:,1)==0); % Finding the index of 0 on the x-axis in the first setp_data file
else
    [~,previous_extremum_minimum] = min( abs(data(:,1)-results(next_results_entry-1, 6)) ); % Finds the index of the previous extremum
end

left_data_index = previous_extremum_minimum-search_range;
right_data_index = previous_extremum_minimum+search_range;

if left_data_index < 1 % Stopping array bounding error for Lorentz fit call
    left_data_index = 1;
end
if right_data_index > size(data,1)
    right_data_index = size(data,1);
end

left_data_point = data(left_data_index,1);
right_data_point = data(right_data_index,1);

try % Error catching so that the program doesn't stop when not enough files are loaded.
    [Ext,Linewidth,Amplitude,~,pal,po]=LocalSmoothLorentzFit(data, left_data_point, right_data_point, 0); % ~ = Shift
catch ME
    warning(['Problem with Lorentz Fit! Aborting! Last file loaded: ' num2str(next_results_entry - 1)]);
    warning([num2str(end_file_number - next_results_entry - 1) ' files not loaded!']);
    results = results(1:next_results_entry - 1, :); % Re-slice array where the error occurred
    end_file_number = start_file_number + next_results_entry - 1;
    msgText = getReport(ME)
    break
end
    
results(next_results_entry,1) = file_number/10; % X-axis data
results(next_results_entry,2) = Amplitude; % Coupling contrast
results(next_results_entry,3) = abs(laser_frequency/Linewidth); % Q-factor
results(next_results_entry,4) = abs((Amplitude*Ext)/Linewidth); % ???
results(next_results_entry,5) = abs(Linewidth); % Linewidth
results(next_results_entry,6) = Ext; % Extremum of the fit

if see_the_fits == true
    nombre=strrep('step: # volt','#',num2str( results(next_results_entry,1) ));
    figure % create new figure
    plot(data(:,1),data(:,2),'ro')
    hold on
    plot(data(previous_extremum_minimum-search_range:previous_extremum_minimum+search_range,1),data(previous_extremum_minimum-search_range:previous_extremum_minimum+search_range,2),'gx')
    plot(pal,po,'b-')
    hold off
    title(nombre)
    xlabel('Frequency MHz')
    xlim([data(previous_extremum_minimum-search_range) data(previous_extremum_minimum+search_range)])
    set(gca,'FontSize',24)
end

clear tu Ext Linewidth Amplitude Shift
next_results_entry = next_results_entry + 1;
waitbar(file_number/ (end_file_number - start_file_number + 1))

end

% offset_voltage_axis = results(:,1);
% results(:,1) = 50 * (results(:,1) - max(results(:,1))); % Converting to distance
results(:,2) = results(:,2) ./ max(results(:,2)); % Make contrast relative
results(:,7) = (results(:,6)-results(1,6)) * -1; % Shift from 0, -1 ensures correct direction

if see_pcolor
    data_array = data_array(1:next_results_entry - 1, :); % Resizing data_array to account for any errors caught
end

clear 'step*'
close(h)

cd(file_location)

t = datetime('now');

figure
subplot(4,1,1) % Linewidth
plot(results(:,1),results(:,5),'ro')
set(gca,'FontSize',16)
xlabel('Step (V)')
ylabel('Linewidth (MHz)')
if see_custom_title
    title([custom_title ' Sweep Results'])
else
    title([file_name_prefix ' results ' char(t)], 'Interpreter', 'none')
end

subplot(4,1,2)
plot(results(:,1),results(:,3),'ro') % Q-Factor
set(gca,'FontSize',16)
xlabel('Step (V)')
ylabel('Q factor')

subplot(4,1,3)
plot(results(:,1),results(:,7),'ro') % Frequency Shift
set(gca,'FontSize',16)
xlabel('Step (V)')
ylabel('Shift (MHz)')

subplot(4,1,4)
plot(results(:,1),results(:,2),'ro') % Coupling Contrast
set(gca,'FontSize',16)
xlabel('Step (V)')
ylabel('Relative Contrast')

save_file_name = strcat(string(file_name_prefix), '_results');
set(gcf, 'units', 'normalized', 'Position', [0.1, 0.1, 0.5, 0.8])
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
for i = 1:size(save_file_types,2)
    print(gcf, save_file_name, string(save_file_types(i)), '-r0')
    save_message = strcat('Saved:',{' '}, save_file_name, {' '}, string(save_file_types(i)));
    disp(save_message)
end
disp(' ')

if see_exponential_fit == true || see_dielectric_shift == true
    
    figure
    
    if see_dielectric_shift
        
        subplot(2,1,1)
        plot(results(:,1), results(:,5), 'ro')
%         plot(offset_voltage_axis, results(:,5), 'ro')
        xlabel('Step (V)')
        ylabel('Linewidth (MHz)')
        title('Select the end point for the linear fit')
        
        subplot(2,1,2)
        plot(results(:,1),results(:,7),'ro')
%         plot(offset_voltage_axis,results(:,7),'ro')
        xlabel('Step (V)')
        ylabel('Shift (MHz)')
        
    else
        
        plot(results(:,1), results(:,5), 'ro')
%         plot(offset_voltage_axis, results(:,5), 'ro')
        xlabel('Step (V)')
        ylabel('Linewidth (MHz)')
        title('Select the end point for the linear fit')
        
    end

    set(gcf, 'units', 'normalized', 'Position', [0.2, 0.2, 0.5, 0.5])
    
    [fit_cutoff_input, ~] = ginput(1);

    if fit_cutoff_input > max(results(:,1))
        fit_cutoff_input = max(results(:,1));
    end

    if fit_cutoff_input < min(results(:,1))
        fit_cutoff_input = min(results(:,1));
    end
    
    fit_cutoff = find(round(fit_cutoff_input,1) == results(:,1));

    close
    
else
    fit_cutoff = 1; % Default value if neither if statement condition is met
end

% Exponential Fit:
f = @(b,x) b(1).*exp(b(2).*x) + b(3);

exp_fit_x = results(fit_cutoff:end,1);
exp_fit_y = results(fit_cutoff:end,5);

exponential_fit = coeffvalues(fit(exp_fit_x, exp_fit_y ,'exp1', 'MaxFunEvals', max_fun_eval_value));
B0 = [exponential_fit(1), exponential_fit(2), results(1,5)];

nrmrsd = @(b) norm(results(:,5) - f(b,results(:,1))); % Residual Norm Cost Function

options = optimset('MaxFunEvals',max_fun_eval_value);
[B,rnrm] = fminsearch(nrmrsd, B0, options); % Estimate Parameters �B�

intrinsic_q = laser_frequency / B(3);

if see_exponential_fit == true
    
%     lin_fit_x = results(fit_cutoff:end,1);
%     lin_fit_y = results(fit_cutoff:end,5);
%     
%     linear_fit_shift = polyfit(lin_fit_x, lin_fit_y, 1);
%     linear_fit_shift_line = (linear_fit_shift(1) * results(:,1)) + linear_fit_shift(2);
    
    figure
%     subplot(2,1,1)
    plot(results(:,1), results(:,5), 'pc')
    hold on
    plot(results(fit_cutoff:end,1),results(fit_cutoff:end,5),'pg')
    plot(results(:,1), f(B,results(:,1)), '-r')
    hold off
    grid
    set(gca,'FontSize',16)
    legend('Data','Exponential Fit Data','Exponential Fit Line','Location','NorthWest')
    xlabel('Step (V)')
    ylabel('Linewidth (MHz)')
    if see_custom_title
        title([custom_title ' Sweep Exponential Fit'])
    else
        title([file_name_prefix 'exponential fit ' char(t)], 'Interpreter', 'none')
    end
    
%     subplot(2,1,2)
%     semilogy(results(fit_cutoff:end,1),results(fit_cutoff:end,5),'pg')
%     xlabel('Step (V)')
%     ylabel('Linewidth (MHz)')
    
    save_file_name = strcat(string(file_name_prefix), '_exp');
    set(gcf, 'units', 'normalized', 'Position', [0.0, 0.05, 0.495, 0.495])
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    for i = 1:size(save_file_types,2)
        print(gcf, save_file_name, string(save_file_types(i)), '-r0')
        save_message = strcat('Saved:',{' '}, save_file_name, {' '}, string(save_file_types(i)));
        disp(save_message)
    end
    disp(' ')
    
end

if see_dielectric_shift == true
    
    lin_fit_x = results(1:fit_cutoff,1);
    lin_fit_y = results(1:fit_cutoff,7);
    
    linear_fit_shift = polyfit(lin_fit_x, lin_fit_y, 1);
    linear_fit_shift_line = (linear_fit_shift(1) * results(:,1)) + linear_fit_shift(2);
    
    shift_without_drift = results(:, 7) - linear_fit_shift_line;
    
    fit_line_error = std(linear_fit_shift_line(1:fit_cutoff));
    fit_line_error_vector = fit_line_error * ones(length(results(:,1)),1);
    
    disp(['Standard deviation of linear fit = ' num2str(fit_line_error)])
    disp(' ')

    figure
    subplot(3,1,1) % Linewidth
    plot(results(:,1),results(:,5),'ro')
    set(gca,'FontSize',16)
    xlabel('Step (V)')
    ylabel('Linewidth (MHz)')
    if see_custom_title
        title([custom_title ' Frequency Shift - Linewidth'])
    else
        title([file_name_prefix ' exponential fit ' char(t)], 'Interpreter', 'none')
    end
    
    subplot(3,1,2) % Frequency Shift
    plot(results(:,1),results(:,7),'oc')
    hold on
    plot(results(1:fit_cutoff,1),results(1:fit_cutoff,7),'og')
    plot(results(1:fit_cutoff,1), linear_fit_shift_line(1:fit_cutoff), 'r')
    hold off
    grid
    set(gca,'FontSize',16)
    legend('Data','Linear Fit Data','Linear Fit Line','Location','NorthWest')
    xlabel('Step (V)')
    ylabel('Shift (MHz)')
    if see_custom_title
        title([custom_title ' Frequency Shift - Linear Fit'])
    else
        title([file_name_prefix ' Frequency Shift - Linear fit'], 'Interpreter', 'none')
    end
    
    subplot(3,1,3) % Frequency Shift
    hold on
%     errorbar(results(:,1), shift_without_drift, fit_line_error,'y','CapSize',0)
    errorbar(results(:,1), shift_without_drift, fit_line_error_vector,'k')
    plot(results(:,1), shift_without_drift, 'ro')
    hold off
    grid
    set(gca,'FontSize',16)
    xlabel('Step (V)')
    ylabel('Shift (MHz)')
    if see_custom_title
        title([custom_title ' Frequency Shift - Linear Removed'])
    else
        title([file_name_prefix ' Frequency Shift - Linear Removed'], 'Interpreter', 'none')
    end
    
    save_file_name = strcat(string(file_name_prefix), '_shift');
    set(gcf, 'units', 'normalized', 'Position', [0.5, 0.05, 0.4, 0.495])
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    for i = 1:size(save_file_types,2)
        print(gcf, save_file_name, string(save_file_types(i)), '-r0')
        save_message = strcat('Saved:',{' '}, save_file_name, {' '}, string(save_file_types(i)));
        disp(save_message)
    end
    disp(' ')
end

if see_pcolor
    
    frequency_axis = -1 * (frequency_axis - results(1,6)); % Frequency offset correction
    distance_axis = results(:,1);
    
    figure
    pcolor(frequency_axis, distance_axis, data_array)
    shading flat
    xlabel('Relative Frequency (MHz)')
    ylabel('Step Number')
    if see_custom_title
        title(['Frequency Detuning Of ' custom_title])
    else
        title('Frequency Detuning Of Mode')
    end
    
    save_file_name = strcat(string(file_name_prefix), '_pcolor');
    set(gca,'FontSize',16)
%     set(gcf, 'units', 'normalized', 'Position', [0.5, 0.05, 0.495, 0.495])
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    for i = 1:size(save_file_types,2)
        print(gcf, save_file_name, string(save_file_types(i)), '-r0')
        save_message = strcat('Saved:',{' '}, save_file_name, {' '}, string(save_file_types(i)));
        disp(save_message)
    end
    disp(' ')
end

if save_file_information == true % Saving information to file
    switch file_information_is_exist
        case 0
            file_information = {file_name_prefix, start_file_number, end_file_number};
            
        otherwise
            switch file_information_is_load
                case 0
                    load('file_information.mat')
        
                    file_information_string = string(file_information);
                    file_name_prefix_list = file_information_string(:,1);

                    if max(strcmp(file_name_prefix, file_name_prefix_list)) == 1
                        existing_entry_index = find(strcmp(file_name_prefix, file_name_prefix_list));

                        file_information{existing_entry_index, 2} = start_file_number;
                        file_information{existing_entry_index, 3} = end_file_number;

                    else
                        file_information = [file_information; {file_name_prefix, start_file_number, end_file_number}];
                    end
                    
                case 1
                    file_information{existing_entry_index, 2} = start_file_number;
                    file_information{existing_entry_index, 3} = end_file_number;
            end
    end
    save('file_information.mat','file_information');
    disp('file_information.mat saved')
end

disp('Linewidth Exponential Fit Offset = ')
disp(B(3))
disp('Intrinsic Q-factor = ')
disp(intrinsic_q)