%Automating building a frequency versus refractive index plot for each substrate.
%Requires average_substrate_builder to run to generate the average.mat file first
%Written by Patrick Devane
%Last Modified: 14/07/18

% Loading:
clc %Clearing the command window
close all %Closing all open figures
clear all %Clearing all data from the workspace
set(0,'DefaultAxesFontSize',18); %Increasing the default plot text size

script_folder = 'C:\Users\devpa499\Documents\BitBucket\python-piezocontrol\MATLAB Scipts';
data_folder = 'C:\Users\devpa499\Documents\MATLAB\Project';