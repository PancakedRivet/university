%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Copyright Luke Trainor 2017-2018. All rights reserved %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function n = sellmeier(wavelength, material)
%sellmeier generates a material's refractive index/indices at a wavelength
%sellmeier(wavelength,material)
%The wavelength must be given in microns.
%Where the material is uniaxial, the order is [ordinary, extraordinary] .
%Where biaxial, [nx, ny, nz], where nx<ny<nz.
% A list of materials (which name to use comes first):
% calcite
% diamond
% LN (Lithium Niobate)
% MgF (Magnesium Fluoride)
% CaF (calcium fluoride)
% rutile
% SF11 (N-SF11 (SCHOTT))
% YVO (Yttrium Orthovanadate)
% YSO
% aBBO (alpha - BBO)
% silica
% hKTP (hydrothermally grown KTP)
% fKTP (flux-groxn KTP)
% sapphire
% YSZ yttria-stabilized cubic zirconia
% YAG
% Silicon
% GaAs
% BK7

if nargin ~= 2;
    error('not enough input arguments')
end

%Shitty way of swapping arguments
if ischar(wavelength)
    wavel=material;
    material=wavelength;
    wavelength=wavel;
    clear wavel
end

if exist(material,'file')==2
    % n = eval([material, '(wavelength)']);
    funHandle=str2func(material);
    n = funHandle(wavelength);
else
    error('sellmeier.m does not contain an equation for the specified material')
end

end

%% Versioning

%8/8/17 added KTP (both of them)
%23/8/17 added support for putting the arguments the wrong way around
%13/12/17 added extra materials

%% Materials

function n = aBBO(w)
%aBBO Sellmeier equation for alpha barium borate
% http://www.mt-optics.net/a-BBO.html
a=[2.7471, 2.3174];
b=[0.01878, 0.01224];
c=[0.01822, 0.01667];
d=[0.01354, 0.01516];

n=sqrt(a+b./(w^2-c)-d*w^2);
end

function n = calcite(w)
%calcite Sellmeier equation for calcite
% Ghosh 1999 http://refractiveindex.info/?shelf=main&book=CaCO3&page=Ghosh-o
a=[0.73358749, 0.35859695];
b1=[0.96464345, 0.82427830];
c1=[0.0194325203, 0.0106689543];
b2=[1.82831454, 0.14429128];
c2=[120, 120];

n=sqrt(1+a+(b1*w^2)./(w^2-c1)+(b2*w^2)./(w^2-c2));
end

function n = BK7(w)
%https://refractiveindex.info/?shelf=glass&book=BK7&page=SCHOTT
a1=1.0391212;
b1=0.00600069867;
a2=0.231792344;
b2=0.0200179144;
a3=1.01046945;
b3=103.560653;
n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = diamond(w)
%diamond Sellmeier equation for diamond
% http://refractiveindex.info/?shelf=main&book=C&page=Peter
a1=0.3306;
b1=0.1750^2;
a2=4.3356;
b2=0.1060^2;

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2));
end

function n = GaAs(w)
%diamond Sellmeier equation for GaAs
% https://refractiveindex.info/?shelf=main&book=GaAs&page=Skauli
a0=4.3725;
a1=5.4667;
b1=0.4431^2;
a2=0.0243;
b2=0.8746^2;
a3=1.9575;
b3=36.9166;

n=sqrt(1+a0+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = Silicon(w)
%diamond Sellmeier equation for Silicon
% https://refractiveindex.info/?shelf=main&book=Si&page=Salzberg
a1=10.6684;
b1=0.3015^2;
a2=0.0030;
b2=1.1348^2;
a3=1.5413;
b3=1104;

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = LN(w)
%LN Sellmeier equations for lithium niobate
% Zelmon et al. 1997 http://refractiveindex.info/?shelf=main&book=LiNbO3&page=Zelmon-o
a1=[2.6734, 2.9804];
b1=[0.01764, 0.02047];
a2=[1.2290, 0.5981];
b2=[0.05914, 0.0666];
a3=[12.614, 8.9543];
b3=[474.60, 416.08];

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = MgF(w)
%MgF Sellmeier equations for magnesium fluoride
% Dodge 1984 http://refractiveindex.info/?shelf=main&book=MgF2&page=Dodge-o
a1=[0.48755108, 0.41344023];
b1=[0.04338408^2, 0.03684262^2];
a2=[0.39875031, 0.50497499];
b2=[0.09461422^2, 0.09076162^2];
a3=[2.3120353, 2.4904962];
b3=[23.793604^2, 23.771995^2];

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = CaF(w)
%CaF Sellmeier equations for calcium fluoride
% Daimon and Masumura 2002 25 degrees .138 to 2.326 micron
a1=0.437387571;
b1=0.00173799328;
a2=0.449211397;
b2=0.00782718648;
a3=0.152068715;
b3=0.0124086125;
a4=13.0020420;
b4=4039.76541;

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3)+(a4*w^2)./(w^2-b4));
end

function n = rutile(w)
%rutile Sellmeier equations for rutile
% 
a=[5.913, 7.197];
b=[0.2441, 0.3322];
c=[0.0803, 0.0843];

n=sqrt(a+b./(w^2-c));
end

function n = SF11(w)
%SF11 Sellmeier equations for SF11 glass
% N-SF11 (SCHOTT) http://refractiveindex.info/?shelf=glass&book=SF11&page=SCHOTT
a1=1.73759695;
b1=0.013188707;
a2=0.313747346;
b2=0.0623;
a3=1.89878101;
b3=155.23629;

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = YVO(w)
%YVO Sellmeier equations for yttrium orthovanadate
% Shi et al. 2001 !!NB 0.48-1.34micron!! http://refractiveindex.info/?shelf=main&book=YVO4&page=Shi-e
a=[3.778790, 4.607200];
b=[0.07479, 0.108087];
c=[0.045731, 0.052495];
d=[0.009701, 0.014305];

if w<0.48 || w>1.34
    warning('Caution: YVO Sellmeier equations were measured for 0.48-1.34 micron')
end

n=sqrt(a+b./(w^2-c)-d*w^2);
end

function n = silica(w)
%silica Sellmeier equation for silica/fused quartz
% Malitson 1965, verified 1998 by Tan https://refractiveindex.info/?shelf=main&book=SiO2&page=Malitson

a1=0.6961663;
b1=0.0684043^2;
a2=0.4079426;
b2=0.1162414^2;
a3=0.8974794;
b3=9.896161^2;

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = hKTP(w)
%hKTP Sellmeier equations for hydrothermally grown KTP 
% Out of Handbook of Nonlinear Optical Crystals 2e
% 20 degrees Celcius

a=[2.1146, 2.1518, 2.3136];
b=[0.89188, 0.87862, 1.00012];
c=[0.20861^2, 0.21801^2, 0.23831^2];
d=[0.01320, 0.01327, 0.01679];

n=sqrt(a+(b*w^2)./(w^2-c)-d*w^2);
end

function n = fKTP(w)
%fKTP Sellmeier equations for flux-grown KTP 
% Out of Handbook of Nonlinear Optical Crystals 2e
% 20 degrees Celcius

a=[3.0065, 3.0333, 3.3134];
b=[0.03901, 0.04154, 0.05694];
c=[0.04251, 0.04547, 0.05658];
d=[0.01327, 0.01408, 0.01682];

n=sqrt(a+b./(w^2-c)-d*w^2);
end

function n = sapphire(w)
%sapphire Sellmeier equations for sapphire
% Malitson and Dodge 1972 0.2 -- 5.0 micron
% https://refractiveindex.info/?shelf=main&book=Al2O3&page=Malitson-o
a1=[1.4313493, 1.5039759];
b1=[0.0726631^2, 0.0740288^2];
a2=[0.65054713, 0.55069141];
b2=[0.1193242^2, 0.1216529^2];
a3=[5.3414021, 6.5927379];
b3=[18.028251^2, 20.072248^2];

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = YSO(w)
%YSO Sellmeier equations for yttrium orthosilicate
% http://ieeexplore.ieee.org.ezproxy.otago.ac.nz/document/59689/
%RAY BEACH et al. 1990 Optical Absorption and Stimulated Emission of Neodymium in Yttrium Orthosilicate
a=[3.0895, 3.1173, 3.1871];
b=[0.0334, 0.0283, 0.03022];
c=[0.0043, -0.0133, -0.0138];
d=[0.0199, 0.0, 0.0];

if w<0.4358 || w>0.6439
    warning('Caution: YSO Sellmeier equations were measured for 0.436-0.644 micron')
end

n=sqrt(a+b./(w^2+c)+d*w^2);
end

function n = YSZ(w)
%YSZ Sellmeier equations for yttria-stabilized cubic zirconia
% Wood and Nassau 1982: 12 mol % Y2O3; n 0.361-5.135 µm
% https://refractiveindex.info/?shelf=other&book=ZrO2-Y2O3&page=Wood
a1=1.347091;
b1=0.062543^2;
a2=2.117788;
b2=0.166739^2;
a3=9.452943;
b3=24.320570^2;

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2)+(a3*w^2)./(w^2-b3));
end

function n = YAG(w)
%YAG Sellmeier equations for yttrium aluminium garnet
% David E. Zelmon, David L. Small, and Ralph Page 1998 0.4 to 5.0 micron
% https://www.osapublishing.org/ao/abstract.cfm?uri=ao-37-21-4933
a1=2.28200;
b1=0.01185;
a2=3.27644;
b2=282.734;

n=sqrt(1+(a1*w^2)./(w^2-b1)+(a2*w^2)./(w^2-b2));
end
