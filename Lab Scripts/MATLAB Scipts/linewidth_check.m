%Checking linewidth without the extra processing
%Written by Patrick Devane
%Last Modified: 18/10/18

%Loading the files into MATLAB
clc % clearing command window
close all
clear all
set(0,'DefaultAxesFontSize',18);

script_folder = 'C:\Users\devpa499\Documents\BitBucket\python-piezocontrol\MATLAB Scipts'; % honours PC
data_folder = 'C:\Users\devpa499\Documents\MATLAB\Project\Data 12-09-18 1\'; % honours PC

% script_folder = 'C:\Users\labadmin\Documents\Patrick\BitBucket\Lab-Repo\MATLAB Scipts'; % lab PC
% data_folder = 'C:\Users\labadmin\Documents\Patrick\BitBucket\'; % lab PC

% data_folder = 'E:\Data 12-09-18 2\'; % External HDD

disp('Beginning MATLAB analysis for dielectric tuning project - single save file')
disp('Listing found substrate folders')

dir(data_folder);
tested_window_input = input('Tested Window Type? ','s');
substrate_folder = [data_folder tested_window_input];
cd(substrate_folder)
dir('*.dat')
tested_sweep_input = input('Sweep Number? ');

filename = fullfile(sprintf('./sweep%d.dat', tested_sweep_input));

sweep_name = sprintf('sweep%d',tested_sweep_input);

frequency_axis = dlmread(fullfile(sprintf('./sweep%d_callibration.dat', tested_sweep_input)),' ', 1,0);
if frequency_axis(1) > frequency_axis(end)
    frequency_axis = flipud(frequency_axis);
end

loaded_data = dlmread(filename,' ',1,0); % loading data from file

if (loaded_data(1,1) < loaded_data(1,end))
    sweep_direction = 'Forwards';
else
    sweep_direction = 'Backwards';
    loaded_data = fliplr(loaded_data);
end

first_file_load = 1; % furthest trace
[~, last_file_load] = size(loaded_data); % closest trace
% first_file_load = first_file_load + 0; % modifying loaded files
% last_file_load = last_file_load - 0;

data_array = flipud(loaded_data(2:end,first_file_load:last_file_load)); % flipped to preserve orientation
[data_length, sweep_total] = size(data_array);

distance_array = loaded_data(1,first_file_load:last_file_load);

distances = abs(linspace(distance_array(1),distance_array(end), sweep_total)-max(distance_array));

fileID = fopen(filename,'r');
sweep_data = textscan(fileID,'%*s %s %f %s %f %s %f %s %f %s %f %s %.1f %*d %s %s %s',1);
fclose(fileID);

disp(['Loaded ' filename]);

refractive_index = str2double(tested_window_input);
refractive_index_string = sprintf('(n_s_u_b=%g)',refractive_index);

[~, zero_reference_trace] = max(distances);

colour_gradient = linspace(0,1,sweep_total); % for plots
    
% Plot 1: The raw data outputted to a plot with the trace to be used as the zero reference in green
figure(1)
hold on
for k = 1:sweep_total %Plotting data 
    plot(frequency_axis,data_array(:,k),'Color',[1-colour_gradient(k) 0 colour_gradient(k)]); %Plotting the data of each file from red to blue in colour
end
plot(frequency_axis,data_array(:,zero_reference_trace),'g') %Plotting the trace for the zero reference in green for visibility
title(['Raw Data ' refractive_index_string])
xlabel('Frequency Shift (MHz)')
ylabel('Voltage (V)')
hold off
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0.64, 0.3, 0.3]); %Setting the position of the figure
cd(substrate_folder)

disp(' ')
disp('Select the lower and upper bounds of the trace for analysis: ')
[sidebands,~] = ginput(2);
bounds = [fix(sidebands(1)), fix(sidebands(2))]; %Bounds on the file to eliminate the noise between spectrums
bounds_index = zeros(1,2);
bounds_index(1,1) = find( abs(frequency_axis - sidebands(1)) < 0.1, 1);
bounds_index(1,2) = find(abs(frequency_axis - sidebands(2)) < 0.1, 1);

% Plot 2: Lorentzian Fit

cd(script_folder)

% sweep_total = 5; % uncomment this for debugging

lorentz_results = zeros(sweep_total,6);

for k = 1:sweep_total %Plotting data
    
    lorentz_data = [frequency_axis data_array(:,k)];
    [Ext,Linewidth,Amplitude,Shift,pal,po]=LocalSmoothLorentzFit(lorentz_data, lorentz_data(bounds_index(1),1), lorentz_data(bounds_index(2),1), 0); % if error, check x-axis direction
    lorentz_results(k,1)=distances(k); % abst
    lorentz_results(k,2)=Amplitude; % coupling
    lorentz_results(k,3)=abs(193.5*10^6/Linewidth); % q-factor
    lorentz_results(k,4)=abs((Amplitude*Ext)/Linewidth);
    lorentz_results(k,5)=abs(Linewidth);
    lorentz_results(k,6)=Ext;

end

frequency_axis = frequency_axis - lorentz_results(zero_reference_trace,6); % frequency offset correction
lorentz_results(:,6) = lorentz_results(:,6) - lorentz_results(zero_reference_trace,6); % frequency offset correction

figure(3)
subplot(2,1,1)
plot(lorentz_results(:,1),lorentz_results(:,5),'ro--')
title(['Mode Information ' refractive_index_string])
xlabel('Substrate-Resonator Distance (\mum)')
ylabel('Linewidth (MHz)')
ylim([0 50])
subplot(2,1,2)
hold on
plot(lorentz_results(:,1),zeros(sweep_total,1),'k-') % zero line
plot(lorentz_results(:,1),lorentz_results(:,6),'ro--')
hold off
xlabel('Substrate-Resonator Distance (\mum)')
ylabel('Freq Shift (MHz)')
ylim([-50 50])

set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0, 0.6, 0.6]);
disp(' ')
cd(substrate_folder)
% saveas(gcf,['auto_' sweep_name '_lorentz.jpeg'])
% disp('Mode Information Saved')