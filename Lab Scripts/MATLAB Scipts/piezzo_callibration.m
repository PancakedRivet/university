scale_factor = 16.1;

exp_fit_end_x_scaled = exp_fit_end_x - scale_factor;

subplot(3,1,1)
title('Alfredo program moving towards resonator')
hold on
plot(exp_fit_start_x,exp_fit_start_y)
plot(exp_fit_start)
hold off

subplot(3,1,2)
title('1\mum step backwards, still moving towards resonator')
hold on
plot(exp_fit_end_x,exp_fit_end_y)
plot(exp_fit_end)
hold off

subplot(3,1,3)
title('exponential fits and data overlayed with a difference of 16.1')
hold on
plot(exp_fit_start,exp_fit_start_x,exp_fit_start_y,'k')
plot(exp_fit_end_16_1,exp_fit_end_x_scaled,exp_fit_end_y,'b')
hold off
xlim([1,15])
% ylim([0,10])

save('calibration_fitting.mat','scale_factor','exp_fit_end_x_scaled','exp_fit_start','exp_fit_start_x','exp_fit_end_x','exp_fit_start','exp_fit_start_y','exp_fit_end_y','exp_fit_end_16_1','exp_fit_end_16','exp_fit_end_17')