function [frequency_axis, bounded_data_range, minimum_index_storage] = frequency_scaling(data_array,bounds,side_band_frequency,zero_reference_trace,search_length)
minimum_index_storage = zeros(min(size(data_array)),3); %Initializing the matrix to store the 3 minimum points per data file
bounded_data_range = data_array(:,bounds(1):bounds(2)); %Focussing on the data located between the specified bounds
[~,minimum_index_storage(:,2)] = min(bounded_data_range,[],2); %Finding the central minimas. They are the largest in magnitude of the 3
zero_frequency_index = minimum_index_storage(zero_reference_trace,2); %Finding the index of the central minima of the zero-reference-trace

for k = 1:min(size(data_array))
    left_minimum_range = data_array(k, bounds(1): bounds(1) + minimum_index_storage(k,2) - search_length); %Focussing on the data between the lower bound and the zero-point
    [~,minimum_index_storage(k,1)] = min(left_minimum_range,[],2); %Finding the left side-band between the lower bound and the zero-point
    
    right_minimum_range = data_array(k, (bounds(1) + minimum_index_storage(k,2)) + search_length : bounds(2)); %Focussing on the data between the zero-point bound and the upper bound
    [~,right_minimum_index_rough] = min(right_minimum_range,[],2); %Finding the left side-band between the zero-point and the upper bound
    minimum_index_storage(k,3) = right_minimum_index_rough + minimum_index_storage(k,2) + search_length; %Rescaling the right sideband to be relative to the lower bound
end

side_band_distances_3 = [minimum_index_storage(:,2) - minimum_index_storage(:,1) , minimum_index_storage(:,3) - minimum_index_storage(:,2) , round((minimum_index_storage(:,3) - minimum_index_storage(:,1))/2)]; %Combining the distances between sidebands
side_band_distances_1 = round(mean(side_band_distances_3,2)); %Averaging the 3 distances to find the average length used for each data file
average_side_band_distance = round(mean(side_band_distances_1)); %Averaging the lengths for each data file to find the best length to set the frequency axis
scale_factor = side_band_frequency / average_side_band_distance; %Determing the scale factor based on the number of points and the side band frequency
new_x_axis = linspace(0,bounds(2)-bounds(1),bounds(2)-bounds(1)+1); %Generating a new x-axis for the new frequency scale
frequency_axis = -(new_x_axis - zero_frequency_index) * scale_factor; %Setting the zero-reference minimum as zero and rescaling the rest of the axi accordingly
end