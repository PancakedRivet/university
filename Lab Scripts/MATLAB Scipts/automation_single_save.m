%Automating MATLAB data import, single file data saves
%Written by Patrick Devane
%Last Modified: 10/10/18

%Loading the files into MATLAB
clc % clearing command window
close all
clear all
set(0,'DefaultAxesFontSize',18);

script_folder = 'C:\Users\devpa499\Documents\BitBucket\python-piezocontrol\MATLAB Scipts'; % honours PC
data_folder = 'C:\Users\devpa499\Documents\MATLAB\Project\Data 12-09-18 1\'; % honours PC

% script_folder = 'C:\Users\labadmin\Documents\Patrick\BitBucket\Lab-Repo\MATLAB Scipts'; % lab PC
% data_folder = 'C:\Users\labadmin\Documents\Patrick\BitBucket\'; % lab PC

% data_folder = 'E:\Data 12-09-18 2\'; % External HDD

new_sweep = 0; % set to 1 to delete .mat file

disp('Beginning MATLAB analysis for dielectric tuning project - single save file')
disp('Listing found substrate folders')

dir(data_folder);
tested_window_input = input('Tested Window Type? ','s');
substrate_folder = [data_folder tested_window_input];
cd(substrate_folder)
dir('*.dat')
tested_sweep_input = input('Sweep Number? ');

filename = fullfile(sprintf('./sweep%d.dat', tested_sweep_input));

sweep_name = sprintf('sweep%d',tested_sweep_input);
workspace_file = ['auto_' sweep_name '_data.mat'];

if new_sweep == 1 && exist(workspace_file,'file') ~= 0
    delete(workspace_file);
    disp(['Deleted: ' workspace_file])
end

if exist(workspace_file,'file') == 0
    disp('No MATLAB data file found, loading from sweep file...')
    
    frequency_axis = dlmread(fullfile(sprintf('./sweep%d_callibration.dat', tested_sweep_input)),' ', 1,0);
    if frequency_axis(1) > frequency_axis(end)
        frequency_axis = flipud(frequency_axis);
    end
    
    loaded_data = dlmread(filename,' ',1,0); % loading data from file
    
    if (loaded_data(1,1) < loaded_data(1,end))
        sweep_direction = 'Forwards';
    else
        sweep_direction = 'Backwards';
        loaded_data = fliplr(loaded_data);
    end

    first_file_load = 1; % furthest trace
    [~, last_file_load] = size(loaded_data); % closest trace
%     first_file_load = first_file_load + 0; % modifying loaded files
    last_file_load = last_file_load - 100;

    data_array = flipud(loaded_data(2:end,first_file_load:last_file_load)); % flipped to preserve orientation
    [data_length, sweep_total] = size(data_array);
    
    distance_array = loaded_data(1,first_file_load:last_file_load);
    
    distances = abs(linspace(distance_array(1),distance_array(end), sweep_total)-max(distance_array));
    
    fileID = fopen(filename,'r');
    sweep_data = textscan(fileID,'%*s %s %f %s %f %s %f %s %f %s %f %s %.1f %*d %s %s %s',1);
    fclose(fileID);

    disp(['Loaded ' filename]);

    refractive_index = str2double(tested_window_input);
    refractive_index_string = sprintf('(n_s_u_b=%g)',refractive_index);
    
    [~, zero_reference_trace] = max(distances);

    colour_gradient = linspace(0,1,sweep_total); % for plots
    
    save(workspace_file,'data_array', 'frequency_axis', 'distances', 'zero_reference_trace', 'colour_gradient', 'sweep_total', 'data_length', 'sweep_direction', 'refractive_index', 'refractive_index_string', 'sweep_data')
    disp(['Saved: ' workspace_file])
else
    load(workspace_file)
    disp(['Loaded ' workspace_file])
end

%% Plot 1: The raw data outputted to a plot with the trace to be used as the zero reference in green
figure(1)
hold on
for k = 1:sweep_total %Plotting data 
    plot(frequency_axis,data_array(:,k),'Color',[1-colour_gradient(k) 0 colour_gradient(k)]); %Plotting the data of each file from red to blue in colour
end
plot(frequency_axis,data_array(:,zero_reference_trace),'g') %Plotting the trace for the zero reference in green for visibility
title(['Raw Data ' refractive_index_string])
xlabel('Frequency Shift (MHz)')
ylabel('Voltage (V)')
hold off
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0.64, 0.3, 0.3]); %Setting the position of the figure
cd(substrate_folder)
saveas(gcf,['auto_' sweep_name '_fig1_raw.fig'])
saveas(gcf,['auto_' sweep_name '_fig1_raw.png'])
saveas(gcf,['auto_' sweep_name '_fig1_raw.jpeg'])
disp(' ')
disp('Figure 1: Raw Plot Saved')

if exist('bounds','var') == 0
    disp(' ')
    disp('Select the lower and upper bounds of the trace for analysis: ')
    [sidebands,~] = ginput(2);
    bounds = [fix(sidebands(1)), fix(sidebands(2))]; %Bounds on the file to eliminate the noise between spectrums
    bounds_index = zeros(1,2);
    bounds_index(1,1) = find( abs(frequency_axis - sidebands(1)) < 0.1, 1);
    bounds_index(1,2) = find(abs(frequency_axis - sidebands(2)) < 0.1, 1);
    save(workspace_file,'bounds', 'bounds_index', '-append')
    disp('Bounds Saved')
end

%% Plot 3: Lorentzian Fit

cd(script_folder)

% sweep_total = 5; % uncomment this for debugging

lorentz_results = zeros(sweep_total,6);

for k = 1:sweep_total %Plotting data
    
    lorentz_data = [frequency_axis data_array(:,k)];
    [Ext,Linewidth,Amplitude,Shift,pal,po]=LocalSmoothLorentzFit(lorentz_data, lorentz_data(bounds_index(1),1), lorentz_data(bounds_index(2),1), 0); % if error, check x-axis direction
    lorentz_results(k,1)=distances(k); % abst
    lorentz_results(k,2)=Amplitude; % coupling
    lorentz_results(k,3)=abs(193.5*10^6/Linewidth); % q-factor
    lorentz_results(k,4)=abs((Amplitude*Ext)/Linewidth);
    lorentz_results(k,5)=abs(Linewidth);
    lorentz_results(k,6)=Ext;

end

frequency_axis = frequency_axis - lorentz_results(zero_reference_trace,6); % frequency offset correction
lorentz_results(:,6) = lorentz_results(:,6) - lorentz_results(zero_reference_trace,6); % frequency offset correction

figure(3)
subplot(2,1,1)
plot(lorentz_results(:,1),lorentz_results(:,5),'ro--')
title(['Mode Information ' refractive_index_string])
xlabel('Substrate-Resonator Distance (\mum)')
ylabel('Linewidth (MHz)')
ylim([0 50])
subplot(2,1,2)
hold on
plot(lorentz_results(:,1),zeros(sweep_total,1),'k-') % zero line
plot(lorentz_results(:,1),lorentz_results(:,6),'ro--')
hold off
xlabel('Substrate-Resonator Distance (\mum)')
ylabel('Freq Shift (MHz)')
ylim([-50 50])

set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0, 0.6, 0.6]);
disp(' ')
cd(substrate_folder)
saveas(gcf,['auto_' sweep_name '_fig3_lorentz.fig'])
saveas(gcf,['auto_' sweep_name '_fig3_lorentz.png'])
saveas(gcf,['auto_' sweep_name '_fig3_lorentz.jpeg'])
disp('Figure 3: Lorentz Curve Plot Saved')

%% Plot 4: The PColour plot for the frequency shift
figure(4)
pcolor(frequency_axis,distances,data_array'); %Generating a pcolor plot of the data
title(['Frequency Detuning of Mode ' refractive_index_string])
xlabel('Frequency Detuning from Maximum Displacement (MHz)')
ylabel('Substrate-Resonator Distance (\mum)')
xlim([frequency_axis(bounds_index(1)) frequency_axis(bounds_index(2))])
shading flat
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0.6, 0.64, 0.3, 0.3]); %Setting the position of the figure
disp(' ')
cd(substrate_folder)
saveas(gcf,['auto_' sweep_name '_fig4_pcolor.fig'])
saveas(gcf,['auto_' sweep_name '_fig4_pcolor.png'])
saveas(gcf,['auto_' sweep_name '_fig4_pcolor.jpeg'])
disp('Figure 4: PColor Plot Saved')

%% Figure 5: Breaking the mode trace into linear and exponential pieces
figure(5)
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0, 0.28, 0.3, 0.3]); %Setting the position of the figure
hold on
plot(lorentz_results(:,6),distances,'k')
% pcolor(frequency_axis,distances,data_array'); %Generating a pcolor plot of the data
shading flat

if exist('cutoff_distance','var') == 0
    disp(' ')
    disp('Select the substrate-resonator distance to decompose the result into linear and exponential pieces: ')
    [~, cutoff_distance_input] = ginput(1);
    cutoff_distance = fix(cutoff_distance_input);
    if cutoff_distance > max(distances)
        cutoff_distance = max(distances);
    end
    if cutoff_distance < min(distances)
        cutoff_distance = min(distances);
    end
    save(workspace_file,'cutoff_distance','-append')
    disp(['Cutoff distance saved: ' num2str(cutoff_distance) ' microns'])
    disp(' ')
end

cutoff_index = find(distances == cutoff_distance);

linear_fit_data = [lorentz_results(1:cutoff_index,6), distances(1:cutoff_index)'];
exponential_fit_data = [lorentz_results(cutoff_index:end,6), distances(cutoff_index:end)'];

switch cutoff_distance
    case 0
        linear_fit = polyfit(linear_fit_data(:,2),linear_fit_data(:,1),1);
        linear_fit_line = linear_fit(1)*distances + linear_fit(2);
        p_lin = plot(lorentz_results(1:cutoff_index,6),distances(1:cutoff_index),'r');
        p_linfit = plot(linear_fit_line,distances,'m');
        legend([p_lin p_linfit],'Lin','Fitted Lin','Location','southeast')

    case max(distances)
        exponential_fit = coeffvalues(fit(exponential_fit_data(:,2),exponential_fit_data(:,1),'exp1'));
        exponential_fit_line = exponential_fit(1)*exp(exponential_fit(2)*distances);
        p_exp = plot(lorentz_results(cutoff_index:end,6),distances(cutoff_index:end),'b');
        p_expfit = plot(exponential_fit_line,distances,'g');
        legend([p_exp p_expfit],'Exp','Fitted Exp','Location','southeast')

    otherwise
        linear_fit = polyfit(linear_fit_data(:,2),linear_fit_data(:,1),1);
        linear_fit_line = linear_fit(1)*distances + linear_fit(2);
        exponential_fit = coeffvalues(fit(exponential_fit_data(:,2),exponential_fit_data(:,1),'exp1'));
        exponential_fit_line = exponential_fit(1)*exp(exponential_fit(2)*distances);
        p_lin = plot(lorentz_results(1:cutoff_index,6),distances(1:cutoff_index),'r');
        p_linfit = plot(linear_fit_line,distances,'m');
        p_exp = plot(lorentz_results(cutoff_index:end,6),distances(cutoff_index:end),'b');
        p_expfit = plot(exponential_fit_line,distances,'g');
        legend([p_exp p_lin p_expfit p_linfit],'Exp','Lin','Fitted Exp','Fitted Lin','Location','southeast')
end

hold off
disp(' ')
title(['Frequency Detuning of Mode ' refractive_index_string])
xlabel('Frequency Detuning from Maximum Displacement (MHz)')
ylabel('Substrate-Resonator Distance (\mum)')
cd(substrate_folder)
saveas(gcf,['auto_' sweep_name '_fig5_explin.fig'])
saveas(gcf,['auto_' sweep_name '_fig5_explin.png'])
saveas(gcf,['auto_' sweep_name '_fig5_explin.jpeg'])
disp('Figure 5: Fitted Plot Saved')

%% Figure 6: Mode detuning with the linear component removed
traces_noshift = lorentz_results(:,6) - linear_fit_line';

errorbar_x = (max(traces_noshift(1:cutoff_index)) - min(traces_noshift(1:cutoff_index))) * ones(size(traces_noshift,1),1);
% errorbar_y = 0.05 * ones(size(distances,2),1);
errorbar_y = zeros(size(distances,2),1);

figure(6)
hold on
errorbar(traces_noshift,distances',errorbar_y,errorbar_y,errorbar_x,errorbar_x,'y','CapSize',0)
% errorbar(traces_noshift,distances',errorbar_y,errorbar_y,'o-')
plot(traces_noshift,distances,'k')

if cutoff_distance ~= 0
    exponential_fit_noshift = coeffvalues(fit(distances(cutoff_index:end)',traces_noshift(cutoff_index:end),'exp1','StartPoint',[0,0]));
    exponential_fit_line_noshift = exponential_fit_noshift(1)*exp(exponential_fit_noshift(2)*distances);
    plot(exponential_fit_line_noshift,distances,'g')
end

hold off
title(['Frequency Detuning of Mode' refractive_index_string])
xlabel('Frequency Detuning from Maximum Displacement (MHz)')
ylabel('Substrate-Resonator Distance (\mum)')
% legend('data','fitted exp','Location','southeast')
set(gcf, 'units', 'normalized');
set(gcf, 'Position', [0.3, 0.28, 0.3, 0.3]); %Setting the position of the figure
cd(substrate_folder)
saveas(gcf,['auto_' sweep_name '_fig6_clean.fig'])
saveas(gcf,['auto_' sweep_name '_fig6_clean.png'])
saveas(gcf,['auto_' sweep_name '_fig6_clean.jpeg'])
disp(' ')
disp('Figure 6: Cleaned Trace Saved')

%% END

% complete_workspace = ['auto_complete_' tested_sweep_input '_data.mat'];
% save(complete_workspace)
% disp(' ')
% disp(['Saved: ' complete_workspace])

% FUTURE:
% Figure : Comparing two sucessive sweeps (forwards and backwards)