zero_offset_fsr = [9.187, 9.167, 8.898, 9.177, 9.188, 9.169, 9.177, 9.168, 9.194, 9.191];
one_range_offset_fsr = [9.179, 9.179, 9.190, 8.957, 9.180, 9.189, 9.186, 9.175, 9.181, 9.175, 9.181, 9.186, 9.159, 9.167, 9.200];

combined_fsr = [9.187, 9.167, 8.898, 9.177, 9.188, 9.169, 9.177, 9.168, 9.194, 9.191, 9.179, 9.179, 9.190, 8.957, 9.180, 9.189, 9.186, 9.175, 9.181, 9.175, 9.181, 9.186, 9.159, 9.167, 9.200];

combined_fsr_sorted = sort(combined_fsr);

plot(combined_fsr_sorted,'kx')

% plot(zero_offset_fsr,'kx')
% hold on
% plot(one_range_offset_fsr,'bx')
% hold off