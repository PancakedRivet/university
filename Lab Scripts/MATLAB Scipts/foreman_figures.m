% Building the Foreman Figures from the sweeps in the Best Data folder

file_location = 'C:\Users\devpa499\Documents\MATLAB\Project\Best data sets\';
cd(file_location)

save_file_types = {'-djpeg', '-dpdf'}; % Figure file type extensions. Add '-d' and the file extension (jpeg, png etc)

contact_step = [49.5, 38.2, 7.0, 18.5, 22.6, 25.5, 89.9];
refractive_index_list = [1.43, 1.5, 1.75, 2.27, 2.46, 3.48, 4.0];
frequency_at_contact = [-2.151, -2.327, -2.084, -4.744, -7.266, 5.051, 23.06];
linewidth_increase_at_contact = [-0.036, -0.554, 0.289, 15.465, 35.874, 18.039, 13.56];
standard_deviation = [5.6955, 2.7874, 2.7783, 4.0841, 3.2528, 1.5303, 7.5473];

figure
hold on
errorbar(refractive_index_list, frequency_at_contact, standard_deviation, 'y')
plot(refractive_index_list, frequency_at_contact, 'kx')
hold off
grid
xlabel('Refractive Index (n_{sub})')
ylabel('Mode Shift (MHz)')
title('Frequency Detuning By Substrate')

save_file_name = 'foreman_frequency_shift';
set(gca,'FontSize',16)
% set(gcf, 'units', 'normalized', 'Position', [0.5, 0.05, 0.495, 0.495])
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
for i = 1:size(save_file_types,2)
    print(gcf, save_file_name, string(save_file_types(i)), '-r0')
    save_message = strcat('Saved:',{' '}, save_file_name, {' '}, string(save_file_types(i)));
    disp(save_message)
end
disp(' ')

figure
hold on
errorbar(refractive_index_list, linewidth_increase_at_contact, standard_deviation, 'y')
plot(refractive_index_list, linewidth_increase_at_contact, 'kx')
hold off
grid
xlabel('Refractive Index (n_{sub})')
ylabel('Mode Broadening (MHz)')
title('Mode Broadening By Substrate')

save_file_name = 'foreman_mode_broadening';
set(gca,'FontSize',16)
% set(gcf, 'units', 'normalized', 'Position', [0.5, 0.05, 0.495, 0.495])
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
for i = 1:size(save_file_types,2)
    print(gcf, save_file_name, string(save_file_types(i)), '-r0')
    save_message = strcat('Saved:',{' '}, save_file_name, {' '}, string(save_file_types(i)));
    disp(save_message)
end
disp(' ')