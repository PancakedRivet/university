function [data_array, file_info, distances, files_loaded, zero_reference_trace, sweep_direction] = load_data(data_folder,data_length,first_file,last_file,tested_sweep_input)
cd(data_folder)
disp(['Found ' num2str(last_file) ' files'])
files_loaded = last_file - first_file;
data_array = zeros(files_loaded,data_length);
header_info = cell(files_loaded,3);
distances = zeros(files_loaded,1);
l = 1; %Beginning the iteration at 1 so the arrays fill the first row

for k = first_file+1:last_file; %Loading data
    filename = fullfile(sprintf('./data_%d.dat',k-1));
    fileID = fopen(filename,'r');
    data_import = textscan(fileID,'%f','HeaderLines',1); %Importing the data into a temporary array
    frewind(fileID); %Rewinding the file reader to conduct the second sweep
    distance_import = textscan(fileID,'%*s %.2f %*s %*s %*s'); %Importing the associated information with the data
    frewind(fileID); %Rewinding the file reader to conduct the third sweep
    volt_import = textscan(fileID,'%*s %s %*s %s %11s'); %Importing the associated information with the data
    fclose(fileID); %Closing the file again
    
    data_array(l,:) = data_import{1}(1:data_length); %Transferring the data into the proper array
    distances(l,1) = distance_import{1}(1); %Saving the voltage as a double
    header_info(l,1) = volt_import{1}(1); %Saving the voltage as a string with unit
    header_info(l,2) = volt_import{3}(1); %Saving the time
    header_info(l,3) = volt_import{2}(1); %Saving the date
    l = l + 1; %Increasing the iteration so that the arrays are filled correctly
end

disp(['Loaded ' num2str(files_loaded) ' files']) %Outputting to console the number of found files

distances = abs(distances - max(distances)); %Determining the substrate-resonator distance in microns and saving to a seperate array
sweep_direction = sign(distances(1)-distances(end));
switch sweep_direction
    case 1
        sweep_direction = 'Forwards';
    case -1
        sweep_direction = 'Backwards';
end

file_info = [header_info, cellstr(strcat(num2str(distances), '\mum'))]; %Combining the header information with the distance legend
if distances(first_file+1) < distances(last_file) %Testing whether the first or last loaded file has the zero_point
    zero_reference_trace = last_file; %Setting the zero_reference to the last file
else
    zero_reference_trace = first_file+1; %Setting the zero_reference to the first file
end

filename = ['auto_' tested_sweep_input '_data.mat'];
save(filename,'data_array','sweep_direction','file_info','distances','files_loaded','zero_reference_trace')
disp(['Saved: ' filename])

end