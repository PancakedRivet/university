%Settings
xLimLow = -50;
xLimHigh = 20;
shift_line = -33.42;
yLimLow = 0.95;
yLimHigh = 1;
figureTitle = 'Frequency of a Resonant Mode';
figureLabelX = 'WGM Frequency Shift';
figureLabelY = 'Voltage';

%Plots
figure(1)
title(figureTitle);
xlim([xLimLow,xLimHigh]);
ylim([yLimLow,yLimHigh]);
xlabel(figureLabelX);
ylabel(figureLabelY);
k = 1;
hold on
plot(plot_frequency,plot_data_relative(k,:),'Color',[0 0 1],'LineWidth',1.5);
plot([0 0], ylim,'k--');
hold off

figure(2)
k = 90;
title(figureTitle);
xlim([xLimLow,xLimHigh]);
ylim([yLimLow,yLimHigh]);
xlabel(figureLabelX);
ylabel(figureLabelY);
hold on
plot(plot_frequency,plot_data_relative(k,:),'Color',[1 0 0],'LineWidth',1.5);
plot([shift_line, shift_line], ylim,'k--');
plot([0 0], ylim,'k--');
hold off